#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif


FOUNDATION_EXPORT double Pods_golomt_ios_golomt_iosUITestsVersionNumber;
FOUNDATION_EXPORT const unsigned char Pods_golomt_ios_golomt_iosUITestsVersionString[];

