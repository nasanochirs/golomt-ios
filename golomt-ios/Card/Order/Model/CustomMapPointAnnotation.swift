//
//  CustomMapPointAnnotation.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import MapKit

class CustomMapPointAnnotation: MKPointAnnotation {
    var data = LocationResponse.LocationData.Location()
}
