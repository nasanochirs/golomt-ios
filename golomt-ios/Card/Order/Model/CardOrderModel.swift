//
//  CardOrderModel.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/11/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class CardOrderModel {
    var cardName: String = ""
    var currency: String = ""
    var fee: Double = 0.0
    var addedFee: Double = 0.0
    var typeName: String = ""
    var typeValue: String = ""
    var account: AccountResponse.Account?
    var newCardProduct: String = ""
    var location: LocationResponse.LocationData.Location?

    func getCard() -> String {
        let splittedName = cardName.components(separatedBy: "_")
        let cardType = splittedName.last.orEmpty
        switch cardType {
        case "colorp":
            return "COLOR PURPLE"
        case "colorg":
            return "COLOR GREEN"
        default:
            return cardType.uppercased()
        }
    }
}
