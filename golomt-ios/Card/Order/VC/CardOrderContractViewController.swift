//
//  CardOrderContractViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/10/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class CardOrderContractViewController: BaseUIViewController {
    @IBOutlet var cardImageView: UIImageView!
    @IBOutlet var contractWebView: WKWebView!
    @IBOutlet var approveButton: DefaultGradientButton!
    var cardName: String = ""

    @IBAction func approveAction(_ sender: Any) {
        let cardStoryBoard: UIStoryboard = UIStoryboard(name: "CardOrder", bundle: nil)
        let viewController = cardStoryBoard.instantiateViewController(withIdentifier: "CardOrderTypeID")
        let cardOrderVC = viewController as? CardOrderTypeViewController
        guard let unwrappedVC = cardOrderVC else {
            return
        }
        unwrappedVC.cardName = cardName
        navigationController?.pushViewController(unwrappedVC, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = cardName.localized()
        if let contractUrl = URL(string: cardName.getCardContract()) {
            contractWebView.load(URLRequest(url: contractUrl))
        }
        cardImageView.image = UIImage(named: cardName)
        approveButton.titleText = "card_order_contract_button_title"
    }
}

private extension String {
    func getCardContract() -> String {
        switch self {
        case "card_oyu":
            switch getLanguage() {
            case "en":
                return "https://m.egolomt.mn/social/prod_OYU_en.html"
            case "mn":
                return "https://m.egolomt.mn/social/prod_OYU_mn.html"
            default:
                return ""
            }
        case "card_suvd":
            switch getLanguage() {
            case "en":
                return "https://m.egolomt.mn/social/prod_SUVD_en.html"
            case "mn":
                return "https://m.egolomt.mn/social/prod_SUVD_mn.html"
            default:
                return ""
            }
        case "card_carbon":
            switch getLanguage() {
            case "en":
                return "https://m.egolomt.mn/social/prod_CARBON_en.html"
            case "mn":
                return "https://m.egolomt.mn/social/prod_CARBON_mn.html"
            default:
                return ""
            }
        case "card_gold":
            switch getLanguage() {
            case "en":
                return "https://m.egolomt.mn/social/prod_GOLD_en.html"
            case "mn":
                return "https://m.egolomt.mn/social/prod_GOLD_mn.html"
            default:
                return ""
            }
        case "card_colorp":
            switch getLanguage() {
            case "en":
                return "https://m.egolomt.mn/social/prod_purple_en.html"
            case "mn":
                return "https://m.egolomt.mn/social/prod_purple_mn.html"
            default:
                return ""
            }
        case "card_colorg":
            switch getLanguage() {
            case "en":
                return "https://m.egolomt.mn/social/prod_green_en.html"
            case "mn":
                return "https://m.egolomt.mn/social/prod_green_mn.html"
            default:
                return ""
            }
        default:
            return ""
        }
    }
}
