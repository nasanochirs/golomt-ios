//
//  CardOrderConfirmationViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/16/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class CardOrderConfirmationViewController: TableViewController {
    @IBOutlet var cardImageView: UIImageView!
    
    var cardOrderModel = CardOrderModel()

    lazy var productSection: TableViewModel.Section = {
        var section = TableViewModel.Section(
            title: "card_order_confirmation_product_info_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "card_order_confirmation_product_type_title".localized(), info: self.cardOrderModel.typeName
                )
            ]
        )
        if self.cardOrderModel.newCardProduct.isEmpty {
            section.rows.append(
                TableViewModel.Row(
                    title: "card_order_confirmation_card_old_title".localized(), info: self.cardOrderModel.account?.ACCT_NUMBER
                )
            )
        } else {
            section.rows.append(contentsOf: [
                TableViewModel.Row(
                    title: "card_order_confirmation_card_new_fee_title".localized(), info: self.cardOrderModel.account?.ACCT_NUMBER
                ),
                TableViewModel.Row(
                    title: "card_order_confirmation_card_new_connect_title".localized(), info: "card_order_confirmation_card_new_connect_label".localized()
                )
            ])
        }
        return section
    }()

    lazy var branchSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "card_order_confirmation_branch_info_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "card_order_confirmation_branch_title".localized(), info: self.cardOrderModel.location?.getName()
                )
            ]
        )
    }()

    lazy var warningSection: TableViewModel.Section = {
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(
                    title: nil, info: nil, infoProperty: nil, cell: self.warningField
                )
            ]
        )
    }()

    lazy var warningField: DefaultLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLabelCell
        guard let unwrappedCell = cell else {
            return DefaultLabelCell()
        }
        unwrappedCell.labelText = "card_order_confirmation_warning_label".localized()
        return unwrappedCell
    }()

    lazy var confirmationWarningField: DefaultLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLabelCell
        guard let unwrappedCell = cell else {
            return DefaultLabelCell()
        }
        unwrappedCell.labelText = "card_order_confirmation_warning_label".localized()
        return unwrappedCell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "card_order_confirmation_title".localized()
        setButtonTitle(.Confirm)
        cardImageView.image = UIImage(named: cardOrderModel.cardName)
        model.sections = [productSection, branchSection, warningSection]
        tableView.reloadData()
        onContinue = {
            let loaderViewController = TransactionLoadingController(nibName: "TransactionLoadingController", bundle: nil)
            self.navigationController?.pushViewController(loaderViewController, animated: true)
            let detailViewController = TransactionDetailViewController(nibName: "TableViewController", bundle: nil)
            detailViewController.model.sections = [
                self.productSection,
                self.branchSection
            ]
            detailViewController.controllerTitle = "card_order_detail_title".localized()
            detailViewController.onFinish = {
                if let cardListVC = self.navigationController?.viewControllers[1] {
                    self.navigationController?.popToViewController(cardListVC, animated: true)
                }
                if detailViewController.isSuccessful {
                    NotificationCenter.default.post(name: Notification.Name(NotificationConstants.REFRESH_ACCOUNTS), object: nil)
                }
            }
            ConnectionFactory.cardOrderRequest(
                model: self.cardOrderModel,
                success: { response in
                    let resultMessage = response.getMessage()
                    detailViewController.transactionMessage = resultMessage
                    loaderViewController.setResult(isSuccessful: true, message: resultMessage, completion: {
                        detailViewController.isSuccessful = true
                        detailViewController.model.sections.append(TableViewModel.Section(
                            rows: [
                                TableViewModel.Row(
                                    title: nil,
                                    info: nil,
                                    infoProperty: nil,
                                    cell: self.confirmationWarningField
                                )
                            ]
                        ))
                        self.navigationController?.pushViewController(detailViewController, animated: false)
                    })
                },
                failed: { reason in
                    let resultMessage = reason?.MESSAGE_DESC ?? ""
                    detailViewController.transactionMessage = resultMessage
                    loaderViewController.setResult(isSuccessful: false, message: resultMessage, completion: {
                        detailViewController.isSuccessful = false
                        self.navigationController?.pushViewController(detailViewController, animated: false)
                    })
                }
            )
        }
    }

}
