//
//  CardOrderMapViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import MapKit
import STPopup
import UIKit
import MaterialComponents.MaterialBottomSheet

class CardOrderMapViewController: BaseUIViewController {
    @IBOutlet var mapView: MKMapView!
    var previousView: UIView?
    
    var cardOrderModel = CardOrderModel()
    
    lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.requestWhenInUseAuthorization()
        return manager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "card_order_map_branch_title".localized()
        fetchBranchLocations()
        setUpMapView()
    }
    
    private func fetchBranchLocations() {
        ConnectionFactory.getLocations(
            success: { response in
                response.locationData?.locations.forEach { location in
                    if location.type == "BRANCH" {
                        let latitude = location.coordinates?.getLatitude()
                        let longitude = location.coordinates?.getLongitude()
                        let annotation = CustomMapPointAnnotation()
                        annotation.coordinate = CLLocationCoordinate2D(
                            latitude: CLLocationDegrees(latitude.toDouble), longitude: CLLocationDegrees(longitude.toDouble)
                        )
                        annotation.data = location
                        self.mapView.addAnnotation(annotation)
                    }
                }
            },
            failed: { reason in
                self.handleRequestFailure(reason)
            }
        )
    }
    
    private func setUpMapView() {
        mapView.showsUserLocation = true
        mapView.showsCompass = true
        mapView.showsScale = true
        mapView.delegate = self
        currentLocation()
    }
    
    private func currentLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.showsBackgroundLocationIndicator = true
        locationManager.startUpdatingLocation()
    }
}

extension CardOrderMapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! as CLLocation
        let currentLocation = location.coordinate
        let coordinateRegion = MKCoordinateRegion(center: currentLocation, latitudinalMeters: 1600, longitudinalMeters: 1600)
        mapView.setRegion(coordinateRegion, animated: true)
        locationManager.stopUpdatingLocation()
    }
}

extension CardOrderMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "BranchPin"
        
        if annotation is MKUserLocation {
            return nil
        }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
            annotationView?.image = UIImage(named: "branch")
        } else {
            annotationView?.annotation = annotation
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        mapView.selectedAnnotations.forEach { annotation in
            mapView.deselectAnnotation(annotation, animated: true)
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.previousView?.transform = .identity
            view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        })
        previousView = view
        let annotation = view.annotation as? CustomMapPointAnnotation
        guard let unwrappedAnnotation = annotation else {
            return
        }
        let locationData = unwrappedAnnotation.data
        let controller = LocationController()
        controller.location = locationData
        let bottomSheet = MDCBottomSheetController(contentViewController: controller)
        controller.onBranchSelect = {
            let cardStoryBoard: UIStoryboard = UIStoryboard(name: "CardOrder", bundle: nil)
            let viewController = cardStoryBoard.instantiateViewController(withIdentifier: "CardOrderConfirmationID")
            let cardConfirmVC = viewController as? CardOrderConfirmationViewController
            guard let unwrappedVC = cardConfirmVC else {
                return
            }
            self.cardOrderModel.location = locationData
            unwrappedVC.cardOrderModel = self.cardOrderModel
            bottomSheet.dismiss(animated: true, completion: {
                self.navigationController?.pushViewController(unwrappedVC, animated: true)
            })
        }
        controller.location = locationData
        controller.hasButton = true
        let shapeGenerator = MDCRectangleShapeGenerator()

        let cornerTreatment = MDCRoundedCornerTreatment(radius: 15)
        shapeGenerator.topLeftCorner = cornerTreatment
        shapeGenerator.topRightCorner = cornerTreatment

        bottomSheet.setShapeGenerator(shapeGenerator, for: .preferred)
        bottomSheet.setShapeGenerator(shapeGenerator, for: .extended)
        bottomSheet.setShapeGenerator(shapeGenerator, for: .closed)
        present(bottomSheet, animated: true, completion: nil)
//        popupController.style = .bottomSheet
//        popupController.present(in: self)
    }
}
