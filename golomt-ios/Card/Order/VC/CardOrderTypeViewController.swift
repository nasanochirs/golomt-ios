//
//  CardOrderTypeViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/11/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class CardOrderTypeViewController: BaseUIViewController {
    @IBOutlet var cardImageView: UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var containerStackView: UIStackView!
    
    lazy var continueButton: DefaultGradientButton = {
        let button = DefaultGradientButton()
        button.titleText = "card_order_type_button_title"
        button.addTarget(self, action: #selector(continueAction), for: .touchUpInside)
        return button
    }()
    
    var cardOrders = [CardOrderModel]()
    var selectedOrder = -1
    var cardName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "card_order_type_title".localized()
        cardImageView.image = UIImage(named: cardName)
        tableView.registerCell(nibName: "CardTypeButtonCell")
        tableView.tableFooterView = UIView()
        fetchTypeList()
    }
    
    @objc private func continueAction() {
        let cardStoryBoard: UIStoryboard = UIStoryboard(name: "CardOrder", bundle: nil)
        let viewController = cardStoryBoard.instantiateViewController(withIdentifier: "CardOrderAccountID")
        let cardAccountVC = viewController as? CardOrderAccountViewController
        guard let unwrappedVC = cardAccountVC else {
            return
        }
        unwrappedVC.cardOrderModel = cardOrders[selectedOrder]
        navigationController?.pushViewController(unwrappedVC, animated: true)
    }
    
    private func fetchCardInfo(typeList: [DataListResponse.Item]) {
        showLoader()
        ConnectionFactory.fetchParameterValue(
            parameterName: cardName.uppercased(),
            success: { response in
                self.hideLoader()
                let parameters = response.parameter?.P_VAL?.components(separatedBy: "|") ?? []
                for parameter in parameters {
                    let valueArray = parameter.components(separatedBy: "_")
                    if valueArray.count < 3 {
                        continue
                    }
                    let customType = valueArray[0] + "_" + valueArray[1]
                    typeList.forEach { type in
                        if customType == type.CM_CODE {
                            let model = CardOrderModel()
                            model.cardName = self.cardName
                            model.currency = valueArray[1]
                            model.fee = valueArray[2].toDouble
                            model.typeName = type.CD_DESC.orEmpty
                            model.typeValue = type.CM_CODE.orEmpty
                            self.cardOrders.append(model)
                        }
                    }
                }
                self.tableView.reloadData()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
    
    private func fetchTypeList() {
        showLoader()
        ConnectionFactory.fetchList(
            code: "CRD",
            success: { response in
                self.hideLoader()
                self.fetchCardInfo(typeList: response.list ?? [])
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
}

extension CardOrderTypeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        cardOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardTypeButtonCell") as? CardTypeButtonCell
        guard let unwrappedCell = cell else {
            return UITableViewCell()
        }
        let cardOrder = cardOrders[indexPath.row]
        unwrappedCell.labelText = cardOrder.typeName
        return unwrappedCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedOrder = indexPath.row
        continueButton.widthAnchor.constraint(equalToConstant: containerStackView.bounds.size.width - 40).isActive = true
        continueButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        containerStackView.addArrangedSubview(continueButton)
        self.tableView.layoutIfNeeded()
        self.tableView.layoutSubviews()
        self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
}
