//
//  CardOrderViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/10/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import SwipeView
import UIKit

class CardOrderViewController: BaseUIViewController {
    @IBOutlet var cardListCarousel: SwipeView!
    @IBOutlet var cardListTitleLabel: UILabel!

    var cards = [
        "card_carbon", "card_suvd", "card_oyu", "card_gold", "card_colorp", "card_colorg"
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "card_choose_plastic_title".localized()

        cardListCarousel.isPagingEnabled = true
        cardListCarousel.isVertical = true
        cardListCarousel.truncateFinalPage = true
        cardListTitleLabel.textColor = .defaultPrimaryText
        cardListTitleLabel.useMediumFont()
        cardListTitleLabel.makeBold()
        cardListTitleLabel.text = "card_order_list_title".localized()
        cardListTitleLabel.allCaps()
    }

    @objc private func handleCardOrder() {}
}

extension CardOrderViewController: SwipeViewDelegate, SwipeViewDataSource {
    func numberOfItems(in swipeView: SwipeView!) -> Int {
        return cards.count
    }

    func swipeView(_ swipeView: SwipeView!, viewForItemAt index: Int, reusing view: UIView!) -> UIView! {
        var itemView: UIImageView
        let card = cards[index]

        let width = cardListCarousel.bounds.size.width - 100
        let height = width * 0.9
        itemView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        itemView.image = UIImage(named: card)
        itemView.corner(cornerRadius: 10)
        let imageHeight = itemView.image?.size.height ?? 0.0
        let imageWidth = itemView.image?.size.width ?? 0.0
        let scale: CGFloat
        if imageWidth > imageHeight {
            scale = itemView.bounds.width / imageWidth
        } else {
            scale = itemView.bounds.height / imageHeight
        }
        let size = CGSize(width: imageWidth * scale, height: imageHeight * scale)
        itemView.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height + 20)
        itemView.contentMode = .scaleAspectFit
        let cardNameLabel = UILabel(frame: CGRect(x: 40, y: size.height - 60, width: size.width, height: 40))
        cardNameLabel.text = card.localized()
        cardNameLabel.textColor = .white
        cardNameLabel.useXLargeFont()
        cardNameLabel.makeBold()

        itemView.addSubview(cardNameLabel)
        return itemView
    }

    func swipeView(_ swipeView: SwipeView!, didSelectItemAt index: Int) {
        let cardStoryBoard: UIStoryboard = UIStoryboard(name: "CardOrder", bundle: nil)
        let viewController = cardStoryBoard.instantiateViewController(withIdentifier: "CardOrderContractID")
        let cardContractVC = viewController as? CardOrderContractViewController
        guard let unwrappedVC = cardContractVC else {
            return
        }
        unwrappedVC.cardName = cards[index]
        navigationController?.pushViewController(unwrappedVC, animated: true)
    }
}
