//
//  CardOrderAccountViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/12/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class CardOrderAccountViewController: TableViewController {
    @IBOutlet var cardImageView: UIImageView!
    
    var cardOrderModel = CardOrderModel()
    var accountList = [AccountResponse.Account]()
    var productList: String = ""
    
    lazy var accountTypeField: DefaultSegmentedGroupCell = {
        let nib = Bundle.main.loadNibNamed("DefaultSegmentedGroupCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultSegmentedGroupCell else {
            return DefaultSegmentedGroupCell()
        }
        cell.title = "card_order_account_type_title".localized()
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.segments = [DefaultSegmentedGroupCell.SegmentModel(code: "", value: "card_order_account_type_old_label".localized()), DefaultSegmentedGroupCell.SegmentModel(code: "", value: "card_order_account_type_new_label".localized())]
        cell.selectedSegmentIndex = 0
        cell.onSegmentChange = { index in
            self.accountField.selectedItem = nil
            self.selectedAccount = nil
            switch index {
            case 0:
                self.setOldAccountList()
                self.accountField.title = "card_order_account_picker_old_title".localized()
                _ = self.accountField.hasError()
            case 1:
                self.accountList = operativeAccountList
                self.accountField.title = "card_order_account_picker_new_title".localized()
                _ = self.accountField.hasError()
            default:
                break
            }
        }
        return cell
    }()
    
    lazy var accountField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerPlainCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultPickerButtonCell
        guard let unwrappedCell = cell else {
            return DefaultPickerButtonCell()
        }
        unwrappedCell.title = "card_order_account_picker_old_title".localized()
        unwrappedCell.onPickerClick = {
            let controller = AccountPickerController()
            controller.onAccountSelect = { account in
                self.accountField.selectedItem = account
                self.selectedAccount = account
            }
            controller.selectedAccount = self.selectedAccount
            controller.title = unwrappedCell.title
            controller.accounts = self.accountList
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()
    
    lazy var feeField: DefaultVerticalTitleLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultVerticalTitleLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultVerticalTitleLabelCell
        guard let unwrappedCell = cell else {
            return DefaultVerticalTitleLabelCell()
        }
        unwrappedCell.titleText = "card_order_account_fee_title"
        unwrappedCell.labelText = "card_order_account_fee_label".localized(with: self.cardOrderModel.fee, self.cardOrderModel.currency)
        return unwrappedCell
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "card_order_account_title".localized()
        cardImageView.image = UIImage(named: cardOrderModel.cardName)
        fetchFilterAccountList()
        model.sections = [
            TableViewModel.Section(
                rows: [
                    TableViewModel.Row(
                        cell: accountTypeField
                    ),
                    TableViewModel.Row(
                        cell: accountField
                    ),
                    TableViewModel.Row(
                        cell: feeField
                    )
                ]
            )
        ]
        tableView.reloadData()
        onContinue = {
            if self.accountField.hasError() {
                return
            }
            if self.selectedAccount?.ACCT_BALANCE.toAmount ?? 0.0 < self.cardOrderModel.fee {
                let dialog = errorDialog(message: "card_order_account_balance_error".localized())
                self.present(dialog, animated: true)
                return
            }
            self.cardOrderModel.account = self.selectedAccount
            switch self.accountTypeField.selectedSegmentIndex {
            case 1:
                self.showLoader()
                ConnectionFactory.fetchParameterValue(
                    parameterName: "DEBCARD_PROD",
                    success: { response in
                        self.hideLoader()
                        guard let value = response.parameter?.P_VAL else {
                            return
                        }
                        self.cardOrderModel.newCardProduct = value
                        self.showLoader()
                        ConnectionFactory.fetchParameterValue(
                            parameterName: value + "_" + self.cardOrderModel.currency + "_MIN",
                            success: { response in
                                self.hideLoader()
                                guard let feeValue = response.parameter?.P_VAL else {
                                    return
                                }
                                self.cardOrderModel.addedFee = feeValue.toDouble
                            },
                            failed: { reason in
                                self.hideLoader()
                                self.handleRequestFailure(reason)
                            }
                        )
                    },
                    failed: { reason in
                        self.hideLoader()
                        self.handleRequestFailure(reason)
                        return
                    }
                )
            default:
                self.cardOrderModel.newCardProduct = ""
            }
            let cardStoryBoard: UIStoryboard = UIStoryboard(name: "CardOrder", bundle: nil)
            let viewController = cardStoryBoard.instantiateViewController(withIdentifier: "CardOrderMapID")
            let cardMapVC = viewController as? CardOrderMapViewController
            guard let unwrappedVC = cardMapVC else {
                return
            }
            unwrappedVC.cardOrderModel = self.cardOrderModel
            self.navigationController?.pushViewController(unwrappedVC, animated: true)
        }
    }
    
    private func fetchFilterAccountList() {
        showLoader()
        ConnectionFactory.fetchParameterValue(
            parameterName: "DEBCARD_PRODLIST",
            success: { response in
                self.hideLoader()
                self.productList = response.parameter?.P_VAL ?? ""
                self.setOldAccountList()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
    
    private func setOldAccountList() {
        var filteredList = [AccountResponse.Account]()
        productList.components(separatedBy: "|").forEach { product in
            filteredList.append(contentsOf: operativeAccountList.filter {
                $0.PRODUCT_CATEGORY.orEmpty.contains(product) && $0.ACCT_CURRENCY.orEmpty == self.cardOrderModel.currency
            })
        }
        accountList = filteredList.uniqueValues()
    }
}
