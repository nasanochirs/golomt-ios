//
//  CardOrderRequest.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/16/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class CardOrderRequest: BaseRequest {
    var body = Body()

    private enum CodingKeys: String, CodingKey {
        case body
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(body, forKey: .body)
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    override init() {
        super.init()
        header.__SRVCID__ = "CDCN"
    }

    struct Body: Codable {
        var FULLNAME = ""
        var SHORTNAME = ""
        var CARD_PLASTIC = ""
        var CARDTYPE = ""
        var USERCHOSENCURRENCY = ""
        var ISNEWACCOUNT = "N"
        var PRODUCTTYPE = ""
        var CRN_ONE = ""
        var NAME_ON_CARD = ""
        var ACCOUNT_ON_CARD = "Y"
        var INSTANTCARD = "N"
        var OPERATIVE_SOLID =  ""
        var CUST_ID = ""
        var FEE_AMOUNT = 0.0
        var PRIMARY_SOLID = ""
        var REMITTER_ACCOUNT = ""
        var DEBIT_ACCT_CRN = ""
        var FEE_ADDED = 0.0
        var AMT_ONE = ""
        var ACCOUNT = ""
    }
}
