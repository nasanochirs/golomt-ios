//
//  CardPinCodeViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/8/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class CardChangePinViewController: BaseUIViewController {
    @IBOutlet var messageButton: DefaultGradientButton!
    @IBOutlet var emailButton: DefaultGradientButton!
    @IBOutlet var tableView: UITableView!
    var card: CardListResponse.Card?
    var isNew: Bool = false
    var requestBody = CardPinRequest.Body()

    @IBAction func messageAction(_ sender: Any) {
        handlePinRequest(channel: .SMS)
    }

    @IBAction func emailAction(_ sender: Any) {
        handlePinRequest(channel: .EMAIL)
    }

    lazy var cardNumberField: DefaultVerticalTitleLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultVerticalTitleLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultVerticalTitleLabelCell
        guard let unwrappedCell = cell else {
            return DefaultVerticalTitleLabelCell()
        }
        return unwrappedCell
    }()

    lazy var warningField: DefaultLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLabelCell
        guard let unwrappedCell = cell else {
            return DefaultLabelCell()
        }
        unwrappedCell.labelText = "card_pin_code_warning_label".localized()
        if self.isNew {
            unwrappedCell.labelText = "card_pin_code_activate_warning_label".localized()
        }
        return unwrappedCell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = nil
        tableView.contentInsetAdjustmentBehavior = .never
        messageButton.titleText = "card_pin_code_sms_title".localized()
        emailButton.titleText = "card_pin_code_email_title".localized()
        switch isNew {
        case true:
            title = "card_activate_title".localized()
        case false:
            title = "card_pin_code_title".localized()
        }
        guard let unwrappedCard = card else {
            return
        }
        cardNumberField.labelText = unwrappedCard.getMaskedNumber()
        cardNumberField.titleText = "card_pin_code_card_number_title"
    }

    private func handlePinRequest(channel: CardPinRequest.Channel) {
        guard let unwrappedCard = card else {
            return
        }

        func handleRequest() {
            let expireDateArray = unwrappedCard.CARD_EXPIRE_DATE.orEmpty.components(separatedBy: "-")
            let expireDate = expireDateArray.last.orEmpty.suffix(2) + expireDateArray.first.orEmpty
            requestBody.CARD_NUMBER = unwrappedCard.CARD_NUMBER_ARRAY.orEmpty
            requestBody.CARD_EXPIRED = String(expireDate)
            requestBody.PREF_CHANNEL = channel.rawValue
            requestBody.CURRENT_OTP = ""
            showLoader()
            ConnectionFactory.handlePinRequest(
                body: requestBody,
                success: { response in
                    self.hideLoader()
                    let confirmationDialog = UIAlertController(title: "card_pin_code_otp_dialog_title".localized(), message: response.getMessage(), preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "card_pin_code_otp_dialog_negative_label".localized(), style: .cancel, handler: nil)
                    let confirmAction = UIAlertAction(
                        title: "card_pin_code_otp_dialog_positive_label".localized(),
                        style: .default,
                        handler: { _ in
                            self.requestBody.CURRENT_OTP = confirmationDialog.textFields?.first?.text ?? ""
                            self.showLoader()
                            ConnectionFactory.handlePinRequest(
                                body: self.requestBody,
                                success: { response in
                                    self.hideLoader()
                                    let pinVC = CardPinViewController(nibName: "TableViewController", bundle: nil)
                                    pinVC.card = self.card
                                    pinVC.image64 = response.pin?.PIN_CODE
                                    pinVC.isNew = self.isNew
                                    self.navigationController?.pushViewController(pinVC, animated: true)
                                },
                                failed: { reason in
                                    self.hideLoader()
                                    self.handleRequestFailure(reason)
                                }
                            )
                        }
                    )
                    confirmationDialog.addAction(cancelAction)
                    confirmationDialog.addAction(confirmAction)
                    confirmationDialog.addTextField { textField in
                        textField.keyboardType = .numberPad
                        if #available(iOS 12.0, *) {
                            textField.textContentType = .oneTimeCode
                        }
                        textField.delegate = self
                    }
                    self.present(confirmationDialog, animated: true)
                },
                failed: { reason in
                    self.hideLoader()
                    self.handleRequestFailure(reason)
                }
            )
        }

        switch isNew {
        case true:
            let expireDateArray = unwrappedCard.CARD_EXPIRE_DATE.orEmpty.components(separatedBy: "-")
            let expireDate = expireDateArray.last.orEmpty + expireDateArray.first.orEmpty
            ConnectionFactory.handeCardStatusRequest(
                body: CardStatusRequest.Body(
                    CARD_NUMBER: unwrappedCard.CARD_NUMBER_ARRAY.orEmpty,
                    CARD_EXPIRE: expireDate,
                    CARD_STATUS: CardStatusRequest.Status.CACTIVE.rawValue
                ),
                success: { _ in
                    handleRequest()
                },
                failed: { reason in
                    self.handleRequestFailure(reason)
                }
            )
        case false:
            handleRequest()
        }
    }
}

extension CardChangePinViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return cardNumberField
        case 1:
            return warningField
        default:
            return UITableViewCell()
        }
    }
}

extension CardChangePinViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let stringRange = Range(range, in: textField.text.orEmpty) else {
            return false
        }
        let newString = textField.text.orEmpty.replacingCharacters(in: stringRange, with: string)
        return newString.count <= 4
    }
}
