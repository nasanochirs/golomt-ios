//
//  CardPinViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/9/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import Lottie
import UIKit

class CardPinViewController: TableViewController {
    var image64: String?
    var card: CardListResponse.Card?
    var isNew: Bool = false

    lazy var resultField: TransactionDetailResultCell = {
        let nib = Bundle.main.loadNibNamed("TransactionDetailResultCell", owner: self, options: nil)
        let cell = nib?.first as? TransactionDetailResultCell
        guard let unwrappedCell = cell else {
            return TransactionDetailResultCell()
        }
        switch isNew {
        case true:
            unwrappedCell.setResult(message: "card_activate_finish_success_label".localized(), isSuccessful: true)
        case false:
            unwrappedCell.setResult(message: "card_pin_code_finish_success_label".localized(), isSuccessful: true)
        }
        return unwrappedCell
    }()

    lazy var imageField: DefaultVerticalTitleImageCell = {
        let nib = Bundle.main.loadNibNamed("DefaultVerticalTitleImageCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultVerticalTitleImageCell
        guard let unwrappedCell = cell else {
            return DefaultVerticalTitleImageCell()
        }
        return unwrappedCell
    }()

    lazy var warningField: DefaultLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLabelCell
        guard let unwrappedCell = cell else {
            return DefaultLabelCell()
        }
        switch isNew {
        case true:
            unwrappedCell.labelText = "card_pin_code_result_warning_label".localized()
        case false:
            unwrappedCell.labelText = "card_pin_code_result_warning_label".localized()
        }
        return unwrappedCell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
//        finishButton.titleText = "card_pin_code_finish_button_title"
        navigationItem.hidesBackButton = true
        switch isNew {
        case true:
            title = "card_activate_title".localized()
            imageField.titleText = "card_activate_finish_new_pin_label"
        case false:
            title = "card_pin_code_title".localized()
            imageField.titleText = "card_pin_code_finish_new_pin_label"
        }
        let decodedData = Data(base64Encoded: image64.orEmpty)
        if let data = decodedData {
            imageField.labelImage = UIImage(data: data)
        }
        
        model.sections = [
            TableViewModel.Section(
                title: "",
                rows: [
                    TableViewModel.Row(
                        cell: resultField
                    ),
                    TableViewModel.Row(
                        cell: imageField
                    ),
                    TableViewModel.Row(
                        cell: warningField
                    )
                ]
            )
        ]
        
        tableView.reloadData()
        
        onContinue = {
            if let cardListVC = self.navigationController?.viewControllers[1] {
                self.navigationController?.popToViewController(cardListVC, animated: true)
                if self.isNew {
                    NotificationCenter.default.post(name: Notification.Name(NotificationConstants.REFRESH_CARD_LIST), object: nil)
                }
            }
        }
    }
}
