//
//  CardPinResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/8/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class CardPinResponse: BaseResponse {
    let pin: CardPin?
    
    private enum CodingKeys: String, CodingKey {
        case ChangeStatus
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(pin, forKey: .ChangeStatus)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.pin = try container.decodeIfPresent(CardPin.self, forKey: .ChangeStatus)
        try super.init(from: decoder)
    }
    
    struct CardPin: Codable {
        var PIN_CODE: String?
        var ERROR_PIN: String?
    }
}
