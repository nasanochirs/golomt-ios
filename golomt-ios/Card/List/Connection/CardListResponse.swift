//
//  CardListResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/2/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class CardListResponse: BaseResponse {
    let cardList: [Card]?
    
    private enum CodingKeys: String, CodingKey {
        case resultList_REC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(cardList, forKey: .resultList_REC)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.cardList = try container.decodeIfPresent([Card].self, forKey: .resultList_REC)
        try super.init(from: decoder)
    }
    
    class Card: Codable {
        var CARD_EXPIRE_DATE: String?
        var CARD_NUMBER_ARRAY: String?
        var CARD_CURR_ARRAY: String?
        var CARD_TYPE_ARRAY: String?
        var CARD_STATUS_ARRAY: String?
        var CARD_ACC_NO: String?
        var CARD_PRODUCT_ARRAY: String?
        var CARD_SUBTYPE_ARRAY: String?
        var CARD_SUBTYPE_DESC_ARRA: String?
        
        enum CardStatus: String {
            case BLOCKED = "L"
            case NEW = "U"
            case ACTIVE = ""
            case EXPIRED
        }
        
        func getMaskedNumber() -> String {
            let cardNumber = CARD_NUMBER_ARRAY.orEmpty
            return "card_number_masked".localized(with: String(cardNumber.prefix(4)), String(cardNumber.suffix(4)))
        }
        
        func getStatus() -> CardStatus? {
            let dateArray = CARD_EXPIRE_DATE.orEmpty.components(separatedBy: "-")
            var dateComponents = DateComponents()
            dateComponents.year = Int(dateArray.last.orEmpty)
            if let month = Int(dateArray.first.orEmpty) {
                dateComponents.month = month + 1
            }
            dateComponents.day = -1
            dateComponents.hour = 24
            dateComponents.timeZone = TimeZone(abbreviation: "GMT +8:00")
            let date = Calendar.current.date(from: dateComponents)
            guard let unwrappedDate = date else {
                return .EXPIRED
            }
            if unwrappedDate < Date() {
                return .EXPIRED
            }
            return CardStatus(rawValue: CARD_STATUS_ARRAY.orEmpty)
        }
    }
}
