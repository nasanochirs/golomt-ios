//
//  CardListViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/30/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import iCarousel
import UIKit

class CardListController: BaseUIViewController {
    @IBOutlet var cardListCarousel: iCarousel!
    @IBOutlet var tableView: UITableView!

    var cardNumber: String?
    var optionButton: AccountCell.OPTION_BUTTON?
    var cards = [CardListResponse.Card]()
    var menuCells = [UITableViewCell]()

    static let BLOCK_TAG = 123

    lazy var orderButton: UIBarButtonItem? = {
        UIBarButtonItem(title: "card_order_title".localized(), style: .done, target: self, action: #selector(handleCardOrder))
    }()

    lazy var tableLoader: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: .whiteLarge)
        spinner.color = .defaultPrimaryText
        return spinner
    }()

    lazy var blockMenu: CardMenuCell = {
        let nib = Bundle.main.loadNibNamed("CardMenuCell", owner: self, options: nil)
        let cell = nib?.first as? CardMenuCell
        guard let unwrappedCell = cell else {
            return CardMenuCell()
        }
        unwrappedCell.setMenu(label: "card_menu_block", image: "card_block")
        unwrappedCell.onMenuClick = {
            var card = self.cards[self.cardListCarousel.currentItemIndex]
            let maskedNumber = card.getMaskedNumber()
            let blockDialog = UIAlertController(title: "card_block_alert_title".localized(with: maskedNumber), message: nil, preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "card_block_alert_negative_label".localized(), style: .cancel, handler: nil)
            let blockAction = UIAlertAction(title: "card_block_alert_positive_label".localized(), style: .destructive, handler: { _ in

                let requestBody = CardStatusRequest.Body(
                    CARD_NUMBER: card.CARD_NUMBER_ARRAY.orEmpty,
                    CARD_EXPIRE: card.CARD_EXPIRE_DATE.orEmpty,
                    CARD_STATUS: CardStatusRequest.Status.BLOCK.rawValue
                )
                self.showLoader()
                ConnectionFactory.handeCardStatusRequest(
                    body: requestBody,
                    success: { _ in
                        self.hideLoader()
                        let view = self.cardListCarousel.currentItemView
                        view?.subviews.forEach { subView in
                            if subView.tag == CardListController.BLOCK_TAG {
                                subView.isHidden = false
                                UIView.animate(
                                    withDuration: 1,
                                    animations: {
                                        subView.alpha = 0.4
                                    }
                                ) { _ in
                                    card.CARD_STATUS_ARRAY = CardListResponse.Card.CardStatus.BLOCKED.rawValue
                                    self.setMenu()
                                }
                            }
                        }
                    }, failed: { reason in
                        self.hideLoader()
                        self.handleRequestFailure(reason)
                    }
                )
                })
            blockDialog.addAction(cancelAction)
            blockDialog.addAction(blockAction)
            self.present(blockDialog, animated: true)
        }
        return unwrappedCell
    }()

    lazy var unblockMenu: CardMenuCell = {
        let nib = Bundle.main.loadNibNamed("CardMenuCell", owner: self, options: nil)
        let cell = nib?.first as? CardMenuCell
        guard let unwrappedCell = cell else {
            return CardMenuCell()
        }
        unwrappedCell.setMenu(label: "card_menu_unblock", image: "card_block")
        unwrappedCell.onMenuClick = {
            let card = self.cards[self.cardListCarousel.currentItemIndex]
            let maskedNumber = card.getMaskedNumber()
            let unblockDialog = UIAlertController(title: "card_unblock_alert_title".localized(with: maskedNumber), message: nil, preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "card_unblock_alert_negative_label".localized(), style: .cancel, handler: { _ in

                })
            let unblockAction = UIAlertAction(title: "card_unblock_alert_positive_label".localized(), style: .destructive, handler: { _ in
                let requestBody = CardStatusRequest.Body(
                    CARD_NUMBER: card.CARD_NUMBER_ARRAY.orEmpty,
                    CARD_EXPIRE: card.CARD_EXPIRE_DATE.orEmpty,
                    CARD_STATUS: CardStatusRequest.Status.UNBLOCK.rawValue
                )
                self.showLoader()
                ConnectionFactory.handeCardStatusRequest(
                    body: requestBody,
                    success: { _ in
                        self.hideLoader()
                        let view = self.cardListCarousel.currentItemView
                        view?.subviews.forEach { subView in
                            if subView.tag == CardListController.BLOCK_TAG {
                                UIView.animate(
                                    withDuration: 1,
                                    animations: {
                                        subView.alpha = 0
                                    }
                                ) { _ in
                                    subView.isHidden = true
                                    card.CARD_STATUS_ARRAY = CardListResponse.Card.CardStatus.ACTIVE.rawValue
                                    self.setMenu()
                                }
                            }
                        }
                    }, failed: { reason in
                        self.hideLoader()
                        self.handleRequestFailure(reason)
                    }
                )
            })
            unblockDialog.addAction(cancelAction)
            unblockDialog.addAction(unblockAction)
            self.present(unblockDialog, animated: true)
        }
        return unwrappedCell
    }()

    lazy var pinMenu: CardMenuCell = {
        let nib = Bundle.main.loadNibNamed("CardMenuCell", owner: self, options: nil)
        let cell = nib?.first as? CardMenuCell
        guard let unwrappedCell = cell else {
            return CardMenuCell()
        }
        unwrappedCell.setMenu(label: "card_menu_pin", image: "card_pin")
        unwrappedCell.onMenuClick = {
            let cardStoryBoard: UIStoryboard = UIStoryboard(name: "CardList", bundle: nil)
            let viewController = cardStoryBoard.instantiateViewController(withIdentifier: "CardPinChangeID")
            let cardPinVC = viewController as? CardChangePinViewController
            guard let unwrappedVC = cardPinVC else {
                return
            }
            let card = self.cards[self.cardListCarousel.currentItemIndex]
            unwrappedVC.card = card
            self.navigationController?.pushViewController(unwrappedVC, animated: true)
        }
        return unwrappedCell
    }()

    lazy var eTokenCodeMenu: CardMenuCell = {
        let nib = Bundle.main.loadNibNamed("CardMenuCell", owner: self, options: nil)
        let cell = nib?.first as? CardMenuCell
        guard let unwrappedCell = cell else {
            return CardMenuCell()
        }
        switch isEToken() {
        case true:
            unwrappedCell.setMenu(label: "card_menu_e_token", image: "card_ecode")
        case false:
            unwrappedCell.setMenu(label: "card_menu_e_code", image: "card_ecode")
        }
        unwrappedCell.onMenuClick = {
            let eCodeVC = CardChangeECodeViewController(nibName: "TableViewController", bundle: nil)
            let card = self.cards[self.cardListCarousel.currentItemIndex]
            eCodeVC.card = card
            self.navigationController?.pushViewController(eCodeVC, animated: true)
        }
        return unwrappedCell
    }()

    lazy var activateMenu: CardMenuCell = {
        let nib = Bundle.main.loadNibNamed("CardMenuCell", owner: self, options: nil)
        let cell = nib?.first as? CardMenuCell
        guard let unwrappedCell = cell else {
            return CardMenuCell()
        }
        unwrappedCell.onMenuClick = {
            let cardStoryBoard: UIStoryboard = UIStoryboard(name: "CardList", bundle: nil)
            let viewController = cardStoryBoard.instantiateViewController(withIdentifier: "CardPinChangeID")
            let cardPinVC = viewController as? CardChangePinViewController
            guard let unwrappedVC = cardPinVC else {
                return
            }
            let card = self.cards[self.cardListCarousel.currentItemIndex]
            unwrappedVC.card = card
            unwrappedVC.isNew = true
            self.navigationController?.pushViewController(unwrappedVC, animated: true)
        }
        unwrappedCell.setMenu(label: "card_menu_activate", image: "block_card")
        return unwrappedCell
    }()

    lazy var extendMenu: CardMenuCell = {
        let nib = Bundle.main.loadNibNamed("CardMenuCell", owner: self, options: nil)
        let cell = nib?.first as? CardMenuCell
        guard let unwrappedCell = cell else {
            return CardMenuCell()
        }
        unwrappedCell.setMenu(label: "card_menu_extend", image: "block_card")
        return unwrappedCell
    }()

    lazy var statementMenu: CardMenuCell = {
        let nib = Bundle.main.loadNibNamed("CardMenuCell", owner: self, options: nil)
        let cell = nib?.first as? CardMenuCell
        guard let unwrappedCell = cell else {
            return CardMenuCell()
        }
        unwrappedCell.setMenu(label: "card_menu_statement", image: "card_statement")
        unwrappedCell.onMenuClick = {
            let statementStoryBoard: UIStoryboard = UIStoryboard(name: "Statement", bundle: nil)
            let viewController = statementStoryBoard.instantiateViewController(withIdentifier: "CCDStatementID")
            let statementVC = viewController as? CCDStatementViewController
            guard let unwrappedVC = statementVC else {
                return
            }
            let card = self.cards[self.cardListCarousel.currentItemIndex]
            let cardNumber = card.CARD_NUMBER_ARRAY.orEmpty
            let accounts = creditAccountList.filter {
                $0.ACCT_NUMBER.orEmpty == cardNumber
            }
            guard let account = accounts.first else {
                return
            }
            unwrappedVC.account = account
            self.navigationController?.pushViewController(unwrappedVC, animated: true)
        }
        return unwrappedCell
    }()

    lazy var paymentMenu: CardMenuCell = {
        let nib = Bundle.main.loadNibNamed("CardMenuCell", owner: self, options: nil)
        let cell = nib?.first as? CardMenuCell
        guard let unwrappedCell = cell else {
            return CardMenuCell()
        }
        unwrappedCell.setMenu(label: "card_menu_payment", image: "card_payment")
        unwrappedCell.onMenuClick = {
            let card = self.cards[self.cardListCarousel.currentItemIndex]
            let cardNumber = card.CARD_NUMBER_ARRAY.orEmpty
            let accounts = creditAccountList.filter {
                $0.ACCT_NUMBER.orEmpty == cardNumber
            }
            guard let account = accounts.first else {
                return
            }
            let ccdVC = CreditCardTransactionViewController(nibName: "TableViewController", bundle: nil)
            ccdVC.selectedBeneficiaryAccount = account
            self.navigationController?.pushViewController(ccdVC, animated: true)
        }
        return unwrappedCell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "card_title".localized()

        cardListCarousel.type = .rotary
        cardListCarousel.isPagingEnabled = true

        tableView.backgroundView = tableLoader
        tableView.tableFooterView = UIView()

        navigationItem.rightBarButtonItem = orderButton

        fetchCardList()

        NotificationCenter.default.addObserver(self, selector: #selector(fetchCardList), name: Notification.Name(NotificationConstants.REFRESH_CARD_LIST), object: nil)
    }

    @objc private func handleCardOrder() {
        let storyboard = UIStoryboard(name: "CardOrder", bundle: nil)
        let viewController = storyboard.instantiateInitialViewController()
        guard let VC = viewController else {
            return
        }
        navigationController?.pushViewController(VC, animated: true)
    }

    @objc private func fetchCardList() {
        showLoader()
        ConnectionFactory.fetchCardList(
            success: { response in
                self.hideLoader()
                self.cards = response.cardList ?? []
                self.cardListCarousel.reloadData()
                let currentIndex = self.cardListCarousel.currentItemIndex
                let foundIndex = self.cards.firstIndex(where: { $0.CARD_NUMBER_ARRAY == self.cardNumber.orEmpty })
                if let index = foundIndex {
                    self.cardListCarousel.currentItemIndex = index
                    self.cardListCarousel.reloadItem(at: index, animated: true)
                } else {
                    self.cardListCarousel.reloadItem(at: currentIndex, animated: true)
                }
            }, failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }

    private func setMenu() {
        if tableView.numberOfSections > 0 {
            menuCells = []
            tableView.reloadData()
        }
        tableLoader.startAnimating()
        let card = cards[cardListCarousel.currentItemIndex]
        switch card.getStatus() {
        case .ACTIVE:
            menuCells = [blockMenu, pinMenu, eTokenCodeMenu]
            let foundIndex = cards.firstIndex(where: { $0.CARD_NUMBER_ARRAY == self.cardNumber.orEmpty })
            if foundIndex != nil {
                switch optionButton {
                case .CCD_STATUS:
                    blockMenu.onMenuClick?()
                case .CCD_PIN:
                    pinMenu.onMenuClick?()
                case .CCD_E_CODE:
                    eTokenCodeMenu.onMenuClick?()
                default:
                    break
                }
            }
            self.cardNumber = nil
            self.optionButton = nil
        case .BLOCKED:
            menuCells = [unblockMenu]
        case .NEW:
            menuCells = [activateMenu]
        case .EXPIRED:
            menuCells = [extendMenu]
        case .none:
            break
        }
        switch card.CARD_TYPE_ARRAY {
        case "CCD":
            menuCells.append(contentsOf: [statementMenu, paymentMenu])
        default:
            break
        }
        Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { _ in
            self.tableLoader.stopAnimating()
            self.tableView.reloadData()
        }
    }
}

extension CardListController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuCells.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return menuCells[indexPath.row]
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {}
}

extension CardListController: iCarouselDelegate, iCarouselDataSource {
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        if carousel.currentItemIndex > -1 {
            setMenu()
        }
    }

    func numberOfItems(in carousel: iCarousel) -> Int {
        return cards.count
    }

    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var itemView: UIImageView
        let card = cards[index]

        let width = cardListCarousel.bounds.size.width - 100
        let height = width * 0.9
        itemView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        itemView.image = card.CARD_SUBTYPE_ARRAY.orEmpty.getCardImage()
        itemView.corner(cornerRadius: 10)
        let imageHeight = itemView.image?.size.height ?? 0.0
        let imageWidth = itemView.image?.size.width ?? 0.0
        let scale: CGFloat
        if imageWidth > imageHeight {
            scale = itemView.bounds.width / imageWidth
        } else {
            scale = itemView.bounds.height / imageHeight
        }

        let size = CGSize(width: imageWidth * scale, height: imageHeight * scale)
        let numberLabel = UILabel(frame: CGRect(x: 40, y: size.height - 70, width: size.width, height: 20))
        itemView.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        itemView.contentMode = .scaleAspectFill
        let dateArray = card.CARD_EXPIRE_DATE.orEmpty.components(separatedBy: "-")
        var dateComponents = DateComponents()
        dateComponents.year = Int(dateArray.last.orEmpty)
        dateComponents.month = Int(dateArray.first.orEmpty)
        numberLabel.text = card.getMaskedNumber()
        numberLabel.textColor = .white
        numberLabel.useLargeFont()
        numberLabel.makeBold()
        let expiryDateLabel = UILabel(frame: CGRect(x: 40, y: size.height - 50, width: size.width, height: 20))
        expiryDateLabel.text = "card_expiry_date".localized(with: dateComponents.year ?? 0000, dateComponents.month ?? 00)
        expiryDateLabel.textColor = .white
        expiryDateLabel.useLargeFont()
        expiryDateLabel.makeBold()
        let frozenCard = UIImageView(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        frozenCard.image = UIImage(named: "card_frozen")
        frozenCard.contentMode = .scaleAspectFill
        frozenCard.tag = CardListController.BLOCK_TAG
        frozenCard.corner(cornerRadius: 10)
        itemView.addSubview(frozenCard)
        switch card.getStatus() {
        case .BLOCKED:
            frozenCard.alpha = 0.4
            frozenCard.isHidden = false
        default:
            frozenCard.alpha = 0
            frozenCard.isHidden = true
        }
        itemView.addSubview(numberLabel)
        itemView.addSubview(expiryDateLabel)
        setMenu()

        return itemView
    }

    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        switch option {
        default:
            return value
        }
    }
}

private extension String {
    func getCardImage() -> UIImage? {
        switch self {
        case "3000", "3001", "3004", "3005", "3010", "3011", "3014", "3015":
            return UIImage(named: "card_amex_gold")
        case "3002", "3003", "3006", "3007", "3012", "3013", "3016", "3017":
            return UIImage(named: "card_amex_green")
        case "4550", "4560":
            return UIImage(named: "card_zes_visa")
        case "4570", "4575", "4580", "4590":
            return UIImage(named: "card_business_visa")
        case "5002", "5004", "5008", "5009", "5010", "5015", "5016", "5018", "5023", "5029", "5187", "5197", "5610", "5611", "5650", "5651", "5652", "5653", "6001", "6003", "6006":
            return UIImage(named: "card_master_gold")
        case "6007", "6008", "6021", "6022":
            return UIImage(named: "card_gan")
        case "6023", "6024", "6025":
            return UIImage(named: "card_oyu_unionpay")
        case "6026":
            return UIImage(named: "card_suvd_unionpay")
        case "6027":
            return UIImage(named: "card_carbon_unionpay")
        case "5100":
            return UIImage(named: "card_oyu_master")
        case "5150", "5151":
            return UIImage(named: "card_suvd_master")
        case "5152", "5153":
            return UIImage(named: "card_oyu_master")
        case "5154", "5155":
            return UIImage(named: "card_carbon_master")
        case "9003":
            return UIImage(named: "card_oyu")
        case "9012":
            return UIImage(named: "card_carbon")
        case "9011":
            return UIImage(named: "card_suvd")
        case "4211", "4218", "4214", "4401", "4402":
            return UIImage(named: "card_oyu_visa")
        case "4217", "4223":
            return UIImage(named: "card_carbon_visa")
        case "4221", "4222":
            return UIImage(named: "card_suvd_visa")
        case "5520", "5522":
            return UIImage(named: "card_colorp")
        case "5521", "5523":
            return UIImage(named: "card_colorg")
        case "4540":
            return UIImage(named: "card_platinum")
        default:
            return UIImage(named: "card_default")
        }
    }
}
