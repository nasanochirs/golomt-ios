//
//  CardMenuCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/2/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class CardMenuCell: UITableViewCell {
    @IBOutlet var menuImage: UIImageView!
    @IBOutlet var menuLabel: UILabel!
    var onMenuClick: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }
    
    func setMenu(label: String, image: String) {
        menuLabel.text = label.localized()
        menuImage.image = UIImage(named: image)
        menuImage.setTint(color: .defaultPrimaryText)
    }
    
    private func initComponent() {
        menuLabel.useMediumFont()
        menuLabel.textColor = .defaultPrimaryText
        contentView.addTapGesture(tapNumber: 1, target: self, action: #selector(handleMenuClick))
    }
    
    @objc private func handleMenuClick() {
        onMenuClick?()
    }
}
