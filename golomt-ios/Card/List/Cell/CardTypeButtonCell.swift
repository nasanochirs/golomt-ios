//
//  CardTypeButtonCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/11/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class CardTypeButtonCell: UITableViewCell {
    @IBOutlet var typeContainerView: UIView!
    @IBOutlet var typeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        switch selected {
        case true:
            typeContainerView.setDefaultBlueGradient()
            typeLabel.textColor = .white
            typeContainerView.layer.cornerRadius = 25
        case false:
            typeContainerView.setGradient(startColor: UIColor.defaultSecondaryBackground.cgColor, endColor: UIColor.defaultSecondaryBackground.cgColor)
            typeLabel.textColor = .defaultPrimaryText
            typeContainerView.layer.cornerRadius = 25
        }
    }

    private func initComponent() {
        selectionStyle = .none
        typeLabel.adjustsFontSizeToFitWidth = true
        typeLabel.useMediumFont()
    }
    
    var labelText: String = "" {
        didSet {
            typeLabel.text = labelText
        }
    }
}
