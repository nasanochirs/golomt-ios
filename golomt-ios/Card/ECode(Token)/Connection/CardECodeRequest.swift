//
//  CardECodeRequest.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/9/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class CardECodeRequest: BaseRequest {
    var body = Body()

    private enum CodingKeys: String, CodingKey {
        case body
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(body, forKey: .body)
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    override init() {
        super.init()
        header.__SRVCID__ = "CARDE"
    }

    enum Event: String {
        case ECODE_REGISTER, ETOKEN_REGISTER
    }

    struct Body: Codable {
        var CARD_NUMBER: String = ""
        var EVENT: String = ""
        var CURRENCY: String = ""
        var ECODE: String = ""
        var CIF_ID: String = ""
    }
}
