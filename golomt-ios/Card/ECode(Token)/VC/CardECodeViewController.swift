//
//  CardECodeViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/10/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class CardECodeViewController: TableViewController {
    var card: CardListResponse.Card?

    lazy var resultField: TransactionDetailResultCell = {
        let nib = Bundle.main.loadNibNamed("TransactionDetailResultCell", owner: self, options: nil)
        let cell = nib?.first as? TransactionDetailResultCell
        guard let unwrappedCell = cell else {
            return TransactionDetailResultCell()
        }
        switch isEToken() {
        case true:
            title = "card_e_token_title".localized()
            unwrappedCell.setResult(message: "card_e_token_finish_success_label".localized(), isSuccessful: true)
        case false:
            title = "card_e_code_title".localized()
            unwrappedCell.setResult(message: "card_e_code_finish_success_label".localized(), isSuccessful: true)
        }
        return unwrappedCell
    }()

    lazy var warningField: DefaultLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLabelCell
        guard let unwrappedCell = cell else {
            return DefaultLabelCell()
        }
        switch isEToken() {
        case true:
            unwrappedCell.labelText = "card_e_token_result_warning_label".localized()
        case false:
            unwrappedCell.labelText = "card_e_code_result_warning_label".localized()
        }
        return unwrappedCell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
//        finishButton.titleText = "card_pin_code_finish_button_title"
        navigationItem.hidesBackButton = true
        title = "card_pin_code_title".localized()
        model.sections = [
            TableViewModel.Section(
                title: "",
                rows: [
                    TableViewModel.Row(
                        cell: resultField
                    ),
                    TableViewModel.Row(
                        cell: warningField
                    )
                ]
            )
        ]
        tableView.reloadData()
        onContinue = {
            if let cardListVC = self.navigationController?.viewControllers[1] {
                self.navigationController?.popToViewController(cardListVC, animated: true)
            }
        }
    }
}
