//
//  CardChangeECodeViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/9/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class CardChangeECodeViewController: TableViewController {
    var card: CardListResponse.Card?
    var requestBody = CardPinRequest.Body()

    lazy var cardNumberField: DefaultVerticalTitleLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultVerticalTitleLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultVerticalTitleLabelCell
        guard let unwrappedCell = cell else {
            return DefaultVerticalTitleLabelCell()
        }
        return unwrappedCell
    }()

    lazy var warningField: DefaultLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLabelCell
        guard let unwrappedCell = cell else {
            return DefaultLabelCell()
        }
        switch isEToken() {
        case true:
            unwrappedCell.labelText = "card_e_token_warning_label".localized()
        case false:
            unwrappedCell.labelText = "card_e_code_warning_label".localized()
        }
        return unwrappedCell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        model.sections = [
            TableViewModel.Section(
                title: "",
                rows: [
                    TableViewModel.Row(
                        cell: cardNumberField
                    ),
                    TableViewModel.Row(
                        cell: warningField
                    )
                ]
            )
        ]
        tableView.reloadData()
        onContinue = {
            self.handleChangeRequest()
        }
        switch isEToken() {
        case true:
//            eCodeButton.titleText = "card_e_token_link_button_title"
            title = "card_e_token_title".localized()
        case false:
//            eCodeButton.titleText = "card_e_code_create_button_title"
            title = "card_e_code_title".localized()
        }
        guard let unwrappedCard = card else {
            return
        }
        cardNumberField.labelText = unwrappedCard.getMaskedNumber()
        cardNumberField.titleText = "card_pin_code_card_number_title"
    }

    private func handleChangeRequest() {
        switch isEToken() {
        case true:
            let confirmationDialog = UIAlertController(title: "card_e_token_otp_dialog_title".localized(), message: nil, preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "card_e_token_otp_dialog_negative_label".localized(), style: .cancel, handler: nil)
            let confirmAction = UIAlertAction(
                title: "card_e_token_otp_dialog_positive_label".localized(),
                style: .default,
                handler: { _ in
                    self.requestChange(event: .ETOKEN_REGISTER, code: "")
                }
            )
            confirmationDialog.addAction(cancelAction)
            confirmationDialog.addAction(confirmAction)
            present(confirmationDialog, animated: true)
        case false:
            let confirmationDialog = UIAlertController(title: "card_e_code_otp_dialog_title".localized(), message: nil, preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "card_e_code_otp_dialog_negative_label".localized(), style: .cancel, handler: nil)
            let confirmAction = UIAlertAction(
                title: "card_e_code_otp_dialog_positive_label".localized(),
                style: .default,
                handler: { _ in
                    self.requestChange(event: .ECODE_REGISTER, code: confirmationDialog.textFields?.first?.text ?? "")
                }
            )
            confirmationDialog.addAction(cancelAction)
            confirmationDialog.addAction(confirmAction)
            confirmationDialog.addTextField { textField in
                textField.keyboardType = .numberPad
                textField.isSecureTextEntry = true
                textField.delegate = self
            }
            present(confirmationDialog, animated: true)
        }
    }

    private func requestChange(event: CardECodeRequest.Event, code: String) {
        let cardNumber = card?.CARD_NUMBER_ARRAY ?? ""
        let cardCurrency = card?.CARD_CURR_ARRAY ?? ""
        let requestBody = CardECodeRequest.Body(
            CARD_NUMBER: cardNumber,
            EVENT: event.rawValue,
            CURRENCY: cardCurrency,
            ECODE: code,
            CIF_ID: userId
        )
        showLoader()
        ConnectionFactory.handleECodeRequest(
            body: requestBody,
            success: { _ in
                self.hideLoader()
                let eCodeVC = CardECodeViewController(nibName: "TableViewController", bundle: nil)
                eCodeVC.card = self.card
                self.navigationController?.pushViewController(eCodeVC, animated: true)
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
}

extension CardChangeECodeViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let stringRange = Range(range, in: textField.text.orEmpty) else {
            return false
        }
        let newString = textField.text.orEmpty.replacingCharacters(in: stringRange, with: string)
        return newString.count <= 4
    }
}
