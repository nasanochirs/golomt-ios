//
//  HelpViewController.swift
//  golomt-ios
//
//  Created by Khulan on 8/3/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class HelpViewController: TableViewController {
    lazy var helpFields = [
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(cell: viewSmartBankGuide),
                TableViewModel.Row(cell: securityAdvice),
                TableViewModel.Row(cell: FAQ),
                TableViewModel.Row(cell: erxesField),
                TableViewModel.Row(cell: fields),
            ]
        ),
    ]
    lazy var viewSmartBankGuide: HelpLabelCell = {
        let nib = Bundle.main.loadNibNamed("HelpLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? HelpLabelCell else {
            return HelpLabelCell()
        }
        cell.labelText = "help_smartbank_label".localized()
        return cell
    }()
    
    lazy var securityAdvice: HelpLabelCell = {
        let nib = Bundle.main.loadNibNamed("HelpLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? HelpLabelCell else {
            return HelpLabelCell()
        }
        cell.labelText = "help_security_label".localized()
        return cell
    }()
    
    lazy var FAQ: HelpLabelCell = {
        let nib = Bundle.main.loadNibNamed("HelpLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? HelpLabelCell else {
            return HelpLabelCell()
        }
        cell.labelText = "help_faq_label".localized()
        return cell
    }()
    
    lazy var erxesField: HelpButtonCell = {
        let nib = Bundle.main.loadNibNamed("HelpButtonCell", owner: self, options: nil)
        guard let cell = nib?.first as? HelpButtonCell else {
            return HelpButtonCell()
        }
        cell.corner(cornerRadius: 20)
        cell.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        cell.backgroundColor = .defaultSecondaryBackground
        cell.firstLabel.text = "help_call_center_label".localized()
        cell.secondlabel.text = "help_online_assistant_label".localized()
        cell.thirdLabel.text = "help_chatbot_label".localized()
        cell.firstView.addTapGesture(tapNumber: 1, target: self, action: #selector(callClicked))
        cell.thirdView.addTapGesture(tapNumber: 1, target: self, action: #selector(helpChatbot))
        cell.firstImageView.image = UIImage(named: "transaction_foreign")
        cell.secondImageView.image = UIImage(named: "transaction_foreign")
        cell.thirdImageView.image = UIImage(named: "transaction_foreign")
        return cell
    }()
    
    lazy var fields: HelpButtonCell = {
        let nib = Bundle.main.loadNibNamed("HelpButtonCell", owner: self, options: nil)
        guard let cell = nib?.first as? HelpButtonCell else {
            return HelpButtonCell()
        }
        
        cell.firstView.addTapGesture(tapNumber: 1, target: self, action: #selector(locationClicked))
        cell.secondView.addTapGesture(tapNumber: 1, target: self, action: #selector(rateClicked))
        cell.thirdView.addTapGesture(tapNumber: 1, target: self, action: #selector(calculatorClicked))
        cell.firstLabel.text = "location_title".localized()
        cell.secondlabel.text = "rate_title".localized()
        cell.thirdLabel.text = "calculator_title".localized()
        cell.firstImageView.image = UIImage(named: "location")
        cell.secondImageView.image = UIImage(named: "rate")
        cell.thirdImageView.image = UIImage(named: "Group 1202")
        return cell
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "help_title".localized()
        hasButton = false
        setInitialRows()
        tableView.reloadData()
    }
    
    private func setInitialRows() {
        model.sections = helpFields
        tableView.reloadData()
    }
    
    @objc private func locationClicked() {
        let storyboard = UIStoryboard(name: "Location", bundle: nil)
        let viewController = storyboard.instantiateInitialViewController()
        guard let VC = viewController else {
            return
        }
        let navigationViewController = BaseNavigationViewController(rootViewController: VC)
        present(navigationViewController, animated: true)
    }
    
    @objc private func rateClicked() {
        let storyboard = UIStoryboard(name: "Rate", bundle: nil)
        let viewController = storyboard.instantiateInitialViewController()
        guard let VC = viewController else {
            return
        }
        let navigationViewController = BaseNavigationViewController(rootViewController: VC)
        present(navigationViewController, animated: true)
    }

    @objc private func calculatorClicked() {
        let VC = CalculatorViewController(nibName: "TableViewController", bundle: nil)
        let navigationViewController = BaseNavigationViewController(rootViewController: VC)
        present(navigationViewController, animated: true)
    }
    
    @objc private func callClicked() {
        if let url = URL(string: "telprompt://\(18001646)") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @objc private func helpSmart() {
        if let url = URL(string: "https://www.egolomt.mn/help/smart") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @objc private func helpSecurity() {
        if let url = URL(string: "https://www.egolomt.mn/security/mn") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @objc private func helpFaq() {
        if let url = URL(string: "https://www.egolomt.mn/faq") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @objc private func helpChatbot() {
        if let url = URL(string: "https://m.me/golomtbank") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            helpSmart()
        case 1:
            helpSecurity()
        case 2:
            helpFaq()
        default:
            break
        }
    }
}
