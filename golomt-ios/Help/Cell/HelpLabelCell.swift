//
//  HelpLabelCell.swift
//  golomt-ios
//
//  Created by Khulan on 8/3/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class HelpLabelCell: UITableViewCell {
    @IBOutlet var buttonLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        buttonLabel.useLargeFont()
        buttonLabel.textColor = .defaultPrimaryText
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var labelText: String = "" {
        didSet {
            buttonLabel.text = labelText
        }
    }
    
}
