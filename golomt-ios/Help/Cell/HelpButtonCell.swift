//
//  HelpButtonCell.swift
//  golomt-ios
//
//  Created by Khulan on 8/3/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class HelpButtonCell: UITableViewCell {
    @IBOutlet var firstView: UIStackView!
    @IBOutlet var secondView: UIStackView!
    @IBOutlet var thirdView: UIStackView!
    @IBOutlet var firstImageView: UIImageView!
    @IBOutlet var secondImageView: UIImageView!
    @IBOutlet var thirdImageView: UIImageView!
    @IBOutlet var firstLabel: UILabel!
    @IBOutlet var secondlabel: UILabel!
    @IBOutlet var thirdLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }
    
    private func initComponent() {
        selectionStyle = .none
        
        firstLabel.setFormat()
        secondlabel.setFormat()
        thirdLabel.setFormat()
     }

}

fileprivate extension UILabel {
    func setFormat() {
        self.useLargeFont()
        self.wordWrap()
        textColor = .defaultPrimaryText
    }
}
