//
//  RateConverterViewController.swift
//  golomt-ios
//
//  Created by Khulan on 7/9/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class RateConverterViewController: BaseUIViewController {
    @IBOutlet var tableView: UITableView!
//    @IBOutlet var segmentedControl: UISegmentedControl!
    
    var filtered = [RateResponse.Rate]()
    var rateList = [RateResponse.Rate]()
    var selectedCountry: RateResponse.Rate?
    var selectedIndex = 1
    var currencyInput = 1.0
    var calculatedAmount = 1.0
    var rateType = ""
    
    lazy var currencyTypeField: DefaultSegmentedGroupCell = {
        let nib = Bundle.main.loadNibNamed("DefaultSegmentedGroupCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultSegmentedGroupCell
        guard let unwrappedCell = cell else {
            return DefaultSegmentedGroupCell()
        }

        unwrappedCell.separator?.isHidden = true
        unwrappedCell.titleLabel?.isHidden = true
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        unwrappedCell.segments = [DefaultSegmentedGroupCell.SegmentModel(code: "", value: "rate_sell_title".localized()),
                                  DefaultSegmentedGroupCell.SegmentModel(code: "", value: "rate_buy_title".localized()),
                                  DefaultSegmentedGroupCell.SegmentModel(code: "", value: "rate_formal_title".localized())]
        cell?.contentView.backgroundColor = .defaultPrimaryBackground
        cell?.segmentedGroup.addTarget(self, action: #selector(handleSegmentChange), for: .valueChanged)
        if self.selectedIndex == 2 {
            unwrappedCell.selectedSegmentIndex = 2
        }
        else {
            unwrappedCell.selectedSegmentIndex = 0
        }
        return unwrappedCell
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "rate_converter_title".localized() + rateType.capitalized
        navigationItem.largeTitleDisplayMode = .never
        setupTableView()
        setupNavigationButton()
        addMNT()
        tableView.keyboardDismissMode = .onDrag
        tableView.reloadData()
    }
    
    @objc private func handleSegmentChange() {
        tableView.reloadData()
    }
    
    private func setupNavigationButton() {
        if #available(iOS 13.0, *) {
            self.navigationItem.leftBarButtonItem = nil
        }
        else {
            let closeButtonItem = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(dismissController))
            navigationItem.leftBarButtonItem = closeButtonItem
        }
    }
     
    private func setupTableView() {
        tableView.separatorColor = .defaultSeparator
        tableView.register(UINib(nibName: "RateTextFieldCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "RateTextFieldCell")
        tableView.registerCell(nibName: "RateConverterCell")
        tableView.hideScrollIndicators()
    }
    
    private func addMNT() {
        guard let selectedCountry = selectedCountry else {
            return
        }
        filtered.insert(RateResponse.Rate(non_cash_buy: selectedCountry.non_cash_buy, cash_avg: selectedCountry.cash_avg, cash_buy: selectedCountry.cash_buy, date: selectedCountry.date, non_cash_sell: selectedCountry.non_cash_sell, cash_sell: selectedCountry.cash_sell, currency: CurrencyConstants.MNT.rawValue), at: 0)
    }
    
    @objc private func handleValueChange(_ textField: UITextField) {
        currencyInput = textField.text.toDouble
        var indexPaths = [IndexPath]()
        for i in 0 ... filtered.count - 1 {
            indexPaths.append(IndexPath(row: i, section: 1))
        }
        tableView.reloadData()
    }
    
    @objc private func dismissController() {
        dismiss(animated: true, completion: {})
    }
    
    func getCurrencyCode(forCurrencyCode code: String) -> String? {
        let locale = Locale(identifier: getLanguage())
        return locale.localizedString(forCurrencyCode: code)?.capitalized
    }
}

extension RateConverterViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 0
        default:
            return filtered.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "RateTextFieldCell") as? RateTextFieldCell
            cell?.countryImage.text = selectedCountry?.currency?.toFlag
            cell?.currency.text = selectedCountry?.currency
            cell?.amountTextField.addTarget(self, action: #selector(handleValueChange(_:)), for: .editingChanged)
            cell?.clipsToBounds = true
            cell?.amountTextField.becomeFirstResponder()
            return cell
        default:
            return currencyTypeField
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 63
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 40
        default:
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RateConverterCell") as? RateConverterCell else {
            return UITableViewCell()
        }
        switch indexPath.row {
        case 0:
            let mng = CurrencyConstants.MNT
            cell.countryName.text = mng.rawValue
            cell.countryImage.text = mng.rawValue.toFlag
        default:
            cell.countryName.text = filtered[indexPath.row].currency
            cell.countryImage.text = filtered[indexPath.row].currency?.toFlag
        }
        let currencyCode = filtered[indexPath.row].currency
        cell.currencyText.text = getCurrencyCode(forCurrencyCode: currencyCode.orEmpty)

        if selectedIndex == 0 {
            switch currencyTypeField.selectedSegmentIndex {
            case 0:
                if filtered[indexPath.row].currency == "MNT" {
                    calculatedAmount = filtered[indexPath.row].cash_sell.orZero * currencyInput
                }
                else {
                    guard let selectedCountry = self.selectedCountry else {
                        break
                    }
                    calculatedAmount = currencyInput * selectedCountry.cash_sell.orZero / filtered[indexPath.row].cash_sell.orZero
                }
                cell.amount.text = calculatedAmount.formattedWithComma

            case 1:
                if filtered[indexPath.row].currency == "MNT" {
                    calculatedAmount = filtered[indexPath.row].cash_buy.orZero * currencyInput
                }
                else {
                    guard let selectedCountry = self.selectedCountry else {
                        break
                    }
                    calculatedAmount = currencyInput * selectedCountry.cash_buy.orZero / filtered[indexPath.row].cash_buy.orZero
                }
                cell.amount.text = calculatedAmount.formattedWithComma

            case 2:
                if filtered[indexPath.row].currency == "MNT" {
                    calculatedAmount = filtered[indexPath.row].cash_avg.orZero * currencyInput
                }
                else {
                    guard let selectedCountry = self.selectedCountry else {
                        break
                    }
                    calculatedAmount = currencyInput * selectedCountry.cash_avg.orZero / filtered[indexPath.row].cash_avg.orZero
                }
                cell.amount.text = calculatedAmount.formattedWithComma

            default:
                break
            }
        }
        else {
            switch currencyTypeField.selectedSegmentIndex {
            case 0:
                if filtered[indexPath.row].currency == "MNT" {
                    calculatedAmount = filtered[indexPath.row].non_cash_sell.orZero * currencyInput
                }
                else {
                    guard let selectedCountry = self.selectedCountry else {
                        break
                    }
                    calculatedAmount = currencyInput * selectedCountry.non_cash_sell.orZero / filtered[indexPath.row].non_cash_sell.orZero
                }
                cell.amount.text = calculatedAmount.formattedWithComma

            case 1:
                if filtered[indexPath.row].currency == "MNT" {
                    calculatedAmount = filtered[indexPath.row].non_cash_buy.orZero * currencyInput
                }
                else {
                    guard let selectedCountry = self.selectedCountry else {
                        break
                    }
                    calculatedAmount = currencyInput * selectedCountry.non_cash_buy.orZero / filtered[indexPath.row].non_cash_buy.orZero
                }
                cell.amount.text = calculatedAmount.formattedWithComma

            case 2:
                if filtered[indexPath.row].currency == "MNT" {
                    calculatedAmount = filtered[indexPath.row].cash_avg.orZero * currencyInput
                }
                else {
                    guard let selectedCountry = self.selectedCountry else {
                        break
                    }
                    calculatedAmount = currencyInput * selectedCountry.cash_avg.orZero / filtered[indexPath.row].cash_avg.orZero
                }
                cell.amount.text = calculatedAmount.formattedWithComma

            default:
                break
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let currentCell = tableView.cellForRow(at: indexPath)! as! RateConverterCell
        
        UIPasteboard.general.string = currentCell.amount.text
        let infoDialog = UIAlertController(title: nil, message: "rate_copy_success".localized(), preferredStyle: .alert)
        present(infoDialog, animated: true, completion: nil)
        
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            infoDialog.dismiss(animated: true, completion: nil)
        }
    }
}
