//
//  RateViewController.swift
//  golomt-ios
//
//  Created by Khulan Odkhuu on 7/7/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class RateViewController: BaseUIViewController {
    @IBOutlet var tableView: UITableView!
    var rateList = [RateResponse.Rate]()
    
    lazy var rateType: DefaultSegmentedGroupCell = {
        let nib = Bundle.main.loadNibNamed("DefaultSegmentedGroupCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultSegmentedGroupCell
        guard let unwrappedCell = cell else {
            return DefaultSegmentedGroupCell()
        }

        unwrappedCell.separator?.isHidden = true
        unwrappedCell.titleLabel?.isHidden = true
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        unwrappedCell.segments = [DefaultSegmentedGroupCell.SegmentModel(code: "", value: "rate_cash_title".localized()),
                                  DefaultSegmentedGroupCell.SegmentModel(code: "", value: "rate_non_cash_title".localized()),
                                  DefaultSegmentedGroupCell.SegmentModel(code: "", value: "rate_mongol_bank_title".localized())]
        unwrappedCell.segmentedGroup.addTarget(self, action: #selector(handleSegmentChange), for: .valueChanged)
        unwrappedCell.selectedSegmentIndex = 0
        return unwrappedCell
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "rate_title".localized()
        tableView.contentInsetAdjustmentBehavior = .never
        setupTableView()
        fetchRates()
        setupNavigationButton()
        tableView.reloadData()
    }
    
    @objc private func handleSegmentChange() {
        tableView.reloadData()
    }
    
    private func setupTableView() {
        tableView.separatorColor = .defaultSeparator
        tableView.registerCell(nibName: "RateCell")
        tableView.hideScrollIndicators()
    }
    
    private func setupNavigationButton() {
        if #available(iOS 13.0, *) {
            self.navigationItem.leftBarButtonItem = nil
        } else {
            let closeButtonItem = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(dismissController))
            navigationItem.leftBarButtonItem = closeButtonItem
        }
    }
    
    private func fetchRates() {
        showLoader()
        ConnectionFactory.fetchRates(
            success: { response in
                self.rateList = response.rateList ?? []
                
                self.tableView.reloadData()
                self.hideLoader()
        
            }, failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
    
    @objc private func dismissController() {
        dismiss(animated: true, completion: {})
    }
}

extension RateViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 0
        default:
            return rateList.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            return rateType
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RateCell") as? RateCell
            guard let unwrappedCell = cell else {
                return UITableViewCell()
            }
            cell?.countryImage.text = ""
            cell?.countryName.text = "rate_currency_title".localized()
            cell?.buyLabel.text = "rate_buy_title".localized()
            if rateType.selectedSegmentIndex == 2 {
                cell?.sellLabel.text = ""
            } else {
                cell?.sellLabel.text = "rate_sell_title".localized()
            }

            return unwrappedCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 63
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 50
        default:
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RateCell") as? RateCell else {
            return UITableViewCell()
        }
        cell.countryImage.text = rateList[indexPath.row].currency?.toFlag
        cell.countryName.text = rateList[indexPath.row].currency
        let rate = rateList[indexPath.row]
        switch rateType.segmentedGroup.selectedSegmentIndex {
        case 0:
            cell.buyLabel.text = rate.cash_buy?.formattedWithComma
            cell.sellLabel.text = rate.cash_sell?.formattedWithComma
        case 1:
            cell.buyLabel.text = rate.non_cash_buy?.formattedWithComma
            cell.sellLabel.text = rate.non_cash_sell?.formattedWithComma
        case 2:
            cell.buyLabel.text = rate.cash_avg?.formattedWithComma
            cell.sellLabel.text = ""
        default:
            break
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = UIStoryboard(name: "Rate", bundle: Bundle.main).instantiateViewController(withIdentifier: "RateConverter") as? RateConverterViewController
        let selectedCountry = rateList[indexPath.row].currency
        vc?.filtered = rateList.filter {
            $0.currency != selectedCountry.orEmpty
        }
        vc?.rateList = rateList
        vc?.selectedCountry = rateList[indexPath.row]
        vc?.selectedIndex = rateType.segmentedGroup.selectedSegmentIndex
        vc?.rateType = rateType.segmentedGroup.titleForSegment(at: rateType.segmentedGroup.selectedSegmentIndex).orEmpty
        let navigationViewController = BaseNavigationViewController(rootViewController: vc!)
        present(navigationViewController, animated: true)
    }
}
