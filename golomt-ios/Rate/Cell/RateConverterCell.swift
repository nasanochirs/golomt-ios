//
//  RateConverterCell.swift
//  golomt-ios
//
//  Created by Khulan on 7/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class RateConverterCell : UITableViewCell {
    @IBOutlet var countryImage: UILabel!
    @IBOutlet var countryName: UILabel!
    @IBOutlet var amount: UILabel!
    @IBOutlet var currencyText: UILabel!
    
    override func awakeFromNib() {
           super.awakeFromNib()
           initComponent()
       }
            
    private func initComponent() {
        countryImage.textColor = .defaultPrimaryText
        countryImage.useXXLargeFont()
        countryName.textColor = .defaultPrimaryText
        countryName.useLargeFont()
        amount.textColor = .defaultPrimaryText
        amount.useLargeFont()
        amount.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(copyValuePressed)))
        currencyText.useSmallFont()
        currencyText.textColor = .defaultPrimaryText
    }
    
    @objc func copyValuePressed() {
        UIPasteboard.general.string = amount.text
    }
}
