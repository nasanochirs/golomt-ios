//
//  RateCell.swift
//  golomt-ios
//
//  Created by Khulan Odkhuu on 7/7/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class RateCell: UITableViewCell {
    
    @IBOutlet var countryImage: UILabel!
    @IBOutlet var countryName: UILabel!
    @IBOutlet var buyLabel: UILabel!
    @IBOutlet var sellLabel: UILabel!
    
    override func awakeFromNib() {
           super.awakeFromNib()
           initComponent()
       }
     
     private func initComponent() {
        countryName.textColor = .defaultPrimaryText
        countryName.useLargeFont()
        countryImage.textColor = .defaultPrimaryText
        countryImage.useXXLargeFont()
        buyLabel.textColor = .defaultPrimaryText
        buyLabel.useLargeFont()
        sellLabel.textColor = .defaultPrimaryText
        sellLabel.useLargeFont()
        self.contentView.backgroundColor = .defaultPrimaryBackground
         
     }
}
