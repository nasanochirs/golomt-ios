//
//  RateTextFieldCell.swift
//  golomt-ios
//
//  Created by Khulan on 7/9/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class RateTextFieldCell: UITableViewHeaderFooterView {
    @IBOutlet var countryImage: UILabel!
    @IBOutlet var currency: UILabel!
    @IBOutlet var amountTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }
         
    private func initComponent() {
        countryImage.textColor = .defaultPrimaryText
        countryImage.useXXLargeFont()
        currency.textColor = .defaultPrimaryText
        currency.useLargeFont()
        amountTextField.textColor = .defaultPrimaryText
        amountTextField.useLargeFont()
        amountTextField.keyboardType = .numberPad
    }
}
