//
//  LocationViewController.swift
//  golomt-ios
//
//  Created by Khulan on 7/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import MapKit
import MaterialComponents.MaterialBottomSheet
import STPopup
import UIKit

class LocationViewController: BaseUIViewController {
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var segmentControl: UISegmentedControl!
    @IBOutlet var openSwitch: UISwitch!
    @IBOutlet var openLabel: UILabel!
    @IBOutlet var switchContainerView: UIView!
    var previousView: UIView?
    var locations = [LocationResponse.LocationData.Location]()
    
    lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.requestWhenInUseAuthorization()
        return manager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "location_title".localized()
        fetchBranchLocations()
        setUpMapView()
        setupNavigationButton()
        segmentControl.setTitle("location_atm_title".localized(), forSegmentAt: 0)
        segmentControl.setTitle("location_all_title".localized(), forSegmentAt: 1)
        segmentControl.setTitle("location_branch_title".localized(), forSegmentAt: 2)
        segmentControl.addTarget(self, action: #selector(handleSegmentChange), for: .valueChanged)
        segmentControl.selectedSegmentIndex = 1
        openLabel.useSmallFont()
        openLabel.text = "Нээлттэй"
        switchContainerView.backgroundColor = .defaultTertiaryBackground
        switchContainerView.corner(cornerRadius: 15)
        switchContainerView.layer.shadowColor = UIColor.defaultPrimaryBackground.cgColor
        switchContainerView.layer.shadowOpacity = 1
        switchContainerView.layer.shadowOffset = .zero
        switchContainerView.layer.shadowRadius = 10
        openSwitch.addTarget(self, action: #selector(handleSegmentChange), for: .valueChanged)
        openLabel.addTapGesture(tapNumber: 1, target: self, action: #selector(handleSegmentChange))
    }
    
    @objc private func handleSegmentChange() {
        switch segmentControl.selectedSegmentIndex {
        case 0:
            drawAnnotations(at: locations.filter { $0.type != LocationResponse.BranchType.BRANCH.rawValue })
        case 1:
            drawAnnotations(at: locations)
        case 2:
            drawAnnotations(at: locations.filter { $0.type == LocationResponse.BranchType.BRANCH.rawValue })
        default:
            break
        }
    }
    
    private func setupNavigationButton() {
        let closeButtonItem = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(dismissController))
        navigationItem.leftBarButtonItem = closeButtonItem
    }
    
    @objc private func dismissController() {
        dismiss(animated: true, completion: {})
    }
    
    private func fetchBranchLocations() {
        do {
            let locations = try getLocation()
            self.locations = locations
            self.drawAnnotations(at: locations)
        } catch {
            
        }
        ConnectionFactory.getLocations(
            success: { response in
                self.locations = response.locationData?.locations ?? []
                do {
                    try setLocation(location: self.locations)
                } catch {

                }
                self.drawAnnotations(at: self.locations)
            },
            failed: { reason in
                self.handleRequestFailure(reason)
            }
        )
    }
    
    private func drawAnnotations(at locations: [LocationResponse.LocationData.Location]) {
        mapView.removeAnnotations(mapView.annotations)
        for (_, location) in locations.enumerated() {
            if openSwitch.isOn, !(location.timetable?.isOpen() ?? false) {
                continue
            }
            let latitude = location.coordinates?.getLatitude()
            let longitude = location.coordinates?.getLongitude()
            let annotation = CustomMapPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(
                latitude: CLLocationDegrees(latitude.toDouble), longitude: CLLocationDegrees(longitude.toDouble)
            )
            annotation.title = nil
            annotation.data = location
            mapView.addAnnotation(annotation)
        }
        mapView.reloadInputViews()
        fetchQueues()
    }
    
    private func fetchQueues() {
        mapView.annotations.forEach { annotation in
            let location = CLLocation(latitude: annotation.coordinate.latitude, longitude: annotation.coordinate.longitude)
            guard let userLocation = self.mapView.userLocation.location else {
                return
            }
            let distance = location.distance(from: userLocation)
            if distance < 1000 {
                guard let annotation = annotation as? CustomMapPointAnnotation else {
                    return
                }
                let data = annotation.data
                if data.type != LocationResponse.BranchType.BRANCH.rawValue {
                    return
                }
                ConnectionFactory.getBranchQueue(
                    solId: data.solId.orEmpty,
                    success: { response in
                        guard let unwrappedQueue = response.queues?.first else {
                            return
                        }
                        self.mapView.removeAnnotation(annotation)
                        annotation.title = unwrappedQueue.count?.toString ?? "0"
                        self.mapView.addAnnotation(annotation)
                        if let userLocation = self.locationManager.location?.coordinate {
                            self.mapView.addOverlay(MKCircle(center: userLocation, radius: 1000))
                        }
                    }, failed: { _ in
                    }
                )
            }
        }
    }
    
    private func setUpMapView() {
        mapView.showsUserLocation = true
        mapView.showsCompass = true
        mapView.showsScale = true
        mapView.delegate = self
        currentLocation()
    }
    
    private func currentLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.showsBackgroundLocationIndicator = true
        locationManager.startUpdatingLocation()
    }
}

extension LocationViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! as CLLocation
        let currentLocation = location.coordinate
        let coordinateRegion = MKCoordinateRegion(center: currentLocation, latitudinalMeters: 2000, longitudinalMeters: 2000)
        mapView.setRegion(coordinateRegion, animated: true)
        locationManager.stopUpdatingLocation()
    }
}

extension LocationViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "BranchPin"
        
        if annotation is MKUserLocation {
            return nil
        }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
            annotationView?.isEnabled = true
            if let data = (annotation as? CustomMapPointAnnotation)?.data {
                annotationView?.image = UIImage(named: data.type.orEmpty)?.scaled(to: CGSize(width: 30, height: 30))
            }
        } else {
            if let title = annotation.title, !title.orEmpty.isEmpty {
                let annotationLabel = UILabel(frame: CGRect(x: 12, y: -12, width: 20, height: 20))
                annotationLabel.numberOfLines = 0
                annotationLabel.textAlignment = .center
                annotationLabel.font = .systemFont(ofSize: 12)
                annotationLabel.text = title.orEmpty
                annotationLabel.textColor = .white
                annotationLabel.tag = 180
                annotationLabel.backgroundColor = .red
                annotationLabel.layer.cornerRadius = 10
                annotationLabel.clipsToBounds = true
                annotationView?.addSubview(annotationLabel)
                annotationView?.sendSubviewToBack(annotationLabel)
            } else {
                annotationView?.subviews.forEach { subView in
                    if subView.tag == 180 {
                        subView.removeFromSuperview()
                    }
                }
            }
            if let data = (annotation as? CustomMapPointAnnotation)?.data {
                annotationView?.image = UIImage(named: data.type.orEmpty)?.scaled(to: CGSize(width: 30, height: 30))
            }
            annotationView?.annotation = annotation
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        mapView.selectedAnnotations.forEach { annotation in
            mapView.deselectAnnotation(annotation, animated: false)
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.previousView?.transform = .identity
            view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
         })
        previousView = view
        let annotation = view.annotation as? CustomMapPointAnnotation
        guard let unwrappedAnnotation = annotation else {
            return
        }
        let locationData = unwrappedAnnotation.data
        let controller = LocationController()
        controller.location = locationData
        controller.hasButton = false
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: controller)
        let shapeGenerator = MDCRectangleShapeGenerator()
        
        let cornerTreatment = MDCRoundedCornerTreatment(radius: 15)
        shapeGenerator.topLeftCorner = cornerTreatment
        shapeGenerator.topRightCorner = cornerTreatment
        
        bottomSheet.setShapeGenerator(shapeGenerator, for: .preferred)
        bottomSheet.setShapeGenerator(shapeGenerator, for: .extended)
        bottomSheet.setShapeGenerator(shapeGenerator, for: .closed)
        present(bottomSheet, animated: true, completion: nil)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        var circleRenderer = MKCircleRenderer()
        if let overlay = overlay as? MKCircle {
            circleRenderer = MKCircleRenderer(circle: overlay)
            circleRenderer.fillColor = .clear
            
            if #available(iOS 13, *) {
                if traitCollection.userInterfaceStyle == .dark {
                    circleRenderer.strokeColor = .white
                } else {
                    circleRenderer.strokeColor = .defaultPurpleGradientStart
                }
            } else {
                circleRenderer.strokeColor = .defaultPurpleGradientStart
            }
            circleRenderer.alpha = 0.1
            circleRenderer.lineWidth = 3
            return circleRenderer
        }
        return MKOverlayRenderer(overlay: overlay)
    }
}
