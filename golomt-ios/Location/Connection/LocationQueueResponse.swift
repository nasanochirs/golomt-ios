//
//  LocationQueueResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 8/5/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LocationQueueResponse: BaseResponse {
    var queues: [Queue]?
    
    private enum CodingKeys: String, CodingKey {
        case body
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(queues, forKey: .body)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.queues = try container.decodeIfPresent([Queue].self, forKey: .body)
        try super.init(from: decoder)
    }

    struct Queue: Codable {
        var branchId: Int?
        var count: Int?
        var types: [QueueType]?
        
        struct QueueType: Codable {
            var typeName: String?
            var count: Int?
        }
    }
}
