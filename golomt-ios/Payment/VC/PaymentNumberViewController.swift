//
//  PaymentNumberViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 8/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class PaymentNumberViewController: TableViewController {
    var type: PaymentType?
    var unitModel = UnitModel()
    
    lazy var numberField: PaymentNumberCell = {
        let nib = Bundle.main.loadNibNamed("PaymentNumberCell", owner: self, options: nil)
        guard let cell = nib?.first as? PaymentNumberCell else {
            return PaymentNumberCell()
        }
        cell.title = "Утасны дугаар"
        cell.textLength = 8
        cell.onTextFinished = {
            self.handleContinue()
        }
        return cell
    }()
    
    lazy var firstField: DefaultLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLabelCell
        guard let unwrappedCell = cell else {
            return DefaultLabelCell()
        }
        unwrappedCell.labelText = "change_password_first_label".localized()
        return unwrappedCell
    }()
    
    lazy var secondField: DefaultLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLabelCell
        guard let unwrappedCell = cell else {
            return DefaultLabelCell()
        }
        unwrappedCell.labelText = "change_password_second_label".localized()
        return unwrappedCell
    }()
    
    enum PaymentType {
        case Unit, Tax, CustomsBarcode, CustomsInvoice, CustomsRegister, EBarimt
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switch self.type {
        case .Unit:
            title = "unit_payment_title".localized()
        case .Tax:
            title = "tax_payment_title".localized()
        case .CustomsInvoice:
            title = "customs_payment_invoice_title".localized()
        case .CustomsBarcode:
            title = "customs_payment_barcode_title".localized()
        case .CustomsRegister:
            title = "customs_payment_register_title".localized()
        case .EBarimt:
            title = "ebarimt_payment_title".localized()
        default:
            title = ""
        }
        model.sections = [
            TableViewModel.Section(
                title: "",
                rows: [
                    TableViewModel.Row(cell: self.numberField),
                    TableViewModel.Row(cell: self.firstField),
                    TableViewModel.Row(cell: self.secondField),
                ]
            ),
        ]
        tableView.reloadData()
        onContinue = {
            self.handleContinue()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.numberField.focusTextField()
    }
    
    private func handleContinue() {
        let index = self.numberField.inputText.index(self.numberField.inputText.startIndex, offsetBy: 2)
        let substring = self.numberField.inputText[..<index]
        switch Int(substring) {
        case 99, 95, 94, 85:
            self.unitModel.type = .Mobicom
        case 96, 91, 90:
            self.unitModel.type = .Skytel
        case 98, 97, 93:
            self.unitModel.type = .GMobile
        case 88, 86, 89, 80:
            self.unitModel.type = .Unitel
        default:
            self.chooseOperator()
        }
        let depositStoryBoard: UIStoryboard = UIStoryboard(name: "Unit", bundle: nil)
        let viewController = depositStoryBoard.instantiateViewController(withIdentifier: "UnitID")
        let typeVC = viewController as? UnitChooseViewController
        guard let unwrappedVC = typeVC else {
            return
        }
        unwrappedVC.phoneNumber = self.numberField.inputText
        unwrappedVC.unitModel = self.unitModel
        self.navigationController?.pushViewController(unwrappedVC, animated: true)
    }
    
    private func chooseOperator() {
        let controller = PickerController()
        controller.title = "unit_choose_operator_label".localized()
        controller.list = [(0, "unit_gmobile_label".localized()), (1, "unit_mobicom_label".localized()), (2, "unit_skytel_label".localized()), (3, "unit_unitel_label".localized())]
        controller.onItemSelect = { type in
            controller.popupController?.dismiss()
            let type = type as? (index: Int, text: String)
            switch type?.index {
            case 0:
                self.unitModel.type = .GMobile
            case 1:
                self.unitModel.type = .Mobicom
            case 2:
                self.unitModel.type = .Skytel
            case 3:
                self.unitModel.type = .Unitel
            default:
                break
            }
        }
        controller.selectedItem = self.unitModel.type
        let popupController = STPopupController(rootViewController: controller)
        popupController.style = .bottomSheet
        popupController.present(in: self)
    }
}
