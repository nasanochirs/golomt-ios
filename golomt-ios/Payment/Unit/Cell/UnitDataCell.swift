//
//  UnitDataCell.swift
//  golomt-ios
//
//  Created by Khulan on 8/12/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class UnitDataCell: UICollectionViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var infoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = .defaultSecondaryBackground
        contentView.corner(cornerRadius: 15)
        titleLabel.makeBold()
        titleLabel.useLargeFont()
        titleLabel.textColor = .defaultPrimaryText
        titleLabel.adjustsFontSizeToFitWidth = true
        
        infoLabel.makeBold()
        infoLabel.useSmallFont()
        infoLabel.textColor = .defaultSecondaryText
        infoLabel.adjustsFontSizeToFitWidth = true
        // Initialization code
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                contentView.backgroundColor = .defaultSelectionBackground
                titleLabel.textColor = .white
                infoLabel.textColor = .white
            } else {
                contentView.backgroundColor = .defaultSecondaryBackground
                titleLabel.textColor = .defaultPrimaryText
                infoLabel.textColor = .defaultSecondaryText
            }
        }
    }
    
    var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }
    
    var info: String? = "" {
        didSet {
            infoLabel?.text = info
        }
    }

    
}
