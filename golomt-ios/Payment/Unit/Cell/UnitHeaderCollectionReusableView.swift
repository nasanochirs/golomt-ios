//
//  UnitHeaderCollectionReusableView.swift
//  golomt-ios
//
//  Created by Khulan on 8/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class UnitHeaderCollectionReusableView: UICollectionReusableView {
    static let identifier = "HeaderCollectionReusableView"
    
    @IBOutlet var titleLabel: UILabel!
    
    var unitModel = UnitModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.textColor = .defaultPrimaryText
        titleLabel.useMediumFont()
        titleLabel.makeBold()
    
        // Initialization code
    }
    
    var title: String = "" {
        didSet {
            titleLabel.text = title
            titleLabel.allCaps()
        }
    }
}
