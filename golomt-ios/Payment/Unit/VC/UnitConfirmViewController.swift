//
//  UnitConfirmViewController.swift
//  golomt-ios
//
//  Created by Khulan on 8/14/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class UnitConfirmViewController: TableViewController {
    var unitModel = UnitModel()
    var phoneNumber = ""
    var selectedSenderAccount: AccountResponse.Account?

    lazy var senderSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "transaction_confirm_sender_info_label".localized(),
            rows: [
                TableViewModel.Row(
                    title: "transaction_confirm_sender_account_number_title".localized(), info: "transaction_confirm_sender_account_number_label".localized(with: self.unitModel.senderAccountInfo.ACCT_NUMBER ?? "", self.unitModel.senderAccountInfo.ACCT_CURRENCY ?? "")
                ),
                TableViewModel.Row(
                    title: "transaction_confirm_sender_account_nickname_title".localized(), info: self.unitModel.senderAccountInfo.ACCT_NICKNAME
                )
            ]
        )
    }()

    lazy var transactionSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "transaction_confirm_transaction_info_label".localized(),
            rows: [
                TableViewModel.Row(
                    title: "transaction_confirm_transaction_amount_title".localized(),
                    info: self.unitModel.transactionInfo.displayAmount ?? "",
                    infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "transaction_confirm_transaction_remark_title".localized(), info: self.unitModel.type?.rawValue
                ),
                TableViewModel.Row(
                    title: "unit_mobile_number_label".localized(), info: self.phoneNumber
                )
            ]
        )
    }()

    lazy var senderAccountField: DefaultAccountPickerCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAccountPickerCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAccountPickerCell
        guard let unwrappedCell = cell else {
            return DefaultAccountPickerCell()
        }
        self.onAccountSelect = { account in
            self.senderAccountField.account = account
            self.selectedAccount = account
            self.unitModel.senderAccountInfo = account
        }
        unwrappedCell.onPickerClick = {
            self.showSenderAccountPicker()
        }
        unwrappedCell.containerBackgroundView.backgroundColor = .defaultPrimaryBackground
        return unwrappedCell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Баталгаажуулах".localized()
        setButtonTitle(.Confirm)
        model.sections = [
            transactionSection,
            TableViewModel.Section(
                rows: [
                    TableViewModel.Row(
                        cell: senderAccountField
                    )
                ]
            )
        ]
        onContinue = {
            self.handleContinue()
        }
    }

    @objc private func deleteOrder() {}

    private func handleContinue() {
        let loaderViewController = TransactionLoadingController(nibName: "TransactionLoadingController", bundle: nil)
        navigationController?.pushViewController(loaderViewController, animated: true)
        let detailViewController = TransactionDetailViewController(nibName: "TableViewController", bundle: nil)
        detailViewController.model.sections = [senderSection, transactionSection]

        detailViewController.onFinish = {
            self.navigationController?.popToRootViewController(animated: true)
            if detailViewController.isSuccessful {
                NotificationCenter.default.post(name: Notification.Name(NotificationConstants.REFRESH_ACCOUNTS), object: nil)
            }
        }
        ConnectionFactory.buyDataUnit(
            model: unitModel,
            success: { response in
                let resultMessage = response.getMessage()
                detailViewController.transactionMessage = resultMessage
                loaderViewController.setResult(
                    isSuccessful: true,
                    message: resultMessage,
                    completion: {
                        detailViewController.isSuccessful = true
                        self.navigationController?.pushViewController(detailViewController, animated: false)
                    }
                )
            },
            failed: { reason in
                let resultMessage = reason?.MESSAGE_DESC ?? ""
                detailViewController.transactionMessage = resultMessage
                loaderViewController.setResult(isSuccessful: false, message: resultMessage, completion: {
                    detailViewController.isSuccessful = false
                    self.navigationController?.pushViewController(detailViewController, animated: false)
                })
            }
        )
    }
}
