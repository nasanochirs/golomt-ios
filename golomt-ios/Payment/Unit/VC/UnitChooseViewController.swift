//
//  UnitChooseViewController.swift
//  golomt-ios
//
//  Created by Khulan on 8/12/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class UnitChooseViewController: BaseUIViewController {
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var containerStackView: UIStackView!
        
    var unitModel = UnitModel()
    var phoneNumber = ""
    
    lazy var continueButton: DefaultGradientButton = {
        let button = DefaultGradientButton()
        button.titleText = "card_order_type_button_title"
        button.addTarget(self, action: #selector(continueAction), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = phoneNumber
        collectionView.registerCell(nibName: "UnitDataCell")
        collectionView.register(UINib(nibName: "UnitHeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: UnitHeaderCollectionReusableView.identifier)
        unitModel.unitList.removeAll()
        unitList()
    }
    
    private func unitList() {
        showLoader()
        ConnectionFactory.fetchList(
            code: unitModel.type?.rawValue ?? "",
            success: { response in
                response.list?.forEach { item in
                    let splitted = item.CD_DESC.orEmpty.components(separatedBy: "/")
                    let unit = UnitModel.Unit(
                        code: splitted.count > 0 ? item.CM_CODE.orEmpty : "",
                        type: splitted.count > 1 ? splitted[1] : "",
                        title: splitted.count > 2 ? splitted[2] : "",
                        info: ""
                    )
                    if splitted.count > 3 {
                        for i in 3 ... (splitted.count - 1) {
                            unit.info += "\(splitted[i]) "
                        }
                    }
                    self.unitModel.unitList.append(unit)
                    self.collectionView.reloadData()
                    self.hideLoader()
                }
                
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
    
    @objc private func continueAction() {
        let confirmVC = UnitConfirmViewController(nibName: "TableViewController", bundle: nil)
        confirmVC.unitModel = unitModel
        confirmVC.unitModel.transactionInfo = UnitModel.TransactionInfo(
            amount: unitModel.selectedUnit?.title.toDigits.toDouble,
            displayAmount: unitModel.selectedUnit?.title ?? ""
        )
        confirmVC.phoneNumber = phoneNumber
        confirmVC.unitModel.phoneNumber = phoneNumber
        navigationController?.pushViewController(confirmVC, animated: true)
    }
}

extension UnitChooseViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let unitList = unitModel.unitList.filter { $0.type == "unit" }
        let dataList = unitModel.unitList.filter { $0.type == "data" }
        switch section {
        case 0:
            return dataList.count
        default:
            return unitList.count
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UnitDataCell", for: indexPath) as! UnitDataCell
        switch indexPath.section {
        case 0:
            let dataList = unitModel.unitList.filter { $0.type == "data" }
            let data = dataList[indexPath.row]
            cell.title = data.title
            cell.info = data.info
        case 1:
            let unitList = unitModel.unitList.filter { $0.type == "unit" }
            let unit = unitList[indexPath.row]
            cell.title = unit.title
            cell.info = unit.info
        default:
            break
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return .init(width: view.frame.width, height: CGFloat(40))
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: UnitHeaderCollectionReusableView.identifier, for: indexPath) as! UnitHeaderCollectionReusableView
        switch indexPath.section {
        case 0:
            headerView.title = "Дата".localized()
        default:
            headerView.title = "Нэгж".localized()
        }
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        unitModel.selectedUnit = unitModel.unitList[indexPath.row]
        continueButton.widthAnchor.constraint(equalToConstant: containerStackView.bounds.size.width - 40).isActive = true
        continueButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        containerStackView.addArrangedSubview(continueButton)
        self.collectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
    }
}

extension UnitChooseViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (view.frame.width / 3) + 30, height: CGFloat(100))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }
}
