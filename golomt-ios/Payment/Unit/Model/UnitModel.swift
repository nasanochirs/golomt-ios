//
//  UnitModel.swift
//  golomt-ios
//
//  Created by Khulan on 8/12/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class UnitModel {
    var type: OperatorType? = nil
    var unitList = [Unit]()
    var selectedUnit: Unit? = nil
    var senderAccountInfo = AccountResponse.Account()
    var transactionInfo = TransactionInfo()
    var phoneNumber = ""
    
    enum OperatorType: String {
        case GMobile = "GMO"
        case Mobicom = "MOB"
        case Skytel = "SKY"
        case Unitel = "UNI"
    }
    
    class Unit {
        var code: String = ""
        var type: String = ""
        var title: String = ""
        var info: String = ""
        
        init (code: String, type: String, title: String, info: String ) {
            self.code = code
            self.type = type
            self.title = title
            self.info = info
        }
    }
    
    struct TransactionInfo {
        var amount: Double?
        var displayAmount: String?
    }
}
