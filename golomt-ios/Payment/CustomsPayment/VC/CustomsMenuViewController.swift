//
//  CustomsMenuViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 8/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class CustomsMenuViewController: MenuTableViewController {
    lazy var detailSection: MenuTableViewModel.Section = {
        var section = MenuTableViewModel.Section(
            rows: [
                MenuTableViewModel.Row(
                    icon: UIImage(named: "customs_barcode"), title: "customs_payment_barcode_title".localized()
                ),
                MenuTableViewModel.Row(
                    icon: UIImage(named: "customs_invoice"), title: "customs_payment_invoice_title".localized()
                ),
                MenuTableViewModel.Row(
                    icon: UIImage(named: "customs_register"), title: "customs_payment_register_title".localized()
                )
            ]
        )
        return section
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "customs_payment_title".localized()
        tableView.registerCell(nibName: "DefaultMenuCell")
        tableView.rowHeight = 70
        model.sections = [detailSection]
        tableView.reloadData()

        onClick = { row in
            let paymentVC = PaymentNumberViewController(nibName: "TableViewController", bundle: nil)
            switch row {
                case 0:
                    paymentVC.type = .CustomsBarcode
                case 1:
                    paymentVC.type = .CustomsInvoice
                case 2:
                    paymentVC.type = .CustomsRegister
                default:
                    break
            }
            self.navigationController?.pushViewController(paymentVC, animated: true)
        }
    }
}
