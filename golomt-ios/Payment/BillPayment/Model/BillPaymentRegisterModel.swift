//
//  BillPaymentRegisterModel.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class BillPaymentRegisterModel {
    var consumerCode = ""
    var subscriptionDay = ""
    var organization: BillPaymentOrganizationResponse.Organization? = nil
    var nickname = ""
}
