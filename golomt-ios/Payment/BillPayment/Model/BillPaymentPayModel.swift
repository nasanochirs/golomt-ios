//
//  BillPaymentPayModel.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/31/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class BillPaymentPayModel {
    var amount = 0.0
    var account: AccountResponse.Account?
    var payment: PaymentResponse.Payment?
    var organization: BillPaymentOrganizationResponse.Organization?
}
