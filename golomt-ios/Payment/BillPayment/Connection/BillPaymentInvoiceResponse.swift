//
//  BillPaymentInvoiceResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/31/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class BillPaymentInvoiceResponse: BaseResponse {
    let response: DefaultResponse?
    
    private enum CodingKeys: String, CodingKey {
        case `default`
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(response, forKey: .default)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.response = try container.decodeIfPresent(DefaultResponse.self, forKey: .default)
        try super.init(from: decoder)
    }
    
    struct DefaultResponse: Codable {
        var AMOUNT: String?
        var MSISDN: String?
        var OTP: String?
    }
}
