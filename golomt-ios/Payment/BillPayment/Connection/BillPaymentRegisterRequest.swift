//
//  BillPaymentRegisterRequest.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class BillPaymentRegisterRequest: BaseRequest {
    var body = Body()

    private enum CodingKeys: String, CodingKey {
        case body
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(body, forKey: .body)
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    override init() {
        super.init()
        header.__SRVCID__ = "RGSDPY"
    }

    struct Body: Codable {
        var BILLER_NAME = ""
        var LOCAL_BILLER_ID = ""
        var CONSUMER_CODE = ""
        var BILLER_TYPE = ""
        var BILLER_NICK_NAME = ""
        var SUBSCRIPTION_START_DATE = ""
        var REG_RQD = "Y"
        var PAYMENT_START_DATE = ""
    }
}
