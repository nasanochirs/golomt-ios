//
//  BillPaymentOrganizationResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/27/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class BillPaymentOrganizationResponse: BaseResponse {
    let organizationList: [Organization]?
    
    private enum CodingKeys: String, CodingKey {
        case BillerList_REC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(organizationList, forKey: .BillerList_REC)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.organizationList = try container.decodeIfPresent([Organization].self, forKey: .BillerList_REC)
        try super.init(from: decoder)
    }
    
    struct Organization: Codable {
        var PAYEE_FORMAT_INFO: String?
        var BILLER_NAME_INFO: String?
        var BUSINESS_CATEGORY_DESC: String?
        var BILLER_ID_INFO: String?
        var BILLER_TYPE_INFO: String?
        var BUSINESS_CATEGORY_VALUE: String?
        var BILLER_CURRENCY_INFO: String?
        
        func getOrganizationImage() -> UIImage? {
            switch BUSINESS_CATEGORY_VALUE.orEmpty {
            case "000001":
                return UIImage(named: "ELECTRICITY")
            case "000002":
                return UIImage(named: "PHONE")
            case "000003":
                return UIImage(named: "HOA")
            case "000004":
                return UIImage(named: "INTERNET")
            case "000005":
                return nil
            case "000006":
                return UIImage(named: "OTHER")
            case "000007":
                return UIImage(named: "AIRLINE")
            default:
                return nil
            }
        }
    }
}
