//
//  BillPaymentOrganizationViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/27/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit
import STPopup

class BillPaymentOrganizationViewController: BaseUIViewController {
    @IBOutlet var collectionView: UICollectionView!
    
    var organizationList = [BillPaymentOrganizationResponse.Organization]()
    var organizations = [(key: String, value: [BillPaymentOrganizationResponse.Organization])]()

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.registerCell(nibName: "BillPaymentOrganizationCell")
        organizations = Dictionary(grouping: organizationList, by: { $0.BUSINESS_CATEGORY_VALUE.orEmpty }).sorted { $0.0 < $1.0 }
    }
    
    func showPicker(with organizations: [BillPaymentOrganizationResponse.Organization]) {
        let controller = PickerController()
        controller.list = organizations
        controller.onItemSelect = { organization in
            controller.popupController?.dismiss()
            let VC = BillPaymentRegisterViewController(nibName: "TableViewController", bundle: nil)
            VC.organization = organization as? BillPaymentOrganizationResponse.Organization
            self.navigationController?.pushViewController(VC, animated: true)
        }
        controller.title = "Байгууллага сонгоно уу".localized()
        let popupController = STPopupController(rootViewController: controller)
        popupController.style = .bottomSheet
        popupController.present(in: self)
    }
}

extension BillPaymentOrganizationViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        organizations.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BillPaymentOrganizationCell", for: indexPath) as! BillPaymentOrganizationCell
        let organization = organizations[indexPath.row]
        cell.setMenu(data: organization)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let organization = organizations[indexPath.row]
        showPicker(with: organization.value)
    }
    
}

extension BillPaymentOrganizationViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (view.frame.size.width - 30) / 2
        let height = (view.frame.size.width - 30) / 2
        return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
}
