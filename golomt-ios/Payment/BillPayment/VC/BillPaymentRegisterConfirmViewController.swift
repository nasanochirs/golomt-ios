//
//  BillPaymentRegisterConfirmViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class BillPaymentRegisterConfirmViewController: TableViewController {
    var registerModel = BillPaymentRegisterModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Confirm)
        title = "Баталгаажуулах".localized()
        tableView.reloadData()
        onContinue = {
            self.handleContinue()
        }
    }

    private func handleContinue() {
        let loaderViewController = TransactionLoadingController(nibName: "TransactionLoadingController", bundle: nil)
        self.navigationController?.pushViewController(loaderViewController, animated: true)
        let detailViewController = TransactionDetailViewController(nibName: "TableViewController", bundle: nil)
        detailViewController.controllerTitle = "Төлбөр бүртгэх".localized()
        detailViewController.onFinish = {
            if let rootVC = self.navigationController?.viewControllers[0] {
                self.navigationController?.popToViewController(rootVC, animated: true)
            }
            if detailViewController.isSuccessful {
                NotificationCenter.default.post(name: Notification.Name(NotificationConstants.REFRESH_BILLS), object: nil)
            }
        }
        detailViewController.model.sections = [
            self.model.sections[0],
        ]
        ConnectionFactory.registerBillPayment(
            model: self.registerModel,
            success: { response in
                let resultMessage = response.getMessage()
                detailViewController.transactionMessage = resultMessage
                loaderViewController.setResult(isSuccessful: true, message: resultMessage, completion: {
                    detailViewController.isSuccessful = true
                    self.navigationController?.pushViewController(detailViewController, animated: false)
                })
            },
            failed: { reason in
                let resultMessage = reason?.MESSAGE_DESC ?? ""
                detailViewController.transactionMessage = resultMessage
                loaderViewController.setResult(isSuccessful: false, message: resultMessage, completion: {
                    detailViewController.isSuccessful = false
                    self.navigationController?.pushViewController(detailViewController, animated: false)
                })
            }
        )
    }
}
