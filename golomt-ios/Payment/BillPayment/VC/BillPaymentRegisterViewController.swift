//
//  BillPaymentRegisterViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/28/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup

class BillPaymentRegisterViewController: TableViewController {
    var organization: BillPaymentOrganizationResponse.Organization?
    let registerModel = BillPaymentRegisterModel()

    lazy var dateField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultPickerButtonCell else {
            return DefaultPickerButtonCell()
        }
        cell.title = "Сар бүрийн төлбөр төлөлт хийдэг өдөр".localized()
        cell.onPickerClick = {
            let controller = PickerController()
            controller.title = "Төлөлт хийдэг өдөр сонгоно уу?".localized()
            controller.list = Array(stride(from: 1, to: 29, by: 1))
            controller.onItemSelect = { number in
                controller.popupController?.dismiss()
                cell.selectedItem = number
            }
            controller.selectedItem = self.dateField.selectedItem
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()

    lazy var nicknameField: DefaultTextFieldInfoCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldInfoCell else {
            return DefaultTextFieldInfoCell()
        }
        cell.title = "Төлбөрийн товч нэр".localized()
        cell.hideLine()
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Continue)
        title = "Бүртгэх".localized()

        tableView.registerCell(nibName: "DefaultTextFieldCell")
        let section = TableViewModel.Section(rows: [])
        model.sections = [section]
        organization?.PAYEE_FORMAT_INFO.orEmpty.components(separatedBy: ":t").forEach { form in
            let separated = form.components(separatedBy: "|")
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultTextFieldCell") as? DefaultTextFieldInfoCell else {
                return
            }
            cell.title = separated.count > 0 ? separated[0] : ""
            cell.textLength = separated.count > 1 ? separated[2].toInt + 1 : 255
            cell.lengthValidation = .Less
            cell.onStateChange = {
                self.tableView.performBatchUpdates(
                    {
                        cell.layoutSubviews()
                    },
                    completion: nil
                )
            }
            if separated.count > 3 {
                switch separated[3] {
                case "NUMERIC".lowercased():
                    cell.keyboardType = .numberPad
                default:
                    cell.keyboardType = .default
                }
            }
            section.rows.append(TableViewModel.Row(cell: cell))
        }
        model.sections.append(
            TableViewModel.Section(
                rows: [
                    TableViewModel.Row(cell: dateField),
                    TableViewModel.Row(cell: nicknameField)
                ]
            )
        )
        tableView.reloadData()
        onContinue = {
            self.handleContinue()
        }
    }

    private func handleContinue() {
        var hasError = false
        let dateError = dateField.hasError()
        let nicknameError = nicknameField.hasError()
        model.sections[0].rows.forEach { row in
            let cell = row.cell as? DefaultTextFieldInfoCell
            hasError = cell?.hasError() ?? false
        }
        if hasError || dateError || nicknameError {
            return
        }
        var consumerCode = ""
        let confirmVC = BillPaymentRegisterConfirmViewController(nibName: "TableViewController", bundle: nil)
        let section = TableViewModel.Section(rows: [])
        confirmVC.model.sections.append(
            section
        )
        model.sections[0].rows.forEach { row in
            let cell = row.cell as? DefaultTextFieldInfoCell
            let inputText = cell?.inputText
            consumerCode += inputText.orEmpty + "|"
            section.rows.append(
                TableViewModel.Row(
                    title: cell?.title,
                    info: cell?.inputText
                )
            )
        }
        let selectedDay = dateField.selectedItem as? Int
        guard let day = selectedDay else {
            return
        }
        registerModel.consumerCode = consumerCode
        registerModel.subscriptionDay = String(format: "%02d", day)
        registerModel.organization = organization
        registerModel.nickname = nicknameField.inputText
        section.rows.append(
            TableViewModel.Row(
                title: dateField.title,
                info: dateField.label
            )
        )
        section.rows.append(
            TableViewModel.Row(
                title: nicknameField.title,
                info: nicknameField.inputText
            )
        )
        confirmVC.registerModel = registerModel
        self.navigationController?.pushViewController(confirmVC, animated: true)
    }
}
