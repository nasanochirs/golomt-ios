//
//  BillPaymentViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/30/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import FSCalendar
import UIKit

class BillPaymentViewController: BaseUIViewController {
    @IBOutlet var calendar: FSCalendar!
    @IBOutlet var separator: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var calendarHeightConstraint: NSLayoutConstraint!
    
    var selectedDayPaymentList = [PaymentResponse.Payment]()
    var otherPaymentList = [PaymentResponse.Payment]()
    var organizationList = [BillPaymentOrganizationResponse.Organization]()
    var selectedDay = Date()
    var selectedPayment: PaymentResponse.Payment?
    var selectedAccount: AccountResponse.Account?
    var shortcut: LoginResponse.Shortcut?
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendar, action: #selector(self.calendar.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
    }()
    
    lazy var registerButton: UIBarButtonItem? = {
        UIBarButtonItem(title: "bill_payment_register_title".localized(), style: .done, target: self, action: #selector(handleRegister))
    }()
    
    lazy var emptyField: BillPaymentEmptyCell = {
        let nib = Bundle.main.loadNibNamed("BillPaymentEmptyCell", owner: self, options: nil)
        guard let cell = nib?.first as? BillPaymentEmptyCell else {
            return BillPaymentEmptyCell()
        }
        cell.labelText = "bill_payment_empty_label".localized()
        return cell
    }()
    
    lazy var helpButton: UIBarButtonItem? = {
        UIBarButtonItem(image: UIImage(named: "help"), style: .plain, target: self, action: nil)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "bill_payment_title".localized()
        
        self.view.addGestureRecognizer(self.scopeGesture)
        
        self.tableView.panGestureRecognizer.require(toFail: self.scopeGesture)
        self.tableView.registerCell(nibName: "BillPaymentCell")
        self.tableView.register(UINib(nibName: "BaseHeaderCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "BaseHeaderCell")
        self.tableView.tableFooterView = UIView()
        
        self.separator.backgroundColor = .defaultSeparator
        
        let dateComponents = Calendar.current.dateComponents(in: .current, from: Date())
        self.setSelectedDay(dateComponents.day ?? 0)
        
        self.setupCalendar()
        
        self.fetchOrganizationList()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleRefreshNotification(notification:)), name: Notification.Name(NotificationConstants.BILLS_REFRESHED), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationItem.rightBarButtonItem = self.registerButton
    }
    
    @objc private func handleRefreshNotification(notification: Notification) {
        let dateComponents = Calendar.current.dateComponents(in: .current, from: self.selectedDay)
        self.setSelectedDay(dateComponents.day ?? 0)
    }
    
    private func fetchOrganizationList() {
        self.showLoader()
        ConnectionFactory.fetchBillingOrganizationList(
            success: { response in
                self.hideLoader()
                self.organizationList = response.organizationList ?? []
                if self.selectedPayment != nil {
                    self.handlePaymentSelect(self.selectedPayment!)
                }
                if self.shortcut != nil {
                    self.selectedAccount = operativeAccountList.filter { $0.ACCT_NUMBER == self.shortcut!.ACCT }.first
                    self.selectedPayment = billPaymentList.filter { $0.SUBSCRIPTION_IDS == self.shortcut!.ENTITYID }.first
                    guard let selectedPayment = self.selectedPayment else {
                        return
                    }
                    self.handlePaymentSelect(selectedPayment, amount: self.shortcut!.TRANAMOUNT)
                }
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
    
    private func setupCalendar() {
        self.calendar.scope = .month
        self.calendar.backgroundColor = .defaultPrimaryBackground
        self.calendar.appearance.headerTitleColor = .defaultBlueGradientEnd
        self.calendar.appearance.weekdayTextColor = .defaultBlueGradientEnd
        self.calendar.appearance.headerDateFormat = "yyyy, MMM"
        self.calendar.appearance.titlePlaceholderColor = .gray
        self.calendar.appearance.titleDefaultColor = .defaultPrimaryText
        self.calendar.appearance.eventDefaultColor = .defaultRed
//        calendar.today = nil
        self.calendar.select(Date())
        self.calendar.locale = Locale(identifier: getLanguage())
        self.calendar.firstWeekday = 2
        self.calendar.placeholderType = .fillHeadTail
    }
    
    private func setSelectedDay(_ day: Int) {
        self.selectedDayPaymentList = billPaymentList.filter {
            $0.getPaymentDate() == day
        }
        self.otherPaymentList = billPaymentList.filter {
            $0.getPaymentDate() != day
        }
        self.tableView.reloadData()
        self.calendar.reloadData()
    }
    
    @objc private func handleRegister() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "BillPayment", bundle: nil)
        guard let organizationVC = storyBoard.instantiateViewController(withIdentifier: "BillPaymentOrganizationID") as? BillPaymentOrganizationViewController else {
            return
        }
        organizationVC.organizationList = self.organizationList
        self.navigationController?.pushViewController(organizationVC, animated: true)
    }
    
    private func handlePaymentSelect(_ payment: PaymentResponse.Payment, amount: String? = nil) {
        let payVC = BillPaymentPayViewController(nibName: "TableViewController", bundle: nil)
        let filterOrganizationList = organizationList.filter { $0.BILLER_ID_INFO == payment.BNF_ID }
        payVC.payment = payment
        payVC.organization = filterOrganizationList.first
        payVC.selectedAccount = self.selectedAccount
        payVC.amount = amount.orEmpty
        self.navigationController?.pushViewController(payVC, animated: true)
    }
}

extension BillPaymentViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "BaseHeaderCell") as? BaseHeaderCell
        switch section {
        case 0:
            cell?.setupLabel(self.selectedDay.toFormatted)
        case 1:
            cell?.setupLabel("bill_payment_others_header".localized())
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            switch self.selectedDayPaymentList.count {
            case 0:
                return 1
            default:
                return self.selectedDayPaymentList.count
            }
        case 1:
            return self.otherPaymentList.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BillPaymentCell") as? BillPaymentCell else {
            return UITableViewCell()
        }
        switch indexPath.section {
        case 0:
            switch self.selectedDayPaymentList.count {
            case 0:
                return self.emptyField
            default:
                let payment = self.selectedDayPaymentList[indexPath.row]
                cell.setPayment(payment)
            }
        case 1:
            let payment = self.otherPaymentList[indexPath.row]
            cell.setPayment(payment)
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        var payment: PaymentResponse.Payment?
        switch indexPath.section {
        case 0:
            payment = self.selectedDayPaymentList[indexPath.row]
        case 1:
            payment = self.otherPaymentList[indexPath.row]
        default:
            payment = nil
        }
        if let payment = payment {
            self.handlePaymentSelect(payment)
        }
    }
}

extension BillPaymentViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let shouldBegin = self.tableView.contentOffset.y <= -self.tableView.contentInset.top
        if shouldBegin {
            let velocity = self.scopeGesture.velocity(in: self.view)
            switch self.calendar.scope {
            case .month:
                return velocity.y < 0
            case .week:
                return velocity.y > 0
            default:
                break
            }
        }
        return shouldBegin
    }
}

extension BillPaymentViewController: FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarHeightConstraint.constant = bounds.height
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
        let dateComponents = Calendar.current.dateComponents(in: .current, from: date)
        self.setSelectedDay(dateComponents.day ?? 0)
        self.selectedDay = date
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        let dateComponents = Calendar.current.dateComponents(in: .current, from: date)
        let paymentList = billPaymentList.filter {
            $0.getPaymentDate() == dateComponents.day
        }
        return paymentList.count
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {}
}
