//
//  BillPaymentPayViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/30/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class BillPaymentPayViewController: TableViewController {
    var organization: BillPaymentOrganizationResponse.Organization?
    var payment: PaymentResponse.Payment?
    var productList = [DataListResponse.Item]()
    var amount: String = ""
    let payModel = BillPaymentPayModel()

    lazy var amountField: DefaultAmountFieldCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAmountFieldCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultAmountFieldCell else {
            return DefaultAmountFieldCell()
        }
        cell.title = "Төлөх дүн".localized()
        cell.selectedCurrency = CurrencyConstants.MNT
        cell.inputText = self.amount
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()

    lazy var invoiceField: BillPaymentInvoiceCell = {
        let nib = Bundle.main.loadNibNamed("BillPaymentInvoiceCell", owner: self, options: nil)
        guard let cell = nib?.first as? BillPaymentInvoiceCell else {
            return BillPaymentInvoiceCell()
        }
        cell.onInvoiceAction = {
            self.handleInvoice()
        }
        return cell
    }()

    lazy var productPickerField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultPickerButtonCell else {
            return DefaultPickerButtonCell()
        }
        cell.title = "Бүтээгдэхүүний төрөл".localized()
        cell.onPickerClick = {
            let controller = PickerController()
            controller.title = "Бүтээгдэхүүн сонгоно уу?".localized()
            controller.list = self.productList
            controller.onItemSelect = { product in
                controller.popupController?.dismiss()
                cell.selectedItem = product
                guard let payment = self.payment else {
                    return
                }
                guard let item = product as? DataListResponse.Item else {
                    return
                }
                self.showLoader()
                ConnectionFactory.fetchParameterValue(
                    parameterName: payment.BILLER_CARD_TYPE.orEmpty + "_CARD_" + item.CM_CODE.orEmpty,
                    success: { response in
                        self.hideLoader()
                        let amount = response.parameter?.P_VAL
                        self.amountField.inputText = amount.orEmpty.toAmount.formattedWithComma
                    },
                    failed: { reason in
                        self.hideLoader()
                        self.handleRequestFailure(reason)
                    }
                )
            }
            controller.selectedItem = cell.selectedItem
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()

    lazy var remitterAccountField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultPickerButtonCell else {
            return DefaultPickerButtonCell()
        }
        cell.title = "Төлбөр төлөх данс".localized()
        cell.onPickerClick = {
            let controller = AccountPickerController()
            controller.selectedAccount = self.selectedAccount
            controller.onAccountSelect = { account in
                self.remitterAccountField.selectedItem = account
                self.selectedAccount = account
            }
            controller.title = "Төлбөр төлөх данс".localized()
            controller.accounts = operativeAccountList
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        cell.hideLine()
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Continue)
        title = payment?.BILLERS_NICK_NAME

        if selectedAccount != nil {
            self.remitterAccountField.selectedItem = selectedAccount
        }
        
        tableView.registerCell(nibName: "DefaultTextFieldCell")
        let firstSection = TableViewModel.Section(title: "Үйлчилгээний мэдээлэл", rows: [])
        model.sections = [firstSection]
        guard let organization = organization else {
            return
        }
        guard let payment = payment else {
            return
        }
        for (index, element) in organization.PAYEE_FORMAT_INFO.orEmpty.components(separatedBy: ":t").enumerated() {
            let separated = element.components(separatedBy: "|")
            let title = separated.count > 0 ? separated[0] : ""
            let paymentSeparated = payment.CONSUMER_CODE_DET.orEmpty.components(separatedBy: "|")
            let info = paymentSeparated.count > index ? paymentSeparated[index] : ""
            firstSection.rows.append(TableViewModel.Row(title: title, info: info))
        }
        firstSection.rows.append(TableViewModel.Row(title: "Байгууллагын нэр", info: organization.BILLER_NAME_INFO))
        let secondSection = TableViewModel.Section(title: "Төлбөрийн мэдээлэл", rows: [])
        model.sections.append(secondSection)
        switch payment.PART_PYMT_FLG.orEmpty {
        case "Y":
            secondSection.rows.append(
                TableViewModel.Row(cell: self.productPickerField)
            )
            self.amountField.canEdit = false
            self.showLoader()
            ConnectionFactory.fetchList(
                code: payment.BILLER_CARD_TYPE.orEmpty,
                success: { response in
                    self.hideLoader()
                    self.productList = response.list ?? []
                },
                failed: { reason in
                    self.hideLoader()
                    self.handleRequestFailure(reason)
                }
            )
        default:
            break
        }
        secondSection.rows.append(
            TableViewModel.Row(cell: self.amountField)
        )
        switch payment.LATE_PYMT_FLG.orEmpty {
        case "Y":
            secondSection.rows.append(
                TableViewModel.Row(cell: self.invoiceField)
            )
        default:
            break
        }
        secondSection.rows.append(
            TableViewModel.Row(cell: self.remitterAccountField)
        )
        tableView.reloadData()
        onContinue = {
            self.handleContinue()
        }
    }

    private func handleContinue() {
        let amountError = self.amountField.hasError()
        let accountError = self.remitterAccountField.hasError()
        if amountError || accountError {
            return
        }
        payModel.account = self.selectedAccount
        payModel.amount = self.amountField.inputText.toDouble
        payModel.payment = payment
        payModel.organization = organization
        let payVC = BillPaymentPayConfirmViewController(nibName: "TableViewController", bundle: nil)
        payVC.model.sections = [model.sections[0]]
        payVC.payModel = payModel
        self.navigationController?.pushViewController(payVC, animated: true)
    }

    private func handleInvoice() {
        guard let payment = self.payment else {
            return
        }
        self.showLoader()
        ConnectionFactory.fetchBillPaymentInvoice(
            payment: payment,
            success: { invoice in
                self.hideLoader()
                switch payment.OTP_FLG {
                case "N":
                    self.handleOTP(invoice: invoice)
                default:
                    let amount = invoice.response?.AMOUNT?.toAmount
                    self.amountField.inputText = amount.orZero.formattedWithComma
                }
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }

    private func handleOTP(invoice: BillPaymentInvoiceResponse?) {
        guard let payment = self.payment else {
            return
        }
        guard let invoice = invoice else {
            return
        }
        let confirmationDialog = UIAlertController(title: "Баталгаажуулах код оруулна уу".localized(), message: nil, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Болих".localized(), style: .cancel, handler: nil)
        let confirmAction = UIAlertAction(
            title: "Үргэлжлүүлэх".localized(),
            style: .default,
            handler: { _ in
                let textInput = confirmationDialog.textFields?.first?.text
                if invoice.response?.OTP == textInput {
                    self.showLoader()
                    ConnectionFactory.editBillPayment(
                        payment: payment,
                        success: { _ in
                            self.hideLoader()
                            let amount = invoice.response?.AMOUNT?.toAmount
                            self.amountField.inputText = amount.orZero.formattedWithComma
                        },
                        failed: { reason in
                            self.hideLoader()
                            self.handleRequestFailure(reason)
                        }
                    )
                } else {
                    let dialog = errorDialog(message: "OTP буруу оруулсан тул дахин оруулна уу")
                    self.present(dialog, animated: true)
                }
            }
        )
        confirmationDialog.addAction(cancelAction)
        confirmationDialog.addAction(confirmAction)
        confirmationDialog.addTextField { textField in
            textField.keyboardType = .numberPad
            textField.textContentType = .oneTimeCode
        }
        self.present(confirmationDialog, animated: true)
    }
}
