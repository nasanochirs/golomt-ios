//
//  BillPaymentEmptyCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/10/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class BillPaymentEmptyCell: UITableViewCell {
    @IBOutlet var emptyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        emptyLabel.useSmallFont()
        emptyLabel.textColor = .defaultSecondaryText
        emptyLabel.wordWrap()
        selectionStyle = .none
    }
    
    var labelText: String = "" {
        didSet {
            emptyLabel.text = labelText.localized()
        }
    }}
