//
//  BillPaymentInvoiceCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/31/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class BillPaymentInvoiceCell: UITableViewCell {
    @IBOutlet var checkLineView: UIView!
    @IBOutlet var invoiceButton: UIButton!
    @IBAction func invoiceAction(_ sender: Any) {
        onInvoiceAction?()
    }
    
    var onInvoiceAction: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        checkLineView.backgroundColor = .defaultSeparator
        invoiceButton.backgroundColor = .defaultTertiaryBackground
        invoiceButton.corner(cornerRadius: 10)
        invoiceButton.setTitleColor(.defaultSecondaryText, for: .normal)
        invoiceButton.setTitle("Төлбөрийн үлдэгдэл харах", for: .normal)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
