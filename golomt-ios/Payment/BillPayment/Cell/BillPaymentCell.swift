//
//  BillPaymentCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/30/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class BillPaymentCell: UITableViewCell {
    @IBOutlet var billTypeImage: UIImageView!
    @IBOutlet var billDateLabel: UILabel!
    @IBOutlet var billCodeLabel: UILabel!
    @IBOutlet var billNicknameLabel: UILabel!
    @IBOutlet var billOrganizationLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }

    func setPayment(_ payment: PaymentResponse.Payment) {
        billNicknameLabel.text = payment.BILLERS_NICK_NAME
        billCodeLabel.text = String(payment.CONSUMER_CODE_DET.orEmpty.replacingOccurrences(of: "|", with: ", ").dropLast(2))
        billOrganizationLabel.text = payment.BILLERS_NAME
        let calendar = Calendar.current
        var dateComponents = calendar.dateComponents(in: .current, from: Date())
        dateComponents.setValue(payment.getPaymentDate(), for: .day)
        billDateLabel.text = dateComponents.date?.toDateMonthFormatted
    }

    private func initComponent() {
        billDateLabel.useMediumFont()
        billDateLabel.textColor = .defaultSecondaryText
        billNicknameLabel.useLargeFont()
        billNicknameLabel.makeBold()
        billNicknameLabel.textColor = .defaultPrimaryText
        billOrganizationLabel.useSmallFont()
        billOrganizationLabel.textColor = .defaultSecondaryText
        billCodeLabel.useSmallFont()
        billCodeLabel.textColor = .defaultPrimaryText
    }
}
