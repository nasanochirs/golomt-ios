//
//  BillPaymentOrganizationCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/28/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class BillPaymentOrganizationCell: UICollectionViewCell {
    @IBOutlet var organizationImageView: UIImageView!
    @IBOutlet var organizationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = .defaultSecondaryBackground
        contentView.corner(cornerRadius: 15)
    }

    override var isSelected: Bool {
        didSet {
            if isSelected {
                contentView.backgroundColor = .defaultSelectionBackground
            } else {
                contentView.backgroundColor = .defaultSecondaryBackground
            }
        }
    }
    
    func setMenu(data: (key: String, value: [BillPaymentOrganizationResponse.Organization])) {
        guard let organization = data.value.first else {
            return
        }
        organizationLabel.text = organization.BUSINESS_CATEGORY_DESC.orEmpty
        organizationImageView.image = organization.getOrganizationImage()?.withColor(.defaultPrimaryText)
    }
}
