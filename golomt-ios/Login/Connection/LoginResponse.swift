//
//  LoginResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/11/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LoginResponse: BaseResponse {
    let infoDetails: InfoDetails?
    let userDetails: UserDetails?
    let shortcutList: [Shortcut]?
    let product: Product?
    let forcePasswordChange: ForceChangePassword?
    
    private enum CodingKeys: String, CodingKey {
        case InfoDetails
        case UserDetails
        case ShortcutList_REC
        case ProductOfferList
        case ForceChangePwd
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(infoDetails, forKey: .InfoDetails)
        try container.encode(userDetails, forKey: .UserDetails)
        try container.encode(shortcutList, forKey: .ShortcutList_REC)
        try container.encode(product, forKey: .ProductOfferList)
        try container.encode(forcePasswordChange, forKey: .ForceChangePwd)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.infoDetails = try container.decodeIfPresent(InfoDetails.self, forKey: .InfoDetails)
        self.userDetails = try container.decodeIfPresent(UserDetails.self, forKey: .UserDetails)
        self.shortcutList = try container.decodeIfPresent([Shortcut].self, forKey: .ShortcutList_REC)
        self.product = try container.decodeIfPresent(Product.self, forKey: .ProductOfferList)
        self.forcePasswordChange = try container.decodeIfPresent(ForceChangePassword.self, forKey: .ForceChangePwd)
        try super.init(from: decoder)
    }
    
    struct InfoDetails: Codable {
        let DCHALL_APPROVE: String?
        let DATA_APPROVE: String?
        let PHONE_RENEW_FLG: String?
        let ADDRESS_ID: String?
        let CITY_CODE: String?
        let HOUSE_NO: String?
        let STREET_NO: String?
        let STATE_CODE: String?
        let HOME_EMAIL_ID: String?
        let CELLPH_PHONE_ID: String?
        let STREET_NAME: String?
        let BUILDING_LEVEL: String?
        let ADDRES_RENEW_FLG: String?
        let EMAIL_RENEW_FLG: String?
        let EMAIL: String?
        let PHONE_NUMBER_INFO: String?
    }
    
    struct UserDetails: Codable {
        let USER_ID: String?
        let USER_TYPE: String?
        let DATE_FORMAT: String?
        let CORPORATE_ID: String?
        let RECORD_USER_ID: String?
        let APPLICATION_ID: String?
        let LANG_COUNTRY_CODE: String?
        let LANG_CODE: String?
        let CHANNEL_ID: String?
        let MENU_PROFILEID: String?
        let LOG_SRL_NO: String?
        let SIGNON_STATUS: String?
        let CUSTOMER_ID: String?
        let INDIVIDUAL_ID: String?
        let LANGUAGE_ID: String?
        let USER_LAST_NAME: String?
        let LASTLOGINTIME: String?
        let USER_FIRST_NAME: String?
        let CURRENTLOGINTIME: String?
        let USERTXNTYPES: String?
        let CUSTOMER_NAME: String?
        let AMOUNTFORMATCODE: String?
        let BANK_ID: String?
        let TOT_NUM_LOGIN: String?
        let SEC_AUTH_MODE: String?
        let TXN_ENABLEDFLAG: String?
        let IS_TOKEN: String?
        let PRIMARY_SOLID: String?
        let SHORTNAME: String?
        let FULLNAME: String?
        let FULLNAME_EN: String?
        let USER_EMAIL: String?
        let MOBILE_NUMBER: String?
        let REGISTER_NUM: String?
    }
    
    struct Shortcut: Codable {
        let ENTITYTYPE: String?
        let ENTITYID: String?
        let TRANCRN: String?
        let ENTITYNICKNAME: String?
        let ENTITYROUTE: String?
        let ACCT: String?
        let TRANAMOUNT: String?
        let TRANDESC: String?
        let TRANTYPE: String?
    }
    
    struct Product: Codable {
        let SCHM_CODE: String?
        let SCHM_TYPE: String?
        let IS_POPUP: String?
    }
    
    struct ForceChangePassword: Codable {
        let FORCE_PWD_CHANGE_FLAG: String?
        let FORCE_TXN_PASSWORD_CHANGE_FLG: String?
    }
}
