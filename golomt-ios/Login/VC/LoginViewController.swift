//
//  LoginViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/14/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import IQKeyboardManagerSwift
import SkyFloatingLabelTextField
import UIKit

extension LoginViewController: UITextFieldDelegate {
    static let USERNAME_TAG = 100
    static let PASSWORD_TAG = 101
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch true {
        case textField == usernameTextField:
            passwordTextField.becomeFirstResponder()
        case textField == passwordTextField:
            passwordTextField.resignFirstResponder()
            handleLogin()
        default:
            break
        }
        return false
    }
}

class LoginViewController: BaseUIViewController {
    @IBOutlet var usernameTextField: SkyFloatingLabelTextField!
    @IBOutlet var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet var useBiometricCheckBox: DefaultCheckBox!
    @IBOutlet var passwordLoginStackView: UIStackView!
    @IBOutlet var savedUsernameLabel: UILabel!
    @IBOutlet var profileStackView: UIStackView!
    @IBOutlet var biometricLoginStackView: UIStackView!
    @IBOutlet var logoImage: UIImageView!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var headerStackView: UIStackView!
    @IBOutlet var loginCenterY: NSLayoutConstraint!
    @IBOutlet var rateView: UIView!
    @IBOutlet var locationView: UIView!
    
    @IBOutlet var calculatorView: UIView!
    @IBOutlet var localeButton: UIButton!
    @IBAction func localeAction(_ sender: Any) {
        handleLocale()
    }
    
    @IBOutlet var loginButton: DefaultGradientButton!
    @IBAction func loginAction(_ sender: Any) {
        handleLogin()
    }
    
    @IBOutlet var biometricLoginButton: DefaultGradientButton!
    @IBAction func biometricLoginAction(_ sender: Any) {
        handleBiometricLogin()
    }
    
    @IBOutlet var notMeButton: UIButton!
    @IBAction func notMeAction(_ sender: Any) {
        handleNotMe()
    }
    
    @IBOutlet var useBiometricButton: UIButton!
    @IBAction func useBiometricAction(_ sender: Any) {
        handleUseBiometric()
    }
    
    @IBOutlet var usePasswordButton: UIButton!
    @IBAction func usePasswordAction(_ sender: Any) {
        handleUsePassword()
    }
    
    lazy var loginButtonPoint: CGPoint = {
        let loginButtonPoint = loginButton.convert(loginButton.bounds.origin, to: view)
        return loginButtonPoint
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDefaultBackgroundImage()
        
        savedUsernameLabel.useXLargeFont()
        savedUsernameLabel.textColor = .white
        
        setLogoImage()
        setupTextField()
        setupButtons()
        setupCheckBox()
        setupProfile()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleSessionDestroy),
                                               name: NSNotification.Name(NotificationConstants.SESSION_DESTROYED),
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleSessionExpire),
                                               name: NSNotification.Name(NotificationConstants.SESSION_EXPIRED),
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleLogout),
                                               name: NSNotification.Name(NotificationConstants.LOGOUT),
                                               object: nil)
        
        usernameTextField.delegate = self
        passwordTextField.delegate = self
        passwordTextField.returnKeyType = .go
        
        rateView.addTapGesture(tapNumber: 1, target: self, action: #selector(handleRateClick))
        locationView.addTapGesture(tapNumber: 1, target: self, action: #selector(handleLocationClick))
        calculatorView.addTapGesture(tapNumber: 1, target: self, action: #selector(handleCalculatorClick))

        usernameTextField.text = "nasaa456"
        passwordTextField.text = "Golomt123!"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func handleRateClick() {
        let storyboard = UIStoryboard(name: "Rate", bundle: nil)
        let viewController = storyboard.instantiateInitialViewController()
        guard let VC = viewController else {
            return
        }
        let navigationViewController = BaseNavigationViewController(rootViewController: VC)
        present(navigationViewController, animated: true)
    }
    
    @objc func handleLocationClick() {
        let storyboard = UIStoryboard(name: "Location", bundle: nil)
        let viewController = storyboard.instantiateInitialViewController()
        guard let VC = viewController else {
            return
        }
        let navigationViewController = BaseNavigationViewController(rootViewController: VC)
        present(navigationViewController, animated: true)
    }
    
    @objc func handleCalculatorClick() {
        let VC = CalculatorViewController(nibName: "TableViewController", bundle: nil)
        let navigationViewController = BaseNavigationViewController(rootViewController: VC)
        present(navigationViewController, animated: true)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            UIView.animate(withDuration: 1, animations: {
                self.headerStackView.alpha = 0
                self.loginCenterY.constant = (self.loginButtonPoint.y + self.loginButton.bounds.size.height + 20 - keyboardRectangle.origin.y) * -1
            }, completion: { _ in
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        UIView.animate(withDuration: 1, animations: {
            self.headerStackView.alpha = 1
            self.loginCenterY.constant = 0
        }, completion: { _ in
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        })
    }
    
    @objc private func handleSessionDestroy() {
        resetAll()
        let error = errorDialog(message: "login_session_destroyed_label")
        present(error, animated: true)
    }
    
    @objc private func handleSessionExpire() {
        resetAll()
        let error = errorDialog(message: "notification_session_expired_label")
        present(error, animated: true)
    }
    
    @objc private func handleLogout() {
        resetAll()
    }
    
    private func handleNotMe() {
        deletePreferences()
        useBiometricCheckBox.setCheckState(checked: false)
        profileStackView.isHidden = true
        usernameTextField.isHidden = false
        usernameTextField.text = ""
        useBiometricButton.isHidden = true
        passwordLoginStackView.isHidden = false
        biometricLoginStackView.isHidden = true
    }
    
    private func deletePreferences() {
        setUsername("")
        setLoginName("")
        setPublicKey(nil)
        setProfileImage(nil)
    }
    
    private func handleUsePassword() {
        useBiometricCheckBox.setCheckState(checked: false)
        biometricLoginStackView.isHidden = true
        passwordLoginStackView.isHidden = false
        useBiometricButton.isHidden = false
    }
    
    private func handleUseBiometric() {
        useBiometricCheckBox.setCheckState(checked: true)
        biometricLoginStackView.isHidden = false
        passwordLoginStackView.isHidden = true
    }
    
    private func setLogoImage() {
        switch getLanguage() {
        case "mn":
            logoImage.image = UIImage(named: "image_splash_mongolian")?.withRenderingMode(.alwaysOriginal)
        default:
            logoImage.image = UIImage(named: "image_splash_english")?.withRenderingMode(.alwaysOriginal)
        }
    }
    
    private func setupTextField() {
        usernameTextField.title = "login_name".localized()
        passwordTextField.title = "password".localized()
        usernameTextField.placeholder = "login_name".localized()
        passwordTextField.placeholder = "password".localized()
        usernameTextField.titleColor = .white
        usernameTextField.selectedTitleColor = .white
        usernameTextField.lineColor = .white
        usernameTextField.selectedLineColor = .white
        usernameTextField.placeholderColor = .white
        passwordTextField.titleColor = .white
        passwordTextField.selectedTitleColor = .white
        passwordTextField.lineColor = .white
        passwordTextField.selectedLineColor = .white
        passwordTextField.placeholderColor = .white
        usernameTextField.textColor = .white
        passwordTextField.textColor = .white
        usernameTextField.titleFormatter = { $0 }
        passwordTextField.titleFormatter = { $0 }
        usernameTextField.useMediumFont()
        usernameTextField.makeBold()
        passwordTextField.useMediumFont()
        passwordTextField.makeBold()
        passwordTextField.isSecureTextEntry = true
    }
    
    private func setupButtons() {
        notMeButton.setTitle("not_you".localized(), for: .normal)
        notMeButton.setTitleColor(.defaultPurpleGradientEnd, for: .normal)
        usePasswordButton.setTitle("use_password".localized(), for: .normal)
        usePasswordButton.setTitleColor(.defaultPurpleGradientEnd, for: .normal)
        profileStackView.setCustomSpacing(0, after: profileStackView.subviews[1])
        useBiometricButton.setTitleColor(.defaultPurpleGradientEnd, for: .normal)
        loginButton.setTitle("login".localized(), for: .normal)
        biometricLoginButton.setTitle("login".localized(), for: .normal)
        localeButton.setTitle("locale".localized(), for: .normal)
        let authManager = LocalAuthManager.shared
        switch authManager.biometricType {
        case .none: return
        case .touchID:
            useBiometricButton.setTitle("use_biometric_touchID".localized(), for: .normal)
            biometricLoginButton.setRightImage(name: "touchID", tint: .white)
        case .faceID:
            useBiometricButton.setTitle("use_biometric_faceID".localized(), for: .normal)
            biometricLoginButton.setRightImage(name: "faceID", tint: .white)
        }
    }
    
    private func setupCheckBox() {
        let authManager = LocalAuthManager.shared
        switch authManager.biometricType {
        case .none:
            useBiometricCheckBox.isHidden = true
        case .touchID:
            useBiometricCheckBox.labelText = "login_with_touchID"
        case .faceID:
            useBiometricCheckBox.labelText = "login_with_faceID"
        }
    }
    
    private func setupProfile() {
        if !isBiometricLogin() {
            return
        }
        
        usernameTextField.text = getLoginName().orEmpty
        usernameTextField.isHidden = true
        profileStackView.isHidden = false
        passwordLoginStackView.isHidden = true
        biometricLoginStackView.isHidden = false
        savedUsernameLabel.text = getUsername().orEmpty
        savedUsernameLabel.textColor = .white
        
        profileImage.round(borderColor: .white)
        
        guard let pImage = getProfileImage() else {
            return
        }
        profileImage.image = UIImage(data: pImage)
    }
    
    private func isBiometricLogin() -> Bool {
        if hasBiometric() == false {
            return false
        }
        guard let _ = getLoginName() else {
            return false
        }
        guard let _ = getUsername() else {
            return false
        }
        return true
    }
    
    private func handleLogin() {
        if usernameTextField.text.orEmpty.isEmpty {
            let dialog = errorDialog(message: "login_name_not_empty".localized())
            present(dialog, animated: true)
            return
        }
        if passwordTextField.text.orEmpty.isEmpty {
            let dialog = errorDialog(message: "password_not_empty".localized())
            present(dialog, animated: true)
            return
        }
        showLoader()
        ConnectionFactory.login(
            username: usernameTextField.text.orEmpty,
            password: passwordTextField.text.orEmpty,
            success: { response in
                self.hideLoader()
                if response.userDetails?.SEC_AUTH_MODE == LoginConstants.etoken {
                    self.askEToken()
                } else {
                    self.finishLogin(response)
                }
            },
            failed: { reason in
                self.handleRequestFailure(reason)
            }
        )
    }
    
    private func finishLogin(_ response: LoginResponse) {
        setSession(response: response)
        setupBiometric(username: response.userDetails?.USER_FIRST_NAME ?? "")
    }
    
    private func finishBiometricLogin(_ response: LoginResponse) {
        setSession(response: response)
        navigateToMain(with: nil)
    }
    
    private func askEToken() {
        usernameTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        let confirmationDialog = UIAlertController(title: "login_etoken_dialog_title".localized(), message: "login_etoken_dialog_message".localized(), preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "login_etoken_dialog_negative_label".localized(), style: .cancel, handler: nil)
        let confirmAction = UIAlertAction(
            title: "login_etoken_dialog_positive_label".localized(),
            style: .default,
            handler: { _ in
                let eToken = confirmationDialog.textFields?.first?.text ?? ""
                self.showLoader()
                ConnectionFactory.loginWithEToken(
                    username: self.usernameTextField.text.orEmpty,
                    eToken: eToken,
                    success: { response in
                        self.hideLoader()
                        self.finishLogin(response)
                    },
                    failed: { reason in
                        self.hideLoader()
                        self.handleRequestFailure(reason)
                    }
                )
            }
        )
        confirmationDialog.addAction(cancelAction)
        confirmationDialog.addAction(confirmAction)
        confirmationDialog.addTextField { textField in
            textField.keyboardType = .numberPad
            textField.delegate = self
        }
        present(confirmationDialog, animated: true)
    }
    
    private func setupBiometric(username: String) {
        switch useBiometricCheckBox.isChecked() {
        case true:
            navigateToMain(with: username)
        case false:
            deletePreferences()
            navigateToMain(with: nil)
        }
    }
    
    private func handleBiometricLogin() {
        LocalAuthManager.shared.checkBiometric(reason: "login") { isSuccessful in
            switch isSuccessful {
            case true:
                self.showLoader()
                ConnectionFactory.loginWithBiometric(
                    success: { response in
                        self.hideLoader()
                        if response.userDetails?.SEC_AUTH_MODE == LoginConstants.etoken {
                            self.askEToken()
                        } else {
                            self.finishBiometricLogin(response)
                        }
                    },
                    failed: { reason in
                        self.handleRequestFailure(reason)
                        switch reason?.MESSAGE_CODE {
                        case "24032", "24033", "108891": self.handleNotMe()
                        default: return
                        }
                    }
                )
            case false: return
            }
        }
    }
    
    private func handleLocale() {
        switch getLanguage() {
        case "mn":
            Bundle.setBundleLanguage(language: "en")
        default:
            Bundle.setBundleLanguage(language: "mn")
        }
        let storyboard = UIStoryboard(name: "Splash", bundle: nil)
        UIApplication.shared.windows.first?.rootViewController = storyboard.instantiateInitialViewController()
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }

    private func navigateToMain(with username: String?) {
        let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryBoard.instantiateInitialViewController()
        guard let unwrappedVC = viewController as? MainViewController else {
            return
        }
        unwrappedVC.username = username
        unwrappedVC.loginName = usernameTextField.text
        let navigationViewController = BaseNavigationViewController(rootViewController: unwrappedVC)
        UIApplication.shared.windows.first?.rootViewController = navigationViewController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}
