//
//  FinancesViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Charts
import Foundation
import STPopup
import UIKit

class FinancesViewController: TableViewController {
    var userCategoryList = [FinancesUserCategoryResponse]()
    var monthlyList = [FinancesMonthlyTransactionResponse]()
    var totalTransactionList = [FinancesTotalTransactionResponse]()
    var transactionReport: FinancesTransactionReportResponse?
    var transactionCount = 0
    var transactionList = [FinancesTransactionResponse.Transaction]()
    var transactionSections = [TableViewModel.Section]()
    var month = 0
    var pageNumber = 0
    var isPieChart = true
    let calendar = Calendar.current
    
    lazy var helpButton: UIBarButtonItem? = {
        UIBarButtonItem(image: UIImage(named: "help"), style: .plain, target: self, action: #selector(helpClicked))
    }()
    
    lazy var monthPickerField: FinancesMonthCell = {
        let nib = Bundle.main.loadNibNamed("FinancesMonthCell", owner: self, options: nil)
        guard let cell = nib?.first as? FinancesMonthCell else {
            return FinancesMonthCell()
        }
        cell.setMonthLabel(Date().getMonthYearLabel())
        cell.onCalendarSelect = {
            let controller = ChooseMonthController()
            controller.monthIndex = self.month
            controller.onMonthSelect = { monthIndex, monthLabel, _ in
                controller.dismiss(
                    animated: true,
                    completion: {
                        cell.setMonthLabel(monthLabel)
                        self.month = monthIndex.toInt
                        self.fetchAll()
                    }
                )
            }
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        return cell
    }()
    
    lazy var financesTypeField: DefaultSegmentedGroupCell = {
        let nib = Bundle.main.loadNibNamed("DefaultHeaderSegmentedGroupCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultSegmentedGroupCell else {
            return DefaultSegmentedGroupCell()
        }
        cell.title = ""
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.segments = [DefaultSegmentedGroupCell.SegmentModel(code: "", value: "Тайлан".localized()), DefaultSegmentedGroupCell.SegmentModel(code: "", value: "Төсөв".localized()), DefaultSegmentedGroupCell.SegmentModel(code: "", value: "Гүйлгээ".localized())]
        cell.selectedSegmentIndex = 0
        cell.onSegmentChange = { segment in
            switch segment {
            case 0:
                self.model.sections = [self.typeSection, self.transactionTypeSection, self.pieChartSection, self.amountButtonsSection, self.lineChartSection]
            case 1:
                self.model.sections = [self.typeSection, self.budgetSection]
            case 2:
                switch self.transactionSections.count {
                case 0:
                    self.model.sections = [self.typeSection]
                default:
                    self.model.sections = self.transactionSections
                }
            default:
                break
            }
            self.tableView.reloadData()
        }
        return cell
    }()
    
    lazy var lineChartField: FinancesLineChartCell = {
        let nib = Bundle.main.loadNibNamed("FinancesLineChartCell", owner: self, options: nil)
        guard let cell = nib?.first as? FinancesLineChartCell else {
            return FinancesLineChartCell()
        }
        return cell
    }()
    
    lazy var pieChartField: FinancesPieChartCell = {
        let nib = Bundle.main.loadNibNamed("FinancesPieChartCell", owner: self, options: nil)
        guard let cell = nib?.first as? FinancesPieChartCell else {
            return FinancesPieChartCell()
        }
        return cell
    }()
    
    lazy var amountButtonsField: FinancesAmountButtonsCell = {
        let nib = Bundle.main.loadNibNamed("FinancesAmountButtonsCell", owner: self, options: nil)
        guard let cell = nib?.first as? FinancesAmountButtonsCell else {
            return FinancesAmountButtonsCell()
        }
        cell.setButtonTitles("Дундаж зарлага", "Зарцуулалт ихтэй")
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var transactionTypeField: DefaultSegmentedGroupCell = {
        let nib = Bundle.main.loadNibNamed("DefaultSegmentedGroupCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultSegmentedGroupCell else {
            return DefaultSegmentedGroupCell()
        }
        cell.title = ""
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.segments = [DefaultSegmentedGroupCell.SegmentModel(code: "", value: "Зарлага".localized()), DefaultSegmentedGroupCell.SegmentModel(code: "", value: "Орлого".localized())]
        cell.selectedSegmentIndex = 0
        cell.onSegmentChange = { segment in
            switch segment {
            case 0:
                self.lineChartField.setChart(self.filterMonthly(.Expense))
                self.pieChartField.setChart(self.filterUserCategory(.Expense))
                let expenseTotal = self.filterTotal(.Expense)
                guard let total = expenseTotal.first else {
                    return
                }
                guard let maxAmount = self.transactionReport?.maxAmount else {
                    return
                }
                let amount = total.actual_avg.orZero.formattedWithComma
                let currency = CurrencyConstants.MNT.rawValue.toCurrencySymbol
                self.amountButtonsField.setButtonTitles("Дундаж зарлага", nil)
                self.amountButtonsField.setButtonLabels("amount_with_currency_symbol".localized(with: amount, currency), maxAmount.tran_date.orEmpty)
                self.pieChartSection.title = "finances_pie_chart_expense_title".localized()
                self.tableView.reloadSections(IndexSet(integer: 2), with: .none)
            case 1:
                self.lineChartField.setChart(self.filterMonthly(.Income))
                self.pieChartField.setChart(self.filterUserCategory(.Income))
                let incomeTotal = self.filterTotal(.Income)
                guard let total = incomeTotal.first else {
                    return
                }
                guard let maxAmount = self.transactionReport?.maxAmount else {
                    return
                }
                let amount = total.actual_avg.orZero.formattedWithComma
                let currency = CurrencyConstants.MNT.rawValue.toCurrencySymbol
                self.amountButtonsField.setButtonTitles("Дундаж орлого", nil)
                self.amountButtonsField.setButtonLabels(amount + currency, maxAmount.tran_date.orEmpty)
                self.pieChartSection.title = "finances_pie_chart_income_title".localized()
                self.tableView.reloadSections(IndexSet(integer: 2), with: .none)
            default:
                break
            }
            if !self.isPieChart {
                self.drawCategoryList()
            }
        }
        return cell
    }()
    
    lazy var typeSection: TableViewModel.Section = {
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(
                    cell: self.financesTypeField
                ),
                TableViewModel.Row(
                    cell: self.monthPickerField
                ),
            ]
        )
    }()
    
    lazy var transactionTypeSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: nil,
            rows: [
                TableViewModel.Row(
                    cell: self.transactionTypeField
                ),
            ]
        )
    }()
    
    lazy var lineChartSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "finances_line_chart_title".localized(),
            rows: [
                TableViewModel.Row(
                    cell: self.lineChartField
                ),
            ]
        )
    }()
    
    lazy var pieChartSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "finances_pie_chart_expense_title".localized(),
            image: "finances_pie_list",
            rows: [
                TableViewModel.Row(
                    cell: self.pieChartField
                ),
            ]
        )
    }()
    
    lazy var amountButtonsSection: TableViewModel.Section = {
        TableViewModel.Section(
//            title: "finances_amount_buttons_title".localized(),
            rows: [
                TableViewModel.Row(
                    cell: self.amountButtonsField
                ),
            ]
        )
    }()
    
    lazy var budgetSection: TableViewModel.Section = {
        TableViewModel.Section(
            rows: []
        )
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        model.sections = [typeSection, transactionTypeSection, pieChartSection, amountButtonsSection, lineChartSection]
        onHeaderButtonClick = { section in
            if section == 2 {
                self.handleCategoryChange()
            }
        }
        tableView.registerCell(nibName: "FinancesTransactionCell")
        tableView.registerCell(nibName: "FinancesCategoryCell")
        tableView.registerCell(nibName: "FinancesBudgetCell")
        tableView.register(UINib(nibName: "BaseHeaderCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "BaseHeaderCell")
        tableView.reloadData()
        fetchCategoryList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarController?.navigationItem.rightBarButtonItems = [helpButton!]
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch financesTypeField.selectedSegmentIndex {
        case 0:
            switch indexPath.section {
            case 2:
                switch isPieChart {
                case true:
                    return 250
                case false:
                    return UITableView.automaticDimension
                }
            case 4:
                return 250
            default:
                return UITableView.automaticDimension
            }
        default:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if financesTypeField.selectedSegmentIndex < 2 || transactionSections.count == 0 {
            return
        }
        let sectionCount = transactionSections.count - 1
        let rowCount = transactionSections[indexPath.section].rows.count
        if sectionCount > 0, sectionCount == indexPath.section, rowCount - 1 == indexPath.row, transactionCount > transactionList.count {
            if isLoading() == false {
                let date = Calendar.current.date(byAdding: .month, value: -month, to: Date())
                guard let unwrappedDate = date else {
                    return
                }
                fetchTransactionList(of: unwrappedDate)
            }
        }
    }
    
    private func handleCategoryChange() {
        isPieChart = !isPieChart
        switch isPieChart {
        case false:
            pieChartSection.image = "finances_pie_chart"
            drawCategoryList()
        case true:
            pieChartSection.image = "finances_pie_list"
            pieChartSection.rows = [
                TableViewModel.Row(cell: pieChartField),
            ]
            tableView.reloadSections(IndexSet(integer: 2), with: .automatic)
        }
    }
    
    private func drawCategoryList() {
        var filter = FinancesUserCategoryResponse.TranPart.Expense
        switch transactionTypeField.selectedSegmentIndex {
        case 1:
            filter = .Income
        default:
            break
        }
        var rows = [TableViewModel.Row]()
        var total = 0.0
        filterUserCategory(filter).forEach { category in
            total += category.actual.orZero
        }
        filterUserCategory(filter).forEach { category in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FinancesCategoryCell") as? FinancesCategoryCell else {
                return
            }
            cell.setData(category, totalAmount: total)
            rows.append(TableViewModel.Row(cell: cell))
        }
        pieChartSection.rows = rows
        tableView.reloadSections(IndexSet(integer: 2), with: .automatic)
    }
    
    private func fetchAll() {
        let date = Calendar.current.date(byAdding: .month, value: -month, to: Date())
        guard let unwrappedDate = date else {
            return
        }
        pageNumber = 0
        userCategoryList = []
        monthlyList = []
        totalTransactionList = []
        transactionReport = nil
        transactionList = []
        transactionCount = 0
        transactionSections = []
        fetchUserCategoryList(of: unwrappedDate)
        fetchMonthlyTransactionList(of: unwrappedDate)
        fetchTotalTransactionList(of: unwrappedDate)
        fetchTransactionList(of: unwrappedDate)
        fetchBudgetList(of: unwrappedDate)
    }
    
    private func fetchCategoryList() {
        FinancesConnectionFactory.fetchCategoryList(
            success: { response in
                categoryList = response
                incomeCategoryList = self.filterCategory(.Income)
                expenseCategoryList = self.filterCategory(.Expense)
                transferCategoryList = self.filterCategory(.Transfer)
                self.fetchAll()
            },
            failed: { _ in
            }
        )
    }
    
    private func fetchBudgetList(of date: Date) {
        self.budgetSection.rows = []
        FinancesConnectionFactory.fetchBudgetList(
            date: date,
            success: { response in
                response.forEach { budget in
                    guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "FinancesBudgetCell") as? FinancesBudgetCell else {
                        return
                    }
                    cell.setData(budget)
                    self.budgetSection.rows.append(TableViewModel.Row(cell: cell))
                }
                self.tableView.reloadData()
            },
            failed: { _ in
                self.tableView.reloadData()
            }
        )
    }
    
    private func fetchMonthlyTransactionList(of date: Date) {
        let tmpDate = Calendar.current.date(byAdding: .month, value: -1, to: date)
        guard let unwrappedDate = tmpDate else {
            return
        }
        FinancesConnectionFactory.fetchMonthlyTransactionList(
            date: unwrappedDate,
            success: { response in
                self.monthlyList = response
                self.lineChartField.setChart(self.filterMonthly(.Expense))
            },
            failed: { _ in
            }
        )
    }
    
    private func fetchUserCategoryList(of date: Date) {
        pieChartField.setChart([])
        FinancesConnectionFactory.fetchUserCategoryList(
            date: date,
            success: { response in
                self.userCategoryList = response
                self.pieChartField.setChart(self.filterUserCategory(.Expense))
            },
            failed: { _ in
            }
        )
    }
    
    private func fetchTotalTransactionList(of date: Date) {
        FinancesConnectionFactory.fetchTotalTransactionList(
            date: date,
            success: { response in
                self.totalTransactionList = response
                self.fetchTransactionReport(of: date)
            },
            failed: { _ in
            }
        )
    }
    
    private func fetchTransactionReport(of date: Date) {
        FinancesConnectionFactory.fetchTransactionReport(
            date: date,
            success: { response in
                let expenseTotal = self.filterTotal(.Expense)
                guard let total = expenseTotal.first else {
                    return
                }
                guard let maxAmount = response.maxAmount else {
                    return
                }
                self.transactionReport = response
                let amount = total.actual_avg.orZero.formattedWithComma
                let currency = CurrencyConstants.MNT.rawValue.toCurrencySymbol
                self.amountButtonsField.setButtonLabels("amount_with_currency_symbol".localized(with: amount, currency), maxAmount.tran_date.orEmpty)
            },
            failed: { _ in
            }
        )
    }
    
    private func fetchTransactionList(of date: Date) {
        pageNumber += 1
        if pageNumber > 1 {
            showLoader()
        }
        FinancesConnectionFactory.fetchTransactionList(
            date: date,
            page: pageNumber,
            success: { response in
                self.hideLoader()
                self.transactionCount = response.count ?? 0
                self.transactionList.append(contentsOf: response.results ?? [])
                let transactionByDate = Dictionary(grouping: self.transactionList, by: { $0.tran_date.dateFormatted }).sorted { $1.0 < $0.0 }
                var sections = [TableViewModel.Section]()
                transactionLoop: for (key, value) in transactionByDate {
                    var rows = [TableViewModel.Row]()
                    value.forEach { transaction in
                        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "FinancesTransactionCell") as? FinancesTransactionCell else {
                            return
                        }
                        cell.setData(transaction)
                        rows.append(TableViewModel.Row(cell: cell))
                    }
                    sections.append(TableViewModel.Section(title: key, rows: rows))
                }
                sections.insert(self.typeSection, at: 0)
                self.transactionSections = sections
                if self.financesTypeField.selectedSegmentIndex == 2 {
                    self.model.sections = self.transactionSections
                }
                self.tableView.reloadData()
            },
            failed: { _ in
                self.hideLoader()
                self.tableView.reloadData()
            }
        )
    }
    
    private func filterTotal(_ filter: FinancesTotalTransactionResponse.TranPart) -> [FinancesTotalTransactionResponse] {
        let filtered = totalTransactionList.filter { $0.tranPart == filter }
        return filtered
    }
    
    private func filterCategory(_ filter: FinancesCategoryResponse.TranPart) -> [FinancesCategoryResponse] {
        let filtered = categoryList.filter { $0.tranPart == filter }
        return filtered
    }
    
    private func filterUserCategory(_ filter: FinancesUserCategoryResponse.TranPart) -> [FinancesUserCategoryResponse] {
        let filtered = userCategoryList.filter { $0.tranPart == filter }
        return filtered
    }
    
    private func filterMonthly(_ filter: FinancesMonthlyTransactionResponse.TranPart) -> ([FinancesMonthlyTransactionResponse], [String]) {
        var months = [String]()
        let filtered = monthlyList.filter { $0.tranPart == filter }
        filtered.forEach { response in
            let monthLabel = response.month?.toDate?.getMonthLabel()
            months.append(monthLabel.orEmpty)
        }
        return (filtered, months)
    }
    
    @objc private func helpClicked() {
        let helpVC = HelpViewController(nibName: "TableViewController", bundle: nil)
        navigationController?.pushViewController(helpVC, animated: true)
    }
}
