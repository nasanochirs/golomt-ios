//
//  FinancesTransactionCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class FinancesTransactionCell: UITableViewCell {
    @IBOutlet var categoryImageView: UIImageView!
    @IBOutlet var remarkLabel: UILabel!
    @IBOutlet var accountLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        remarkLabel.useMediumFont()
        remarkLabel.textColor = .defaultPrimaryText
        accountLabel.useSmallFont()
        accountLabel.textColor = .defaultSecondaryText
        amountLabel.useLargeFont()
        amountLabel.textColor = .defaultPrimaryText
        amountLabel.makeBold()
    }

    func setData(_ data: FinancesTransactionResponse.Transaction) {
        categoryImageView.image = data.getImage()
        remarkLabel.text = data.description
        accountLabel.text = data.getName()
        var amount = data.amount_mnt.orZero
        let currency = CurrencyConstants.MNT.rawValue.toCurrencySymbol
        switch data.tranPart {
        case .Income:
            amountLabel.textColor = .defaultGreen
        default:
            amountLabel.textColor = .defaultRed
            amount = amount * -1
        }
        amountLabel.text = "amount_with_currency_symbol".localized(with: amount.formattedWithComma, currency)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
