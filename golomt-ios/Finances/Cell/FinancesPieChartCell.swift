//
//  FinancesPieChartCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/14/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Charts
import UIKit

class FinancesPieChartCell: UITableViewCell {
    @IBOutlet var pieChartView: PieChartView!
    var entries = [PieChartDataEntry]()
    var response = [FinancesUserCategoryResponse]()

    lazy var pieLoader: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: .whiteLarge)
        spinner.color = .defaultPrimaryText
        spinner.translatesAutoresizingMaskIntoConstraints = false
        return spinner
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        pieChartView.addSubview(pieLoader)
        pieLoader.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        pieLoader.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        initChart()
    }

    func setChart(_ data: [FinancesUserCategoryResponse]) {
        entries = []
        if data.count == 0 {
            let dataSet = pieChartView.data?.getDataSetByIndex(0) as! PieChartDataSet
            pieChartView.highlightValue(nil)
            pieChartView.centerText = nil
            dataSet.replaceEntries(entries)
            pieChartView.data?.notifyDataChanged()
            pieChartView.notifyDataSetChanged()
            pieLoader.startAnimating()
            return
        }
        pieLoader.stopAnimating()
        response = data
        pieChartView.highlightValue(nil)
        setInitialCenterText()
        var colors = [UIColor]()
        data.forEach { element in
            entries.append(
                PieChartDataEntry(value: element.actual.orZero, label: "", icon: nil, data: element)
            )
            colors.append(element.getColor())
        }
        entries.sort(by: { $0.x < $1.x })
        let dataSet = pieChartView.data?.getDataSetByIndex(0) as! PieChartDataSet
        dataSet.replaceEntries(entries)
        dataSet.setColors(colors, alpha: 1.0)
        pieChartView.animate(yAxisDuration: 1, easingOption: .linear)
        pieChartView.data?.notifyDataChanged()
        pieChartView.notifyDataSetChanged()
    }

    private func initChart() {
        let dataSet = PieChartDataSet()
        dataSet.label = nil
        dataSet.valueLinePart1OffsetPercentage = 0.9
        dataSet.yValuePosition = .outsideSlice
        dataSet.valueFormatter = self
        dataSet.valueTextColor = .defaultPrimaryText
        let data = PieChartData(dataSet: dataSet)
        pieChartView.data = data
        pieChartView.rotationEnabled = false
        pieChartView.legend.enabled = false
        pieChartView.drawEntryLabelsEnabled = false
        pieChartView.holeColor = .defaultTertiaryBackground
        pieChartView.holeRadiusPercent = 0.7
        pieChartView.sliceTextDrawingThreshold = 30.0
        pieChartView.extraBottomOffset = 10
        pieChartView.delegate = self
    }

    private func setInitialCenterText() {
        var total = 0.0
        response.forEach { element in
            total += element.actual.orZero
        }
        let centerText = NSMutableAttributedString(string: "")
        let name = NSAttributedString(string: "\("finances_total_expense_title".localized())\n")
        centerText.append(name)
        let amount = NSAttributedString(string: total.formattedWithComma)
        centerText.append(amount)
        centerText.append(NSAttributedString(string: "\(CurrencyConstants.MNT.rawValue.toCurrencySymbol)"))
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        centerText.addAttributes([.paragraphStyle: paragraph], range: NSRange(location: 0, length: centerText.length))
        centerText.addAttributes([.foregroundColor: UIColor.defaultSecondaryText], range: NSRange(location: 0, length: name.length))
        centerText.addAttributes([.foregroundColor: UIColor.defaultPrimaryText], range: NSRange(location: name.length, length: amount.length + 1))
        centerText.addAttributes([.font: UIFont.boldSystemFont(ofSize: 17)], range: NSRange(location: name.length, length: amount.length))
        pieChartView.centerAttributedText = centerText
    }
}

extension FinancesPieChartCell: IValueFormatter {
    func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
        guard let data = entry.data as? FinancesUserCategoryResponse else {
            return ""
        }
        let names = data.getName().components(separatedBy: " ")
        var categoryName = ""
        names.forEach { name in
            categoryName += name
            categoryName += "\n"
        }
        return String(categoryName.dropLast())
    }
}

extension FinancesPieChartCell: ChartViewDelegate {
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        guard let category = entry.data as? FinancesUserCategoryResponse else {
            return
        }
        let imageAttachment = NSTextAttachment()
        imageAttachment.bounds = CGRect(x: 0, y: 0, width: 30, height: 30)
        let image = category.getImage()
        imageAttachment.image = image
        let centerText = NSMutableAttributedString(string: "")
        centerText.append(NSAttributedString(attachment: imageAttachment))
        centerText.append(NSAttributedString(string: "\n"))
        let name = NSAttributedString(string: "\(category.getName())\n")
        centerText.append(name)
        let amount = NSAttributedString(string: category.actual.orZero.formattedWithComma)
        centerText.append(amount)
        centerText.append(NSAttributedString(string: "\(CurrencyConstants.MNT.rawValue.toCurrencySymbol)"))
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        centerText.addAttributes([.paragraphStyle: paragraph], range: NSRange(location: 0, length: centerText.length))
        centerText.addAttributes([.foregroundColor: UIColor.defaultSecondaryText], range: NSRange(location: 2, length: name.length))
        let amountStartPoint = 2 + name.length
        centerText.addAttributes([.foregroundColor: UIColor.defaultPrimaryText], range: NSRange(location: amountStartPoint, length: amount.length + 1))
        centerText.addAttributes([.font: UIFont.boldSystemFont(ofSize: 17)], range: NSRange(location: amountStartPoint, length: amount.length))
        pieChartView.centerAttributedText = centerText
    }

    func chartValueNothingSelected(_ chartView: ChartViewBase) {
        setInitialCenterText()
    }
}
