//
//  FinancesAmountButtonsCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/14/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class FinancesAmountButtonsCell: UITableViewCell {
    @IBOutlet var firstButtonView: UIView!
    @IBOutlet var secondButtonView: UIView!
    @IBOutlet var firstButtonTitleLabel: UILabel!
    @IBOutlet var firstButtonTextLabel: UILabel!
    @IBOutlet var secondButtonTitleLabel: UILabel!
    @IBOutlet var secondButtonTextLabel: UILabel!

    var onStateChange: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        firstButtonView.corner(cornerRadius: 10)
        secondButtonView.corner(cornerRadius: 10)
        firstButtonView.backgroundColor = .defaultTertiaryBackground
        secondButtonView.backgroundColor = .defaultTertiaryBackground
        firstButtonTitleLabel?.useXSmallFont()
        firstButtonTextLabel?.useMediumFont()
        firstButtonTextLabel?.makeBold()
        secondButtonTitleLabel?.useXSmallFont()
        secondButtonTextLabel?.useMediumFont()
        secondButtonTextLabel?.makeBold()
        firstButtonTitleLabel.textColor = .defaultSecondaryText
        secondButtonTitleLabel.textColor = .defaultSecondaryText
        firstButtonTextLabel.textColor = .defaultPrimaryText
        secondButtonTextLabel.textColor = .defaultPrimaryText
        selectionStyle = .none
    }

    func setButtonTitles(_ firstTitle: String?, _ secondTitle: String?) {
        if firstTitle != nil {
            firstButtonTitleLabel.text = firstTitle!.localized()
        }
        if secondTitle != nil {
            secondButtonTitleLabel.text = secondTitle!.localized()
        }
    }

    func setButtonLabels(_ firstLabel: String, _ secondLabel: String) {
        firstButtonTextLabel.text = firstLabel.localized()
        secondButtonTextLabel.text = secondLabel.localized()
        onStateChange?()
    }
}
