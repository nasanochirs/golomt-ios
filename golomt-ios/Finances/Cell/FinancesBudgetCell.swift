//
//  FinancesBudgetCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/23/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class FinancesBudgetCell: UITableViewCell {
    @IBOutlet var categoryImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var progressView: UIProgressView!
    @IBOutlet var percentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.useSmallFont()
        nameLabel.textColor = .defaultSecondaryText
        amountLabel.useLargeFont()
        amountLabel.textColor = .defaultPrimaryText
        amountLabel.makeBold()
        percentLabel.useLargeFont()
        percentLabel.textColor = .defaultSecondaryText
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ data: FinancesBudgetResponse) {
        categoryImageView.image = data.getImage()
        nameLabel.text = data.name
        let total = data.amount.orZero
        let currency = CurrencyConstants.MNT.rawValue.toCurrencySymbol
        var spent = 0.0
        data.members?.forEach({ member in
            spent += member.actual.orZero
        })
        amountLabel.text = String(format: "%@%@ / %@%@", spent.formattedWithComma, currency, total.formattedWithComma, currency)
        let percent = spent * 100 / total
        percentLabel.text = "finances_percent_label".localized(with: percent)
        progressView.setProgress(Float(percent) / 100, animated: true)
    }
    
}
