//
//  FinancesCategoryCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/23/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class FinancesCategoryCell: UITableViewCell {
    @IBOutlet var typeImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var percentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.useMediumFont()
        nameLabel.textColor = .defaultSecondaryText
        amountLabel.useLargeFont()
        amountLabel.textColor = .defaultPrimaryText
        amountLabel.makeBold()
        percentLabel.useSmallFont()
        percentLabel.textColor = .defaultSecondaryText
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ data: FinancesUserCategoryResponse, totalAmount: Double) {
        typeImageView.image = data.getImage()
        nameLabel.text = data.getName()
        let amount = data.actual.orZero
        let currency = CurrencyConstants.MNT.rawValue.toCurrencySymbol
        amountLabel.text = "amount_with_currency_symbol".localized(with: amount.formattedWithComma, currency)
        percentLabel.text = "finances_percent_label".localized(with: amount * 100 / totalAmount)
    }
    
}
