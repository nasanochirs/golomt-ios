//
//  FinancesLineChartCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Charts
import UIKit

class FinancesLineChartCell: UITableViewCell {
    @IBOutlet var lineChartView: LineChartView!
    var entries = [ChartDataEntry]()
    var months = [String]()

    override func awakeFromNib() {
        super.awakeFromNib()
        initChart()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setChart(_ data: ([FinancesMonthlyTransactionResponse], [String])) {
        entries = []
        months = data.1
        for (i, element) in data.0.enumerated() {
            entries.append(
                ChartDataEntry(x: Double(i), y: element.actual.orZero)
            )
        }
        entries.sort(by: { $0.x < $1.x })

        let dataSet = lineChartView.data?.getDataSetByIndex(0) as! LineChartDataSet
        dataSet.replaceEntries(entries)
        lineChartView.animate(yAxisDuration: 1, easingOption: .linear)
        lineChartView.data?.notifyDataChanged()
        lineChartView.notifyDataSetChanged()
    }

    private func initChart() {
        let dataSet = LineChartDataSet()
        dataSet.colors = [.defaultPurpleGradientStart]
        dataSet.drawFilledEnabled = true
        dataSet.drawCircleHoleEnabled = false
        dataSet.mode = .horizontalBezier
        dataSet.fillColor = .defaultPurpleGradientEnd
        dataSet.circleColors = [.defaultPurpleGradientStart]
        dataSet.circleRadius = 5
        dataSet.label = nil
        dataSet.drawValuesEnabled = false
        let data = LineChartData(dataSet: dataSet)
        lineChartView.data = data
        lineChartView.pinchZoomEnabled = false
        lineChartView.doubleTapToZoomEnabled = false
        lineChartView.setScaleEnabled(false)
        lineChartView.drawGridBackgroundEnabled = false
        lineChartView.legend.enabled = false
        let xAxis = lineChartView.xAxis
        let leftAxis = lineChartView.leftAxis
        let rightAxis = lineChartView.rightAxis
        xAxis.avoidFirstLastClippingEnabled = true
        xAxis.drawAxisLineEnabled = false
        xAxis.drawGridLinesEnabled = false
        xAxis.valueFormatter = self
        xAxis.setLabelCount(6, force: true)
        leftAxis.drawLabelsEnabled = false
        leftAxis.drawAxisLineEnabled = false
        leftAxis.drawGridLinesEnabled = false
        leftAxis.drawLabelsEnabled = false
        rightAxis.drawLabelsEnabled = false
        rightAxis.drawGridLinesEnabled = false
        rightAxis.drawAxisLineEnabled = false
        rightAxis.drawLabelsEnabled = false
        lineChartView.delegate = self
        let marker = BalloonMarker(color: .defaultSelectionBackground, font: .systemFont(ofSize: 17), textColor: .white, insets: .init(top: 10, left: 5, bottom: 20, right: 5))
        marker.chartView = lineChartView
        lineChartView.marker = marker
    }
}

extension FinancesLineChartCell: ChartViewDelegate {
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {}
}

extension FinancesLineChartCell: IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return months[Int(value)]
    }
}
