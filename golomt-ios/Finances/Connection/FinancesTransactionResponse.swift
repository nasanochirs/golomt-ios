//
//  FinancesTransactionResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class FinancesTransactionResponse: Codable {
    let count: Int?
    let results: [Transaction]?
    
    enum CodingKeys: String, CodingKey {
        case count, results
    }
    
    struct Transaction: Codable {
        var id: Int?
        var tran_date: String?
        var tranPart: TranPart?
        var amount_mnt: Double?
        var description: String?
        var account_id: Int?
        var cpty_name: String?
        var tran_category: Int?
        var deleted: Bool?
        var split_id: String?
        
        enum TranPart: String, Codable {
            case Income = "C"
            case Expense = "D"
            case Transfer = "T"
        }
        
        enum CodingKeys: String, CodingKey {
            case tranPart = "tran_part"
            case id, tran_date, amount_mnt, description, account_id, cpty_name, tran_category, deleted, split_id
        }
        
        func getImage() -> UIImage? {
            let defaultImage = UIImage(named: "UNCATEGORIZED")?.withColor(.white)?.withBackground(color: UIColor(hex: "607d8b")).withRoundedCorners()
            let list = categoryList.filter { $0.id == tran_category }
            guard let category = list.first else {
                return defaultImage
            }
            let image = UIImage(named: category.name.orEmpty)?.withColor(.white)?.withBackground(color: getColor()).withRoundedCorners()
            if image == nil {
                return defaultImage
            }
            return image
        }
        
        func getColor() -> UIColor {
            let list = categoryList.filter { $0.id == tran_category }
            guard let category = list.first else {
                return UIColor(hex: "607d8b")
            }
            return UIColor(hex: String(category.icon.orEmpty.prefix(6)))
        }
        
        func getName() -> String {
            let list = categoryList.filter { $0.id == tran_category }
            guard let category = list.first else {
                return "Ангилагдаагүй"
            }
            let components = category.desc.orEmpty.components(separatedBy: "|")
            switch getLanguage() {
            case "mn":
                return components.first.orEmpty
            default:
                return components.last.orEmpty
            }
        }
    }
}
