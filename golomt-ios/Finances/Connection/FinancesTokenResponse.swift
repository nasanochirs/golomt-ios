//
//  FinancesTokenResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class FinancesTokenResponse: Codable {
    let header: Header?
    let body: Body?

    struct Header: Codable {
        var code: Int?
        var status: String?
        var date: String?
    }
    
    struct Body: Codable {
        var token: String?
        var errorDesc: String?
    }
}
