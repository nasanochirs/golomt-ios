//
//  FinancesMonthlyTransactionResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class FinancesMonthlyTransactionResponse: Codable {
    let tranPart: TranPart?
    let month: String?
    let actual: Double?

    enum CodingKeys: String, CodingKey {
        case tranPart = "tran_part"
        case month, actual
    }

    enum TranPart: String, Codable {
        case Income = "C"
        case Expense = "D"
    }

    typealias TransactionList = [FinancesMonthlyTransactionResponse]
}
