//
//  FinancesTokenRequest.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class FinancesTokenRequest: BaseFinancesRequest {
    var header = Header()
    var body = Body()

    private enum CodingKeys: String, CodingKey {
        case header, body
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(header, forKey: .header)
        try container.encode(body, forKey: .body)
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }
    
    override init() {
        super.init()
    }
    
    struct Header: Codable {
        var languageId = ""

        init() {
            switch getLanguage() {
            case "mn":
                languageId = "002"
            default:
                languageId = "001"
            }
        }
    }
    
    struct Body: Codable {
        var sid = ""
    }
}
