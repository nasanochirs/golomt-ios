//
//  FinancesUserCategoryResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/14/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class FinancesUserCategoryResponse: Codable {
    let tranPart: TranPart?
    let id: Int?
    let actual: Double?

    enum CodingKeys: String, CodingKey {
        case tranPart = "tran_part"
        case id, actual
    }

    enum TranPart: String, Codable {
        case Income = "C"
        case Expense = "D"
        case Transfer = "T"
    }
    
    func getImage() -> UIImage? {
        let defaultImage = UIImage(named: "UNCATEGORIZED")?.withColor(.white)?.withBackground(color: UIColor(hex: "607d8b")).withRoundedCorners()
        let list = categoryList.filter { $0.id == id }
        guard let category = list.first else {
            return defaultImage
        }
        let image = UIImage(named: category.name.orEmpty)?.withColor(.white)?.withBackground(color: getColor()).withRoundedCorners()
        if image == nil {
            return defaultImage
        }
        return image
    }
    
    func getColor() -> UIColor {
        let list = categoryList.filter { $0.id == id }
        guard let category = list.first else {
            return UIColor(hex: "607d8b")
        }
        return UIColor(hex: String(category.icon.orEmpty.prefix(6)))
    }
    
    func getName() -> String {
        let list = categoryList.filter { $0.id == id }
        guard let category = list.first else {
            return "Ангилагдаагүй"
        }
        let components = category.desc.orEmpty.components(separatedBy: "|")
        switch getLanguage() {
        case "mn":
            return components.first.orEmpty
        default:
            return components.last.orEmpty
        }
    }

    typealias CategoryList = [FinancesCategoryResponse]
}
