//
//  FinancesCategoryResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/14/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class FinancesCategoryResponse: Codable {
    let tranPart: TranPart?
    let id: Int?
    let name, desc, icon: String?
    let translate: Bool?

    enum CodingKeys: String, CodingKey {
        case tranPart = "part"
        case id, name, desc, icon, translate
    }

    enum TranPart: String, Codable {
        case Income = "C"
        case Expense = "D"
        case Transfer = "T"
    }

    typealias CategoryList = [FinancesCategoryResponse]
}
