//
//  FinancesBudgetResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/23/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class FinancesBudgetResponse: Codable {
    let id: Int?
    let name, start_date, end_date: String?
    let amount: Double?
    let category_id: Int?
    let members: [Member]?

    enum CodingKeys: String, CodingKey {
        case id, name, start_date, end_date, amount, category_id, members
    }
    
    func getColor() -> UIColor {
        let list = categoryList.filter { $0.id == category_id }
        guard let category = list.first else {
            return UIColor(hex: "607d8b")
        }
        return UIColor(hex: String(category.icon.orEmpty.prefix(6)))
    }
    
    func getImage() -> UIImage? {
        let image = UIImage(named: name.orEmpty)?.withColor(.white)?.withBackground(color: getColor()).withRoundedCorners()
        if image == nil {
            return UIImage(named: "UNCATEGORIZED")?.withColor(.white)?.withBackground(color: UIColor(hex: "607d8b")).withRoundedCorners()
        }
        return image!
    }
    
    struct Member: Codable {
        let id: Int?
        let budget_id: Int?
        let actual: Double?
        let cif_id: String?
        let name: String?
        let is_owner: Bool?
        let is_confirmed: Bool?
        let is_deleted: Bool?
    }
}
