//
//  FinancesTotalTransactionResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/14/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class FinancesTotalTransactionResponse: Codable {
    let tranPart: TranPart?
    let actual_avg: Double?
    let actual: Double?

    enum TranPart: String, Codable {
        case Income = "C"
        case Expense = "D"
        case Transfer = "T"
    }

    enum CodingKeys: String, CodingKey {
        case tranPart = "tran_part"
        case actual_avg, actual
    }

    typealias TransactionList = [FinancesCategoryResponse]
}
