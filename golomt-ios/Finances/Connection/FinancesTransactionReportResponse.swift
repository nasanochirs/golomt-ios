//
//  FinancesTransactionReportResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class FinancesTransactionReportResponse: Codable {
    let maxAmount: Amount?
    
    enum CodingKeys: String, CodingKey {
        case maxAmount = "max_amt"
    }
    
    struct Amount: Codable {
        var tran_date: String?
        var actual: Double?
    }
}
