//
//  DataEncryptDecrypt.h
//  nRange
//
//  Created by Tugsjargal B on 5/18/12.
//  Copyright 2012 IS. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DataEncryptDecrypt : NSObject {
    
}

+ (DataEncryptDecrypt *)sharedENDEcryptor;

- (NSString*)encryptWithAES256:(NSString *)data
				 withStringUID:(NSString *)uid;

- (NSString*)decryptWithAES256:(NSString *)data
				 withStringUID:(NSString *)uid;

- (NSString*)decryptImageWithAES256:(NSString *)data
                      withStringUID:(NSString *)uid;

- (NSString*)encryptImageWithAES256:(NSString *)data
                      withStringUID:(NSString *)uid;

@end
