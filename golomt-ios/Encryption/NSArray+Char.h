//
//  NSArray+Char.h
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/11/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSArray (Char)

- (char *)asChar;

@end

NS_ASSUME_NONNULL_END
