//
//  DataEncryptDecrypt.m
//  nRange
//
//  Created by Tugsjargal B on 5/18/12.
//  Copyright 2012 IS. All rights reserved.
//

#import "DataEncryptDecrypt.h"
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>
#import "NSData+CommonCrypto.h"
#import "NSData+Base64.h"
#import "Swift_Golomt-Swift.h"
#import "NSArray+Char.h"

@implementation DataEncryptDecrypt

static DataEncryptDecrypt * _dataConverter;

+ (DataEncryptDecrypt *)sharedENDEcryptor
{
	if (_dataConverter != nil) {
        return _dataConverter;
    }

    _dataConverter = [[DataEncryptDecrypt alloc] init];

    return _dataConverter;
}

- (NSString *)dataEncryptString:(NSString *)string
{
	NSData *outputData = [string dataUsingEncoding:NSASCIIStringEncoding];
	NSString *outputString = [outputData base64EncodedString];
	return outputString;
}

- (NSString *)dataDecryptString:(NSString*)data
{
	NSData *plainData = [self base64DataFromString:data];
	NSString *tmp = [[NSString alloc] initWithBytes:[plainData bytes]
											 length:[plainData length]
										   encoding:NSASCIIStringEncoding];
    
	return tmp;
}

- (NSString*)encryptWithAES256:(NSString *)data
				 withStringUID:(NSString *)uid
{
	NSData *inputData = [data dataUsingEncoding:NSUTF8StringEncoding];
    
	CCCryptorStatus status = kCCSuccess;
    
////     PROD
//    NSData *_IV = [NSData dataWithBytes:[[UserPreferencesConstants y8w6ohWithL:@[@0x21, @0x33, @0x60, @0xDC, @0x60, @0xD3, @0xBE, @0x37, @0xF1, @0x73, @0xD6, @0x8E, @0x5E, @0xAE, @0x54, @0xAA]] asChar] length:16];
//    NSData *_key = [NSData dataWithBytes:[[UserPreferencesConstants y8w6ohWithL:@[@0x4A, @0x1A, @0x41, @0xB1, @0x4C, @0xC7, @0xC8, @0x6C, @0x8C, @0x44, @0xC2, @0xC0, @0x3F, @0xA3, @0x52, @0xBD]] asChar] length:16];
    
    // UAT
    NSData *_IV = [NSData dataWithBytes:[[UserPreferencesConstants y8w6ohWithL:@[@0x71, @0x3E, @0x0F, @0x8A, @0x7E, @0xCA, @0xA3, @0x36, @0xFF, @0x72, @0xA9, @0xC5, @0x3F, @0xB3, @0x03, @0x9E]] asChar] length:16];
    NSData *_key = [NSData dataWithBytes:[[UserPreferencesConstants y8w6ohWithL:@[@0x78, @0x16, @0x51, @0xDC, @0x23, @0xF2, @0x88, @0x57, @0xB4, @0x50, @0xF9, @0xA3, @0x0D, @0xAC, @0x77, @0xBD]] asChar] length:16];
    
	NSData *outputData = [inputData dataEncryptedUsingAlgorithm: kCCAlgorithmAES128
															key: _key
										   initializationVector: _IV
														options: kCCOptionPKCS7Padding
														  error: &status];
    
	NSString *base64 = [outputData base64EncodedString];
    
	return base64;
}

- (NSString*)encryptImageWithAES256:(NSString *)data
                 withStringUID:(NSString *)uid
{
    NSData *inputData = [data dataUsingEncoding:NSUTF8StringEncoding];
    
    NSData *_key = [NSData dataWithBytes:[[UserPreferencesConstants y8w6ohWithL:@[@0x2F, @0x00, @0x6E, @0xA6, @0x2D, @0xDE, @0xB8, @0x42, @0x85, @0x44, @0xAB, @0xC3, @0x5D, @0xA3, @0x77, @0xBE]] asChar] length:16];
    NSData *_IV = [NSData dataWithBytes:[[UserPreferencesConstants y8w6ohWithL:@[@0x5A, @0x08, @0x0F, @0xAC, @0x45, @0xDB, @0xAE, @0x40, @0x8D, @0x31, @0xDE, @0xB4, @0x2A, @0xDB, @0x60, @0xB8]] asChar] length:16];
    
    CCCryptorStatus status = kCCSuccess;
    
    NSData *outputData = [inputData dataEncryptedUsingAlgorithm: kCCAlgorithmAES128
                                                            key: _key
                                           initializationVector: _IV
                                                        options: kCCOptionPKCS7Padding
                                                          error: &status];
    
    NSString *base64 = [outputData base64EncodedString];
    
    return base64;
}

- (NSData *)base64DataFromString:(NSString *)string
{
    unsigned long ixtext, lentext;
    unsigned char ch, inbuf[4], outbuf[3];
    short i, ixinbuf;
    Boolean flignore, flendtext = false;
    const unsigned char *tempcstring;
    NSMutableData *theData;
	
    if (string == nil)
    {
        return [NSData data];
    }
	
    ixtext = 0;
	
    tempcstring = (const unsigned char *)[string UTF8String];
	
    lentext = [string length];
	
    theData = [NSMutableData dataWithCapacity: lentext];
	
    ixinbuf = 0;
	
    while (true)
    {
        if (ixtext >= lentext)
        {
            break;
        }
		
        ch = tempcstring [ixtext++];
		
        flignore = false;
		
        if ((ch >= 'A') && (ch <= 'Z'))
        {
            ch = ch - 'A';
        }
        else if ((ch >= 'a') && (ch <= 'z'))
        {
            ch = ch - 'a' + 26;
        }
        else if ((ch >= '0') && (ch <= '9'))
        {
            ch = ch - '0' + 52;
        }
        else if (ch == '+')
        {
            ch = 62;
        }
        else if (ch == '=')
        {
            flendtext = true;
        }
        else if (ch == '/')
        {
            ch = 63;
        }
        else
        {
            flignore = true; 
        }

        if (!flignore)
        {
            short ctcharsinbuf = 3;
            Boolean flbreak = false;

            if (flendtext)
            {
                if (ixinbuf == 0)
                {
                    break;
                }

                if ((ixinbuf == 1) || (ixinbuf == 2))
                {
                    ctcharsinbuf = 1;
                }
                else
                {
                    ctcharsinbuf = 2;
                }

                ixinbuf = 3;

                flbreak = true;
            }

            inbuf [ixinbuf++] = ch;
			
            if (ixinbuf == 4)
            {
                ixinbuf = 0;
				
                outbuf[0] = (inbuf[0] << 2) | ((inbuf[1] & 0x30) >> 4);
                outbuf[1] = ((inbuf[1] & 0x0F) << 4) | ((inbuf[2] & 0x3C) >> 2);
                outbuf[2] = ((inbuf[2] & 0x03) << 6) | (inbuf[3] & 0x3F);
				
                for (i = 0; i < ctcharsinbuf; i++)
                {
                    [theData appendBytes: &outbuf[i] length: 1];
                }
            }
			
            if (flbreak)
            {
                break;
            }
        }
    }
	
    return theData;
}

- (NSString*)decryptWithAES256:(NSString *)data
				 withStringUID:(NSString *)uid
{
	NSData *inputData = [self base64DataFromString:data];

	CCCryptorStatus status = kCCSuccess;
//        // PROD
//        NSData *_IV = [NSData dataWithBytes:[[UserPreferencesConstants y8w6ohWithL:@[@0x21, @0x33, @0x60, @0xDC, @0x60, @0xD3, @0xBE, @0x37, @0xF1, @0x73, @0xD6, @0x8E, @0x5E, @0xAE, @0x54, @0xAA]] asChar] length:16];
//        NSData *_key = [NSData dataWithBytes:[[UserPreferencesConstants y8w6ohWithL:@[@0x4A, @0x1A, @0x41, @0xB1, @0x4C, @0xC7, @0xC8, @0x6C, @0x8C, @0x44, @0xC2, @0xC0, @0x3F, @0xA3, @0x52, @0xBD]] asChar] length:16];

    // UAT

    NSData *_IV = [NSData dataWithBytes:[[UserPreferencesConstants y8w6ohWithL:@[@0x71, @0x3E, @0x0F, @0x8A, @0x7E, @0xCA, @0xA3, @0x36, @0xFF, @0x72, @0xA9, @0xC5, @0x3F, @0xB3, @0x03, @0x9E]] asChar] length:16];
    NSData *_key = [NSData dataWithBytes:[[UserPreferencesConstants y8w6ohWithL:@[@0x78, @0x16, @0x51, @0xDC, @0x23, @0xF2, @0x88, @0x57, @0xB4, @0x50, @0xF9, @0xA3, @0x0D, @0xAC, @0x77, @0xBD]] asChar] length:16];
	NSData *outputData = [inputData decryptedDataUsingAlgorithm: kCCAlgorithmAES128
															key: _key
										   initializationVector: _IV
														options: kCCOptionPKCS7Padding
														  error: &status];
    if ([outputData bytes] != nil) {
        NSString *decrypted = [[NSString alloc] initWithData:outputData encoding:NSUTF8StringEncoding];
        int startIndex = 0;
        NSUInteger endIndex = 0;
        for(int i =0; i < [decrypted length]; i++) {
            char ch = [decrypted characterAtIndex:i];
            if(ch == '{') {
                startIndex = i;
                break;
            }
        }
        
        for(NSUInteger i = [decrypted length] - 1; i >= 0; i--) {
            char ch = [decrypted characterAtIndex:i];
            if(ch == '}') {
                endIndex = i;
                break;
            }
        }
     	return [decrypted substringWithRange:NSMakeRange(startIndex, endIndex-startIndex+1)];
    }
    
    return nil;
}

- (NSString*)decryptImageWithAES256:(NSString *)data
                 withStringUID:(NSString *)uid
{
    NSData *inputData = [self base64DataFromString:data];

    NSData *_key = [NSData dataWithBytes:[[UserPreferencesConstants y8w6ohWithL:@[@0x2F, @0x00, @0x6E, @0xA6, @0x2D, @0xDE, @0xB8, @0x42, @0x85, @0x44, @0xAB, @0xC3, @0x5D, @0xA3, @0x77, @0xBE]] asChar] length:16];
    NSData *_IV = [NSData dataWithBytes:[[UserPreferencesConstants y8w6ohWithL:@[@0x5A, @0x08, @0x0F, @0xAC, @0x45, @0xDB, @0xAE, @0x40, @0x8D, @0x31, @0xDE, @0xB4, @0x2A, @0xDB, @0x60, @0xB8]] asChar] length:16];
    
    CCCryptorStatus status = kCCSuccess;
    NSData *outputData = [inputData decryptedDataUsingAlgorithm: kCCAlgorithmAES128
                                                            key: _key
                                           initializationVector: _IV
                                                        options: kCCOptionPKCS7Padding
                                                          error: &status];
    
    NSString *decrypted = [[NSString alloc] initWithData:outputData encoding:NSUTF8StringEncoding];

    return decrypted;
}

@end
