//
//  NSArray+Char.m
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/11/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

#import "NSArray+Char.h"

@implementation NSArray (Char)

- (char *)asChar {
    char *iv = malloc(16 * sizeof(char));
    for (int i = 0 ; i < 16; i++) {
        iv[i] = [[self objectAtIndex:i] charValue];
    }
    return iv;
}

@end
