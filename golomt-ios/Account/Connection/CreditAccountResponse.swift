//
//  CreditAccountResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/27/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class CreditAccountResponse: BaseResponse {
    let creditAccountList: [CreditAccount]?
    
    var accountList: [AccountResponse.Account] {
        var tmpList = [AccountResponse.Account]()
        
        creditAccountList?.forEach { creditAccount in
            let account = AccountResponse.Account()
            account.MAIN_ACCT_TYPE = AccountConstants.credit
            account.ACCT_NICKNAME = creditAccount.CARD_NICKNAME
            account.ACCT_CURRENCY = creditAccount.CURRENCY
            account.ACCT_NUMBER = creditAccount.CREDIT_CARD_NUMBER
            account.ACCT_BALANCE = creditAccount.TOTAL_AMOUNT_DUE
            account.BRANCH_ID = creditAccount.BRANCH_CODE
            account.MINIMUM_AMOUNT_DUE_ARRAY = creditAccount.MINIMUM_AMOUNT_DUE_ARRAY
            account.ACCOUNT_CURRENT_BALANCE_ARRAY = creditAccount.ACCOUNT_CURRENT_BALANCE_ARRAY
            account.TOTAL_CREDIT_LIMIT = creditAccount.TOTAL_CREDIT_LIMIT
            account.AVAILABLE_CREDIT_LIMIT = creditAccount.AVAILABLE_CREDIT_LIMIT
            account.TOTAL_AMOUNT_DUE = creditAccount.TOTAL_AMOUNT_DUE
            switch creditAccount.CARD_STATUS_ARRAY {
            case "C": return
            default: tmpList.append(account)
            }
        }
        return tmpList
    }
    
    private enum CodingKeys: String, CodingKey {
        case CreditCardList_REC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(creditAccountList, forKey: .CreditCardList_REC)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.creditAccountList = try container.decodeIfPresent([CreditAccount].self, forKey: .CreditCardList_REC)
        try super.init(from: decoder)
    }
    
    class CreditAccount: Codable {
        var MINIMUM_AMOUNT_DUE_ARRAY: String?
        var AS_OF_DATE: String?
        var ACCOUNT_OUTSTANDING_INSTALLMENT_ARRAY: String?
        var CARD_STATUS_ARRAY: String?
        var AVAILABLE_CREDIT_LIMIT: String?
        var CARD_HOLDER: String?
        var TOTAL_CREDIT_LIMIT: String?
        var CARD_NICKNAME: String?
        var CREDIT_CARD_ACCOUNT_TYPE: String?
        var CREDIT_CARD_NUMBER: String?
        var CURRENCY: String?
        var ACCOUNT_CATEGORY: String?
        var TOTAL_CASH_LIMIT: String?
        var PRIMARY_CREDIT_CARD: String?
        var LAST_BILLED_AMOUNT: String?
        var BRANCH_CODE: String?
        var TOTAL_CREDIT_LIMIT_ARRAY: String?
        var TOTAL_AMOUNT_DUE: String?
        var ACCOUNT_CURRENT_BALANCE_ARRAY: String?
        var AVAILABLE_CASH_LIMIT: String?
        var CARD_PRODUCTDESC_ARRAY: String?
        var CARD_EXPIREDDATE_ARRAY: String?
        
        func getMaskedNumber() -> String {
            let cardNumber = CREDIT_CARD_NUMBER.orEmpty
            return "card_number_masked".localized(with: String(cardNumber.prefix(4)), String(cardNumber.suffix(4)))
        }
    }
}
