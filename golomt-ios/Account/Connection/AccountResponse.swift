//
//  AccountResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/25/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class AccountResponse: BaseResponse {
    let accountList: [Account]?
    
    private enum CodingKeys: String, CodingKey {
        case AccountSummary_REC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(accountList, forKey: .AccountSummary_REC)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.accountList = try container.decodeIfPresent([Account].self, forKey: .AccountSummary_REC)
        try super.init(from: decoder)
    }
    
    class Account: Codable, Hashable {
        static func == (lhs: AccountResponse.Account, rhs: AccountResponse.Account) -> Bool {
            return lhs.ACCT_NUMBER == rhs.ACCT_NUMBER
        }
        
        var ACCT_NICKNAME: String?
        var BRANCH_ID: String?
        var ACCT_TYPE: String?
        var ACCT_NUMBER: String?
        var ACCT_BALANCE: String?
        var MAIN_ACCT_TYPE: String?
        var ACCT_CURRENCY: String?
        var MATURITY_DATE: String?
        var PRODUCT_CATEGORY: String?
        var MINIMUM_AMOUNT_DUE_ARRAY: String?
        var ACCOUNT_CURRENT_BALANCE_ARRAY: String?
        var TOTAL_CREDIT_LIMIT: String?
        var TOTAL_AMOUNT_DUE: String?
        var AVAILABLE_CREDIT_LIMIT: String?
        var expanded: Bool = false
        
        func getMaskedNumber() -> String {
            let cardNumber = ACCT_NUMBER.orEmpty
            return "card_number_masked".localized(with: String(cardNumber.prefix(4)), String(cardNumber.suffix(4)))
        }
        
        private enum CodingKeys: String, CodingKey {
            case ACCT_NICKNAME, BRANCH_ID, ACCT_TYPE, ACCT_NUMBER, ACCT_BALANCE, MAIN_ACCT_TYPE, ACCT_CURRENCY, MATURITY_DATE, PRODUCT_CATEGORY, MINIMUM_AMOUNT_DUE_ARRAY, ACCOUNT_CURRENT_BALANCE_ARRAY, TOTAL_CREDIT_LIMIT, TOTAL_AMOUNT_DUE,
                AVAILABLE_CREDIT_LIMIT
        }
    }
}
