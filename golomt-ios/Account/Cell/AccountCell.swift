//
//  AccountCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/23/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import MGSwipeTableCell
import UIKit

class AccountCell: MGSwipeTableCell {
    @IBOutlet var containerView: UIView!
    @IBOutlet var menuStackView: UIStackView!
    @IBOutlet var accountContainerView: UIView!
    @IBOutlet var balanceLabel: UILabel!
    @IBOutlet var nicknameLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var accountSubContainerView: UIView!
    @IBOutlet var accountSubView: UIStackView!
    @IBOutlet var accountSubViewInfoLabel: UILabel!
    @IBOutlet var accountSubViewActionButton: UIButton!
    @IBOutlet var accountTypeImage: UIImageView!
    var onOptionSelect: ((_ option: OPTION_BUTTON?) -> Void)?
    private var account: AccountResponse.Account?

    @IBAction func accountSubViewActionAction(_ sender: Any) {
        switch account?.MAIN_ACCT_TYPE {
        case AccountConstants.credit:
            handleCCDPayment()
        case AccountConstants.deposit:
            handleDEPExtension()
        default:
            return
        }
    }

    enum OPTION_BUTTON: Int {
        case GENERAL_DETAIL = 0
        case GENERAL_STATEMENT = 1
        case OPR_CLOSE_ACCOUNT = 2
        case GENERAL_NICKNAME = 3
        case DEP_CLOSE_ACCOUNT = 4
        case DEP_LOAN = 5
        case CCD_PAYMENT = 6
        case CCD_STATUS = 7
        case CCD_PIN = 8
        case CCD_E_CODE = 9
        case LOAN_GRAPHIC = 10
        case LOAN_PAY = 11
        case LOAN_CLOSE = 12
        case DEP_EXTEND = 13
    }

    lazy var detailButton: UIButton = {
        optionButton(title: "option_general_detail", tag: OPTION_BUTTON.GENERAL_DETAIL)
    }()

    lazy var statementButton: UIButton = {
        optionButton(title: "option_general_statement", tag: .GENERAL_STATEMENT)
    }()

    lazy var nicknameButton: UIButton = {
        optionButton(title: "option_general_change_nickname", tag: .GENERAL_NICKNAME)
    }()

    lazy var OPRAccountCloseButton: UIButton = {
        optionButton(title: "option_operative_close_account", tag: .OPR_CLOSE_ACCOUNT)
    }()

    lazy var DEPAccountCloseButton: UIButton = {
        optionButton(title: "option_deposit_close_account", tag: .DEP_CLOSE_ACCOUNT)
    }()

    lazy var DEPAccountLoanButton: UIButton = {
        optionButton(title: "option_deposit_loan", tag: .DEP_LOAN)
    }()

    lazy var CCDAccountPaymentButton: UIButton = {
        optionButton(title: "option_credit_card_payment", tag: .CCD_PAYMENT)
    }()

    lazy var CCDAccountChangeStatusButton: UIButton = {
        optionButton(title: "option_credit_card_change_status", tag: .CCD_STATUS)
    }()

    lazy var CCDAccountPinCodeButton: UIButton = {
        optionButton(title: "option_credit_card_pin_code", tag: .CCD_PIN)
    }()

    lazy var CCDAccountECodeButton: UIButton = {
        optionButton(title: "option_credit_card_e_code", tag: .CCD_E_CODE)
    }()

//    lazy var CCDAccountRenewButton: UIButton = {
//        optionButton(title: "option_credit_card_renew", tag: 10)
//    }()

    lazy var LONAccountGraphicButton: UIButton = {
        optionButton(title: "option_loan_graphic", tag: .LOAN_GRAPHIC)
    }()

    lazy var LONAccountPayButton: UIButton = {
        optionButton(title: "option_loan_pay", tag: .LOAN_PAY)
    }()

    lazy var LONAccountCloseButton: UIButton = {
        optionButton(title: "option_loan_close", tag: .LOAN_CLOSE)
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }

    func setupButtons(buttons: [UIButton]) {
        menuStackView.arrangedSubviews.forEach { button in
            button.removeFromSuperview()
        }
        buttons.forEach { button in
            menuStackView.addArrangedSubview(button)
        }
    }

    func setDropBackground() {
        accountContainerView.backgroundColor = .defaultPurpleGradientEnd
        nicknameLabel.textColor = .white
        numberLabel.textColor = .white
        balanceLabel.textColor = .white
    }

    func setDefaultBackground() {
        accountContainerView.backgroundColor = .defaultSecondaryBackground
        nicknameLabel.textColor = .defaultPrimaryText
        numberLabel.textColor = .defaultSecondaryText
        balanceLabel.textColor = .defaultPrimaryText
    }

    func setupData(account: AccountResponse.Account) {
        self.account = account
        accountSubContainerView.isHidden = true
        setContainerBackgroundColor(color: .clear)
        switch account.MAIN_ACCT_TYPE {
        case AccountConstants.credit:
            if account.MINIMUM_AMOUNT_DUE_ARRAY.toAmount > 0.0 {
                accountSubViewActionButton?.setTitle("CCD_sub_view_button".localized(), for: .normal)
                switch getBalanceVisibility() {
                case true: accountSubViewInfoLabel?.text = "CCD_sub_view_info".localized(with: account.MINIMUM_AMOUNT_DUE_ARRAY.toAmountWithCurrencySymbol)
                case false: accountSubViewInfoLabel?.text = "CCD_sub_view_info".localized(with: "balance_invisible".localized(with: account.ACCT_CURRENCY.orEmpty.toCurrencySymbol))
                }
                accountSubContainerView.addTapGesture(tapNumber: 1, target: self, action: #selector(handleCCDPayment))
                setContainerBackgroundColor(color: .creditAccount)
                setupDataWithSub(account: account)
            }
        case AccountConstants.deposit:
            guard let maturityDate = account.MATURITY_DATE?.toBackwardDate else {
                break
            }
            if maturityDate < Date() {
                accountSubViewActionButton?.setTitle("DEP_sub_view_button".localized(), for: .normal)
                accountSubViewInfoLabel?.text = "DEP_sub_view_info".localized(with: account.MATURITY_DATE.backwardDateFormatted)
                accountSubContainerView.addTapGesture(tapNumber: 1, target: self, action: #selector(handleDEPExtension))
                setContainerBackgroundColor(color: .depositAccount)
                setupDataWithSub(account: account)
            }
        default: break
        }
        setBasicData(account: account)
    }

    @objc private func handleCCDPayment() {
        onOptionSelect?(.CCD_PAYMENT)
    }

    @objc private func handleDEPExtension() {
        onOptionSelect?(.DEP_EXTEND)
    }

    private func setupDataWithSub(account: AccountResponse.Account) {
        accountSubContainerView.isHidden = false
        accountSubViewActionButton?.backgroundColor = UIColor.clear.withAlphaComponent(0.3)
        accountSubViewActionButton?.corner(cornerRadius: 10)
        accountSubViewActionButton?.contentEdgeInsets = UIEdgeInsets(top: 2, left: 20, bottom: 2, right: 20)
        accountSubViewActionButton?.setTitleColor(.white, for: .normal)
        accountSubViewActionButton?.titleLabel?.useSmallFont()
        accountSubViewInfoLabel?.textColor = .white
        accountSubViewInfoLabel?.useSmallFont()
        accountSubView.layoutIfNeeded()
    }

    private func setBasicData(account: AccountResponse.Account) {
        nicknameLabel.text = account.ACCT_NICKNAME
        numberLabel.text = account.ACCT_NUMBER
        switch getBalanceVisibility() {
        case true: balanceLabel.attributedText = account.ACCT_BALANCE.orEmpty.toAmountTest
        case false: balanceLabel.text = "balance_invisible".localized(with: account.ACCT_CURRENCY.orEmpty.toCurrencySymbol)
        }
        switch account.MAIN_ACCT_TYPE {
        case AccountConstants.operative:
            accountTypeImage.image = UIImage(named: "operative")?.withRenderingMode(.alwaysOriginal)
        case AccountConstants.deposit:
            accountTypeImage.image = UIImage(named: "deposit")?.withRenderingMode(.alwaysOriginal)
        case AccountConstants.credit:
            accountTypeImage.image = UIImage(named: "creditcard")?.withRenderingMode(.alwaysOriginal)
            numberLabel.text = account.getMaskedNumber()
        case AccountConstants.loan:
            accountTypeImage.image = UIImage(named: "loan")?.withRenderingMode(.alwaysOriginal)
        default:
            break
        }
        switch account.expanded {
        case true:
            switch account.MAIN_ACCT_TYPE {
            case AccountConstants.operative:
                setContainerBackgroundColor(color: .operativeAccount)
                setupButtons(buttons: [statementButton, detailButton, OPRAccountCloseButton, nicknameButton])
            case AccountConstants.deposit:
                setContainerBackgroundColor(color: .depositAccount)
                setupButtons(buttons: [statementButton, detailButton, DEPAccountCloseButton, DEPAccountLoanButton, nicknameButton])
            case AccountConstants.credit:
                setContainerBackgroundColor(color: .creditAccount)
                setupButtons(buttons: [statementButton, detailButton, CCDAccountPaymentButton, CCDAccountChangeStatusButton, CCDAccountPinCodeButton, CCDAccountECodeButton, nicknameButton])
            case AccountConstants.loan:
                setContainerBackgroundColor(color: .loanAccount)
                setupButtons(buttons: [statementButton, detailButton, LONAccountGraphicButton, LONAccountPayButton, LONAccountCloseButton, nicknameButton])
            default: return
            }
        case false:
            setupButtons(buttons: [])
        }
    }

    private func setContainerBackgroundColor(color: UIColor) {
        containerView.backgroundColor = color
    }

    private func optionButton(title: String, tag: OPTION_BUTTON) -> UIButton {
        let button = UIButton(frame: .zero)
        button.setTitle(title.localized(), for: .normal)
        button.titleLabel?.useMediumFont()
        button.setTitleColor(.white, for: .normal)
        button.tag = tag.rawValue
        button.addBorder(toSide: .Bottom, withColor: .white, andThickness: 0.5)
        let heightAnchor = button.heightAnchor.constraint(equalToConstant: 50)
        heightAnchor.isActive = true
        heightAnchor.priority = UILayoutPriority(rawValue: 999)
        button.addTarget(self, action: #selector(handleOptionClick(_:)), for: .touchUpInside)
        return button
    }

    @objc private func handleOptionClick(_ sender: UIButton) {
        let tag = OPTION_BUTTON(rawValue: sender.tag)
        onOptionSelect?(tag)
    }

    private func initComponent() {
        accountContainerView.corner(cornerRadius: 10)
        containerView.corner(cornerRadius: 10)
        selectionStyle = .none
        balanceLabel.useLargeFont()
        balanceLabel.makeBold()
        balanceLabel.textColor = .defaultPrimaryText
        balanceLabel.adjustsFontSizeToFitWidth = true
        balanceLabel.numberOfLines = 1
        numberLabel.useSmallFont()
        numberLabel.textColor = .defaultSecondaryText
        nicknameLabel.useSmallFont()
        nicknameLabel.makeBold()
        nicknameLabel.textColor = .defaultPrimaryText
        touchOnDismissSwipe = true
    }
}
