//
//  AccountCollectionCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/9/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class AccountCollectionCell: UITableViewCell {
    @IBOutlet var containerView: UIView!
    @IBOutlet var accountStackView: UIStackView!
    @IBOutlet var currencyLabel: UILabel!
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var balanceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }

    func setupData(type: String, accounts: [AccountResponse.Account]) {
        accountStackView.arrangedSubviews.forEach { subView in
            subView.removeFromSuperview()
        }
        switch type {
        case AccountConstants.operative:
            containerView.backgroundColor = .operativeAccount
        case AccountConstants.deposit:
            containerView.backgroundColor = .depositAccount
        case AccountConstants.credit:
            containerView.backgroundColor = .creditAccount
        case AccountConstants.loan:
            containerView.backgroundColor = .loanAccount
        default: break
        }

        var collections = [AccountCollectionModel]()
        accounts.forEach { account in
            var collectionExist = false
            for collection in collections {
                if collection.currency == account.ACCT_CURRENCY {
                    collection.count += 1
                    let balance = (collection.balance.toDouble + account.ACCT_BALANCE.toAmount)
                    collection.balance = balance.formattedWithComma
                    collectionExist = true
                    break
                }
            }
            if !collectionExist {
                let collectionModel = AccountCollectionModel(currency: account.ACCT_CURRENCY.orEmpty, count: 1, info: "", balance: account.ACCT_BALANCE.toAmount.formattedWithComma)
                collections.append(collectionModel)
            }
        }

        collections.forEach { collection in
            let firstLabel = UILabel()
            let thirdLabel = UILabel()
            let fourthLabel = UILabel()
            let horizontalStackView = UIStackView()
            horizontalStackView.axis = .horizontal
            horizontalStackView.alignment = .fill
            horizontalStackView.distribution = .fillProportionally
            accountStackView.addArrangedSubview(horizontalStackView)
            let widthAnchor = accountStackView.widthAnchor
            horizontalStackView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
            firstLabel.textColor = .defaultPrimaryText
            thirdLabel.textColor = .defaultPrimaryText
            fourthLabel.textColor = .defaultPrimaryText
            firstLabel.useSmallFont()
            fourthLabel.useLargeFont()
            fourthLabel.makeBold()
            thirdLabel.textAlignment = .center
            fourthLabel.textAlignment = .right
            firstLabel.text = "collection_view_count_label".localized(with: collection.currency, collection.count)
            thirdLabel.text = collection.info
            switch getBalanceVisibility() {
            case true: fourthLabel.text = collection.balance
            case false: fourthLabel.text = "balance_invisible".localized(with: "")
            }
            horizontalStackView.addArrangedSubview(firstLabel)
//            horizontalStackView.addArrangedSubview(thirdLabel)
            horizontalStackView.addArrangedSubview(fourthLabel)
            let hWidthAnchor = horizontalStackView.widthAnchor
            firstLabel.widthAnchor.constraint(equalTo: hWidthAnchor, multiplier: 0.2).isActive = true
//            thirdLabel.widthAnchor.constraint(equalTo: hWidthAnchor, multiplier: 0.35).isActive = true
            fourthLabel.widthAnchor.constraint(equalTo: hWidthAnchor, multiplier: 0.8).isActive = true
        }

        let infoLabelLocalize = "collection_view_info_label_" + type
        infoLabel.text = infoLabelLocalize.localized()
    }

    private func initComponent() {
        containerView.corner(cornerRadius: 10)
        selectionStyle = .none
        currencyLabel.textColor = .white
        infoLabel.textColor = .white
        balanceLabel.textColor = .white
        currencyLabel.text = "collection_view_currency_label".localized()
        balanceLabel.text = "collection_view_balance_label".localized()
        currencyLabel.useSmallFont()
        infoLabel.useSmallFont()
        balanceLabel.useSmallFont()
    }
}
