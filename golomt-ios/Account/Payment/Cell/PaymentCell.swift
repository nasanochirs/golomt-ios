//
//  PaymentCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/6/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class PaymentCell: UITableViewCell {
    @IBOutlet var containerView: UIView!
    @IBOutlet var paymentNameLabel: UILabel!
    @IBOutlet var paymentOrganizationLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }

    func setupPayment(_ payment: PaymentResponse.Payment) {
        paymentNameLabel.text = payment.BILLERS_NICK_NAME
        paymentOrganizationLabel.text = payment.BILLERS_NAME
    }

    func setDropBackground() {
        containerView.backgroundColor = .defaultPurpleGradientEnd
        paymentNameLabel.textColor = .white
        paymentOrganizationLabel.textColor = .white
    }

    func setDefaultBackground() {
        containerView.backgroundColor = .defaultSecondaryBackground
        paymentNameLabel.textColor = .defaultPrimaryText
        paymentOrganizationLabel.textColor = .defaultSecondaryText
    }

    private func initComponent() {
        selectionStyle = .none
        containerView.corner(cornerRadius: 10)
        paymentOrganizationLabel.useSmallFont()
        paymentOrganizationLabel.textColor = .defaultSecondaryText
        paymentNameLabel.useMediumFont()
        paymentNameLabel.textColor = .defaultPrimaryText
    }
}
