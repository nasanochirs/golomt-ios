//
//  PaymentUnitCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/5/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class PaymentUnitCell: UITableViewCell {
    @IBOutlet var containerView: UIView!
    @IBOutlet var unitImage: UIImageView!
    @IBOutlet var unitLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }

    func setDropBackground() {
        containerView.backgroundColor = .defaultPurpleGradientEnd
        unitLabel.textColor = .white
    }

    func setDefaultBackground() {
        containerView.backgroundColor = .defaultSecondaryBackground
        unitLabel.textColor = .defaultPrimaryText
    }

    private func initComponent() {
        selectionStyle = .none
        unitLabel.text = "buy_unit".localized()
        unitLabel.textColor = .defaultPrimaryText
        containerView.corner(cornerRadius: 10)
    }
}
