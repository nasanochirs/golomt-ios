//
//  Model.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/5/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class PaymentModel: NSObject {
    var sections = [PaymentSection]()
}

struct PaymentSection {
    var type: String
    var payments: [PaymentResponse.Payment]

    func getPaymentHeader() -> String {
        return (type + "_header").localized()
    }
}
