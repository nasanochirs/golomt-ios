//
//  PaymentResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/6/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class PaymentResponse: BaseResponse {
    let paymentList: [Payment]?
    
    private enum CodingKeys: String, CodingKey {
        case BillerRegistrationList_REC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(paymentList, forKey: .BillerRegistrationList_REC)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.paymentList = try container.decodeIfPresent([Payment].self, forKey: .BillerRegistrationList_REC)
        try super.init(from: decoder)
    }
    
    struct Payment: Codable {
        var BILLERS_NICK_NAME: String?
        var BILLERS_NAME: String?
        var CONSUMER_CODE_DET: String?
        var SUBSCRIPTION_IDS: String?
        var BNF_ID: String?
        var BILLER_CARD_TYPE: String?
        var SUBSCRIPTION_START_DATE: String?
        var PAYMENT_START_DATE: String?
        var PART_PYMT_FLG: String?
        var OTP_FLG: String?
        var LATE_PYMT_FLG: String?
        
        func getPaymentDate() -> Int {
            return PAYMENT_START_DATE?.components(separatedBy: "-").last.toInt ?? 0
        }
    }
}
