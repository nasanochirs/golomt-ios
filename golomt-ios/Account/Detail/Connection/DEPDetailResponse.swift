//
//  DEPDetailResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/17/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class DEPDetailResponse: BaseResponse {
    let detail: Detail?
    let maturity: Maturity?
    let description: Description?
    let balance: Balance?
    
    private enum CodingKeys: String, CodingKey {
        case GeneralDetails
        case MaturityInstructions
        case CodeDescription
        case BalanceDetails
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(detail, forKey: .GeneralDetails)
        try container.encode(description, forKey: .CodeDescription)
        try container.encode(balance, forKey: .BalanceDetails)
        try container.encode(maturity, forKey: .MaturityInstructions)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.detail = try container.decodeIfPresent(Detail.self, forKey: .GeneralDetails)
        self.description = try container.decodeIfPresent(Description.self, forKey: .CodeDescription)
        self.balance = try container.decodeIfPresent(Balance.self, forKey: .BalanceDetails)
        self.maturity = try container.decodeIfPresent(Maturity.self, forKey: .MaturityInstructions)
        try super.init(from: decoder)
    }
    
    struct Detail: Codable {
        var ACCOUNT_ID: String?
        var ACCOUNT_STATUS: String?
        var ACCOUNT_NICKNAME: String?
        var ACCOUNT_NAME: String?
        var ACCOUNT_OPEN_DATE: String?
        var ACCOUNT_CATEGORY: String?
        var ACCOUNT_TYPE: String?
        var BRANCH_CODE: String?
        var BRANCH_DESC: String?
        var CURRENCY_CODE: String?
        var INTEREST_RATE: String?
        var DEP_PERIOD_MONTHS: String?
        var MATURITY_DATE: String?
        var MATURITY_AMOUNT: String?
    }
    
    struct Maturity: Codable {
        var AUTO_RENEW: String?
        var AUTO_RENEW_DAY: String?
        var AUTO_RENEW_MONTH: String?
        var AUTO_RENEW_COUNTER: String?
        var AUTO_CLOSURE: String?
    }
    
    struct Description: Codable {
        var ACCOUNT_CATEGORY_DESC: String?
        var ACCOUNT_TYPE_DESC: String?
        var ACCOUNT_STATUS_DESC: String?
        var AUTO_RENEW_DESC: String?
    }
    
    struct Balance: Codable {
        var LEDGER_BAL: String?
        var NRML_BOOKED_AMOUNT_CR: String?
        var CURRENT_BAL: String?
        var OTHER_BAL: String?
        var SBL_INTEREST_RATE: String?
    }
}
