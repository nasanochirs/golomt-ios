//
//  CCDDetailResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/18/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class CCDDetailResponse: BaseResponse {
    let creditCards: [CreditAccountResponse.CreditAccount]?
    let reward: Reward?
    
    private enum CodingKeys: String, CodingKey {
        case CreditCardList_REC
        case RewardPointDetails
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(creditCards, forKey: .CreditCardList_REC)
        try container.encode(reward, forKey: .RewardPointDetails)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.creditCards = try container.decodeIfPresent([CreditAccountResponse.CreditAccount].self, forKey: .CreditCardList_REC)
        self.reward = try container.decodeIfPresent(Reward.self, forKey: .RewardPointDetails)
        try super.init(from: decoder)
    }
    
    struct Reward: Codable {
        var CLOSING_BALANCE: String?
        var REDEEMED_POINTS: String?
        var EARNED_POINT: String?
        var OPENING_BALANCE: String?
        var PAYMENT_DUE_DATE: String?
    }
}
