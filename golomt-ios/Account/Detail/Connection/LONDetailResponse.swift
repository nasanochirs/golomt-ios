//
//  LONDetailResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/17/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LONDetailResponse: BaseResponse {
    let detail: Detail?
    let description: Description?
    let collateral: [Collateral]?
    let repayment: Repayment?
    
    private enum CodingKeys: String, CodingKey {
        case GeneralDetails
        case CollactralDetails_REC
        case CodeDescription
        case RepaymentDetails
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(detail, forKey: .GeneralDetails)
        try container.encode(description, forKey: .CodeDescription)
        try container.encode(collateral, forKey: .CollactralDetails_REC)
        try container.encode(repayment, forKey: .RepaymentDetails)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.detail = try container.decodeIfPresent(Detail.self, forKey: .GeneralDetails)
        self.description = try container.decodeIfPresent(Description.self, forKey: .CodeDescription)
        self.collateral = try container.decodeIfPresent([Collateral].self, forKey: .CollactralDetails_REC)
        self.repayment = try container.decodeIfPresent(Repayment.self, forKey: .RepaymentDetails)
        try super.init(from: decoder)
    }
    
    struct Detail: Codable {
        var ACCOUNT_ID: String?
        var CURRENCY_CODE: String?
        var ACCOUNT_OPEN_DATE: String?
        var LOAN_END_DATE: String?
        var ACCOUNT_STATUS: String?
        var ACCOUNT_CATEGORY: String?
        var ACCOUNT_TYPE: String?
        var ACCOUNT_NICKNAME: String?
        var ACCOUNT_NAME: String?
        var BRANCH_DESC: String?
        var BRANCH_CODE: String?
        var INTEREST_RATE: String?
        var LOAN_PERIOD_MONTHS: String?
        var LIABILITY_AMT: String?
        var DISBURSE_AMT: String?
        var OVERDUE_AMT: String?
        var INTEREST_OVER_DUE: String?
        var PENAL_INTEREST_OVER_DUE: String?
        var DAYS_PAST_DUE: String?
        var NEXT_INSTALLMENT_DATE: String?
        var NEXT_INSTALLMENT_AMOUNT: String?
    }
    
    struct Description: Codable {
        var ACCOUNT_CATEGORY_DESC: String?
        var ACCOUNT_TYPE_DESC: String?
        var ACCOUNT_STATUS_DESC: String?
    }
    
    struct Collateral: Codable {
        var COLLATERAL_CODE_NAME_ARRAY: String?
        var COLLATERAL_VALUE_ARRAY: String?
        var PROPERTY_DOCUMENT_NUMBER_ARRAY: String?
        var CURRENCY_ARRAY: String?
    }
    
    struct Repayment: Codable {
        var PRINICIPLE_OVERDUE: String?
        var LAST_REPAYMENT_DATE: String?
    }
}
