//
//  OPRDetailResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/17/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class OPRDetailResponse: BaseResponse {
    let detail: Detail?
    let description: Description?
    let balance: Balance?
    
    private enum CodingKeys: String, CodingKey {
        case GeneralDetails
        case CodeDescription
        case BalanceDetails
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(detail, forKey: .GeneralDetails)
        try container.encode(description, forKey: .CodeDescription)
        try container.encode(balance, forKey: .BalanceDetails)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.detail = try container.decodeIfPresent(Detail.self, forKey: .GeneralDetails)
        self.description = try container.decodeIfPresent(Description.self, forKey: .CodeDescription)
        self.balance = try container.decodeIfPresent(Balance.self, forKey: .BalanceDetails)
        try super.init(from: decoder)
    }
    
    struct Detail: Codable {
        var ACCOUNT_ID: String?
        var ACCOUNT_CRN: String?
        var AC_OPN_DT: String?
        var ACCOUNT_STATUS: String?
        var ACCOUNT_TYPE: String?
        var ACC_NICK_NAME: String?
        var ACC_NAME: String?
        var BRANCH_DESC: String?
    }
    
    struct Description: Codable {
        var ACCOUNT_CAT_DESC: String?
        var ACCOUNT_TYPE_DESC: String?
        var ACCOUNT_STATUS_DESC: String?
    }
    
    struct Balance: Codable {
        var CUSTOM_AVAILABLE_BAL: String?
        var LEDGER_BAL: String?
        var LIEN_BAL: String?
        var NRML_BOOKED_AMOUNT_CR: String?
    }
}
