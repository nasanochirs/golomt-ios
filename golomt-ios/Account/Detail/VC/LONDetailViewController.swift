//
//  LONDetailViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/17/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class LONDetailViewController: TableViewController {
    var account: AccountResponse.Account?

    lazy var detailSection: TableViewModel.Section = {
        var section = TableViewModel.Section(
            title: "lon_detail_general_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "lon_detail_account_nickname_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "lon_detail_account_name_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "lon_detail_account_number_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "lon_detail_account_duration_title".localized(), info: ""
                )
            ]
        )
        return section
    }()

    lazy var extraSection: TableViewModel.Section = {
        var section = TableViewModel.Section(
            title: "lon_detail_product_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "lon_detail_account_status_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "lon_detail_account_category_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "lon_detail_account_type_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "lon_detail_account_open_date_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "lon_detail_account_branch_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "lon_detail_account_close_date".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "lon_detail_account_interest_rate".localized(), info: ""
                )
            ]
        )
        return section
    }()

    lazy var balanceSection: TableViewModel.Section = {
        var section = TableViewModel.Section(
            title: "lon_detail_balance_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "lon_detail_total_loan_amount_title".localized(), info: "", infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "lon_detail_account_loan_title".localized(), info: "", infoProperty: .DefaultAmount
                )
            ]
        )
        return section
    }()

    lazy var overDueSection: TableViewModel.Section = {
        var section = TableViewModel.Section(
            title: "lon_detail_overdue_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "lon_detail_account_amount_to_pay_title".localized(), info: "", infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "lon_detail_account_total_amount_title".localized(), info: "", infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "lon_detail_account_interest_amount_title".localized(), info: "", infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "lon_detail_account_penalty_interest_title".localized(), info: "", infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "lon_detail_account_overdue_day_title".localized(), info: ""
                )
            ]
        )
        return section
    }()

    lazy var installmentSection: TableViewModel.Section = {
        var section = TableViewModel.Section(
            title: "lon_detail_installment_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "lon_detail_account_next_installment_date_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "lon_detail_account_installment_amount_title".localized(), info: "", infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "lon_detail_account_last_installment_date_title".localized(), info: ""
                )
            ]
        )
        return section
    }()

    lazy var collateralSection: TableViewModel.Section = {
        var section = TableViewModel.Section(
            title: "lon_detail_collateral_details_title".localized(),
            rows: [
            ]
        )
        return section
    }()

    lazy var copyButton: UIBarButtonItem? = {
        UIBarButtonItem(image: UIImage(named: "copy"), style: .plain, target: self, action: #selector(handleCopy))
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "lon_detail_title".localized()
        hasButton = false
        model.sections = [self.detailSection, self.extraSection, self.balanceSection, self.overDueSection, self.installmentSection]
        tableView.reloadData()
        navigationItem.rightBarButtonItem = self.copyButton
        showLoader()
        ConnectionFactory.fetchLONDetail(
            account: selectedAccount,
            success: { response in
                self.hideLoader()
                self.detailSection.rows[0].info = response.detail?.ACCOUNT_NICKNAME
                self.detailSection.rows[1].info = response.detail?.ACCOUNT_NAME
                self.detailSection.rows[2].info = response.detail?.ACCOUNT_ID
                self.detailSection.rows[3].info = response.detail?.LOAN_PERIOD_MONTHS
                self.extraSection.rows[0].info = response.description?.ACCOUNT_STATUS_DESC
                self.extraSection.rows[1].info = response.description?.ACCOUNT_CATEGORY_DESC
                self.extraSection.rows[2].info = response.description?.ACCOUNT_TYPE_DESC
                self.extraSection.rows[3].info = response.detail?.ACCOUNT_OPEN_DATE
                self.extraSection.rows[4].info = response.detail?.BRANCH_DESC
                self.extraSection.rows[5].info = response.detail?.LOAN_END_DATE
                self.extraSection.rows[6].info = response.detail?.INTEREST_RATE
                self.balanceSection.rows[0].info = response.detail?.LIABILITY_AMT?.toAmountWithCurrencySymbol
                self.balanceSection.rows[1].info = response.detail?.DISBURSE_AMT?.toAmountWithCurrencySymbol
                self.overDueSection.rows[0].info = response.detail?.OVERDUE_AMT?.toAmountWithCurrencySymbol
                self.overDueSection.rows[1].info = response.repayment?.PRINICIPLE_OVERDUE?.toAmountWithCurrencySymbol
                self.overDueSection.rows[2].info = response.detail?.INTEREST_OVER_DUE?.toAmountWithCurrencySymbol
                self.overDueSection.rows[3].info = response.detail?.PENAL_INTEREST_OVER_DUE?.toAmountWithCurrencySymbol
                self.overDueSection.rows[4].info = response.detail?.DAYS_PAST_DUE
                self.installmentSection.rows[0].info = response.detail?.NEXT_INSTALLMENT_DATE
                self.installmentSection.rows[1].info = response.detail?.NEXT_INSTALLMENT_AMOUNT?.toAmountWithCurrencySymbol
                self.installmentSection.rows[2].info = response.repayment?.LAST_REPAYMENT_DATE
                response.collateral?.forEach { collateral in
                    self.collateralSection.rows.append(
                        contentsOf: [
                            TableViewModel.Row(
                                title: "lon_detail_account_cloan_no_title".localized(),
                                info: collateral.PROPERTY_DOCUMENT_NUMBER_ARRAY
                            ),
                            TableViewModel.Row(
                                title: "lon_detail_account_cloan_name_title".localized(),
                                info: collateral.COLLATERAL_CODE_NAME_ARRAY
                            ),
                            TableViewModel.Row(
                                title: "lon_detail_account_cloan_amount_title".localized(),
                                info: collateral.COLLATERAL_VALUE_ARRAY, infoProperty: .DefaultAmount
                            )
                        ]
                    )
                }
                if self.collateralSection.rows.count > 0 {
                    self.model.sections.append(self.collateralSection)
                }
                self.tableView.reloadData()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }

    @objc private func handleCopy() {
        let number = self.detailSection.rows[2].info ?? ""
        let name = self.detailSection.rows[1].info ?? ""
        UIPasteboard.general.string = "account_detail_copy".localized(with: number, name)
        self.present(infoDialog(message: "account_detail_copy_success".localized()), animated: true, completion: nil)
    }
}
