//
//  OPRDetailViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/17/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class OPRDetailViewController: TableViewController {
    var account: AccountResponse.Account?

    lazy var detailSection: TableViewModel.Section = {
        var section = TableViewModel.Section(
            title: "opr_detail_general_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "opr_detail_account_nickname_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "opr_detail_account_name_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "opr_detail_account_number_title".localized(), info: ""
                )
            ]
        )
        return section
    }()

    lazy var extraSection: TableViewModel.Section = {
        var section = TableViewModel.Section(
            title: "opr_detail_product_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "opr_detail_account_status_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "opr_detail_account_category_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "opr_detail_account_open_date_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "opr_detail_account_branch_title".localized(), info: ""
                )
            ]
        )
        return section
    }()

    lazy var balanceSection: TableViewModel.Section = {
        var section = TableViewModel.Section(
            title: "opr_detail_balance_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "opr_detail_account_accumulated_interest_title".localized(), info: "", infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "opr_detail_account_available_balance_title".localized(), info: "", infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "opr_detail_account_lien_balance_title".localized(), info: "", infoProperty: .DefaultAmount
                )
            ]
        )
        return section
    }()

    lazy var copyButton: UIBarButtonItem? = {
        UIBarButtonItem(image: UIImage(named: "copy"), style: .plain, target: self, action: #selector(handleCopy))
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "opr_detail_title".localized()
        navigationItem.rightBarButtonItem = copyButton
        hasButton = false
        model.sections = [detailSection, extraSection, balanceSection]
        tableView.reloadData()
        showLoader()
        ConnectionFactory.fetchOPRDetail(
            number: selectedAccount?.ACCT_NUMBER ?? "",
            success: { response in
                self.hideLoader()
                self.detailSection.rows[0].info = response.detail?.ACC_NICK_NAME
                self.detailSection.rows[1].info = response.detail?.ACC_NAME
                self.detailSection.rows[2].info = response.detail?.ACCOUNT_ID
                self.extraSection.rows[0].info = response.description?.ACCOUNT_STATUS_DESC
                self.extraSection.rows[1].info = response.description?.ACCOUNT_CAT_DESC
                self.extraSection.rows[2].info = response.detail?.AC_OPN_DT
                self.extraSection.rows[3].info = response.detail?.BRANCH_DESC
                self.balanceSection.rows[0].info = response.balance?.NRML_BOOKED_AMOUNT_CR?.toAmountWithCurrencySymbol
                self.balanceSection.rows[1].info = response.balance?.CUSTOM_AVAILABLE_BAL?.toAmountWithCurrencySymbol
                self.balanceSection.rows[2].info = response.balance?.LIEN_BAL?.toAmountWithCurrencySymbol
                self.tableView.reloadData()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
    
    @objc private func handleCopy() {
        let number = self.detailSection.rows[2].info ?? ""
        let name = self.detailSection.rows[1].info ?? ""
        UIPasteboard.general.string = "account_detail_copy".localized(with: number, name)
        self.present(infoDialog(message: "account_detail_copy_success".localized()), animated: true, completion: nil)
    }
}
