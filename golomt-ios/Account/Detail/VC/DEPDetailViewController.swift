//
//  DEPDetailViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/17/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class DEPDetailViewController: TableViewController {
    var account: AccountResponse.Account?

    lazy var detailSection: TableViewModel.Section = {
        var section = TableViewModel.Section(
            title: "dep_detail_general_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "dep_detail_account_nickname_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_name_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_number_title".localized(), info: ""
                )
            ]
        )
        return section
    }()

    lazy var extraSection: TableViewModel.Section = {
        var section = TableViewModel.Section(
            title: "dep_detail_product_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "dep_detail_account_status_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_category_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_open_date_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_branch_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_yearly_interest_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_duration_month_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_maturity_date".localized(), info: ""
                )
            ]
        )
        return section
    }()

    lazy var balanceSection: TableViewModel.Section = {
        var section = TableViewModel.Section(
            title: "dep_detail_balance_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "dep_detail_account_accumulated_interest_title".localized(), info: "", infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_available_balance_title".localized(), info: "", infoProperty: .DefaultAmount
                )
            ]
        )
        return section
    }()

    lazy var autoRenewSection: TableViewModel.Section = {
        var section = TableViewModel.Section(
            title: "dep_detail_account_auto_renew_details".localized(),
            rows: [
                TableViewModel.Row(
                    title: "dep_detail_account_auto_renew_has".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_auto_renew_monthly".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_auto_close".localized(), info: ""
                )
            ]
        )
        return section
    }()

    lazy var copyButton: UIBarButtonItem? = {
        UIBarButtonItem(image: UIImage(named: "copy"), style: .plain, target: self, action: #selector(handleCopy))
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "dep_detail_title".localized()
        hasButton = false
        model.sections = [self.detailSection, self.extraSection, self.balanceSection, self.autoRenewSection]
        tableView.reloadData()
        navigationItem.rightBarButtonItem = self.copyButton
        showLoader()
        ConnectionFactory.fetchDEPDetail(
            account: selectedAccount,
            isLoan: false,
            success: { response in
                self.hideLoader()
                self.detailSection.rows[0].info = response.detail?.ACCOUNT_NICKNAME
                self.detailSection.rows[1].info = response.detail?.ACCOUNT_NAME
                self.detailSection.rows[2].info = response.detail?.ACCOUNT_ID
                self.extraSection.rows[0].info = response.description?.ACCOUNT_STATUS_DESC
                self.extraSection.rows[1].info = response.description?.ACCOUNT_CATEGORY_DESC
                self.extraSection.rows[2].info = response.detail?.ACCOUNT_OPEN_DATE
                self.extraSection.rows[3].info = response.detail?.BRANCH_DESC
                self.extraSection.rows[4].info = response.detail?.INTEREST_RATE
                self.extraSection.rows[5].info = response.detail?.DEP_PERIOD_MONTHS
                self.extraSection.rows[6].info = response.detail?.MATURITY_DATE
                self.balanceSection.rows[0].info = response.balance?.NRML_BOOKED_AMOUNT_CR?.toAmountWithCurrencySymbol
                self.balanceSection.rows[1].info = response.balance?.LEDGER_BAL?.toAmountWithCurrencySymbol
                self.autoRenewSection.rows[0].info = response.description?.AUTO_RENEW_DESC
                self.autoRenewSection.rows[1].info = response.maturity?.AUTO_RENEW_MONTH
                self.autoRenewSection.rows[2].info = response.maturity?.AUTO_CLOSURE
                self.tableView.reloadData()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }

    @objc private func handleCopy() {
        let number = self.detailSection.rows[2].info ?? ""
        let name = self.detailSection.rows[1].info ?? ""
        UIPasteboard.general.string = "account_detail_copy".localized(with: number, name)
        self.present(infoDialog(message: "account_detail_copy_success".localized()), animated: true, completion: nil)
    }
}
