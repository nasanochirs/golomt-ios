//
//  CCDDetailViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/18/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class CCDDetailViewController: TableViewController {
    var account: AccountResponse.Account?

    lazy var detailSection: TableViewModel.Section = {
        var section = TableViewModel.Section(
            title: "ccd_detail_general_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "ccd_detail_account_name_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "ccd_detail_account_number_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "ccd_detail_account_category_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "ccd_detail_account_expire_date_title".localized(), info: ""
                )
            ]
        )
        return section
    }()

    lazy var balanceSection: TableViewModel.Section = {
        var section = TableViewModel.Section(
            title: "ccd_detail_balance_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "ccd_detail_account_credit_limit_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "ccd_detail_account_current_balance_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "ccd_detail_account_minimum_balance_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "ccd_detail_account_cash_limit_title".localized(), info: ""
                ),
                TableViewModel.Row(
                    title: "ccd_detail_account_due_date_title".localized(), info: ""
                )
            ]
        )
        return section
    }()

    lazy var rewardSection: TableViewModel.Section = {
        var section = TableViewModel.Section(
            title: "ccd_detail_reward_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "ccd_detail_account_opening_balance_title".localized(), info: "", infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "ccd_detail_account_earned_point_title".localized(), info: "", infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "ccd_detail_account_redeemed_point_title".localized(), info: "", infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "ccd_detail_account_closing_balance_title".localized(), info: "", infoProperty: .DefaultAmount
                )
            ]
        )
        return section
    }()

    lazy var copyButton: UIBarButtonItem? = {
        UIBarButtonItem(image: UIImage(named: "copy"), style: .plain, target: self, action: #selector(handleCopy))
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "opr_detail_title".localized()
        hasButton = false
        model.sections = [self.detailSection, self.balanceSection]
        tableView.reloadData()
        navigationItem.rightBarButtonItem = self.copyButton
        showLoader()
        ConnectionFactory.fetchCCDDetail(
            account: selectedAccount,
            success: { response in
                self.hideLoader()
                let credit = response.creditCards?.first
                self.detailSection.rows[0].info = credit?.CARD_HOLDER
                self.detailSection.rows[1].info = credit?.getMaskedNumber()
                self.detailSection.rows[2].info = credit?.CARD_PRODUCTDESC_ARRAY
                self.detailSection.rows[3].info = credit?.CARD_EXPIREDDATE_ARRAY
                self.balanceSection.rows[0].info = credit?.TOTAL_CREDIT_LIMIT?.toAmountWithCurrencySymbol
                self.balanceSection.rows[1].info = credit?.TOTAL_AMOUNT_DUE?.toAmountWithCurrencySymbol
                self.balanceSection.rows[2].info = credit?.MINIMUM_AMOUNT_DUE_ARRAY?.toAmountWithCurrencySymbol
                self.balanceSection.rows[3].info = credit?.AVAILABLE_CASH_LIMIT?.toAmountWithCurrencySymbol
                self.balanceSection.rows[4].info = response.reward?.PAYMENT_DUE_DATE
                if credit?.CREDIT_CARD_NUMBER?.prefix(1) == "3" {
                    self.rewardSection.rows[0].info = response.reward?.OPENING_BALANCE
                    self.rewardSection.rows[1].info = response.reward?.EARNED_POINT
                    self.rewardSection.rows[2].info = response.reward?.REDEEMED_POINTS
                    self.rewardSection.rows[3].info = response.reward?.CLOSING_BALANCE
                    self.model.sections.append(self.rewardSection)
                }
                self.tableView.reloadData()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }

    private func initComponent() {
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.registerCell(nibName: "DefaultTitleLabelCell")
        tableView.register(UINib(nibName: "BaseHeaderCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "BaseHeaderCell")
        tableView.estimatedSectionHeaderHeight = 100
        tableView.tableFooterView = UIView()
    }

    @objc private func handleCopy() {
        let number = self.detailSection.rows[1].info ?? ""
        let name = self.detailSection.rows[0].info ?? ""
        UIPasteboard.general.string = "account_detail_copy".localized(with: number, name)
        self.present(infoDialog(message: "account_detail_copy_success".localized()), animated: true, completion: nil)
    }
}
