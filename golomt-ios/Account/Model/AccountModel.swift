//
//  AccountModel.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/26/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class AccountModel: NSObject {
    var sections = [AccountSection]()
    var previousExpandedAccountIndex: IndexPath?
    var previousDestinationAccountIndex: IndexPath?
    var previousDestinationTableView: UITableView?

    lazy var accountQueue: Queue = {
        initAccountQueue()
    }()
}

class AccountSection {
    var type: String = ""
    var accounts = [AccountResponse.Account]()
    var collectionView: Bool = false

    init(type: String, accounts: [AccountResponse.Account]) {
        self.type = type
        self.accounts = accounts
    }

    func getAccountHeader() -> String {
        return (type + "_header").localized()
    }
}

class AccountCollectionModel {
    init(currency: String, count: Int, info: String, balance: String) {
        self.currency = currency
        self.count = count
        self.info = info
        self.balance = balance
    }

    var currency: String
    var count: Int
    var info: String
    var balance: String
}
