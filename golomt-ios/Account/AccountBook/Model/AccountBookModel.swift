//
//  AccountBookModel.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/1/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class AccountBookModel: NSObject {
    var sections = [AccountBookSection]()

    lazy var accountBookQueue: Queue = {
        initAccountBookQueue()
    }()
}

struct AccountBookSection {
    var type: String
    var accountBooks: [AccountBookResponse.AccountBook]

    func getAccountBookHeader() -> String {
        return (type + "_header").localized()
    }
}
