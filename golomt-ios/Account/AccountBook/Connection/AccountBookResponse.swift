//
//  AccountBookResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/1/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class AccountBookResponse: BaseResponse {
    let accountBookList: [AccountBook]?
    
    private enum CodingKeys: String, CodingKey {
        case CounterPartyList_REC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(accountBookList, forKey: .CounterPartyList_REC)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.accountBookList = try container.decodeIfPresent([AccountBook].self, forKey: .CounterPartyList_REC)
        try super.init(from: decoder)
    }
    
    struct AccountBook: Codable {
        var BNIF_ACCNT_NUMBER: String?
        var BNIF_NAME: String?
        var BNF_ACCT_CRN: String?
        var BNK_IDENTIFIER: String?
        var BNIF_NICK_NAME: String?
        var PAYEE_LIST_ID: String?
        var BNIF_ID: String?
    }
}
