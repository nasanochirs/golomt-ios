//
//  AccountBookCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/1/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class AccountBookCell: UITableViewCell {
    @IBOutlet var containerView: UIView!
    @IBOutlet var nicknameLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var bankImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }

    func setData(type: String, accountBook: AccountBookResponse.AccountBook) {
        nicknameLabel.text = accountBook.BNIF_NICK_NAME
        numberLabel.text = accountBook.BNIF_ACCNT_NUMBER
        nameLabel.text = accountBook.BNIF_NAME
        if type == TransactionNetworkConstants.golomt.rawValue {
            bankImage.image = UIImage(named: "golomt_logo")
            return
        }
        if let identifier = accountBook.BNK_IDENTIFIER {
            bankImage.image = UIImage(named: identifier)
        }
    }

    func setDropBackground() {
        containerView.backgroundColor = .defaultPurpleGradientEnd
        nameLabel.textColor = .white
        nicknameLabel.textColor = .white
        numberLabel.textColor = .white
    }

    func setDefaultBackground() {
        containerView.backgroundColor = .defaultSecondaryBackground
        nameLabel.textColor = .defaultPrimaryText
        nicknameLabel.textColor = .defaultPrimaryText
        numberLabel.textColor = .defaultSecondaryText
    }

    private func initComponent() {
        containerView.corner(cornerRadius: 10)
        selectionStyle = .none
        nicknameLabel.useSmallFont()
        nicknameLabel.makeBold()
        nicknameLabel.textColor = .defaultPrimaryText
        nicknameLabel.adjustsFontSizeToFitWidth = true
        nicknameLabel.numberOfLines = 1
        numberLabel.useSmallFont()
        numberLabel.textColor = .defaultSecondaryText
        nameLabel.useMediumFont()
        nameLabel.textColor = .defaultPrimaryText
    }
}
