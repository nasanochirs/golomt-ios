//
//  AccountViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/23/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import MGSwipeTableCell
import UIKit

class AccountViewController: BaseUIViewController {
    @IBOutlet var accountTableView: UITableView!
    @IBOutlet var accountTableViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var accountBookTableView: UITableView!
    @IBOutlet var paymentTableView: UITableView!
    @IBOutlet var accountBookDropImage: UIImageView!
    @IBOutlet var paymentDropImage: UIImageView!
    @IBOutlet var paymentToHomeDropImage: UIImageView!
    @IBOutlet var accountBookToHomeDropImage: UIImageView!
    @IBOutlet var dragView: UIImageView!

    lazy var balanceVisibleImage: UIImage? = {
        UIImage(named: "balance_visible")
    }()

    lazy var balanceInvisibleImage: UIImage? = {
        UIImage(named: "balance_invisible")
    }()

    lazy var listImage: UIImage? = {
        UIImage(named: "account_list")
    }()

    lazy var summaryImage: UIImage? = {
        UIImage(named: "account_summary")
    }()

    lazy var visibilityButton: UIBarButtonItem? = {
        UIBarButtonItem(image: UIImage(), style: .plain, target: self, action: #selector(handleBalanceVisibility))
    }()

    lazy var accountStateButton: UIBarButtonItem? = {
        UIBarButtonItem(image: UIImage(named: "account_summary"), style: .plain, target: self, action: #selector(handleSectionCollectionViewChange))
    }()

    lazy var helpButton: UIBarButtonItem? = {
        UIBarButtonItem(image: UIImage(named: "help"), style: .plain, target: self, action: #selector(helpClicked))
    }()

    var accountBookDropInteraction: UIDropInteraction?
    var paymentDropInteraction: UIDropInteraction?
    var accountBookToHomeDropInteraction: UIDropInteraction?
    var paymentToHomeDropInteraction: UIDropInteraction?

    let accountModel = AccountModel()
    let accountBookModel = AccountBookModel()
    let paymentModel = PaymentModel()
    var requestBody = ChangeNicknameRequest.Body()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupAccountTableView()
        setupAccountBookTableView()
        setupPaymentTableView()
        setupInteractions()
        fetchAccounts()
        NotificationCenter.default.addObserver(self, selector: #selector(handleRefreshNotification(notification:)), name: Notification.Name(NotificationConstants.REFRESH_ACCOUNTS), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleRefreshBills(notification:)), name: Notification.Name(NotificationConstants.REFRESH_BILLS), object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupNavigationButtons()
    }

    @objc private func handleRefreshNotification(notification: Notification) {
        refreshAll()
    }

    @objc private func handleRefreshBills(notification: Notification) {
        paymentModel.sections = []
        fetchPayments()
    }

    private func setupAccountTableView() {
        accountTableView.registerCell(nibName: "AccountCell")
        accountTableView.registerCell(nibName: "AccountCollectionCell")
        accountTableView.register(UINib(nibName: "BaseHeaderCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "BaseHeaderCell")
        accountTableView.hideScrollIndicators()
        accountTableView.dragInteractionEnabled = true
        accountTableView.dragDelegate = self
        accountTableView.dropDelegate = self
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshTableView), for: .valueChanged)
        accountTableView.refreshControl = refreshControl
    }

    private func setupAccountBookTableView() {
        accountBookTableView.registerCell(nibName: "AccountBookCell")
        accountBookTableView.register(UINib(nibName: "BaseHeaderCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "BaseHeaderCell")
        accountBookTableView.hideScrollIndicators()
        accountBookTableView.dropDelegate = self
    }

    private func setupPaymentTableView() {
        paymentTableView.registerCell(nibName: "PaymentUnitCell")
        paymentTableView.register(UINib(nibName: "BaseHeaderCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "BaseHeaderCell")
        paymentTableView.registerCell(nibName: "PaymentCell")
        paymentTableView.hideScrollIndicators()
        paymentTableView.dropDelegate = self
    }

    private func setupInteractions() {
        accountBookDropInteraction = UIDropInteraction(delegate: self)
        paymentDropInteraction = UIDropInteraction(delegate: self)
        accountBookToHomeDropInteraction = UIDropInteraction(delegate: self)
        paymentToHomeDropInteraction = UIDropInteraction(delegate: self)
        accountBookDropImage.addInteraction(accountBookDropInteraction!)
        accountBookDropImage.isUserInteractionEnabled = true
        paymentDropImage.addInteraction(paymentDropInteraction!)
        paymentDropImage.isUserInteractionEnabled = true
        accountBookToHomeDropImage.addInteraction(accountBookToHomeDropInteraction!)
        accountBookToHomeDropImage.isUserInteractionEnabled = true
        paymentToHomeDropImage.addInteraction(paymentToHomeDropInteraction!)
        paymentToHomeDropImage.isUserInteractionEnabled = true
    }

    private func fetchAccounts() {
        showLoader()
        guard let type = accountModel.accountQueue.dequeue() else {
            hideLoader()
            accountModel.accountQueue = initAccountQueue()
            fetchAccountBooks()
            return
        }
        switch type {
        case AccountConstants.credit:
            ConnectionFactory.fetchCreditAccount(
                success: { response in
                    let accountList = response.accountList
                    let accountCount = accountList.count
                    creditAccountList = accountList
                    if accountCount > 0 {
                        self.accountModel.sections.append(AccountSection(type: type, accounts: accountList))
                        self.accountTableView.reloadData()
                    }
                    self.fetchAccounts()
                },
                failed: { _ in
                    self.fetchAccounts()
                }
            )
        default:
            ConnectionFactory.fetchAccount(
                accountType: type,
                success: { response in
                    let accountList = response.accountList ?? []
                    let accountCount = accountList.count
                    switch type {
                    case AccountConstants.operative:
                        operativeAccountList = accountList
                    case AccountConstants.deposit:
                        depositAccountList = accountList
                    case AccountConstants.loan:
                        loanAccountList = accountList
                    default:
                        break
                    }
                    if accountCount > 0 {
                        self.accountModel.sections.append(AccountSection(type: type, accounts: accountList))
                        self.accountTableView.reloadData()
                    }
                    self.fetchAccounts()
                },
                failed: { _ in
                    self.fetchAccounts()
                }
            )
        }
    }

    private func fetchAccountBooks() {
        showLoader()
        guard let type = accountBookModel.accountBookQueue.dequeue() else {
            hideLoader()
            fetchPayments()
            accountBookModel.accountBookQueue = initAccountBookQueue()
            return
        }
        ConnectionFactory.fetchAccountBook(
            accountBookType: type,
            success: { response in
                let accountBookList = response.accountBookList ?? []
                let accountBookCount = accountBookList.count
                switch type {
                case TransactionNetworkConstants.golomt.rawValue:
                    golomtAccountBookList = accountBookList
                case TransactionNetworkConstants.bank.rawValue:
                    bankAccountBookList = accountBookList
                default:
                    break
                }
                if accountBookCount > 0 {
                    self.accountBookModel.sections.append(AccountBookSection(type: type, accountBooks: accountBookList))
                    self.accountBookTableView.reloadData()
                }
                self.fetchAccountBooks()
            },
            failed: { _ in
                self.fetchAccountBooks()
            }
        )
    }

    private func fetchPayments() {
        showLoader()
        ConnectionFactory.fetchPayments(
            success: { response in
                let paymentList = response.paymentList ?? []
                let paymentCount = paymentList.count
                billPaymentList = paymentList
                NotificationCenter.default.post(name: Notification.Name(NotificationConstants.BILLS_REFRESHED), object: nil)
                if paymentCount > 0 {
                    self.paymentModel.sections.append(PaymentSection(type: "PAYMENT", payments: paymentList))
                    self.paymentTableView.reloadData()
                }
                self.hideLoader()
            },
            failed: { _ in
                self.hideLoader()
            }
        )
    }

    private func showPaymentTableView() {
        let width = view.frame.width
        changeAccountTableViewLeadingConstraint(constant: width, completed: {
            self.preparePaymentTableViewForDrop()
        })
    }

    private func showAccountBookTableView() {
        let width = view.frame.width
        changeAccountTableViewLeadingConstraint(constant: -width, completed: {
            self.prepareAccountBookTableViewForDrop()
        })
    }

    private func prepareAccountTableViewForDrag() {
        paymentDropImage.isHidden = false
        accountBookDropImage.isHidden = false
        paymentToHomeDropImage.isHidden = true
        accountBookToHomeDropImage.isHidden = true
        UIView.animate(withDuration: 0.5) {
            self.paymentDropImage.transform = CGAffineTransform(translationX: self.paymentDropImage.frame.width, y: 0)
            self.accountBookDropImage.transform = CGAffineTransform(translationX: -self.accountBookDropImage.frame.width, y: 0)
        }
    }

    private func preparePaymentTableViewForDrop() {
        paymentToHomeDropImage.isHidden = false
        UIView.animate(withDuration: 0.5) {
            self.paymentToHomeDropImage.transform = CGAffineTransform(translationX: -self.paymentToHomeDropImage.frame.width, y: 0)
        }
    }

    private func prepareAccountBookTableViewForDrop() {
        accountBookToHomeDropImage.isHidden = false
        UIView.animate(withDuration: 0.5) {
            self.accountBookToHomeDropImage.transform = CGAffineTransform(translationX: self.accountBookToHomeDropImage.frame.width, y: 0)
        }
    }

    private func resetAccountTableViewLeadingConstraint() {
        accountTableViewLeadingConstraint.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.prepareAccountTableViewForDrag()
            self.paymentToHomeDropImage.transform = CGAffineTransform(translationX: self.paymentToHomeDropImage.frame.width, y: 0)
            self.accountBookToHomeDropImage.transform = CGAffineTransform(translationX: -self.accountBookToHomeDropImage.frame.width, y: 0)
        })
    }

    private func finishTableViewDrop() {
        accountTableViewLeadingConstraint.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in
            UIView.animate(withDuration: 0.5, animations: {
                self.paymentDropImage.transform = CGAffineTransform(translationX: -self.paymentDropImage.frame.width, y: 0)
                self.accountBookDropImage.transform = CGAffineTransform(translationX: self.accountBookDropImage.frame.width, y: 0)
            }, completion: { _ in
                self.paymentToHomeDropImage.isHidden = true
                self.accountBookToHomeDropImage.isHidden = true
                self.paymentToHomeDropImage.transform = CGAffineTransform(translationX: self.paymentToHomeDropImage.frame.width, y: 0)
                self.accountBookToHomeDropImage.transform = CGAffineTransform(translationX: -self.accountBookToHomeDropImage.frame.width, y: 0)
            })
        })
    }

    private func changeAccountTableViewLeadingConstraint(constant: CGFloat, completed: @escaping () -> Void) {
        accountTableViewLeadingConstraint.constant = constant
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.paymentDropImage.isHidden = true
            self.accountBookDropImage.isHidden = true
            self.paymentDropImage.transform = CGAffineTransform(translationX: -self.paymentDropImage.frame.width, y: 0)
            self.accountBookDropImage.transform = CGAffineTransform(translationX: self.accountBookDropImage.frame.width, y: 0)
            completed()
        })
    }

    private func setupBeneficiaryNavigation(account: AccountResponse.Account?) {
        if account == nil {
            super.tabBarController?.title = "first_tab_title".localized()
        } else {
            super.tabBarController?.title = "first_tab_title_with_account".localized(with: account!.ACCT_NUMBER.orEmpty)
        }
    }

    private func handleStatementNavigation(_ account: AccountResponse.Account) {
        let statementStoryBoard: UIStoryboard = UIStoryboard(name: "Statement", bundle: nil)

        switch account.MAIN_ACCT_TYPE {
        case AccountConstants.credit:
            let viewController = statementStoryBoard.instantiateViewController(withIdentifier: "CCDStatementID")
            let statementVC = viewController as? CCDStatementViewController
            guard let unwrappedVC = statementVC else {
                return
            }
            unwrappedVC.account = account
            navigationController?.pushViewController(unwrappedVC, animated: true)
        case AccountConstants.loan:
            let viewController = statementStoryBoard.instantiateViewController(withIdentifier: "LONStatementID")
            let statementVC = viewController as? LONStatementViewController
            guard let unwrappedVC = statementVC else {
                return
            }
            unwrappedVC.account = account
            navigationController?.pushViewController(unwrappedVC, animated: true)
        default:
            let viewController = statementStoryBoard.instantiateInitialViewController()
            let statementVC = viewController as? StatementViewController
            guard let unwrappedVC = statementVC else {
                return
            }
            unwrappedVC.account = account
            navigationController?.pushViewController(unwrappedVC, animated: true)
        }
    }

    private func changeNickname(_ account: AccountResponse.Account) {
        let statementStoryBoard: UIStoryboard = UIStoryboard(name: "Statement", bundle: nil)

        switch account.MAIN_ACCT_TYPE {
        case AccountConstants.credit:
            let viewController = statementStoryBoard.instantiateViewController(withIdentifier: "CCDStatementID")
            let statementVC = viewController as? CCDStatementViewController
            guard let unwrappedVC = statementVC else {
                return
            }
            unwrappedVC.account = account
            navigationController?.pushViewController(unwrappedVC, animated: true)
        case AccountConstants.loan:
            let viewController = statementStoryBoard.instantiateViewController(withIdentifier: "LONStatementID")
            let statementVC = viewController as? LONStatementViewController
            guard let unwrappedVC = statementVC else {
                return
            }
            unwrappedVC.account = account
            navigationController?.pushViewController(unwrappedVC, animated: true)
        default:
            let viewController = statementStoryBoard.instantiateInitialViewController()
            let statementVC = viewController as? StatementViewController
            guard let unwrappedVC = statementVC else {
                return
            }
            unwrappedVC.account = account
            navigationController?.pushViewController(unwrappedVC, animated: true)
        }
    }

    private func setupNavigationButtons() {
        switch getBalanceVisibility() {
        case true:
            visibilityButton?.image = balanceInvisibleImage
        case false:
            visibilityButton?.image = balanceVisibleImage
        }
        tabBarController?.navigationItem.rightBarButtonItems = [helpButton!, visibilityButton!, accountStateButton!]
    }

    @objc private func helpClicked() {
        let helpVC = HelpViewController(nibName: "TableViewController", bundle: nil)
        navigationController?.pushViewController(helpVC, animated: true)
    }

    @objc private func handleBalanceVisibility() {
        let visibility = !getBalanceVisibility()
        setBalanceVisibility(visibility)
        switch visibility {
        case true:
            visibilityButton?.image = balanceInvisibleImage
        case false:
            visibilityButton?.image = balanceVisibleImage
        }
        accountTableView.reloadData()
    }

    @objc private func handleSectionCollectionViewChange(_ sender: UIButton) {
        for (index, section) in accountModel.sections.enumerated() {
            section.collectionView = !section.collectionView
            accountTableView.reloadSections([index], with: .automatic)
        }
        let summary = accountModel.sections.first?.collectionView
        switch summary {
        case true:
            accountStateButton?.image = listImage
        case false:
            accountStateButton?.image = summaryImage
        default:
            break
        }
    }

    @objc private func refreshTableView(refreshControl: UIRefreshControl) {
        refreshAll()
        refreshControl.endRefreshing()
    }

    private func refreshAll() {
        accountModel.sections = []
        accountBookModel.sections = []
        paymentModel.sections = []
        accountTableView.reloadData()
        fetchAccounts()
    }

    private func toggleTabbar() {
        guard var frame = tabBarController?.tabBar.frame else { return }
        let hidden = frame.origin.y == view.frame.size.height
        frame.origin.y = hidden ? view.frame.size.height - frame.size.height : view.frame.size.height
        UIView.animate(withDuration: 0.5) {
            self.tabBarController?.tabBar.frame = frame
        }
    }

    private func clearSelectedCell() {
        if let previousIndex = accountModel.previousDestinationAccountIndex {
            if let previousTable = accountModel.previousDestinationTableView {
                let previousCell = previousTable.cellForRow(at: previousIndex)
                switch previousCell {
                case let accountCell as AccountCell:
                    accountCell.setDefaultBackground()
                case let accountBookCell as AccountBookCell:
                    accountBookCell.setDefaultBackground()
                case let paymentCell as PaymentCell:
                    paymentCell.setDefaultBackground()
                case let paymentUnitCell as PaymentUnitCell:
                    paymentUnitCell.setDefaultBackground()
                default: break
                }
            }
        }
    }
}

extension AccountViewController: UIDropInteractionDelegate {
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidEnter session: UIDropSession) {
        switch true {
        case interaction ~= accountBookDropInteraction:
            showAccountBookTableView()
        case interaction ~= paymentDropInteraction:
            showPaymentTableView()
        case interaction ~= accountBookToHomeDropInteraction:
            resetAccountTableViewLeadingConstraint()
        case interaction ~= paymentToHomeDropInteraction:
            resetAccountTableViewLeadingConstraint()
        default:
            return
        }
    }

    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        return true
    }
}

extension AccountViewController: MGSwipeTableCellDelegate {
    func swipeTableCell(_ cell: MGSwipeTableCell, tappedButtonAt index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
        switch direction {
        case .leftToRight:
            let indexPath = accountTableView.indexPath(for: cell)
            guard let unwrappedIndexPath = indexPath else {
                return false
            }
            let section = accountModel.sections[unwrappedIndexPath.section]
            let account = section.accounts[unwrappedIndexPath.row]
            handleStatementNavigation(account)
        case .rightToLeft:
            let indexPath = accountTableView.indexPath(for: cell)
            guard let unwrappedIndexPath = indexPath else {
                return false
            }
            let section = accountModel.sections[unwrappedIndexPath.section]
            let account = section.accounts[unwrappedIndexPath.row]
            switch index {
            case 1:
                let golomtVC = GolomtTransactionViewController(nibName: "TableViewController", bundle: nil)
                golomtVC.selectedAccount = account
                navigationController?.pushViewController(golomtVC, animated: true)
            case 0:
                switch account.MAIN_ACCT_TYPE {
                case AccountConstants.operative:
                    let bankVC = BankTransactionViewController(nibName: "TableViewController", bundle: nil)
                    bankVC.selectedAccount = account
                    navigationController?.pushViewController(bankVC, animated: true)
                case AccountConstants.deposit:
                    let ownVC = OwnTransactionViewController(nibName: "TableViewController", bundle: nil)
                    ownVC.selectedBeneficiaryAccount = account
                    navigationController?.pushViewController(ownVC, animated: true)
                case AccountConstants.credit:
                    let ccdVC = CreditCardTransactionViewController(nibName: "TableViewController", bundle: nil)
                    ccdVC.selectedBeneficiaryAccount = account
                    navigationController?.pushViewController(ccdVC, animated: true)
                case AccountConstants.loan:
                    let loanVC = LoanTransactionViewController(nibName: "TableViewController", bundle: nil)
                    loanVC.selectedBeneficiaryAccount = account
                    navigationController?.pushViewController(loanVC, animated: true)
                default:
                    break
                }
            default:
                break
            }
        default: break
        }
        return false
    }

    func swipeTableCell(_ cell: MGSwipeTableCell, swipeButtonsFor direction: MGSwipeDirection, swipeSettings: MGSwipeSettings, expansionSettings: MGSwipeExpansionSettings) -> [UIView]? {
        let accountCell = cell as! AccountCell
        let indexPath = accountTableView.indexPath(for: accountCell)
        guard let unwrappedIndexPath = indexPath else {
            return nil
        }

        let defaultPadding = CGFloat(2)
        let containerHeight = accountCell.containerView.frame.height
        let accountHeight = accountCell.accountContainerView.frame.height
        let calculatedHeight = containerHeight - accountHeight

        swipeSettings.bottomMargin = calculatedHeight + defaultPadding
        swipeSettings.topMargin = defaultPadding
        swipeSettings.transition = .drag
        swipeSettings.buttonsDistance = 4
        swipeSettings.offset = 16

        let statementButton = MGSwipeButton(title: "swipe_statement".localized(), backgroundColor: .operativeAccount)
        statementButton.titleLabel?.useSmallFont()
        statementButton.layer.cornerRadius = 10

        let betweenGolomtButton = MGSwipeButton(title: "swipe_between_golomt".localized(), backgroundColor: .operativeAccount)
        betweenGolomtButton.wordWrapLabel()
        betweenGolomtButton.titleLabel?.useSmallFont()
        betweenGolomtButton.layer.cornerRadius = 10

        let betweenBankButton = MGSwipeButton(title: "swipe_between_banks".localized(), backgroundColor: .operativeAccount)
        betweenBankButton.wordWrapLabel()
        betweenBankButton.titleLabel?.useSmallFont()
        betweenBankButton.layer.cornerRadius = 10

        let incomeButton = MGSwipeButton(title: "swipe_income".localized(), backgroundColor: .depositAccount)
        incomeButton.wordWrapLabel()
        incomeButton.titleLabel?.useSmallFont()
        incomeButton.layer.cornerRadius = 10

        let creditPayButton = MGSwipeButton(title: "swipe_credit_pay".localized(), backgroundColor: .creditAccount)
        creditPayButton.wordWrapLabel()
        creditPayButton.titleLabel?.useSmallFont()
        creditPayButton.layer.cornerRadius = 10

        let loanPayButton = MGSwipeButton(title: "swipe_loan_pay".localized(), backgroundColor: .loanAccount)
        loanPayButton.wordWrapLabel()
        loanPayButton.titleLabel?.useSmallFont()
        loanPayButton.layer.cornerRadius = 10

        let section = accountModel.sections[unwrappedIndexPath.section]
        let account = section.accounts[unwrappedIndexPath.row]

        switch account.MAIN_ACCT_TYPE {
        case AccountConstants.operative:
            statementButton.backgroundColor = .operativeAccount
        case AccountConstants.deposit:
            statementButton.backgroundColor = .depositAccount
        case AccountConstants.credit:
            statementButton.backgroundColor = .creditAccount
        case AccountConstants.loan:
            statementButton.backgroundColor = .loanAccount
        default:
            break
        }
        switch direction {
        case .leftToRight:
            return [statementButton]
        case .rightToLeft:
            switch account.MAIN_ACCT_TYPE {
            case AccountConstants.operative:
                return [betweenBankButton, betweenGolomtButton]
            case AccountConstants.deposit:
                return [incomeButton]
            case AccountConstants.credit:
                return [creditPayButton]
            case AccountConstants.loan:
                return [loanPayButton]
            default:
                return nil
            }
        default:
            return nil
        }
    }
}

extension AccountViewController: UITableViewDragDelegate {
    func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
        let section = accountModel.sections[indexPath.section]
        if section.collectionView {
            return [UIDragItem]()
        }

        switch section.type {
        case AccountConstants.operative:
            let dragItem = UIDragItem(itemProvider: NSItemProvider())
            let account = section.accounts[indexPath.row]
            dragItem.localObject = account
            dragItem.previewProvider = { () -> UIDragPreview? in
                let parameters = UIDragPreviewParameters()
                parameters.backgroundColor = .clear
                parameters.visiblePath = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: self.dragView.frame.width, height: self.dragView.frame.height), cornerRadius: self.dragView.frame.width / 2)
                return UIDragPreview(view: self.dragView, parameters: parameters)
            }
            accountModel.previousDestinationAccountIndex = nil
            return [dragItem]
        default: return [UIDragItem]()
        }
    }

    func tableView(_ tableView: UITableView, dragPreviewParametersForRowAt indexPath: IndexPath) -> UIDragPreviewParameters? {
        let parameters = UIDragPreviewParameters()
        parameters.backgroundColor = .clear
        let cell = tableView.cellForRow(at: indexPath)
        guard let unwrappedCell = cell else {
            return nil
        }
        parameters.visiblePath = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: unwrappedCell.frame.width, height: unwrappedCell.frame.height - 2), cornerRadius: 0)
        return parameters
    }

    func tableView(_ tableView: UITableView, dragSessionWillBegin session: UIDragSession) {
        prepareAccountTableViewForDrag()
        // TO-IMPROVE
        toggleTabbar()
        setupBeneficiaryNavigation(account: session.items[0].localObject as? AccountResponse.Account)
    }

    func tableView(_ tableView: UITableView, dragSessionDidEnd session: UIDragSession) {
        var previousCell: UITableViewCell?
        if let previousIndex = accountModel.previousDestinationAccountIndex {
            if let previousTable = accountModel.previousDestinationTableView {
                previousCell = previousTable.cellForRow(at: previousIndex)
            }
        }
        switch previousCell {
        case let accountCell as AccountCell:
            accountCell.setDefaultBackground()
        case let accountBookCell as AccountBookCell:
            accountBookCell.setDefaultBackground()
        case let paymentCell as PaymentCell:
            paymentCell.setDefaultBackground()
        case let paymentUnitCell as PaymentUnitCell:
            paymentUnitCell.setDefaultBackground()
        default: break
        }
        setupBeneficiaryNavigation(account: nil)
        toggleTabbar()
        finishTableViewDrop()
    }
}

extension AccountViewController: UITableViewDropDelegate {
    func tableView(_ tableView: UITableView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UITableViewDropProposal {
        if session.localDragSession != nil {
            let location = session.location(in: tableView)
            let indexPath = tableView.indexPathForRow(at: location)
            guard let unwrappedIndexPath = indexPath else {
                clearSelectedCell()
                accountModel.previousDestinationAccountIndex = nil
                accountModel.previousDestinationTableView = nil
                return UITableViewDropProposal(operation: .forbidden, intent: .unspecified)
            }
            if unwrappedIndexPath != accountModel.previousDestinationAccountIndex || tableView != accountModel.previousDestinationTableView {
                let generator = UIImpactFeedbackGenerator(style: .medium)
                generator.impactOccurred()
                let cell = tableView.cellForRow(at: unwrappedIndexPath)
                switch cell {
                case let accountCell as AccountCell:
                    accountCell.setDropBackground()
                case let accountBookCell as AccountBookCell:
                    accountBookCell.setDropBackground()
                case let paymentCell as PaymentCell:
                    paymentCell.setDropBackground()
                case let paymentUnitCell as PaymentUnitCell:
                    paymentUnitCell.setDropBackground()
                default: break
                }
                clearSelectedCell()
            }
            accountModel.previousDestinationAccountIndex = unwrappedIndexPath
            accountModel.previousDestinationTableView = tableView
            return UITableViewDropProposal(operation: .move, intent: .unspecified)
        }

        return UITableViewDropProposal(operation: .cancel, intent: .unspecified)
    }

    func tableView(_ tableView: UITableView, performDropWith coordinator: UITableViewDropCoordinator) {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
        let destinationTableView = accountModel.previousDestinationTableView
        switch (true, coordinator.items.first?.dragItem.localObject, coordinator.destinationIndexPath) {
        case (destinationTableView ~= accountTableView, .some(let sourceAccount), .some(let destinationIndexPath)):
            let account = sourceAccount as! AccountResponse.Account
            let accountSection = accountModel.sections[destinationIndexPath.section]
            if destinationIndexPath.row > accountSection.accounts.count - 1 {
                return
            }
            let beneficiaryAccount = accountSection.accounts[destinationIndexPath.row]
            switch beneficiaryAccount.MAIN_ACCT_TYPE {
            case AccountConstants.operative, AccountConstants.deposit:
                let ownVC = OwnTransactionViewController(nibName: "TableViewController", bundle: nil)
                ownVC.selectedAccount = account
                ownVC.selectedBeneficiaryAccount = beneficiaryAccount
                navigationController?.pushViewController(ownVC, animated: true)
            case AccountConstants.credit:
                let ccdVC = CreditCardTransactionViewController(nibName: "TableViewController", bundle: nil)
                ccdVC.selectedAccount = account
                ccdVC.selectedBeneficiaryAccount = beneficiaryAccount
                navigationController?.pushViewController(ccdVC, animated: true)
            case AccountConstants.loan:
                let loanVC = LoanTransactionViewController(nibName: "TableViewController", bundle: nil)
                loanVC.selectedAccount = account
                loanVC.selectedBeneficiaryAccount = beneficiaryAccount
                navigationController?.pushViewController(loanVC, animated: true)
            default:
                break
            }
        case (destinationTableView ~= accountBookTableView, .some(let sourceAccount), .some(let destinationIndexPath)):
            let account = sourceAccount as! AccountResponse.Account
            let accountBookSection = accountBookModel.sections[destinationIndexPath.section]
            let accountBook = accountBookSection.accountBooks[destinationIndexPath.row]
            switch accountBookSection.type {
            case TransactionNetworkConstants.golomt.rawValue:
                let golomtVC = GolomtTransactionViewController(nibName: "TableViewController", bundle: nil)
                golomtVC.selectedAccount = account
                golomtVC.selectedAccountBook = accountBook
                navigationController?.pushViewController(golomtVC, animated: true)
            case TransactionNetworkConstants.bank.rawValue:
                let bankVC = BankTransactionViewController(nibName: "TableViewController", bundle: nil)
                bankVC.selectedAccount = account
                bankVC.selectedAccountBook = accountBook
                navigationController?.pushViewController(bankVC, animated: true)
            default: break
            }
        case (destinationTableView ~= paymentTableView, .some(let sourceAccount), .some(let destinationIndexPath)):
            let account = sourceAccount as! AccountResponse.Account
            let paymentSection = paymentModel.sections[destinationIndexPath.section]
            let payment = paymentSection.payments[destinationIndexPath.row]
            let paymentStoryboard: UIStoryboard = UIStoryboard(name: "BillPayment", bundle: nil)
            guard let billVC = paymentStoryboard.instantiateInitialViewController() as? BillPaymentViewController else {
                return
            }
            billVC.selectedAccount = account
            billVC.selectedPayment = payment
            navigationController?.pushViewController(billVC, animated: true)
        default: break
        }
    }
}

extension AccountViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        switch true {
        case tableView ~= accountTableView:
            return accountModel.sections.count
        case tableView ~= accountBookTableView:
            return accountBookModel.sections.count
        case tableView ~= paymentTableView:
            return paymentModel.sections.count
        default: return 0
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "BaseHeaderCell") as? BaseHeaderCell

        switch true {
        case tableView ~= accountTableView:
            let accountSection = accountModel.sections[section]
            cell?.setupLabel(accountSection.getAccountHeader())
            return cell
        case tableView ~= accountBookTableView:
            let accountBookSection = accountBookModel.sections[section]
            cell?.setupLabel(accountBookSection.getAccountBookHeader())
        case tableView ~= paymentTableView:
            let paymentSection = paymentModel.sections[section]
            cell?.setupLabel(paymentSection.getPaymentHeader())
        default: break
        }

        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch true {
        case tableView ~= accountTableView:
            let section = accountModel.sections[section]
            if section.collectionView {
                return 1
            } else {
                return section.accounts.count
            }
        case tableView ~= accountBookTableView:
            let section = accountBookModel.sections[section]
            return section.accountBooks.count
        case tableView ~= paymentTableView:
            let section = paymentModel.sections[section]
            switch section.type {
            case "UNIT": return 1
            default: return section.payments.count
            }
        default: return 0
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch true {
        case tableView ~= paymentTableView:
            let section = paymentModel.sections[indexPath.section]
            switch section.type {
            case "UNIT": return 70
            default: break
            }
        default: break
        }
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch true {
        case tableView ~= accountTableView:
            let section = accountModel.sections[indexPath.section]
            switch section.collectionView {
            case true:
                let cell = tableView.dequeueReusableCell(withIdentifier: "AccountCollectionCell") as? AccountCollectionCell
                guard let unwrappedCell = cell else {
                    return UITableViewCell()
                }
                unwrappedCell.setupData(type: section.type, accounts: section.accounts)
                return unwrappedCell
            case false:
                let account = section.accounts[indexPath.row]
                let cell = tableView.dequeueReusableCell(withIdentifier: "AccountCell") as? AccountCell
                guard let unwrappedCell = cell else {
                    return UITableViewCell()
                }
                unwrappedCell.setupData(account: account)
                unwrappedCell.delegate = self
                unwrappedCell.onOptionSelect = { [weak self] tag in
                    switch tag {
                    case .GENERAL_DETAIL:
                        switch account.MAIN_ACCT_TYPE {
                        case AccountConstants.operative:
                            let detailVC = OPRDetailViewController(nibName: "TableViewController", bundle: nil)
                            detailVC.selectedAccount = account
                            self?.navigationController?.pushViewController(detailVC, animated: true)
                        case AccountConstants.deposit:
                            let detailVC = DEPDetailViewController(nibName: "TableViewController", bundle: nil)
                            detailVC.selectedAccount = account
                            self?.navigationController?.pushViewController(detailVC, animated: true)
                        case AccountConstants.loan:
                            let detailVC = LONDetailViewController(nibName: "TableViewController", bundle: nil)
                            detailVC.selectedAccount = account
                            self?.navigationController?.pushViewController(detailVC, animated: true)
                        case AccountConstants.credit:
                            let detailVC = CCDDetailViewController(nibName: "TableViewController", bundle: nil)
                            detailVC.selectedAccount = account
                            self?.navigationController?.pushViewController(detailVC, animated: true)
                        default:
                            break
                        }
                    case .GENERAL_STATEMENT:
                        self?.handleStatementNavigation(account)
                    case .GENERAL_NICKNAME:
                        self?.requestBody.BRANCH_ID = account.BRANCH_ID.orEmpty
                        self?.requestBody.ACCOUNT_ID = account.ACCT_NUMBER.orEmpty
                        self?.requestBody.ACCOUNT_NICK_NAME = ""
                        let confirmationDialog = UIAlertController(title: "card_pin_code_otp_dialog_title".localized(), message: "option_general_change_nickname_dialog_label".localized(), preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: "card_pin_code_otp_dialog_negative_label".localized(), style: .cancel, handler: nil)
                        let confirmAction = UIAlertAction(
                            title: "card_pin_code_otp_dialog_positive_label".localized(),
                            style: .default,
                            handler: { _ in
                                let text = confirmationDialog.textFields?.first?.text
                                self?.requestBody.ACCOUNT_NICK_NAME = text.orEmpty
                                self?.showLoader()
                                ConnectionFactory.changeNickname(
                                    body: self?.requestBody,
                                    success: { _ in
                                        self?.hideLoader()
                                        account.ACCT_NICKNAME = text.orEmpty
                                        tableView.reloadData()
                                    },
                                    failed: { reason in
                                        self?.hideLoader()
                                        self?.handleRequestFailure(reason)
                                    }
                                )
                            }
                        )
                        confirmationDialog.addAction(cancelAction)
                        confirmationDialog.addAction(confirmAction)
                        confirmationDialog.addTextField { textField in
                            textField.keyboardType = .default
                            if #available(iOS 12.0, *) {
                                textField.textContentType = .oneTimeCode
                            }
                            textField.delegate = self
                        }
                        self?.present(confirmationDialog, animated: true)
                    case .OPR_CLOSE_ACCOUNT:
                        let closeVC = OperativeCloseViewController(nibName: "TableViewController", bundle: nil)
                        closeVC.selectedAccount = account
                        self?.navigationController?.pushViewController(closeVC, animated: true)
                    case .CCD_PAYMENT:
                        let ccdVC = CreditCardTransactionViewController(nibName: "TableViewController", bundle: nil)
                        ccdVC.selectedBeneficiaryAccount = account
                        self?.navigationController?.pushViewController(ccdVC, animated: true)
                    case .DEP_CLOSE_ACCOUNT:
                        let closeVC = CloseDepositViewController(nibName: "TableViewController", bundle: nil)
                        closeVC.selectedAccount = account
                        self?.navigationController?.pushViewController(closeVC, animated: true)
                    case .DEP_LOAN:
                        let loanVC = LoanCollateralViewController(nibName: "TableViewController", bundle: nil)
                        loanVC.selectedAccount = account
                        self?.navigationController?.pushViewController(loanVC, animated: true)
                    case .LOAN_GRAPHIC:
                        let loanStoryBoard: UIStoryboard = UIStoryboard(name: "Loan", bundle: nil)
                        let viewController = loanStoryBoard.instantiateViewController(withIdentifier: "LoanGraphicID")
                        guard let graphicVC = viewController as? LoanGraphicViewController else {
                            return
                        }
                        graphicVC.account = account
                        self?.navigationController?.pushViewController(graphicVC, animated: true)
                    case .LOAN_PAY:
                        let loanVC = LoanTransactionViewController(nibName: "TableViewController", bundle: nil)
                        loanVC.selectedBeneficiaryAccount = account
                        self?.navigationController?.pushViewController(loanVC, animated: true)
                    case .LOAN_CLOSE:
                        let loanVC = LoanCloseViewController(nibName: "TableViewController", bundle: nil)
                        loanVC.selectedAccount = account
                        self?.navigationController?.pushViewController(loanVC, animated: true)
                    case .CCD_STATUS:
                        let storyboard = UIStoryboard(name: "CardList", bundle: nil)
                        guard let VC = storyboard.instantiateInitialViewController() as? CardListController else {
                            return
                        }
                        VC.cardNumber = account.ACCT_NUMBER
                        VC.optionButton = .CCD_STATUS
                        self?.navigationController?.pushViewController(VC, animated: true)
                    case .CCD_PIN:
                        let storyboard = UIStoryboard(name: "CardList", bundle: nil)
                        guard let VC = storyboard.instantiateInitialViewController() as? CardListController else {
                            return
                        }
                        VC.cardNumber = account.ACCT_NUMBER
                        VC.optionButton = .CCD_PIN
                        self?.navigationController?.pushViewController(VC, animated: true)
                    case .CCD_E_CODE:
                        let storyboard = UIStoryboard(name: "CardList", bundle: nil)
                        guard let VC = storyboard.instantiateInitialViewController() as? CardListController else {
                            return
                        }
                        VC.cardNumber = account.ACCT_NUMBER
                        VC.optionButton = .CCD_E_CODE
                        self?.navigationController?.pushViewController(VC, animated: true)
                    case .DEP_EXTEND:
                        let VC = ExtendDepositViewController(nibName: "TableViewController", bundle: nil)
                        
                        self?.navigationController?.pushViewController(VC, animated: true)
                    default: break
                    }
                }
                return unwrappedCell
            }
        case tableView ~= accountBookTableView:
            let section = accountBookModel.sections[indexPath.section]
            let accountBook = section.accountBooks[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "AccountBookCell") as? AccountBookCell
            guard let unwrappedCell = cell else {
                return UITableViewCell()
            }
            unwrappedCell.setData(type: section.type, accountBook: accountBook)
            return unwrappedCell
        case tableView ~= paymentTableView:
            let section = paymentModel.sections[indexPath.section]
            switch section.type {
            case "UNIT":
                let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentUnitCell") as? PaymentUnitCell
                guard let unwrappedCell = cell else {
                    return UITableViewCell()
                }
                return unwrappedCell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCell") as? PaymentCell
                let payment = section.payments[indexPath.row]
                guard let unwrappedCell = cell else {
                    return UITableViewCell()
                }
                unwrappedCell.setupPayment(payment)
                return unwrappedCell
            }
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = accountModel.sections[indexPath.section]

        if section.collectionView {
            return
        }

        tableView.deselectRow(at: indexPath, animated: true)

        let account = section.accounts[indexPath.row]

        if accountModel.previousExpandedAccountIndex != nil, accountModel.previousExpandedAccountIndex != indexPath {
            let previousSection = accountModel.sections[accountModel.previousExpandedAccountIndex!.section]
            let previousAccount = previousSection.accounts[accountModel.previousExpandedAccountIndex!.row]
            previousAccount.expanded = false
        }

        let expanded = account.expanded
        account.expanded = !expanded

        switch accountModel.previousExpandedAccountIndex {
        case nil:
            tableView.reloadRows(at: [indexPath], with: .fade)
        default:
            tableView.reloadRows(at: [indexPath, accountModel.previousExpandedAccountIndex!], with: .fade)
        }

        if account.expanded {
            tableView.scrollToRow(at: indexPath, at: .none, animated: true)
        }

        accountModel.previousExpandedAccountIndex = indexPath
    }
}

extension AccountViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let stringRange = Range(range, in: textField.text.orEmpty) else {
            return false
        }
        let newString = textField.text.orEmpty.replacingCharacters(in: stringRange, with: string)
        return newString.count <= 255
    }
}
