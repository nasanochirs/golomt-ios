//
//  SceneDelegate.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 2/21/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    var expireDate: Date?

    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo _: UISceneSession, options _: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let _ = (scene as? UIWindowScene) else { return }
    }

    @available(iOS 13.0, *)
    func sceneDidDisconnect(_: UIScene) {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    @available(iOS 13.0, *)
    func sceneDidBecomeActive(_: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    @available(iOS 13.0, *)
    func sceneWillResignActive(_: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    @available(iOS 13.0, *)
    func sceneWillEnterForeground(_: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
        if expireDate != nil {
            if Date().compare(expireDate!) == .orderedDescending {
                LoadingDialog.shared.hideLoader()
                let loginStoryBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
                guard let loginVC = loginStoryBoard.instantiateInitialViewController() else {
                    return
                }
                UIApplication.shared.windows.first?.rootViewController = loginVC
                UIApplication.shared.windows.first?.makeKeyAndVisible()
                NotificationCenter.default.post(name: Notification.Name(NotificationConstants.SESSION_EXPIRED), object: nil)
            }
            expireDate = nil
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        }
    }

    @available(iOS 13.0, *)
    func sceneDidEnterBackground(_: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
        LoadingDialog.shared.hideLoader()
        if userDetails != nil {
            var now = Date()
            now = now.addingTimeInterval(60 * 15)
            expireDate = now
            var calendar = Calendar(identifier: .gregorian)
            calendar.timeZone = NSTimeZone.local
            let components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second, .timeZone], from: now)
            let objNotificationContent = UNMutableNotificationContent()
            objNotificationContent.body = "notification_session_expired_label".localized()
            objNotificationContent.sound = UNNotificationSound.default
            let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
            let request = UNNotificationRequest(
                identifier: "ten",
                content: objNotificationContent,
                trigger: trigger)

            let center = UNUserNotificationCenter.current()

            center.add(request, withCompletionHandler: { error in

                if error == nil {
                    print("Local Notification succeeded")
                } else {
                    print("Local Notification failed")
                }
            })
        } else {
            expireDate = nil
        }
    }
}
