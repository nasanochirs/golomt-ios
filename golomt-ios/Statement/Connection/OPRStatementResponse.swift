//
//  OPRStatementResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/16/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class OPRStatementResponse: BaseResponse {
    let transactionList: [Transaction]?
    let transactionHistory: TransactionHistory?
    
    private enum CodingKeys: String, CodingKey {
        case TransactionsList_REC
        case OpTransactionHistory
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(transactionList, forKey: .TransactionsList_REC)
        try container.encode(transactionHistory, forKey: .OpTransactionHistory)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.transactionList = try container.decodeIfPresent([Transaction].self, forKey: .TransactionsList_REC)
        self.transactionHistory = try container.decodeIfPresent(TransactionHistory.self, forKey: .OpTransactionHistory)
        try super.init(from: decoder)
    }
    
    struct TransactionHistory: Codable {
        var TOTAL_RECORD: String?
    }
    
    struct Transaction: Codable {
        var AMT_TYPE: String?
        var TRANS_REMARKS: String?
        var AMOUNT: String?
        var TXN_POSTED_DATE: String?
        var TRANS_BALANCE: String?
        var INSTRUMENT_ID: String?
        var EXCHANGE_RATE: String?
        var COUNTER_PARTY_NAME: String?
    }
}
