//
//  OPRStatementRequest.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/16/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class StatementRequest: BaseRequest {
    var body = Body()

    private enum CodingKeys: String, CodingKey {
        case body
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(body, forKey: .body)
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    override init() {
        super.init()
        header.__SRVCID__ = "OPTRHS"
    }

    struct Body: Codable {
        var ACCOUNT_ID: String = ""
        var ACCOUNT_TYPE: String = ""
        var FROM_TXN_DATE: String = ""
        var TO_TXN_DATE: String = ""
        var AMOUNT_TYPE: String = ""
        var SET_NUMBER: Int = 0
        var MAIN_ACCOUNT_TYPE = ""
    }
}
