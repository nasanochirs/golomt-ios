//
//  LONStatementResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/27/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LONStatementResponse: BaseResponse {
    let transactionList: [Transaction]?
    
    private enum CodingKeys: String, CodingKey {
        case LNTransactionList_REC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(transactionList, forKey: .LNTransactionList_REC)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.transactionList = try container.decodeIfPresent([Transaction].self, forKey: .LNTransactionList_REC)
        try super.init(from: decoder)
    }
    
    struct Transaction: Codable {
        var LN_CREDIT_AMOUNT_ARRAY: String?
        var LN_DEBIT_AMOUNT_ARRAY: String?
        var TXN_DATE_ARRAY: String?
        var LN_TXN_REMARKS_ARRAY: String?
        var LN_OP_ACCOUNT_ARRAY: String?
        var LN_OUTSTANDING_BAL: String?
        
        func getAmount() -> String {
            if LN_CREDIT_AMOUNT_ARRAY.toAmount > 0 {
                return LN_CREDIT_AMOUNT_ARRAY.orEmpty
            } else {
                return LN_DEBIT_AMOUNT_ARRAY.orEmpty
            }
        }
    }
}
