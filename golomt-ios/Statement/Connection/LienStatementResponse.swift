//
//  LienStatementResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/16/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LienStatementResponse: BaseResponse {
    let lienTransactionList: [LienTransaction]?
    
    private enum CodingKeys: String, CodingKey {
        case LienDetails_REC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(lienTransactionList, forKey: .LienDetails_REC)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.lienTransactionList = try container.decodeIfPresent([LienTransaction].self, forKey: .LienDetails_REC)
        try super.init(from: decoder)
    }
    
    struct LienTransaction: Codable {
        var LIEN_TYPE_DESC: String?
        var EXPIRY_DATE: String?
        var LIEN_DATE: String?
        var AMOUNT: String?
        var REMARKS: String?
    }
}
