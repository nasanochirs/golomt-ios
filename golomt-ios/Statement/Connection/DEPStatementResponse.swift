//
//  DEPStatementResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/19/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class DEPStatementResponse: BaseResponse {
    let transactionList: [Transaction]?
    
    private enum CodingKeys: String, CodingKey {
        case TransactionList_REC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(transactionList, forKey: .TransactionList_REC)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.transactionList = try container.decodeIfPresent([Transaction].self, forKey: .TransactionList_REC)
        try super.init(from: decoder)
    }
    
    struct Transaction: Codable {
        var LAST_POSTED_DATE: String?
        var INSTRUMNETID: String?
        var LAST_TRANSACTION_DATE: String?
        var TRANSACTION_AMOUNT: String?
        var TRANSACTION_REMARKS: String?
        var TRANSACTION_DATE: String?
        var LAST_TRANSACTION_BALANCE: String?
        var LAST_TRANSACTION_SRL_NO: String?
        var LAST_TRANSACTION_ID: String?
        var TRANSACTION_BALANCE: String?
        var TRANSACTION_CATEGORY: String?
        var TRANSACTION_AMOUNT_TYPE: String?
    }
}
