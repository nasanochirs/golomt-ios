//
//  CCDStatementResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/21/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class CCDStatementResponse: BaseResponse {
    let transactionList: [Transaction]?
    let transactionHistoryList: [TransactionHistory]?
    let summary: Summary?
    
    private enum CodingKeys: String, CodingKey {
        case TransactionsList_REC, TransactionHistoryList_REC, CardSummary
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(transactionList, forKey: .TransactionsList_REC)
        try container.encode(transactionHistoryList, forKey: .TransactionHistoryList_REC)
        try container.encode(summary, forKey: .CardSummary)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.transactionList = try container.decodeIfPresent([Transaction].self, forKey: .TransactionsList_REC)
        self.transactionHistoryList = try container.decodeIfPresent([TransactionHistory].self, forKey: .TransactionHistoryList_REC)
        self.summary = try container.decodeIfPresent(Summary.self, forKey: .CardSummary)
        try super.init(from: decoder)
    }
    
    struct Transaction: Codable {
        var TRANSACTION_DATE: String?
        var ORIGINAL_AMOUNT: String?
        var POSTED_DATE: String?
        var BILLING_AMOUNT: String?
        var TRANSACTION_REMARK: String?
        var CREDIT_CARD_NUMBER: String?
        var TRANSACTION_NUMBER: String?
        var TXN_TYPE: String?
        var TXN_TYPE_DESC: String?
        var ORIGINAL_CURRENCY: String?
        
        func getTransactionType() -> String {
            if ORIGINAL_AMOUNT.toAmount > 0 {
                return StatementConstants.income
            } else {
                return StatementConstants.expense
            }
        }
    }
    
    struct TransactionHistory: Codable {
        var BILL_AMOUNT_ARRAY: String?
        var DATE_ARRAY: String?
        var CURRENCY_CODE_ARRAY: String?
        var TRXN_AMOUNT_ARRAY: String?
        var TRXN_DESC_ARRAY: String?
        var CREDIT_CARD_NUMBER_ARRAY: String?
    }
    
    struct Summary: Codable {
        var INTERESTCHARGE: String?
        var CURRBAL: String?
        var TOTALCRTRANAMOUNT: String?
        var TOTCRAMT: String?
        var EXTRAVAGANTSPENDING: String?
        var MINPAYMENTDUEAMT: String?
        var TOTDBAMT: String?
        var LASTDUEDATE: String?
        var EFFECTIVELIMIT: String?
        var LATELIMITCHARGE: String?
        var EXCEEDLIMITCHARGE: String?
        var LOYALTYPOINT: String?
        var CARDOUTSTANDINGBALANCE: String?
        var ACCTAVAILABLELIMIT: String?
        var TOTALDBTRANAMOUNT: String?
        var ACCTAPPROVEDIPP: String?
        var OPENBAL: String?
        
        func getUsedAmount() -> String? {
            let amount = EFFECTIVELIMIT.toAmount - ACCTAVAILABLELIMIT.toAmount
            return amount.formattedWithComma + " " + EFFECTIVELIMIT.toCurrency
        }
    }
}
