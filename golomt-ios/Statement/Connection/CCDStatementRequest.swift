//
//  CCDStatementRequest.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/21/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class CCDStatementRequest: BaseRequest {
    var body = Body()

    enum RequestType: String {
        case current = "CCDUBS"
        case previous = "CCDPST"
    }

    private enum CodingKeys: String, CodingKey {
        case body
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(body, forKey: .body)
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    override init() {
        super.init()
    }

    struct Body: Codable {
        var ACCOUNT_TYPE: String?
        var ACCOUNT_NICKNAME: String?
        var CARD_NUMBER: String?
        var SELECTED_MONTH: String?
        var SELECTED_CURRENCY_CODE: String?
        var SELECTED_YEAR: String?
    }
}
