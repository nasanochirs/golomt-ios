//
//  StatementModel.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class StatementModel: NSObject {
    var sections = [StatementSection(type: "HEADER", statements: [])]
    
    var pageNumber = 1
    var startDate = ""
    var endDate = ""
    var account = AccountResponse.Account()
    var filterType = ""
    var totalCount = 0
}

class StatementSection {
    var type: String
    var statements: [Any]
    
    enum StaticTypes: String {
        case LIEN
        case CONFIRMED
        case UNCONFIRMED
        case CURRENT_TRANSFER
        case CREDIT_CARD_SUMMARY
    }
    
    init(type: String, statements: [Any]) {
        self.type = type
        self.statements = statements
    }
    
    struct CreditSummary {
        var label: String
        var info: String?
    }
    
    func getStatementHeader() -> String {
        switch type {
        case StaticTypes.LIEN.rawValue:
            return "statement_lien_header".localized()
        case StaticTypes.CONFIRMED.rawValue:
            return "statement_confirmed_header".localized()
        case StaticTypes.UNCONFIRMED.rawValue:
            return "statement_unconfirmed_header".localized()
        case StaticTypes.CREDIT_CARD_SUMMARY.rawValue:
            return "statement_credit_card_summary_header".localized()
        default:
            return type
        }
    }
}
