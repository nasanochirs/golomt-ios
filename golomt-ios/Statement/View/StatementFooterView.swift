//
//  StatementFooterView.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/20/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class StatementFooterView: UIView {
    @IBOutlet var totalIncomeTitleLabel: UILabel!
    @IBOutlet var totalExpenseTitleLabel: UILabel!
    @IBOutlet var totalIncomeLabel: UILabel!
    @IBOutlet var totalExpenseLabel: UILabel!
    @IBOutlet var containerView: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        initComponent()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initComponent()
    }

    func setData(income: Double, expense: Double) {
        totalIncomeLabel.text = "statement_format_income".localized(with: income.formattedWithComma)
        totalExpenseLabel.text = "statement_format_expense".localized(with: expense.formattedWithComma)
    }

    private func initComponent() {
        let bundle = Bundle(for: StatementFooterView.self)
        bundle.loadNibNamed("StatementFooterView", owner: self, options: nil)
        addSubview(containerView)
        containerView.frame = bounds
        containerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        totalIncomeTitleLabel.useSmallFont()
        totalExpenseTitleLabel.useSmallFont()
        totalIncomeTitleLabel.textColor = .defaultSecondaryText
        totalExpenseTitleLabel.textColor = .defaultSecondaryText
        totalIncomeTitleLabel.text = "statement_footer_income".localized()
        totalExpenseTitleLabel.text = "statement_footer_expense".localized()
        totalIncomeLabel.useLargeFont()
        totalIncomeLabel.makeBold()
        totalIncomeLabel.textColor = .defaultGreen
        totalExpenseLabel.useLargeFont()
        totalExpenseLabel.makeBold()
        totalExpenseLabel.textColor = .defaultRed
    }
}
