//
//  LONStatementHeaderCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/27/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class LONStatementHeaderCell: UITableViewCell {
    @IBOutlet var startDateButton: UIButton!
    @IBOutlet var arrowRightImage: UIImageView!
    @IBOutlet var endDateButton: UIButton!
    @IBOutlet var containerView: UIView!
    @IBOutlet var calendarContainerStackView: UIStackView!
    
    @IBAction func dateAction(_ sender: Any) {
        calendarAction()
    }
    
    var onCalendarSelect: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }
    
    func setDateLabel(startDate: String, endDate: String) {
        startDateButton.setTitle(startDate, for: .normal)
        endDateButton.setTitle(endDate, for: .normal)
    }
    
    func setBackground() {
        containerView.backgroundColor = .defaultHeader
    }
    
    @objc private func calendarAction() {
        onCalendarSelect?()
    }
    
    private func initComponent() {
        selectionStyle = .none
        setBackground()
        startDateButton.backgroundColor = .clear
        endDateButton.backgroundColor = .clear
        startDateButton.titleLabel?.useLargeFont()
        startDateButton.titleLabel?.makeBold()
        startDateButton.setTitleColor(.white, for: .normal)
        endDateButton.titleLabel?.useLargeFont()
        endDateButton.titleLabel?.makeBold()
        endDateButton.setTitleColor(.white, for: .normal)
        arrowRightImage.setTint(color: .white)
        calendarContainerStackView.addTapGesture(tapNumber: 1, target: self, action: #selector(calendarAction))
    }
}
