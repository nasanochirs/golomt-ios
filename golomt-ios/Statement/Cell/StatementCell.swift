//
//  StatementCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/16/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class StatementCell: UITableViewCell {
    @IBOutlet var remarkLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }

    func setData(_ data: Any) {
        switch data {
        case let transaction as LienStatementResponse.LienTransaction:
            remarkLabel.text = transaction.REMARKS
            dateLabel.text = transaction.LIEN_DATE?.backwardDateFormatted
            amountLabel.text = "statement_format_expense".localized(with: transaction.AMOUNT.toAmountWithCurrencySymbol)
            amountLabel.textColor = .defaultRed
        case let transaction as OPRStatementResponse.Transaction:
            remarkLabel.text = transaction.TRANS_REMARKS
            dateLabel.text = transaction.TXN_POSTED_DATE?.backwardDateTimeFormatted
            switch transaction.AMT_TYPE {
            case StatementConstants.income:
                amountLabel.text = "statement_format_income".localized(with: transaction.AMOUNT.toAmountWithCurrencySymbol)
                amountLabel.textColor = .defaultGreen
            case StatementConstants.expense:
                amountLabel.text = "statement_format_expense".localized(with: transaction.AMOUNT.toAmountWithCurrencySymbol)
                amountLabel.textColor = .defaultRed
            default:
                break
            }
        case let transaction as DEPStatementResponse.Transaction:
            remarkLabel.text = transaction.TRANSACTION_REMARKS
            dateLabel.text = transaction.TRANSACTION_DATE?.backwardDateTimeFormatted
            switch transaction.TRANSACTION_AMOUNT_TYPE {
            case StatementConstants.income:
                amountLabel.text = "statement_format_income".localized(with: transaction.TRANSACTION_AMOUNT.toAmountWithCurrencySymbol)
                amountLabel.textColor = .defaultGreen
            case StatementConstants.expense:
                amountLabel.text = "statement_format_expense".localized(with: transaction.TRANSACTION_AMOUNT.toAmountWithCurrencySymbol)
                amountLabel.textColor = .defaultRed
            default:
                break
            }
        case let transaction as CCDStatementResponse.Transaction:
            remarkLabel.text = transaction.TRANSACTION_REMARK
            dateLabel.text = transaction.TRANSACTION_DATE?.backwardDateTimeFormatted
            amountLabel.text = transaction.ORIGINAL_AMOUNT.toAmountWithCurrencySymbol
            switch transaction.getTransactionType() {
            case StatementConstants.income:
                amountLabel.textColor = .defaultGreen
            case StatementConstants.expense:
                amountLabel.textColor = .defaultRed
            default: break
            }
        case let transaction as CCDStatementResponse.TransactionHistory:
            remarkLabel.text = transaction.TRXN_DESC_ARRAY
            dateLabel.text = transaction.DATE_ARRAY?.backwardDateTimeFormatted
            amountLabel.text = transaction.TRXN_AMOUNT_ARRAY.toAmountWithCurrencySymbol
            amountLabel.textColor = .defaultRed
        case let transaction as LONStatementResponse.Transaction:
            remarkLabel.text = transaction.LN_TXN_REMARKS_ARRAY
            dateLabel.text = transaction.TXN_DATE_ARRAY?.backwardDateTimeFormatted
            amountLabel.text = "statement_format_expense".localized(with: transaction.getAmount().toAmountWithCurrencySymbol)
            amountLabel.textColor = .defaultRed
        default: break
        }
    }

    private func initComponent() {
        selectionStyle = .default
        remarkLabel.textColor = .defaultPrimaryText
        remarkLabel.makeBold()
        remarkLabel.useSmallFont()
        remarkLabel.lineBreakMode = .byTruncatingTail
        dateLabel.textColor = .defaultSecondaryText
        dateLabel.useSmallFont()
        dateLabel.sizeToFit()
        amountLabel.useLargeFont()
        amountLabel.makeBold()
    }
}
