//
//  StatementCCDHeaderCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/21/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class CCDStatementHeaderCell: UITableViewCell {
    @IBOutlet var containerView: UIView!
    @IBOutlet var monthButton: UIButton!
    @IBOutlet var monthChangeLabel: UILabel!
    var onCalendarSelect: (() -> Void)?

    @IBAction func monthSelectAction(_ sender: Any) {
        onCalendarSelect?()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }

    func setMonthLabel(_ monthLabel: String) {
        monthButton.setTitle(monthLabel, for: .normal)
    }

    func setBackground() {
        containerView.backgroundColor = .defaultHeader
    }

    private func initComponent() {
        selectionStyle = .none
        monthButton.setLeftImage(name: "calendar")
        monthButton.titleLabel?.useLargeFont()
        monthButton.titleLabel?.makeBold()
        monthButton.setTitleColor(.white, for: .normal)
        monthChangeLabel.textColor = .defaultSecondaryText
        monthChangeLabel.useMediumFont()
        monthChangeLabel.text = "statement_change_month_label".localized()
        monthChangeLabel.addTapGesture(tapNumber: 1, target: self, action: #selector(monthSelectAction(_:)))
        setBackground()
    }
}
