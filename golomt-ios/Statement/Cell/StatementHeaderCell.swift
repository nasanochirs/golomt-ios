//
//  StatementHeaderCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class StatementHeaderCell: UITableViewCell {
    @IBOutlet var dateSegmentedControl: UISegmentedControl?
    @IBOutlet var containerView: UIView!
    @IBOutlet var startDateButton: UIButton!
    @IBOutlet var endDateButton: UIButton!
    @IBOutlet var arrowRightImage: UIImageView!
    @IBOutlet var dateStackView: UIStackView!
    @IBAction func startDateAction(_ sender: Any) {
        handleCustomDate()
    }

    @IBAction func endDateButton(_ sender: Any) {
        handleCustomDate()
    }

    var onDateSegmentChange: ((_ selectedIndex: Int?) -> Void)?
    var onCustomDateSelect: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }

    func setDateLabel(startDate: String, endDate: String) {
        startDateButton.setTitle(startDate, for: .normal)
        endDateButton.setTitle(endDate, for: .normal)
    }

    func setBackground() {
        containerView.backgroundColor = .defaultHeader
    }

    func handleCustomDateSelected() {
        dateSegmentedControl?.selectedSegmentIndex = 0
    }

    @objc private func dateChanged() {
        switch dateSegmentedControl?.selectedSegmentIndex {
        case 0:
            handleCustomDate()
        default:
            onDateSegmentChange?(dateSegmentedControl?.selectedSegmentIndex)
        }
    }

    @objc private func handleCustomDate() {
        onCustomDateSelect?()
    }

    private func initComponent() {
        selectionStyle = .none
        initDateSegment()
        setBackground()
        startDateButton.backgroundColor = .clear
        endDateButton.backgroundColor = .clear
        startDateButton.titleLabel?.useLargeFont()
        startDateButton.titleLabel?.makeBold()
        startDateButton.setTitleColor(.white, for: .normal)
        endDateButton.titleLabel?.useLargeFont()
        endDateButton.titleLabel?.makeBold()
        endDateButton.setTitleColor(.white, for: .normal)
        arrowRightImage.setTint(color: .white)
        dateStackView.addTapGesture(tapNumber: 1, target: self, action: #selector(handleCustomDate))
    }

    private func initDateSegment() {
        let selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.defaultPrimaryText]
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.defaultSecondaryText]
        if #available(iOS 13.0, *) {
        } else {
            dateSegmentedControl?.tintColor = .white
        }
        dateSegmentedControl?.setTitleTextAttributes(titleTextAttributes, for: .normal)
        dateSegmentedControl?.setTitleTextAttributes(selectedTitleTextAttributes, for: .selected)
        dateSegmentedControl?.setTitle("statement_custom_date".localized(), forSegmentAt: 0)
        dateSegmentedControl?.setTitle("statement_week".localized(), forSegmentAt: 1)
        dateSegmentedControl?.setTitle("statement_month".localized(), forSegmentAt: 2)
        dateSegmentedControl?.setTitle("statement_quarter".localized(), forSegmentAt: 3)
        dateSegmentedControl?.selectedSegmentIndex = 1
        dateSegmentedControl?.addTarget(self, action: #selector(dateChanged), for: .valueChanged)
    }
}
