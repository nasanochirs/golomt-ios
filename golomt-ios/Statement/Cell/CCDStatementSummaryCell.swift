//
//  CCDStatementSummaryCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/22/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class CCDStatementSummaryCell: UITableViewCell {
    @IBOutlet var firstInfoTitleLabel: UILabel!
    @IBOutlet var secondInfoTitleLabel: UILabel!
    @IBOutlet var firstInfoLabel: UILabel!
    @IBOutlet var secondInfoLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }

    func setSummary(_ summary: [StatementSection.CreditSummary]) {
        let firstInfo = summary.first
        let secondInfo = summary.last
        firstInfoTitleLabel.text = firstInfo?.label
        firstInfoLabel.text = firstInfo?.info
        secondInfoTitleLabel.text = secondInfo?.label
        secondInfoLabel.text = secondInfo?.info
    }

    private func initComponent() {
        selectionStyle = .none
        firstInfoTitleLabel.wordWrap()
        firstInfoTitleLabel.useSmallFont()
        firstInfoTitleLabel.textColor = .defaultSecondaryText
        secondInfoTitleLabel.wordWrap()
        secondInfoTitleLabel.useSmallFont()
        secondInfoTitleLabel.textColor = .defaultSecondaryText
        firstInfoLabel.useLargeFont()
        firstInfoLabel.makeBold()
        firstInfoLabel.textColor = .defaultPrimaryText
        secondInfoLabel.adjustsFontSizeToFitWidth = true
        secondInfoLabel.useLargeFont()
        secondInfoLabel.makeBold()
        secondInfoLabel.textColor = .defaultPrimaryText
    }
}
