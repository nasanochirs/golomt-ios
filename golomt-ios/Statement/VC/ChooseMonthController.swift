//
//  ChooseMonthController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/21/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class ChooseMonthController: PopupController {
    var monthIndex = 0
    var monthLabel = ""
    var year = Date().getYear()
    var onMonthSelect: ((_ monthIndex: String, _ monthLabel: String, _ year: String) -> Void)?
    
    lazy var pickerView: UIPickerView = {
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 214))
        pickerView.backgroundColor = .defaultPrimaryBackground
        pickerView.delegate = self
        return pickerView
    }()
    
    lazy var chooseButton: UIButton = {
        let button = DefaultGradientButton(frame: CGRect(x: 20, y: self.pickerView.frame.maxY + 20, width: self.view.frame.size.width - 40, height: 60))
        button.setTitle("statement_month_picker_choose".localized(), for: .normal)
        button.addTarget(self, action: #selector(handleMonthSelection), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initComponent()
    }
    
    @objc private func handleMonthSelection() {
        onMonthSelect?(monthIndex.toString, monthLabel, year.toString)
    }
    
    private func initComponent() {
        view.addSubview(pickerView)
        view.addSubview(chooseButton)
        
        title = "statement_month_picker_title".localized()
        
        var height = CGFloat(chooseButton.frame.maxY)
        height += view.safeAreaInsets.bottom
        height += 20
        contentSizeInPopup = CGSize(width: view.bounds.size.width, height: height)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        pickerView.selectRow(monthIndex, inComponent: 0, animated: true)
    }
}

extension ChooseMonthController: UIPickerViewDataSource, UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        40.0
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        12
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let date = Calendar.current.date(byAdding: .month, value: -row, to: Date())
        guard let unwrappedDate = date else {
            return nil
        }
        return unwrappedDate.getMonthYearLabel()
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let date = Calendar.current.date(byAdding: .month, value: -row, to: Date())
        guard let unwrappedDate = date else {
            return
        }
        monthIndex = row
        monthLabel = unwrappedDate.getMonthYearLabel()
        year = unwrappedDate.getYear()
    }
}
