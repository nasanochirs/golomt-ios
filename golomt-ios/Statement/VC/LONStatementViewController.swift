//
//  LONStatementViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/27/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class LONStatementViewController: BaseUIViewController {
    @IBOutlet var tableView: UITableView!

    let statementModel = StatementModel()
    var account = AccountResponse.Account()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "statement_title".localized(with: account.ACCT_NUMBER.orEmpty, account.ACCT_CURRENCY.orEmpty)
        navigationItem.largeTitleDisplayMode = .never
        setupTableView()
        initStatement()
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        tableView.reloadData()
    }

    private func setupTableView() {
        tableView.separatorColor = .defaultSeparator
        tableView.registerCell(nibName: "StatementCell")
        tableView.registerCell(nibName: "LONStatementHeaderCell")
        tableView.tableFooterView = StatementFooterView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 100))
        tableView.hideScrollIndicators()
    }

    private func initStatement() {
        let startDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())
        let endDate = Date()
        guard let unwrappedStartDate = startDate else {
            return
        }
        statementModel.startDate = unwrappedStartDate.toFormatted
        statementModel.endDate = endDate.toFormatted
        fetchStatement()
    }

    private func fetchStatement() {
        statementModel.sections = [StatementSection(type: "HEADER", statements: [])]
        statementModel.account = account

        fetchLONStatement()
    }

    private func fetchLONStatement() {
        showLoader()
        ConnectionFactory.fetchLONStatement(
            model: statementModel,
            success: { response in
                let transactions = response.transactionList ?? []
                let transactionByDate = Dictionary(grouping: transactions, by: { $0.TXN_DATE_ARRAY.backwardDateFormatted }).sorted { $1.0 < $0.0 }
                transactionByDate.forEach { (key: String, value: [LONStatementResponse.Transaction]) in
                    self.statementModel.sections.append(StatementSection(type: key, statements: value))
                    self.tableView.reloadData()
                }
                self.hideLoader()
            }, failed: { reason in
                self.tableView.reloadData()
                self.handleRequestFailure(reason)
                self.hideLoader()
            }
        )
    }

    private func resetStatements() {
        statementModel.sections = [StatementSection(type: "HEADER", statements: [])]
        statementModel.account = account
        statementModel.pageNumber = 1
        tableView.reloadData()
    }
}

extension LONStatementViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return statementModel.sections.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0: return 0
        default: return 60
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0: return nil
        default:
            let section = statementModel.sections[section]
            let header = UIButton()
            header.setTitle(section.getStatementHeader(), for: .normal)
            header.contentEdgeInsets = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
            header.titleLabel?.useMediumFont()
            header.titleLabel?.makeBold()
            header.sideCorner(cornerRadius: 10, side: .Right)
            header.sizeToFit()
            header.frame.size = header.intrinsicContentSize
            switch isDark() {
            case true:
                header.setDefaultPurpleGradient()
            case false:
                header.setDefaultBlueGradient()
            }
            let headerContainerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
            switch section.type {
            case StatementSection.StaticTypes.CREDIT_CARD_SUMMARY.rawValue:
                headerContainerView.backgroundColor = .defaultPrimaryBackground
            default:
                headerContainerView.backgroundColor = .defaultSecondaryBackground
            }
            headerContainerView.addSubview(header)
            header.translatesAutoresizingMaskIntoConstraints = false
            header.centerYAnchor.constraint(equalTo: headerContainerView.centerYAnchor).isActive = true
            return headerContainerView
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 1
        default:
            let section = statementModel.sections[section]
            return section.statements.count
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LONStatementHeaderCell") as? LONStatementHeaderCell
            guard let unwrappedCell = cell else {
                return UITableViewCell()
            }
            unwrappedCell.setDateLabel(startDate: statementModel.startDate, endDate: statementModel.endDate)
            unwrappedCell.onCalendarSelect = {
                self.resetStatements()
                let controller = ChooseDateRangeController()
                controller.onDateRangeSelect = { startDate, endDate in
                    controller.dismiss(animated: true, completion: {
                        self.statementModel.startDate = startDate
                        self.statementModel.endDate = endDate
                        self.fetchStatement()
                        })
                }
                let popupController = STPopupController(rootViewController: controller)
                popupController.style = .bottomSheet
                popupController.present(in: self)
                return
            }
            return unwrappedCell

        default:
            let section = statementModel.sections[indexPath.section]
            let statement = section.statements[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "StatementCell") as? StatementCell
            guard let unwrappedCell = cell else {
                return UITableViewCell()
            }
            unwrappedCell.setData(statement)
            return unwrappedCell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = statementModel.sections[indexPath.section]
        if section.type != "HEADER" {
            let statement = section.statements[indexPath.row]
            let detailVC = StatementDetailViewController(nibName: "StatementDetailViewController", bundle: nil)
            detailVC.statement = statement
            let navigationViewController = BaseNavigationViewController(rootViewController: detailVC)
            present(navigationViewController, animated: true)
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var totalExpense = 0.0
        statementModel.sections.forEach { section in
            section.statements.forEach { statement in
                if let statement = statement as? LONStatementResponse.Transaction {
                    totalExpense += statement.getAmount().toAmount
                }
            }
        }
        let footerView = tableView.tableFooterView as? StatementFooterView
        footerView?.setData(income: 0.0, expense: totalExpense)
    }
}
