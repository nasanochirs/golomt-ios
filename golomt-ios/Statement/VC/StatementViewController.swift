//
//  StatementVC.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import FSCalendar
import STPopup
import UIKit

class StatementViewController: BaseUIViewController {
    @IBOutlet var statementTableView: UITableView!

    let statementModel = StatementModel()
    var account = AccountResponse.Account()

    lazy var filterButton: UIBarButtonItem? = {
        UIBarButtonItem(image: UIImage(named: "filter"), style: .plain, target: self, action: #selector(handleFilter))
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "statement_title".localized(with: account.ACCT_NUMBER.orEmpty, account.ACCT_CURRENCY.orEmpty)
        navigationItem.largeTitleDisplayMode = .never
        if let filterButton = filterButton {
            navigationItem.rightBarButtonItems = [filterButton]
        }
        setupTableView()
        fetchInitialStatement()
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        statementTableView.reloadData()
    }

    @objc private func handleFilter() {
        let filterDialog = UIAlertController(title: "statement_filter_dialog_title".localized(), message: nil, preferredStyle: .actionSheet)
        let incomeFilterAction = UIAlertAction(title: "statement_income_filter".localized(), style: .default, handler: { _ in
            self.statementModel.filterType = StatementConstants.income
            self.fetchStatement()
        })
        let expenseFilterAction = UIAlertAction(title: "statement_expense_filter".localized(), style: .default, handler: { _ in
            self.statementModel.filterType = StatementConstants.expense
            self.fetchStatement()
        })
        let noFilterAction = UIAlertAction(title: "statement_no_filter".localized(), style: .default, handler: { _ in
            self.statementModel.filterType = ""
            self.fetchStatement()
        })
        let cancelFilterAction = UIAlertAction(title: "statement_cancel_filter".localized(), style: .cancel, handler: nil)
        filterDialog.addAction(incomeFilterAction)
        filterDialog.addAction(expenseFilterAction)
        filterDialog.addAction(noFilterAction)
        filterDialog.addAction(cancelFilterAction)
        present(filterDialog, animated: true)
    }

    private func fetchInitialStatement() {
        let startDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())
        let endDate = Date()
        guard let unwrappedStartDate = startDate else {
            return
        }
        statementModel.startDate = unwrappedStartDate.toFormatted
        statementModel.endDate = endDate.toFormatted
        fetchStatement()
    }

    private func setupTableView() {
        statementTableView.separatorColor = .defaultSeparator
        statementTableView.registerCell(nibName: "StatementHeaderCell")
        statementTableView.registerCell(nibName: "StatementCell")
        statementTableView.estimatedRowHeight = 150
        statementTableView.tableFooterView = StatementFooterView(frame: CGRect(x: 0, y: 0, width: statementTableView.bounds.width, height: 100))
        statementTableView.hideScrollIndicators()
    }

    private func resetStatements() {
        statementModel.sections = [StatementSection(type: "HEADER", statements: [])]
        statementModel.account = account
        statementModel.pageNumber = 1
        statementTableView.reloadData()
    }

    private func fetchStatement() {
        resetStatements()
        switch account.MAIN_ACCT_TYPE {
        case AccountConstants.operative:
            fetchLienStatement()
        case AccountConstants.deposit:
            fetchDEPStatement()
        default:
            break
        }
    }

    private func fetchLienStatement() {
        showLoader()
        ConnectionFactory.fetchLienStatement(
            accountNumber: account.ACCT_NUMBER.orEmpty,
            success: { response in
                let lienList = response.lienTransactionList ?? []
                var newLienList = [LienStatementResponse.LienTransaction]()
                lienList.forEach { lien in
                    if let lienDate = lien.LIEN_DATE?.toBackwardDate {
                        let startDate = self.statementModel.startDate.toDate!
                        let endDate = self.statementModel.endDate.toDate!
                        switch true {
                        case lienDate.compare(startDate) != lienDate.compare(endDate):
                            newLienList.append(lien)
                        case lienDate.toFormatted == self.statementModel.startDate:
                            newLienList.append(lien)
                        case lienDate.toFormatted == self.statementModel.endDate:
                            newLienList.append(lien)
                        default: break
                        }
                    }
                }
                let lienCount = newLienList.count
                if lienCount > 0 {
                    self.statementModel.sections.append(StatementSection(type: StatementSection.StaticTypes.LIEN.rawValue, statements: newLienList))
                }
                self.hideLoader()
                self.fetchOPRStatement()
            }, failed: { _ in
                self.hideLoader()
                self.fetchOPRStatement()
            }
        )
    }

    private func fetchOPRStatement() {
        showLoader()
        ConnectionFactory.fetchOPRStatement(
            model: statementModel,
            success: { response in
                let transactions = response.transactionList ?? []
                self.statementModel.totalCount = response.transactionHistory?.TOTAL_RECORD.toInt ?? 0
                let transactionByDate = Dictionary(grouping: transactions, by: { $0.TXN_POSTED_DATE.backwardDateFormatted }).sorted { $1.0 < $0.0 }
                transactionLoop: for (key, value) in transactionByDate {
                    for section in self.statementModel.sections {
                        if section.type == key {
                            section.statements.append(contentsOf: value)
                            continue transactionLoop
                        }
                    }
                    self.statementModel.sections.append(StatementSection(type: key, statements: value))
                }
                self.statementTableView.reloadData()
                self.hideLoader()
            }, failed: { reason in
                self.statementTableView.reloadData()
                self.handleRequestFailure(reason)
                self.hideLoader()
            }
        )
    }

    private func fetchDEPStatement() {
        showLoader()
        ConnectionFactory.fetchDEPStatement(
            model: statementModel,
            success: { response in
                let transactions = response.transactionList ?? []
                let transactionByDate = Dictionary(grouping: transactions, by: { $0.LAST_TRANSACTION_DATE.orEmpty }).sorted { $1.0 < $0.0 }
                transactionByDate.forEach { (key: String, value: [DEPStatementResponse.Transaction]) in
                    self.statementModel.sections.append(StatementSection(type: key, statements: value))
                    self.statementTableView.reloadData()
                }
                self.hideLoader()
            }, failed: { reason in
                self.statementTableView.reloadData()
                self.handleRequestFailure(reason)
                self.hideLoader()
            }
        )
    }

    private func loadMore() {
        statementModel.pageNumber += 1
        fetchOPRStatement()
    }
}

extension StatementViewController: FSCalendarDataSource, FSCalendarDelegate {}

extension StatementViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return statementModel.sections.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0: return 0
        default: return 60
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0: return nil
        default:
            let section = statementModel.sections[section]
            let header = UIButton()
            header.setTitle(section.getStatementHeader(), for: .normal)
            header.contentEdgeInsets = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
            header.titleLabel?.useMediumFont()
            header.titleLabel?.makeBold()
            header.sideCorner(cornerRadius: 10, side: .Right)
            header.sizeToFit()
            header.frame.size = header.intrinsicContentSize
            switch isDark() {
            case true:
                header.setDefaultPurpleGradient()
            case false:
                header.setDefaultBlueGradient()
            }
            let headerContainerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
            headerContainerView.backgroundColor = .defaultSecondaryBackground
            headerContainerView.addSubview(header)
            header.translatesAutoresizingMaskIntoConstraints = false
            header.centerYAnchor.constraint(equalTo: headerContainerView.centerYAnchor).isActive = true
            return headerContainerView
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 1
        default:
            let section = statementModel.sections[section]
            return section.statements.count
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "StatementHeaderCell") as? StatementHeaderCell
            guard let unwrappedCell = cell else {
                return UITableViewCell()
            }
            unwrappedCell.onCustomDateSelect = {
                let controller = ChooseDateRangeController()
                controller.onDateRangeSelect = { startDate, endDate in
                    controller.dismiss(animated: true, completion: {
                        self.statementModel.startDate = startDate
                        self.statementModel.endDate = endDate
                        self.fetchStatement()
                        unwrappedCell.handleCustomDateSelected()
                    })
                }
                let popupController = STPopupController(rootViewController: controller)
                popupController.style = .bottomSheet
                popupController.present(in: self)
            }
            unwrappedCell.onDateSegmentChange = { index in
                var startDate: Date? = Date()
                let endDate = Date()
                switch index {
                case 0:
                    break
                case 1:
                    startDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())
                case 2:
                    startDate = Calendar.current.date(byAdding: .day, value: -30, to: Date())
                case 3:
                    startDate = Calendar.current.date(byAdding: .day, value: -90, to: Date())
                default:
                    break
                }
                guard let unwrappedStartDate = startDate else {
                    return
                }
                self.statementModel.startDate = unwrappedStartDate.toFormatted
                self.statementModel.endDate = endDate.toFormatted
                self.fetchStatement()
            }
            unwrappedCell.setBackground()
            unwrappedCell.setDateLabel(startDate: statementModel.startDate, endDate: statementModel.endDate)
            return unwrappedCell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "StatementCell") as? StatementCell
            guard let unwrappedCell = cell else {
                return UITableViewCell()
            }
            let section = statementModel.sections[indexPath.section]
            let statement = section.statements[indexPath.row]
            unwrappedCell.setData(statement)
            return unwrappedCell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = statementModel.sections[indexPath.section]
        if section.type != "HEADER" {
            let statement = section.statements[indexPath.row]
            let detailVC = StatementDetailViewController(nibName: "StatementDetailViewController", bundle: nil)
            detailVC.account = account
            detailVC.statement = statement
            let navigationViewController = BaseNavigationViewController(rootViewController: detailVC)
            present(navigationViewController, animated: true)
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let sectionCount = statementModel.sections.count - 1
        let sectionStatementCount = statementModel.sections[indexPath.section].statements.count
        let totalCount = statementModel.totalCount
        var count = 0
        var totalIncome = 0.0
        var totalExpense = 0.0
        statementModel.sections.forEach { section in
            if section.type != StatementSection.StaticTypes.LIEN.rawValue {
                count += section.statements.count
                section.statements.forEach { statement in
                    switch statement {
                    case let OPRStatement as OPRStatementResponse.Transaction:
                        switch OPRStatement.AMT_TYPE {
                        case StatementConstants.income:
                            totalIncome += OPRStatement.AMOUNT.toAmount
                        case StatementConstants.expense:
                            totalExpense += OPRStatement.AMOUNT.toAmount
                        default:
                            break
                        }
                    case let DEPStatement as DEPStatementResponse.Transaction:
                        switch DEPStatement.TRANSACTION_AMOUNT_TYPE {
                        case StatementConstants.income:
                            totalIncome += DEPStatement.TRANSACTION_AMOUNT.toAmount
                        case StatementConstants.expense:
                            totalExpense += DEPStatement.TRANSACTION_AMOUNT.toAmount
                        default:
                            break
                        }
                    default: break
                    }
                }
            }
        }
        let footerView = statementTableView.tableFooterView as? StatementFooterView
        footerView?.setData(income: totalIncome, expense: totalExpense)
        if sectionCount > 0, sectionCount == indexPath.section, sectionStatementCount - 1 == indexPath.row, totalCount > count {
            if isLoading() == false {
                loadMore()
            }
        }
    }
}
