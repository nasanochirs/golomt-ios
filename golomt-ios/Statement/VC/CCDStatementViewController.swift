//
//  CCDStatementViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/22/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class CCDStatementViewController: BaseUIViewController {
    @IBOutlet var tableView: UITableView!

    let statementModel = StatementModel()
    var account = AccountResponse.Account()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "statement_title".localized(with: account.ACCT_NUMBER.orEmpty, account.ACCT_CURRENCY.orEmpty)
        navigationItem.largeTitleDisplayMode = .never
        setupTableView()
        initStatement()
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        tableView.reloadData()
    }

    private func setupTableView() {
        tableView.separatorColor = .defaultSeparator
        tableView.registerCell(nibName: "StatementCell")
        tableView.registerCell(nibName: "CCDStatementSummaryCell")
        tableView.registerCell(nibName: "CCDStatementHeaderCell")
        tableView.tableFooterView = StatementFooterView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 100))
        tableView.hideScrollIndicators()
    }

    private func initStatement() {
        statementModel.startDate = "0"
        statementModel.endDate = Date().getYear().toString
        fetchStatement()
    }

    private func fetchStatement() {
        statementModel.sections = [StatementSection(type: "HEADER", statements: [])]
        statementModel.account = account

        fetchCCDStatement()
    }

    private func fetchCCDStatement() {
        showLoader()
        ConnectionFactory.fetchCCDStatement(
            model: statementModel,
            success: { response in
                switch self.statementModel.startDate {
                case "0":
                    let transactionHistory = response.transactionHistoryList ?? []
                    if transactionHistory.count > 0 {
                        self.statementModel.sections.append(StatementSection(type: StatementSection.StaticTypes.UNCONFIRMED.rawValue, statements: transactionHistory))
                    }
                    let transactions = response.transactionList ?? []
                    self.statementModel.sections.append(StatementSection(type: StatementSection.StaticTypes.CONFIRMED.rawValue, statements: transactions))
                    self.tableView.reloadData()
                default:
                    let summary = response.summary
                    let customFormatter = DateFormatter()
                    customFormatter.dateFormat = "yyyy-MM-dd h:mm:ss a"
                    let formattedDueDate = customFormatter.date(from: summary?.LASTDUEDATE ?? "")
                    var summarySections = [
                        StatementSection.CreditSummary(
                            label: "statement_previous_month_balance_title".localized(),
                            info: summary?.OPENBAL.toAmountWithCurrency
                        ),
                        StatementSection.CreditSummary(
                            label: "statement_current_month_balance_title".localized(),
                            info: summary?.TOTCRAMT.toAmountWithCurrency
                        ),
                        StatementSection.CreditSummary(
                            label: "statement_minimum_payment_due_title".localized(),
                            info: summary?.MINPAYMENTDUEAMT.toAmountWithCurrency
                        ),
                        StatementSection.CreditSummary(
                            label: "statement_payment_due_date_title".localized(),
                            info: formattedDueDate?.toFormatted
                        ),
                        StatementSection.CreditSummary(
                            label: "statement_total_payment_title".localized(),
                            info: summary?.TOTDBAMT.toAmountWithCurrency
                        ),
                        StatementSection.CreditSummary(
                            label: "statement_total_expense_title".localized(),
                            info: summary?.CURRBAL.toAmountWithCurrency
                        ),
                        StatementSection.CreditSummary(
                            label: "statement_credit_limit_title".localized(),
                            info: summary?.EFFECTIVELIMIT.toAmountWithCurrency
                        ),
                        StatementSection.CreditSummary(
                            label: "statement_current_used_limit_title".localized(),
                            info: summary?.getUsedAmount()
                        ),
                        StatementSection.CreditSummary(
                            label: "statement_available_limit_title".localized(),
                            info: summary?.ACCTAVAILABLELIMIT.toAmountWithCurrency
                        ),
                        StatementSection.CreditSummary(
                            label: "statement_interest_charge_title".localized(),
                            info: summary?.INTERESTCHARGE.toAmountWithCurrency
                        )
                    ]

                    switch true {
                    case summary?.ACCTAPPROVEDIPP?.toAmount ?? 0.0 > 0.0:
                        summarySections.append(
                            StatementSection.CreditSummary(
                                label: "statement_installment_loan_title".localized(),
                                info: summary?.ACCTAPPROVEDIPP.toAmountWithCurrency
                            ))
                    case summary?.LOYALTYPOINT?.toAmount ?? 0.0 > 0.0:
                        summarySections.append(
                            StatementSection.CreditSummary(
                                label: "statement_loyalty_points_title".localized(),
                                info: summary?.LOYALTYPOINT.toAmountWithCurrency
                            ))
                    case summary?.EXTRAVAGANTSPENDING?.toAmount ?? 0.0 > 0.0:
                        summarySections.append(
                            StatementSection.CreditSummary(
                                label: "statement_over_credit_limit_title".localized(),
                                info: summary?.EXTRAVAGANTSPENDING.toAmountWithCurrency
                            ))
                    case summary?.EXCEEDLIMITCHARGE?.toAmount ?? 0.0 > 0.0:
                        summarySections.append(
                            StatementSection.CreditSummary(
                                label: "statement_excess_limit_charge_title".localized(),
                                info: summary?.EXCEEDLIMITCHARGE.toAmountWithCurrency
                            ))
                    case summary?.LATELIMITCHARGE?.toAmount ?? 0.0 > 0.0:
                        summarySections.append(
                            StatementSection.CreditSummary(
                                label: "statement_late_payment_charge_title".localized(),
                                info: summary?.LATELIMITCHARGE.toAmountWithCurrency
                            ))
                    default:
                        break
                    }

                    self.statementModel.sections.append(StatementSection(type: StatementSection.StaticTypes.CREDIT_CARD_SUMMARY.rawValue, statements: summarySections.chunked(into: 2)))

                    let transactions = response.transactionList ?? []
                    let transactionByDate = Dictionary(grouping: transactions, by: { $0.TRANSACTION_DATE.orEmpty }).sorted { $1.0 < $0.0 }
                    transactionByDate.forEach { (key: String, value: [CCDStatementResponse.Transaction]) in
                        self.statementModel.sections.append(StatementSection(type: key.backwardDateFormatted, statements: value))
                        self.tableView.reloadData()
                    }
                }
                self.hideLoader()
            }, failed: { reason in
                self.tableView.reloadData()
                self.handleRequestFailure(reason)
                self.hideLoader()
            }
        )
    }
}

extension CCDStatementViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return statementModel.sections.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0: return 0
        default: return 60
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0: return nil
        default:
            let section = statementModel.sections[section]
            let header = UIButton()
            header.setTitle(section.getStatementHeader(), for: .normal)
            header.contentEdgeInsets = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
            header.titleLabel?.useMediumFont()
            header.titleLabel?.makeBold()
            header.sideCorner(cornerRadius: 10, side: .Right)
            header.sizeToFit()
            header.frame.size = header.intrinsicContentSize
            switch isDark() {
            case true:
                header.setDefaultPurpleGradient()
            case false:
                header.setDefaultBlueGradient()
            }
            let headerContainerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
            switch section.type {
            case StatementSection.StaticTypes.CREDIT_CARD_SUMMARY.rawValue:
                headerContainerView.backgroundColor = .defaultPrimaryBackground
            default:
                headerContainerView.backgroundColor = .defaultSecondaryBackground
            }
            headerContainerView.addSubview(header)
            header.translatesAutoresizingMaskIntoConstraints = false
            header.centerYAnchor.constraint(equalTo: headerContainerView.centerYAnchor).isActive = true
            return headerContainerView
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 1
        default:
            let section = statementModel.sections[section]
            return section.statements.count
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = statementModel.sections[indexPath.section]
        switch section.type {
        case "HEADER":
            return UITableView.automaticDimension
        default:
            return UITableView.automaticDimension
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CCDStatementHeaderCell") as? CCDStatementHeaderCell
            guard let unwrappedCell = cell else {
                return UITableViewCell()
            }
            let date = Calendar.current.date(byAdding: .month, value: -statementModel.startDate.toInt, to: Date())
            if let date = date {
                unwrappedCell.setMonthLabel(date.getMonthYearLabel())
            }
            unwrappedCell.onCalendarSelect = {
                let controller = ChooseMonthController()
                controller.monthIndex = self.statementModel.startDate.toInt
                controller.onMonthSelect = { monthIndex, monthLabel, year in
                    controller.dismiss(animated: true, completion: {
                        unwrappedCell.setMonthLabel(monthLabel)
                        self.statementModel.startDate = monthIndex
                        self.statementModel.endDate = year
                        self.fetchStatement()
                    })
                }
                let popupController = STPopupController(rootViewController: controller)
                popupController.style = .bottomSheet
                popupController.present(in: self)
            }
            return unwrappedCell

        default:
            let section = statementModel.sections[indexPath.section]
            let statement = section.statements[indexPath.row]
            switch statement {
            case let summary as [StatementSection.CreditSummary]:
                let cell = tableView.dequeueReusableCell(withIdentifier: "CCDStatementSummaryCell") as? CCDStatementSummaryCell
                guard let unwrappedCell = cell else {
                    return UITableViewCell()
                }
                unwrappedCell.setSummary(summary)
                return unwrappedCell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "StatementCell") as? StatementCell
                guard let unwrappedCell = cell else {
                    return UITableViewCell()
                }
                unwrappedCell.setData(statement)
                return unwrappedCell
            }
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = statementModel.sections[indexPath.section]
        if section.type != "HEADER" {
            let statement = section.statements[indexPath.row]
            if statement is [StatementSection.CreditSummary] {
                return
            }
            let detailVC = StatementDetailViewController(nibName: "StatementDetailViewController", bundle: nil)
            detailVC.statement = statement
            let navigationViewController = BaseNavigationViewController(rootViewController: detailVC)
            present(navigationViewController, animated: true)
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var totalIncome = 0.0
        var totalExpense = 0.0
        statementModel.sections.forEach { section in
            section.statements.forEach { statement in
                switch statement {
                case let transaction as CCDStatementResponse.Transaction:
                    // TO DO

                    switch transaction.getTransactionType() {
                    case StatementConstants.income:
                        totalIncome += transaction.ORIGINAL_AMOUNT.toAmount
                    case StatementConstants.expense:
                        totalExpense += transaction.ORIGINAL_AMOUNT.toAmount
                    default:
                        break
                    }
                default:
                    break
                }
            }
        }
        let footerView = tableView.tableFooterView as? StatementFooterView
        footerView?.setData(income: totalIncome, expense: totalExpense * -1)
    }
}
