//
//  StatementDetailViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/28/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class StatementDetailViewController: BaseUIViewController {
    var statement: Any?
    var account: AccountResponse.Account?
    var model = TableViewModel()
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "statement_detail_title".localized()
        initComponent()
        setupNavigationButton()
    }
    
    private func setupNavigationButton() {
        if #available(iOS 13.0, *) {
            self.navigationItem.leftBarButtonItem = nil
        } else {
            let closeButtonItem = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(dismissController))
            navigationItem.leftBarButtonItem = closeButtonItem
        }
    }
    
    private func initComponent() {
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.registerCell(nibName: "DefaultVerticalTitleLabelCell")
        tableView.estimatedSectionHeaderHeight = 100
        tableView.tableFooterView = UIView()
        
        let accountNumberRow = TableViewModel.Row(
            title: "statement_detail_transaction_sender_title".localized(), info: account?.ACCT_NUMBER ?? ""
        )
        
        switch statement {
        case let transaction as LienStatementResponse.LienTransaction:
            model.sections.append(
                TableViewModel.Section(
                    title: "statement_detail_header".localized(),
                    rows: [
                        accountNumberRow,
                        TableViewModel.Row(
                            title: "statement_detail_transaction_amount_title".localized(), info: transaction.AMOUNT.toAmountWithCurrency, infoProperty: .Expense
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_type_title".localized(), info: "statement_detail_transaction_type_expense_label".localized()
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_remark_title".localized(), info: transaction.REMARKS.orEmpty
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_date_title".localized(), info: transaction.LIEN_DATE.backwardDateFormatted
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_time_title".localized(), info: transaction.LIEN_DATE.backwardTimeFormatted
                        )
                    ]
                )
            )
        case let transaction as OPRStatementResponse.Transaction:
            let transactionType: String
            let transactionTypeProperty: TableViewModel.InfoProperty
            let transactionAmount: String
            switch transaction.AMT_TYPE {
            case StatementConstants.income:
                transactionType = "statement_detail_transaction_type_income_label".localized()
                transactionTypeProperty = .Income
                transactionAmount = "statement_format_income".localized(with: transaction.AMOUNT.toAmountWithCurrency)
            case StatementConstants.expense:
                transactionType = "statement_detail_transaction_type_expense_label".localized()
                transactionTypeProperty = .Expense
                transactionAmount = "statement_format_expense".localized(with: transaction.AMOUNT.toAmountWithCurrency)
            default:
                transactionType = ""
                transactionTypeProperty = .Default
                transactionAmount = transaction.AMOUNT.toAmountWithCurrency
            }
            let section = TableViewModel.Section(
                title: "statement_detail_header".localized(),
                rows: [
                    accountNumberRow
                ]
            )
            if transaction.COUNTER_PARTY_NAME != nil, !transaction.COUNTER_PARTY_NAME!.isEmpty {
                section.rows.append(
                    TableViewModel.Row(
                        title: "statement_detail_transaction_beneficiary_title".localized(), info: transaction.COUNTER_PARTY_NAME!
                    )
                )
            }
            section.rows.append(contentsOf:
                [
                    TableViewModel.Row(
                        title: "statement_detail_transaction_amount_title".localized(), info: transactionAmount, infoProperty: transactionTypeProperty
                    ),
                    TableViewModel.Row(
                        title: "statement_detail_transaction_type_title".localized(), info: transactionType
                    ),
                    TableViewModel.Row(
                        title: "statement_detail_transaction_remark_title".localized(), info: transaction.TRANS_REMARKS.orEmpty
                    ),
                    TableViewModel.Row(
                        title: "statement_detail_transaction_date_title".localized(), info: transaction.TXN_POSTED_DATE.backwardDateFormatted
                    ),
                    TableViewModel.Row(
                        title: "statement_detail_transaction_time_title".localized(), info: transaction.TXN_POSTED_DATE.backwardTimeFormatted
                    ),
                    TableViewModel.Row(
                        title: "statement_detail_transaction_balance_title".localized(), info: transaction.TRANS_BALANCE.toAmountWithCurrency
                    )
                ]
            )
            model.sections.append(
                section
            )
        case let transaction as DEPStatementResponse.Transaction:
            let transactionType: String
            let transactionTypeProperty: TableViewModel.InfoProperty
            let transactionAmount: String
            switch transaction.TRANSACTION_AMOUNT_TYPE {
            case StatementConstants.income:
                transactionType = "statement_detail_transaction_type_income_label".localized()
                transactionTypeProperty = .Income
                transactionAmount = "statement_format_income".localized(with: transaction.TRANSACTION_AMOUNT.toAmountWithCurrency)
            case StatementConstants.expense:
                transactionType = "statement_detail_transaction_type_expense_label".localized()
                transactionTypeProperty = .Expense
                transactionAmount = "statement_format_expense".localized(with: transaction.TRANSACTION_AMOUNT.toAmountWithCurrency)
            default:
                transactionType = ""
                transactionTypeProperty = .Default
                transactionAmount = transaction.TRANSACTION_AMOUNT.toAmountWithCurrency
            }
            model.sections.append(
                TableViewModel.Section(
                    title: "statement_detail_header".localized(),
                    rows: [
                        accountNumberRow,
                        TableViewModel.Row(
                            title: "statement_detail_transaction_amount_title".localized(), info: transactionAmount, infoProperty: transactionTypeProperty
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_type_title".localized(), info: transactionType
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_remark_title".localized(), info: transaction.TRANSACTION_REMARKS.orEmpty
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_date_title".localized(), info: transaction.LAST_POSTED_DATE.orEmpty
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_balance_title".localized(), info: transaction.LAST_TRANSACTION_BALANCE.toAmountWithCurrency
                        )
                    ]
                )
            )
        case let transaction as CCDStatementResponse.Transaction:
            let transactionType: String
            let transactionTypeProperty: TableViewModel.InfoProperty
            let transactionAmount: String
            switch transaction.getTransactionType() {
            case StatementConstants.income:
                transactionType = "statement_detail_transaction_type_income_label".localized()
                transactionTypeProperty = .Income
                transactionAmount = "statement_format_income".localized(with: transaction.ORIGINAL_AMOUNT.toAmountWithCurrency)
            case StatementConstants.expense:
                transactionType = "statement_detail_transaction_type_expense_label".localized()
                transactionTypeProperty = .Expense
                transactionAmount = transaction.ORIGINAL_AMOUNT.toAmountWithCurrency
            default:
                transactionType = ""
                transactionTypeProperty = .Default
                transactionAmount = transaction.ORIGINAL_AMOUNT.toAmountWithCurrency
            }
            model.sections.append(
                TableViewModel.Section(
                    title: "statement_detail_header".localized(),
                    rows: [
                        accountNumberRow,
                        TableViewModel.Row(
                            title: "statement_detail_transaction_amount_title".localized(), info: transactionAmount, infoProperty: transactionTypeProperty
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_type_title".localized(), info: transactionType
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_remark_title".localized(), info: transaction.TRANSACTION_REMARK.orEmpty
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_date_title".localized(), info: transaction.POSTED_DATE.backwardDateFormatted
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_time_title".localized(), info: transaction.POSTED_DATE.backwardTimeFormatted
                        )
                    ]
                )
            )
        case let transaction as CCDStatementResponse.TransactionHistory:
            model.sections.append(
                TableViewModel.Section(
                    title: "statement_detail_header".localized(),
                    rows: [
                        accountNumberRow,
                        TableViewModel.Row(
                            title: "statement_detail_transaction_amount_title".localized(), info: transaction.TRXN_AMOUNT_ARRAY.toAmountWithCurrency, infoProperty: .Expense
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_type_title".localized(), info: "statement_detail_transaction_type_expense_label".localized()
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_remark_title".localized(), info: transaction.TRXN_DESC_ARRAY.orEmpty
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_date_title".localized(), info: transaction.DATE_ARRAY.backwardDateFormatted
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_time_title".localized(), info: transaction.DATE_ARRAY.backwardTimeFormatted
                        )
                    ]
                )
            )
        case let transaction as LONStatementResponse.Transaction:
            let amount = "statement_format_expense".localized(with: transaction.getAmount().toAmountWithCurrency)
            model.sections.append(
                TableViewModel.Section(
                    title: "statement_detail_header".localized(),
                    rows: [
                        accountNumberRow,
                        TableViewModel.Row(
                            title: "statement_detail_transaction_amount_title".localized(), info: amount, infoProperty: .Expense
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_type_title".localized(), info: "statement_detail_transaction_type_expense_label".localized()
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_remark_title".localized(), info: transaction.LN_TXN_REMARKS_ARRAY.orEmpty
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_date_title".localized(), info: transaction.TXN_DATE_ARRAY.backwardDateFormatted
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_time_title".localized(), info: transaction.TXN_DATE_ARRAY.backwardTimeFormatted
                        ),
                        TableViewModel.Row(
                            title: "statement_detail_transaction_balance_title".localized(), info: transaction.LN_OUTSTANDING_BAL.toAmountWithCurrency
                        )
                    ]
                )
            )
        default:
            break
        }
    }
    
    @objc private func dismissController() {
        dismiss(animated: true, completion: {})
    }
}

extension StatementDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        model.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = model.sections[section]
        return section.rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultVerticalTitleLabelCell") as? DefaultVerticalTitleLabelCell
        guard let unwrappedCell = cell else {
            return UITableViewCell()
        }
        let section = model.sections[indexPath.section]
        let row = section.rows[indexPath.row]
        unwrappedCell.setData(row)
        return unwrappedCell
    }
}
