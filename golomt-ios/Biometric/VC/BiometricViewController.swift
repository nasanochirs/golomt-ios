//
//  BiometricViewController.swift
//  golomt-ios
//
//  Created by Khulan on 7/25/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class BiometricViewController: TableViewController {
    
    var authManager = LocalAuthManager.shared
    var loginName = ""

    lazy var imageField: BiometricImageCell = {
        let nib = Bundle.main.loadNibNamed("BiometricImageCell", owner: self, options: nil)
        let cell = nib?.first as? BiometricImageCell
        guard let unwrappedCell = cell else {
            return BiometricImageCell()
        }
        switch authManager.biometricType {
            case .none:
                break
            case .touchID:
                unwrappedCell.labelImage = UIImage(named: "touchID")
                unwrappedCell.titleText = "biometric_setup_touch_id_label".localized()
            case .faceID:
                unwrappedCell.labelImage = UIImage(named: "faceID")
                unwrappedCell.titleText = "biometric_setup_face_id_label".localized()
        }
        return unwrappedCell
    }()
    
    lazy var firstField: DefaultLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLabelCell
        guard let unwrappedCell = cell else {
            return DefaultLabelCell()
        }
        switch authManager.biometricType {
            case .none:
                break
            case .touchID:
                unwrappedCell.labelText = "biometric_first_touch_id_label".localized()
            case .faceID:
                unwrappedCell.labelText = "biometric_first_face_id_label".localized()
        }
        return unwrappedCell
    }()
    
    lazy var secondField: DefaultLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLabelCell
        guard let unwrappedCell = cell else {
            return DefaultLabelCell()
        }
        switch authManager.biometricType {
            case .none:
                break
            case .touchID:
                unwrappedCell.labelText = "biometric_second_touch_id_label".localized()
            case .faceID:
                unwrappedCell.labelText = "biometric_second_face_id_label".localized()
        }
        return unwrappedCell
    }()
    
    lazy var thirdField: DefaultLabelCell = {
           let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
           let cell = nib?.first as? DefaultLabelCell
           guard let unwrappedCell = cell else {
               return DefaultLabelCell()
           }
        switch authManager.biometricType {
            case .none:
                break
            case .touchID:
                unwrappedCell.labelText = "biometric_third_touch_id_label".localized()
            case .faceID:
                unwrappedCell.labelText = ""
        }
           return unwrappedCell
       }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        switch authManager.biometricType {
            case .none:
                break
            case .touchID:
                title = "biometric_touch_id_title".localized()
            case .faceID:
                title = "biometric_face_id_title".localized()
        }
        
        navigationItem.largeTitleDisplayMode = .never
        hasFooterButton = false
        tableView.isScrollEnabled = false
        onContinue = {
            self.biometric()
        }
        model.sections = [
            TableViewModel.Section(
                title: "",
                rows: [
                    TableViewModel.Row(
                        cell: imageField
                    ),
                    TableViewModel.Row(
                        cell: firstField
                    ),
                    TableViewModel.Row(
                        cell: secondField
                    ),
                    TableViewModel.Row(
                        cell: thirdField
                    )
                ]
            )
        ]
        tableView.reloadData()
    }
    
    private func navigateToMain() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    private func biometric() {
        self.authManager.setupBiometric(reason: "login") { isSuccessful in
            switch isSuccessful {
            case true:
                self.showLoader()
                ConnectionFactory.registerBiometric(
                    success: { _ in
                        self.hideLoader()
                        switch LocalAuthManager.shared.biometricType {
                        case .none:
                            break
                        case .touchID:
                            self.present(successDialog(message: "biometric_touch_id_success_title".localized(), dismiss: self.navigateToMain), animated: true, completion: nil)
                        case .faceID:
                            self.present(successDialog(message: "biometric_face_id_success_title".localized(), dismiss: self.navigateToMain), animated: true, completion: nil)
                        }
                        setLoginName(self.loginName)
                        setUsername(profileName)
                        setProfileImage(nil)
                        NotificationCenter.default.post(name: Notification.Name(NotificationConstants.REFRESH_PROFILE), object: nil)
                },
                    failed: { reason in
                        self.hideLoader()
                        self.handleRequestFailure(reason)
                }
            )
            case false:
                return
            }
        }
    }

}

