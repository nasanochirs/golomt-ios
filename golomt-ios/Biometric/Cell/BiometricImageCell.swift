//
//  BiometricImageCell.swift
//  golomt-ios
//
//  Created by Khulan on 7/25/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class BiometricImageCell: UITableViewCell {
    @IBOutlet var labelText: UILabel!
    @IBOutlet var labelImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        labelText.textColor = .defaultPrimaryText
        labelText.useXXLargeFont()
        selectionStyle = .none
    }

    var titleText: String = "" {
        didSet {
            labelText.text = titleText.localized()
        }
    }

    var labelImage: UIImage? {
        didSet {
            labelImageView.image = labelImage?.withColor(.defaultPrimaryText)
        }
    }
}
