//
//  DownPaymentCell.swift
//  golomt-ios
//
//  Created by Khulan on 7/17/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class DownPaymentCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var percentLabel: UILabel!
    @IBOutlet var amountLabel: UITextField!
    @IBOutlet var percentTextField: UITextField!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var checkField: DefaultCheckField!
    @IBOutlet var percentSeparator: UIView!
    @IBOutlet var separator: UIView!
    
    var onTextChangedPercent: (() -> Void)?
    var onStateChange: (() -> Void)?
    var onTextChangedAmount: (() -> Void)?
    
    var textLength: Int = 255
    var lengthValidation: LengthValidation?
     
    enum LengthValidation {
        case Less, Equal, Greater
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }
    
    func hideLine() {
        checkField.checkLine.isHidden = true
    }
    
    func showLine() {
        checkField.checkLine.isHidden = false
    }
    
    func hasError() -> Bool {
        errorLabel.text = nil
        separator.backgroundColor = .defaultSeparator
        errorLabel.textColor = .defaultError
        if amountLabel.text == nil {
            separator.backgroundColor = .defaultError
            errorLabel.text = "error_not_empty".localized(with: title)
            checkField.changeCheckState(checked: nil)
            onStateChange?()
            return true
        }

        switch lengthValidation {
        case .Equal:
            if percentInputText.count < textLength {
                separator.backgroundColor = .defaultError
                errorLabel.text = "error_must_be_equal_to".localized(with: textLength)
                checkField.changeCheckState(checked: nil)
                onStateChange?()
                return true
            }
        case .Greater:
            if percentInputText.count <= textLength {
                separator.backgroundColor = .defaultError
                errorLabel.text = "error_must_be_greater_than".localized(with: textLength)
                checkField.changeCheckState(checked: nil)
                onStateChange?()
                return true
            }
        default:
            return false
        }
        onStateChange?()
        return false
    }
    
    func checkError() -> Bool? {
        errorLabel.text = nil
        separator.backgroundColor = .defaultSeparator
        if percentInputText.isEmpty {
            separator.backgroundColor = .defaultError
            errorLabel.textColor = .defaultError
            errorLabel.text = "error_not_empty".localized(with: title)
            onStateChange?()
            return nil
        }
        onStateChange?()
        return false
    }
    
    var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }
    
    var percentInputText: String {
        get {
            percentTextField.text.orEmpty
        }
        set {
            percentTextField.text = newValue
            if !newValue.isEmpty {
                percentTextField.sendActions(for: .editingChanged)
            } else {
                checkField.changeCheckState(checked: false)
            }
        }
    }
    
    private func initComponent() {
        selectionStyle = .none
        
        titleLabel.textColor = .defaultSeparator
        titleLabel.useSmallFont()
        titleLabel.makeBold()
        
        errorLabel.textColor = .red
        errorLabel.useSmallFont()
        
        amountLabel.borderStyle = .none
        amountLabel.textColor = .defaultPrimaryText
        amountLabel.useLargeFont()
        amountLabel.addTarget(self, action: #selector(textFieldDidChangeAmount), for: .editingChanged)
        amountLabel.delegate = self

        percentLabel.textColor = .defaultPrimaryText
        percentLabel.useXLargeFont()
        percentLabel.text = "%"
           
        percentTextField.borderStyle = .none
        percentTextField.addTarget(self, action: #selector(textFieldDidChangePercent), for: .editingChanged)
        percentTextField.textColor = .defaultPrimaryText
        percentTextField.useLargeFont()
        percentTextField.delegate = self
        
        separator.backgroundColor = .defaultSeparator
        percentSeparator.backgroundColor = .defaultSeparator
    }
    
    @objc private func textFieldDidChangeAmount() {
        onTextChangedAmount?()
        setCheck()
    }
     
    @objc private func textFieldDidChangePercent() {
        onTextChangedPercent?()
        setCheck()
    }

    private func setCheck() {
        switch checkError() {
        case nil:
            checkField.changeCheckState(checked: nil)
        case false:
            checkField.changeCheckState(checked: true)
        case true:
            checkField.changeCheckState(checked: false)
        default:
            break
        }
        validateLength()
    }
    
    private func validateLength() {
        switch lengthValidation {
        case .Equal:
            if percentInputText.count < textLength {
                separator.backgroundColor = .defaultWarning
                errorLabel.textColor = .defaultWarning
                errorLabel.text = "error_must_be_equal_to".localized(with: textLength)
                checkField.changeCheckState(checked: nil, color: .defaultWarning)
                onStateChange?()
            }
        case .Greater:
            if percentInputText.count < textLength {
                separator.backgroundColor = .defaultWarning
                errorLabel.textColor = .defaultWarning
                errorLabel.text = "error_must_be_greater_than".localized(with: textLength)
                checkField.changeCheckState(checked: nil, color: .defaultWarning)
                onStateChange?()
            }
        default:
            break
        }
    }

    var keyboardType: UIKeyboardType = .default {
        didSet {
            amountLabel.keyboardType = keyboardType
            percentTextField.keyboardType = keyboardType
        }
    }
    
    func getNumber(_ amountString: String?) -> String? {
        var amountString = amountString.orZero
        amountString = amountString.replacingOccurrences(of: ",", with: "")

        let amount = Double(amountString) ?? 0.0

        let numberFormatter = NumberFormatter()
        numberFormatter.formatterBehavior = .behavior10_4
        numberFormatter.numberStyle = .decimal
        numberFormatter.roundingMode = .down
        numberFormatter.decimalSeparator = "."
        numberFormatter.groupingSeparator = ","
        numberFormatter.usesGroupingSeparator = true

        if amountString.contains(".") {
            let rangeOfDot = amountString.range(of: ".")
            let rangeLowerBound = rangeOfDot?.lowerBound

            if let rangeLowerBound = rangeLowerBound {
                let location = amountLabel.text.orEmpty.distance(from: amountString.startIndex, to: rangeLowerBound)
                numberFormatter.minimumFractionDigits = amountString.count - location - 1
                numberFormatter.maximumFractionDigits = amountString.count - location - 1
                if location == amountString.count - 1 {
                    return "\(numberFormatter.string(from: NSNumber(value: amount)) ?? "")."
                } else if location > amountString.count - 3 {
                    return numberFormatter.string(from: NSNumber(value: amount))
                } else {
                    numberFormatter.minimumFractionDigits = 2
                    numberFormatter.maximumFractionDigits = 2
                    return numberFormatter.string(from: NSNumber(value: amount))
                }
            }
        } else {
            numberFormatter.minimumFractionDigits = 0
            numberFormatter.maximumFractionDigits = 0

            if amount == 0 {
                return nil
            }

            return numberFormatter.string(from: NSNumber(value: amount))
        }

        return numberFormatter.string(from: NSNumber(value: amount))
    }
}

extension DownPaymentCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.keyboardType == .decimalPad {
            var repString = string

            if string == "," {
                repString = "."
            }

            let newText = textField.text.orEmpty as NSString?

            var proposedNewString = newText?.replacingCharacters(in: range, with: repString)
            proposedNewString = getNumber(proposedNewString)

            textField.text = proposedNewString

            textField.sendActions(for: .editingChanged)

            return false
        }
        guard let stringRange = Range(range, in: percentInputText) else {
            return false
        }
        let newString = percentInputText.replacingCharacters(in: stringRange, with: string)
        switch lengthValidation {
        case .Equal:
            return newString.count <= textLength
        case .Less:
            return newString.count < textLength
        default:
            return true
        }
    }
}
