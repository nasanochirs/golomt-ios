//
//  CalculatorViewController.swift
//  golomt-ios
//
//  Created by Khulan on 7/16/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class CalculatorViewController: TableViewController {
    
    var currencyInput = ""
    
    lazy var mortgageSection = [
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(cell: currencyTypeField),
                TableViewModel.Row(cell: apartmentPrice),
                TableViewModel.Row(cell: downPayment),
                TableViewModel.Row(cell: loanAmountMortgage),
                TableViewModel.Row(cell: interestAnnualMortgage),
                TableViewModel.Row(cell: loanTermYear),
                TableViewModel.Row(cell: monthlyPaymentMortgage),
                TableViewModel.Row(cell: mothlyIncome),
            ]
        ),
    ]
    
    lazy var salarySection = [
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(cell: currencyTypeField),
                TableViewModel.Row(cell: interestMonthly),
                TableViewModel.Row(cell: loanTermMonthsSalary),
                TableViewModel.Row(cell: loanAmountSalary),
                TableViewModel.Row(cell: monthlyPaymentSalary),
                TableViewModel.Row(cell: incomeAmount),
            ]
        ),
    ]
    
    lazy var savingsSection = [
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(cell: currencyTypeField),
                TableViewModel.Row(cell: balanceAmount),
                TableViewModel.Row(cell: interestAnnualSavings),
                TableViewModel.Row(cell: loanTermMonthsSavings),
                TableViewModel.Row(cell: monthlyDeposit),
                TableViewModel.Row(cell: noteText),
                TableViewModel.Row(cell: interest),
                TableViewModel.Row(cell: totalAmount),
            ]
        ),
    ]
    
    lazy var currencyTypeField: DefaultSegmentedGroupCell = {
        let nib = Bundle.main.loadNibNamed("DefaultSegmentedGroupCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultSegmentedGroupCell else {
            return DefaultSegmentedGroupCell()
        }
        cell.title = ""
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.segments = [
            DefaultSegmentedGroupCell.SegmentModel(code: "", value: "calculator_mortgage_title".localized()),
            DefaultSegmentedGroupCell.SegmentModel(code: "", value: "calculator_salary_loan_title".localized()),
            DefaultSegmentedGroupCell.SegmentModel(code: "", value: "calculator_savings_title".localized()),
        ]
        cell.selectedSegmentIndex = 1
        cell.onSegmentChange = { segment in
            switch segment {
            case 0:
                self.model.sections = self.mortgageSection
                self.tableView.reloadData()
            case 1:
                self.model.sections = self.salarySection
                self.tableView.reloadData()
            case 2:
                self.model.sections = self.savingsSection
                self.tableView.reloadData()
            default:
                break
            }
        }
        return cell
    }()
    
    lazy var monthlyPaymentMortgage: DefaultTitleLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTitleLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTitleLabelCell else {
            return DefaultTitleLabelCell()
        }
        let transactionTypeProperty: TableViewModel.InfoProperty
        cell.infoLabel.text = "0.0"
        cell.setData(TableViewModel.Row(title: "calculator_monthly_payment_label".localized(), info: cell.infoLabel.text, infoProperty: .DefaultAmount))
        return cell
    }()
    
    lazy var monthlyPaymentSalary: DefaultTitleLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTitleLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTitleLabelCell else {
            return DefaultTitleLabelCell()
        }
        cell.infoLabel.text = "0.0"
        cell.setData(TableViewModel.Row(title: "calculator_monthly_payment_label".localized(), info: cell.infoLabel.text, infoProperty: .DefaultAmount))
        return cell
    }()
    
    lazy var mothlyIncome: DefaultTitleLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTitleLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTitleLabelCell else {
            return DefaultTitleLabelCell()
        }
        cell.infoLabel.text = "0.0"
        cell.setData(TableViewModel.Row(title: "calculator_monthly_income_label".localized(), info: cell.infoLabel.text, infoProperty: .DefaultAmount))
        return cell
    }()
    
    lazy var incomeAmount: DefaultTitleLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTitleLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTitleLabelCell else {
            return DefaultTitleLabelCell()
        }
        cell.infoLabel.text = "0.0"
        cell.setData(TableViewModel.Row(title: "calculator_income_amount_label".localized(), info: cell.infoLabel.text, infoProperty: .DefaultAmount))
        return cell
    }()
    
    lazy var interest: DefaultTitleLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTitleLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTitleLabelCell else {
            return DefaultTitleLabelCell()
        }
        cell.titleLabel.text = "calculator_interest_label".localized()
        cell.infoLabel.text = "0.0"
        cell.setData(TableViewModel.Row(title: "calculator_interest_label".localized(), info: cell.infoLabel.text, infoProperty: .DefaultAmount))
        return cell
    }()
    
    lazy var totalAmount: DefaultTitleLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTitleLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTitleLabelCell else {
            return DefaultTitleLabelCell()
        }
        cell.infoLabel.text = "0.0"
        cell.setData(TableViewModel.Row(title: "calculator_total_amount_label".localized(), info: cell.infoLabel.text, infoProperty: .DefaultAmount))
        return cell
    }()
    
    lazy var apartmentPrice: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        cell.textField.keyboardType = UIKeyboardType.decimalPad
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.textField.placeholder = "0"
        cell.textField.becomeFirstResponder()
        cell.onTextChanged = {
            if self.downPayment.amountLabel.text.orEmpty.isEmpty, self.downPayment.percentTextField.text.orEmpty.isEmpty {
                self.loanAmountMortgage.inputText = self.apartmentPrice.inputText.formattedWithComma
            }
            else if !self.downPayment.percentTextField.text.orEmpty.isEmpty {
                self.downPayment.amountLabel.text = (self.apartmentPrice.inputText.toDouble * self.downPayment.percentTextField.text.toDouble / 100).formattedWithComma
                self.loanAmountMortgage.inputText = (self.apartmentPrice.inputText.toDouble - self.downPayment.amountLabel.text.toDouble).formattedWithComma
            }
            if self.apartmentPrice.inputText.isEmpty {
                self.loanAmountMortgage.inputText = self.apartmentPrice.inputText.formattedWithComma
                self.downPayment.amountLabel.text = "0.0"
                self.downPayment.percentTextField.text = "0.0"
            }
            self.calculateMortgage()
        }
        
        cell.titleLabel.text = "calculator_mortgage_apartment_price_title".localized()
        return cell
    }()
    
    lazy var downPayment: DownPaymentCell = {
        let nib = Bundle.main.loadNibNamed("DownPaymentCell", owner: self, options: nil)
        guard let cell = nib?.first as? DownPaymentCell else {
            return DownPaymentCell()
        }
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.amountLabel.placeholder = "0.0"
        cell.percentTextField.placeholder = "0"
        cell.textLength = 4
        cell.lengthValidation = .Less
        cell.title = "calculator_mortgage_down_payment_title".localized()
        cell.onTextChangedPercent = {
            self.calculateMortgage()
            if !self.apartmentPrice.inputText.isEmpty {
                let amount = self.apartmentPrice.inputText.toDouble * self.downPayment.percentTextField.text.toDouble / 100
                self.downPayment.amountLabel.text = amount.formattedWithComma
                self.loanAmountMortgage.inputText = (self.apartmentPrice.inputText.toDouble - self.downPayment.amountLabel.text.toDouble).formattedWithComma
            }
            else {
                self.downPayment.amountLabel.text = "0.0"
            }
            if self.downPayment.percentTextField.text.toDouble > 100.0 {
                self.downPayment.percentTextField.text = "100"
                let amount = self.apartmentPrice.inputText.toDouble * self.downPayment.percentTextField.text.toDouble / 100
                self.downPayment.amountLabel.text = amount.formattedWithComma
                self.loanAmountMortgage.inputText = (self.apartmentPrice.inputText.toDouble - self.downPayment.amountLabel.text.toDouble).formattedWithComma
            }
        }
        cell.onTextChangedAmount = {
            self.calculateMortgage()
            if !self.apartmentPrice.inputText.isEmpty {
                let percentage = (self.downPayment.amountLabel.text.toDouble / self.apartmentPrice.inputText.toDouble) * 100
                self.downPayment.percentTextField.text = percentage.formattedWithComma
                self.loanAmountMortgage.inputText = (self.apartmentPrice.inputText.toDouble - self.downPayment.amountLabel.text.toDouble).formattedWithComma
            }
            else {
                self.downPayment.percentTextField.text = "0.0"
            }
            if self.downPayment.percentTextField.text.toDouble > 100.0 {
                self.downPayment.percentTextField.text = "100"
            }
        }
        return cell
    }()
    
    lazy var loanAmountMortgage: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        
        cell.textField.keyboardType = UIKeyboardType.decimalPad
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.onTextChanged = {
            self.calculateMortgage()
        }
        cell.textField.placeholder = "0"
        cell.titleLabel.text = "calculator_loan_amount_title".localized()
        return cell
    }()
    
    lazy var loanAmountSalary: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        
        cell.textField.keyboardType = UIKeyboardType.decimalPad
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.onTextChanged = {
            self.calculateSalary()
        }
        cell.textField.placeholder = "0"
        cell.titleLabel.text = "calculator_loan_amount_title".localized()
        cell.hideLine()
        return cell
    }()
    
    lazy var interestAnnualMortgage: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        
        cell.textField.keyboardType = UIKeyboardType.decimalPad
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.onTextChanged = {
            self.calculateMortgage()
        }
        cell.textField.placeholder = "0.0"
        cell.titleLabel.text = "calculator_interest_annual_title".localized()
        return cell
    }()
    
    lazy var interestAnnualSavings: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        
        cell.textField.keyboardType = UIKeyboardType.decimalPad
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.onTextChanged = {
            self.calculateSavings()
        }
        cell.textField.placeholder = "0.0"
        cell.titleLabel.text = "calculator_interest_title".localized()
        return cell
    }()
    
    lazy var loanTermYear: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        
        cell.textField.keyboardType = UIKeyboardType.decimalPad
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.onTextChanged = {
            self.calculateMortgage()
        }
        cell.textField.placeholder = "0"
        cell.titleLabel.text = "calculator_mortgage_loan_term_title".localized()
        cell.hideLine()
        return cell
    }()
    
    lazy var interestMonthly: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        
        cell.textField.keyboardType = UIKeyboardType.decimalPad
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.onTextChanged = {
            self.calculateSalary()
        }
        cell.textField.placeholder = "0.0"
        cell.textField.becomeFirstResponder()
        cell.titleLabel.text = "calculator_salary_interest_title".localized()
        return cell
    }()
    
    lazy var loanTermMonthsSavings: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        
        cell.textField.keyboardType = UIKeyboardType.decimalPad
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.onTextChanged = {
            self.calculateSavings()
        }
        cell.textField.placeholder = "0"
        cell.titleLabel.text = "calculator_loan_term_title".localized()
        
        return cell
    }()
    
    lazy var loanTermMonthsSalary: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        
        cell.textField.keyboardType = UIKeyboardType.decimalPad
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.onTextChanged = {
            self.calculateSalary()
        }
        cell.textField.placeholder = "0"
        cell.titleLabel.text = "calculator_loan_term_title".localized()
        return cell
    }()
    
    lazy var balanceAmount: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        
        cell.textField.keyboardType = UIKeyboardType.decimalPad
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.onTextChanged = {
            self.calculateSavings()
        }
        cell.textField.placeholder = "0"
        cell.textField.becomeFirstResponder()
        cell.titleLabel.text = "calculator_savings_balance_amount_title".localized()
        return cell
    }()
    
    lazy var monthlyDeposit: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        
        cell.textField.keyboardType = UIKeyboardType.decimalPad
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.onTextChanged = {
            self.calculateSavings()
        }
        cell.textField.placeholder = "0"
        cell.titleLabel.text = "calculator_savings_monthly_deposit_title".localized()
        cell.hideLine()
        return cell
    }()
    
    lazy var noteText: DefaultLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLabelCell
        guard let unwrappedCell = cell else {
            return DefaultLabelCell()
        }
        unwrappedCell.labelLabel.textAlignment = .center
        unwrappedCell.labelLabel.textColor = .defaultSecondaryText
        unwrappedCell.labelText = "calculator_note_text".localized()
        return unwrappedCell
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "calculator_title".localized()
        setupNavigationButton()
        tableView.reloadData()
        setInitialRows()
        continueButton?.isHidden = true
    }
    
    private func setupNavigationButton() {
        if #available(iOS 13.0, *) {
            self.navigationItem.leftBarButtonItem = nil
        }
        else {
            let closeButtonItem = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(dismissController))
            navigationItem.leftBarButtonItem = closeButtonItem
        }
    }
    
    @objc private func dismissController() {
        dismiss(animated: true, completion: {})
    }
    
    private func setInitialRows() {
        model.sections = salarySection
        tableView.reloadData()
    }
    
    private func calculateSalary() {
        if !interestMonthly.inputText.isEmpty, !loanTermMonthsSalary.inputText.isEmpty, !loanAmountSalary.inputText.isEmpty {
            let d = interestMonthly.inputText.toDouble / 100
            var d1 = pow(1 + d, loanTermMonthsSalary.inputText.toDouble)
            d1 = 1 / d1
            d1 = 1 - d1
            d1 = d1 / d
            monthlyPaymentSalary.infoLabel.text = (loanAmountSalary.inputText.toDouble / d1).formattedWithComma
            incomeAmount.infoLabel.text = ((monthlyPaymentSalary.infoLabel.text.toDouble * 100) / 45).formattedWithComma
        }
    }
    
    private func calculateSavings() {
        if !balanceAmount.inputText.isEmpty, !interestAnnualSavings.inputText.isEmpty, !loanTermMonthsSavings.inputText.isEmpty, !monthlyDeposit.inputText.isEmpty {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy"
            let currentDate = Date()
            var dateComponent = DateComponents()
            dateComponent.month = loanTermMonthsSavings.inputText.toInt
            let futureDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)
            let components = Calendar.current.dateComponents([.day], from: currentDate, to: futureDate!)
            
            let interestTotal = (balanceAmount.inputText.toDouble * Double(components.day!) * ((interestAnnualSavings.inputText.toDouble / 100) / 365)) + (monthlyDeposit.inputText.toDouble * ((((loanTermMonthsSavings.inputText.toDouble + 1) * loanTermMonthsSavings.inputText.toDouble) / 2) - loanTermMonthsSavings.inputText.toDouble) * ((interestAnnualSavings.inputText.toDouble / 100) * Double(components.day!) / (365 * loanTermMonthsSavings.inputText.toDouble)))
            
            let amounTotal = balanceAmount.inputText.toDouble + (monthlyDeposit.inputText.toDouble * (loanTermMonthsSavings.inputText.toDouble - 1)) + interestTotal
            
            totalAmount.infoLabel.text = amounTotal.formattedWithComma
            interest.infoLabel.text = interestTotal.formattedWithComma
        }
    }
    
    private func calculateMortgage() {
        if !loanAmountMortgage.inputText.isEmpty, !interestAnnualMortgage.inputText.isEmpty, !loanTermYear.inputText.isEmpty, !downPayment.percentTextField.text.orEmpty.isEmpty, !downPayment.amountLabel.text.orEmpty.isEmpty {
            let d = interestAnnualMortgage.inputText.toDouble / 12 / 100
            var d1 = pow(1 + d, loanTermYear.inputText.toDouble * 12)
            d1 = 1 / d1
            d1 = 1 - d1
            d1 = d1 / d
            let paymount = loanAmountMortgage.inputText.toDouble / d1
            monthlyPaymentMortgage.infoLabel.text = paymount.formattedWithComma
            mothlyIncome.infoLabel.text = (paymount * (100.0 / 45.0)).formattedWithComma
        }
    }
    
}
