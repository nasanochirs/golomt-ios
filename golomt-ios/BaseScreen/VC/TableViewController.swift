//
//  TableViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/27/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import DZNEmptyDataSet
import Foundation
import IQKeyboardManagerSwift
import STPopup
import UIKit

class TableViewController: BaseUIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var continueButton: DefaultGradientButton?
    @IBAction func continueAction(_ sender: Any) {
        onContinue?()
    }

    var model = TableViewModel()
    var selectedAccount: AccountResponse.Account?
    var onContinue: (() -> Void)?
    var onHeaderButtonClick: ((_ section: Int) -> Void)?
    var accountPickerTitle = "sender_account_title".localized()
    var onAccountSelect: ((AccountResponse.Account) -> Void)?

    lazy var footerView: UIView = {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: 70))
        footerButton.frame = CGRect(x: 20, y: 10, width: view.bounds.size.width - 40, height: 50)
        footerView.addSubview(footerButton)
        return footerView
    }()

    lazy var footerButton: DefaultGradientButton = {
        let button = DefaultGradientButton()
        button.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        button.addTarget(self, action: #selector(continueAction), for: .touchUpInside)
        button.titleText = "transaction_continue_button_title"
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(nibName: "DefaultTitleLabelCell")
        tableView.register(UINib(nibName: "BaseHeaderCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "BaseHeaderCell")
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.delegate = self
        tableView.dataSource = self
        tableView.keyboardDismissMode = .onDrag
        tableView.estimatedRowHeight = 400
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.separatorStyle = .none
        tableView.tableFooterView = nil
        continueButton?.titleText = "transaction_continue_button_title"
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }

    var hasFooterButton: Bool = false {
        didSet {
            if hasFooterButton {
                continueButton?.isHidden = true
                tableView.tableFooterView = footerView
            }
        }
    }

    var hasButton: Bool = true {
        didSet {
            if !hasButton {
                continueButton?.isHidden = true
                tableView.tableFooterView = nil
            }
        }
    }

    func setButtonTitle(_ type: ButtonType) {
        switch type {
        case .Continue:
            continueButton?.titleText = "continue_button_title"
            footerButton.titleText = "continue_button_title"
        case .Confirm:
            continueButton?.titleText = "confirm_button_title"
            footerButton.titleText = "confirm_button_title"
        case .Finish:
            continueButton?.titleText = "finish_button_title"
            footerButton.titleText = "finish_button_title"
        case .Accept:
            continueButton?.titleText = "accept_button_title"
            footerButton.titleText = "accept_button_title"
        }
    }

    enum ButtonType {
        case Continue, Confirm, Finish, Accept
    }

    func showSenderAccountPicker() {
        let controller = AccountPickerController()
        controller.accounts = operativeAccountList
        controller.selectedAccount = selectedAccount
        controller.title = accountPickerTitle
        controller.onAccountSelect = onAccountSelect
        let popupController = STPopupController(rootViewController: controller)
        popupController.style = .bottomSheet
        popupController.present(in: self)
    }

    func showSenderAccountPicker(_ list: [AccountResponse.Account]) {
        let controller = AccountPickerController()
        controller.accounts = list
        controller.selectedAccount = selectedAccount
        controller.title = accountPickerTitle
        controller.onAccountSelect = onAccountSelect
        let popupController = STPopupController(rootViewController: controller)
        popupController.style = .bottomSheet
        popupController.present(in: self)
    }

    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }

        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)

        if notification.name == UIResponder.keyboardWillHideNotification {
            tableView.contentInset = .zero
        } else {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        }

        tableView.scrollIndicatorInsets = tableView.contentInset
    }
}

extension TableViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return model.sections.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let section = model.sections[section]
        if section.title == nil {
            return 0
        }
        return 40
    }

    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        let section = model.sections[section]
        if section.title == nil {
            return 0
        }
        return 40
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let tableSection = model.sections[section]
        if tableSection.title == nil {
            return nil
        }
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "BaseHeaderCell") as! BaseHeaderCell
        cell.setupLabel(tableSection.title.orEmpty)
        cell.buttonImage = tableSection.image.orEmpty
        cell.onButtonClick = {
            self.onHeaderButtonClick?(section)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let section = model.sections[section]
        if section.title == nil {
            return 0
        }
        return 1
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = model.sections[section]
        return section.rows.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = model.sections[indexPath.section]
        let row = section.rows[indexPath.row]
        let cell = row.cell
        if cell == nil {
            let titleCell = tableView.dequeueReusableCell(withIdentifier: "DefaultTitleLabelCell") as! DefaultTitleLabelCell
            titleCell.setData(row)
            return titleCell
        } else {
            return cell!
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension TableViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        if model.sections.count == 0 {
            return true
        }
        return false
    }

    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let attributedString = NSAttributedString(string: "HOOSON")
        return attributedString
    }

    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        let image = UIImage(named: "tableview_empty")!
        let size = CGSize(width: view.bounds.size.width, height: view.bounds.size.height / 2)
        let aspectScaledToFitImage = image.af.imageAspectScaled(toFit: size)
        return aspectScaledToFitImage
    }
}
