//
//  PaymentNumberCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 8/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class PaymentNumberCell: UITableViewCell {
    @IBOutlet var textField: UITextField!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var separatorView: UIView!
    
    var onTextChanged: (() -> Void)?
    var onTextFinished: (() -> Void)?
    var onStateChange: (() -> Void)?
    var textLength: Int = 255
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }
    
    var inputText: String {
        get {
            textField.text.orEmpty
        }
        set {
            textField.text = newValue
            if !newValue.isEmpty {
                textField.sendActions(for: .editingChanged)
            }
        }
    }
    
    func focusTextField() {
        textField.becomeFirstResponder()
    }
    
    private func initComponent() {
        selectionStyle = .none
        
        titleLabel.textColor = .defaultSeparator
        titleLabel.useSmallFont()
        titleLabel.makeBold()
        
        errorLabel.textColor = .red
        errorLabel.useSmallFont()
        
        textField.borderStyle = .none
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        textField.keyboardType = .numberPad
        textField.textColor = .defaultPrimaryText
        textField.delegate = self
        textField.useXLargeFont()
        textField.makeBold()
        
        separatorView.backgroundColor = .defaultSeparator
    }
    
    @objc private func textFieldDidChange() {
        onTextChanged?()
        if inputText.count == textLength {
            onTextFinished?()
        }
    }
    
    private func validateLength() {
        if inputText.count <= textLength {
            separatorView.backgroundColor = .defaultWarning
            errorLabel.textColor = .defaultWarning
            errorLabel.text = "error_must_be_greater_than".localized(with: textLength)
        }
    }
}

extension PaymentNumberCell: UITextViewDelegate, UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let stringRange = Range(range, in: inputText) else {
            return false
        }
        let newString = inputText.replacingCharacters(in: stringRange, with: string)
        if newString.count <= textLength {
            return true
        }
        return false
    }
}
