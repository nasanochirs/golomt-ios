//
//  ContractViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/24/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class ContractViewController: BaseUIViewController {
    @IBOutlet var webView: WKWebView!
    @IBOutlet var confirmButton: DefaultGradientButton!
    @IBOutlet var confirmCheckBox: DefaultCheckBox!

    @IBAction func confirmAction(_ sender: Any) {
        switch true {
        case confirmCheckBox.isHidden:
            onButtonClick?()
        case confirmCheckBox.isChecked():
            onButtonClick?()
        default:
            break
        }
    }

    var controllerTitle = ""
    var buttonTitle = ""
    var contractLink = ""
    var checkBoxLabel = ""
    var onButtonClick: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = controllerTitle
        confirmButton.titleText = buttonTitle
        webView.scrollView.contentInsetAdjustmentBehavior = .never
        confirmCheckBox.isHidden = checkBoxLabel.isEmpty
        confirmCheckBox.labelText = checkBoxLabel
        if let contractUrl = URL(string: contractLink) {
            webView.load(URLRequest(url: contractUrl))
        }
    }
}
