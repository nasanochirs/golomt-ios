//
//  UIViewControllerWithBackground.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 2/21/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import IQKeyboardManagerSwift
import STPopup
import UIKit

class BaseUIViewController: UIViewController {
    var returnKeyHandler: IQKeyboardReturnKeyHandler?
    var didAppear = false

    override func viewDidLoad() {
        super.viewDidLoad()
        returnKeyHandler = IQKeyboardReturnKeyHandler(controller: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        didAppear = true
        super.viewDidAppear(animated)
    }

    func setDefaultBackgroundImage() {
        setBackgroundImage("image_background")
    }

    func setBackgroundImage(_ imageName: String) {
        let backgroundImage = UIImageView(frame: view.bounds)
        backgroundImage.image = UIImage(named: imageName)
        backgroundImage.contentMode = .scaleAspectFill
        view.insertSubview(backgroundImage, at: 0)
    }

    func handleRequestFailure(_ reason: BaseResponse.HeaderMessage?) {
        hideLoader()
        guard let reason = reason else {
            return
        }
        let alert = errorDialog(message: reason.MESSAGE_DESC)
        present(alert, animated: true)
    }

    func showLoader() {
        DispatchQueue.main.async {
            LoadingDialog.shared.showLoader()
        }
    }

    func hideLoader() {
        DispatchQueue.main.async {
            LoadingDialog.shared.hideLoader()
        }
    }

    func isLoading() -> Bool {
        return LoadingDialog.shared.isLoading()
    }

    func isDark() -> Bool {
        if #available(iOS 12.0, *) {
            if traitCollection.userInterfaceStyle == .dark {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
}
