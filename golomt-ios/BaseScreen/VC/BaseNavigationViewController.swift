//
//  BaseNavigationViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class BaseNavigationViewController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        setNavigationBar()
    }

    func setNavigationBar() {
        let navigationBar = self.navigationBar
        navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
        let attributes = [
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 30),
            .foregroundColor: UIColor.white
        ]
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.largeTitleTextAttributes = attributes
            appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            appearance.backgroundColor = .defaultHeader
            navigationBar.tintColor = .white
            navigationBar.standardAppearance = appearance
            navigationBar.scrollEdgeAppearance = appearance
        } else {
            navigationBar.barTintColor = UIColor(patternImage: UIImage(named: "header")!)
            navigationBar.tintColor = .white
            navigationBar.largeTitleTextAttributes = attributes
            navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
            navigationBar.isTranslucent = false
        }
    }
}
