//
//  PopupController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/21/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class PopupController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.popupController?.containerView.backgroundColor = .defaultPrimaryBackground
        self.popupController?.containerView.layer.cornerRadius = 15
        self.popupController?.backgroundView?.addTapGesture(tapNumber: 1, target: self, action: #selector(self.handleDismiss))

        var height = CGFloat(300)
        height += view.safeAreaInsets.bottom
        self.contentSizeInPopup = CGSize(width: self.view.bounds.size.width, height: height)
        self.popupController?.hidesCloseButton = true
    }

    @objc private func handleDismiss() {
        self.popupController?.dismiss()
    }
}
