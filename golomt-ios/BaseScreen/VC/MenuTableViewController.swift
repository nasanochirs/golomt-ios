//
//  MenuTableViewController.swift
//  golomt-ios
//
//  Created by Khulan Odkhuu on 7/4/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import DZNEmptyDataSet
import Foundation
import IQKeyboardManagerSwift
import STPopup
import UIKit

class MenuTableViewController: BaseUIViewController {
    @IBOutlet var tableView: UITableView!

    var model = MenuTableViewModel()
    var onClick: ((Int) -> Void)?
    var row = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(nibName: "DefaultMenuCell")
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.keyboardDismissMode = .onDrag
        tableView.estimatedRowHeight = 400
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.separatorStyle = .none
    }
}

extension MenuTableViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return model.sections.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        let section = model.sections[section]
        if section.title == nil {
            return 0
        }
        return 40
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = model.sections[section]
        return section.rows.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = model.sections[indexPath.section]
        let row = section.rows[indexPath.row]
        let cell = row.cell
        if cell == nil {
            let titleCell = tableView.dequeueReusableCell(withIdentifier: "DefaultMenuCell") as! DefaultMenuCell
            titleCell.setData(data: row)
            return titleCell
        } else {
            return cell!
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        row = indexPath.row
        onClick?(row)
    }
}

extension MenuTableViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        if model.sections.count == 0 {
            return true
        }
        return false
    }

    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let attributedString = NSAttributedString(string: "HOOSON")
        return attributedString
    }

    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        let image = UIImage(named: "tableview_empty")!
        let size = CGSize(width: view.bounds.size.width, height: view.bounds.size.height / 2)
        let aspectScaledToFitImage = image.af.imageAspectScaled(toFit: size)
        return aspectScaledToFitImage
    }
}
