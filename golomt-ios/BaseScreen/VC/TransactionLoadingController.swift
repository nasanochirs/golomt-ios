//
//  TransactionLoadingController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/23/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Lottie
import UIKit

class TransactionLoadingController: BaseUIViewController {
    @IBOutlet var animationView: AnimationView!
    @IBOutlet var resultMessageLabel: UILabel!
    @IBOutlet var resultContainerView: UIStackView!
    @IBOutlet var widthConstraint: NSLayoutConstraint!
    @IBOutlet var heightConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.animationSpeed = 1
        animationView.play()
        resultMessageLabel.textColor = .defaultPrimaryText
        navigationItem.hidesBackButton = true
    }

    func setResult(isSuccessful: Bool, message: String, completion: @escaping () -> Void) {
        if isSuccessful {
            animationView.animation = Animation.named("lottie-transaction-success")
            animationView.loopMode = .playOnce
            animationView.play { _ in
                self.animateTransactionResult(completion: completion)
            }
        } else {
            animationView.animation = Animation.named("lottie-transaction-failed")
            animationView.loopMode = .playOnce
            animationView.play { _ in
                self.animateTransactionResult(completion: completion)
            }
        }
        resultMessageLabel.text = message
    }

    private func animateTransactionResult(completion: @escaping () -> Void) {
        UIView.animate(withDuration: 0.5, animations: {
            self.widthConstraint.constant = 50
            self.heightConstraint.constant = 50
            self.view.layoutIfNeeded()
            let safeAreaTop = self.view.safeAreaInsets.top
            self.resultContainerView.frame.origin.y = safeAreaTop + 5
        }, completion: { _ in
            completion()
        })
    }
}
