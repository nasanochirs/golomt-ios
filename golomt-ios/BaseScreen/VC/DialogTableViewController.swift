//
//  TableViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/4/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class DialogTableViewController: UITableViewController {
    static let minHeight = CGFloat(100)

    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }

    override func viewDidLoad() {
        self.popupController?.containerView.layer.cornerRadius = 10
        self.tableView.tableFooterView = UIView()
        self.popupController?.backgroundView?.addTapGesture(tapNumber: 1, target: self, action: #selector(self.handleDismiss))
        self.popupController?.containerView.backgroundColor = .defaultPrimaryBackground
        self.popupController?.hidesCloseButton = true
        self.view.backgroundColor = .defaultPrimaryBackground
        self.tableView.separatorColor = .defaultSeparator
    }

    func getContentHeight() -> CGFloat {
        var rowHeight: CGFloat = 0.0
        for cell in tableView.visibleCells {
            switch cell {
            case let verticalCell as DefaultVerticalTitleLabelCell:
                rowHeight += verticalCell.infoLabel.getSize().height
                rowHeight += verticalCell.titleLabel.getSize().height
                // Constaint stackview height
//                rowHeight += verticalCell.containerStackView.spacing
//                rowHeight += verticalCell.topConstraint.constant
//                rowHeight += verticalCell.bottomConstraint.constant
            default:
                rowHeight += cell.bounds.size.height
            }
        }
        let headerHeight = self.tableView.tableHeaderView?.bounds.size.height ?? 0.0
        let footerHeight = self.tableView.tableFooterView?.bounds.size.height ?? 0.0
        var height = rowHeight + headerHeight + footerHeight
        if height < DialogTableViewController.minHeight {
            height = DialogTableViewController.minHeight
        }
        if height > UIScreen.main.bounds.size.height * 0.7 {
            height = UIScreen.main.bounds.size.height * 0.7
        }
        return height
    }

    func configContentSize() {
        self.contentSizeInPopup = CGSize(width: self.view.bounds.size.width, height: self.getContentHeight())
    }

    func configContentSize(with customHeight: CGFloat) {
        let maxHeight = UIScreen.main.bounds.size.height * 0.7
        let safeAreaBottom = popupController?.safeAreaInsets.bottom ?? 0.0
        if self.getContentHeight() + customHeight > maxHeight {
            self.contentSizeInPopup = CGSize(width: self.view.bounds.size.width, height: maxHeight - customHeight + safeAreaBottom)
        } else {
            self.contentSizeInPopup = CGSize(width: self.view.bounds.size.width, height: safeAreaBottom + self.getContentHeight())
        }
    }

    @objc private func handleDismiss() {
        self.popupController?.dismiss()
    }
}
