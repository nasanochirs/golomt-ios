//
//  DefaultGradientButton.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/11/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class DefaultGradientButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initComponent()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.initComponent()
    }

    private func initComponent() {
        self.setTitle(nil, for: .normal)
        self.setTitleColor(.white, for: .normal)
        self.titleLabel?.useMediumFont()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.bounds.height / 2
        self.setDefaultPurpleGradient()
    }

    override func prepareForInterfaceBuilder() {
        self.initComponent()
    }

    @IBInspectable var titleText: String = "" {
        didSet {
            self.setTitle(titleText.localized(), for: .normal)
        }
    }
}
