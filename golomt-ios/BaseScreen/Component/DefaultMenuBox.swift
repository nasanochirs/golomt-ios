//
//  DefaultMenuBox.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class DefaultMenuBox: UIView {
    @IBOutlet var containerView: UIView!
    @IBOutlet var menuImage: UIImageView!
    @IBOutlet var menuLabel: UILabel!
    var onMenuTap: (() -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        initComponent()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initComponent()
    }

    private func initComponent() {
        let bundle = Bundle(for: DefaultMenuBox.self)
        bundle.loadNibNamed("DefaultMenuBox", owner: self, options: nil)
        addSubView(view: containerView, left: 0, top: 0, right: 0, bottom: 0)
        backgroundColor = .defaultPrimaryBackground
        containerView.frame = bounds
        containerView.corner(cornerRadius: 15)
        containerView.backgroundColor = .defaultSecondaryBackground
        containerView.addTapGesture(tapNumber: 1, target: self, action: #selector(handleTap))
        menuLabel.textColor = .defaultPrimaryText
        menuImage.setTint(color: .defaultPrimaryText)
    }

    @objc private func handleTap() {
        onMenuTap?()
    }

    @IBInspectable var menuLabelText: String = "" {
        didSet {
            menuLabel.text = menuLabelText.localized()
        }
    }

    @IBInspectable var menuImageName: String = "" {
        didSet {
            menuImage.image = UIImage(named: menuImageName)
        }
    }
}
