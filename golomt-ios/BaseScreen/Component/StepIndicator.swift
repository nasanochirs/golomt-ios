//
//  StepIndicator.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/11/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import CHIPageControl
import UIKit

@IBDesignable
class StepIndicator: UIView {
    lazy var indicator: CHIPageControlJaloro = {
        let indicator = CHIPageControlJaloro()
        indicator.elementWidth = 40
        indicator.elementHeight = 4
        indicator.radius = 3
        indicator.numberOfPages = 1
        return indicator
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        initComponent()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initComponent()
    }

    private func initComponent() {
        addSubView(view: indicator, left: 0, top: 0, right: 0, bottom: 0)
    }

    @IBInspectable var number: Int = 1 {
        didSet {
            indicator.numberOfPages = number
            indicator.elementWidth = UIScreen.main.bounds.width / CGFloat(number) - 20
            indicator.padding = 15
        }
    }
    
    @IBInspectable var current: Double = 1.0 {
        didSet {
            indicator.progress = current - 1
            indicator.inactiveTransparency = 1
            var colors = [UIColor]()
            for _ in 1...Int(current) {
                colors.append(UIColor(hex: "157EFB"))
            }
            let inActiveColor = UIColor(hex: "abd1fe")
            if Int(current) + 1 <= indicator.numberOfPages {
                for _ in Int(current + 1)...indicator.numberOfPages {
                    colors.append(inActiveColor)
                }
            }
            indicator.tintColors = colors
        }
    }
}
