//
//  DefaultCheckBox.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 2/28/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import M13Checkbox
import UIKit

@IBDesignable
class DefaultCheckBox: UIView {
    @IBOutlet var stackView: UIStackView!
    @IBOutlet var checkBox: M13Checkbox!
    @IBOutlet var label: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        initComponent()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initComponent()
    }

    private func initComponent() {
        let bundle = Bundle(for: DefaultCheckBox.self)
        bundle.loadNibNamed("DefaultCheckBox", owner: self, options: nil)
        addSubview(stackView)
        stackView.frame = bounds
        stackView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        stackView.addTapGesture(tapNumber: 1, target: self, action: #selector(handleTap))
        checkBox.stateChangeAnimation = .expand(.fill)
        checkBox.boxType = .square
        checkBox.isEnabled = false
        checkBox.tintColor = .white
        checkBox.secondaryCheckmarkTintColor = .black
        label.textColor = .white
    }

    func setCheckState(checked: Bool) {
        switch checked {
        case true:
            checkBox.setCheckState(.checked, animated: true)
        case false:
            checkBox.setCheckState(.unchecked, animated: true)
        }
    }

    func isChecked() -> Bool {
        switch checkBox.checkState {
        case .checked:
            return true
        default:
            return false
        }
    }

    @objc private func handleTap() {
        checkBox.toggleCheckState(true)
    }

    @IBInspectable var isPrimary: Bool = false {
        didSet {
            if isPrimary {
                label.textColor = .defaultPrimaryText
                checkBox.tintColor = .defaultPrimaryText
                checkBox.secondaryCheckmarkTintColor = .defaultSecondaryText
            }
        }
    }

    @IBInspectable var labelText: String = "" {
        didSet {
            label.text = labelText.localized()
        }
    }
}
