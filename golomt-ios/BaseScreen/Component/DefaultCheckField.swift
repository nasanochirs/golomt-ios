//
//  DefaultCheckField.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/4/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import M13Checkbox
import UIKit

@IBDesignable
class DefaultCheckField: UIView {
    @IBOutlet var stackView: UIStackView!
    @IBOutlet var checkBox: M13Checkbox!
    @IBOutlet var checkLine: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        initComponent()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initComponent()
    }

    func changeCheckState(checked: Bool?) {
        switch checked {
        case true:
            checkBox.tintColor = .defaultPurpleGradientStart
            checkBox.setCheckState(.checked, animated: true)
        case false:
            checkBox.setCheckState(.unchecked, animated: true)
        default:
            checkBox.tintColor = .defaultError
            checkBox.setCheckState(.mixed, animated: true)
        }
    }

    func changeCheckState(checked: Bool?, color: UIColor) {
        checkBox.tintColor = color
        switch checked {
        case true:
            checkBox.setCheckState(.checked, animated: true)
        case false:
            checkBox.setCheckState(.unchecked, animated: true)
        default:
            checkBox.setCheckState(.mixed, animated: true)
        }
    }

    private func initComponent() {
        let bundle = Bundle(for: DefaultCheckField.self)
        bundle.loadNibNamed("DefaultCheckField", owner: self, options: nil)
        addSubview(stackView)
        stackView.frame = bounds
        stackView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        checkBox.stateChangeAnimation = .expand(.fill)
        checkBox.boxType = .circle
        checkBox.isEnabled = false
        checkBox.tintColor = .defaultPurpleGradientStart
        checkBox.secondaryCheckmarkTintColor = .white
        checkLine.backgroundColor = .defaultSeparator
    }
}
