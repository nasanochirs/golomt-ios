//
//  DateRangePickerCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/23/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import FSCalendar
import UIKit

enum SelectionType: Int {
    case none
    case single
    case leftBorder
    case middle
    case rightBorder
}

class DateRangePickerCell: FSCalendarCell {
    var selectionLayer: CAShapeLayer = CAShapeLayer()
    var middleLayer: CAShapeLayer = CAShapeLayer()
    
    var selectionType: SelectionType = .none {
        didSet {
            setNeedsLayout()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initComponent()
    }
    
    required init!(coder aDecoder: NSCoder!) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.titleLabel.frame = self.contentView.bounds
        
        self.backgroundView?.frame = self.bounds.insetBy(dx: 1, dy: 1)
        self.selectionLayer.frame = self.contentView.bounds
        self.middleLayer.frame = self.contentView.bounds
        
        if self.selectionType == .middle {
            self.middleLayer.path = UIBezierPath(rect: self.middleLayer.bounds).cgPath
        }
        else if self.selectionType == .leftBorder {
            self.selectionLayer.path = UIBezierPath(roundedRect: self.selectionLayer.bounds, byRoundingCorners: [.topLeft, .bottomLeft], cornerRadii: CGSize(width: self.selectionLayer.frame.width / 2, height: self.selectionLayer.frame.width / 2)).cgPath
        }
        else if self.selectionType == .rightBorder {
            self.selectionLayer.path = UIBezierPath(roundedRect: self.selectionLayer.bounds, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: self.selectionLayer.frame.width / 2, height: self.selectionLayer.frame.width / 2)).cgPath
        }
        else if self.selectionType == .single {
            let diameter: CGFloat = min(self.selectionLayer.frame.height, self.selectionLayer.frame.width)
            self.selectionLayer.path = UIBezierPath(ovalIn: CGRect(x: self.contentView.frame.width / 2 - diameter / 2, y: self.contentView.frame.height / 2 - diameter / 2, width: diameter, height: diameter)).cgPath
        }
    }
    
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        self.selectionLayer.frame = self.contentView.bounds
        self.middleLayer.frame = self.contentView.bounds
    }
    
    private func initComponent() {
        let selectionLayer = CAShapeLayer()
        selectionLayer.fillColor = UIColor.defaultPurpleGradientEnd.cgColor
        selectionLayer.actions = [
            "hidden": NSNull()
        ]
        self.contentView.layer.insertSublayer(selectionLayer, below: self.titleLabel.layer)
        self.selectionLayer = selectionLayer
        
        let middleLayer = CAShapeLayer()
        let blueGradient = UIColor.defaultPurpleGradientEnd.withAlphaComponent(0.3)
        middleLayer.fillColor = blueGradient.cgColor
        middleLayer.actions = [
            "hidden": NSNull()
        ]
        self.contentView.layer.insertSublayer(middleLayer, below: self.titleLabel.layer)
        self.middleLayer = middleLayer
        
        self.shapeLayer.isHidden = true
    }
}
