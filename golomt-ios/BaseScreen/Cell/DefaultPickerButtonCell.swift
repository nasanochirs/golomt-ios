//
//  DefaultPickerButtonCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class DefaultPickerButtonCell: UITableViewCell {
    @IBOutlet var checkField: DefaultCheckField?
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var selectedLabel: UILabel!
    @IBOutlet var arrowDownImage: UIImageView!
    @IBOutlet var separator: UIView!
    @IBOutlet var pickerStackView: UIStackView!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var imageButton: UIButton?
    @IBAction func imageButtonAction(_ sender: Any) {
        onImageClick?()
    }
    
    var onImageClick: (() -> Void)?
    var onPickerClick: (() -> Void)?
    var onItemSelect: ((Any?) -> Void)?
    var pickerList = [Any]()
    var onStateChange: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }
    
    var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }
    
    var label: String? {
        get {
            return selectedLabel.text
        }
    }
    
    var hasImage: Bool = true {
        didSet {
            switch hasImage {
            case true:
                imageButton?.isHidden = false
            case false:
                imageButton?.isHidden = true
            }
        }
    }
    
    
    var selectedItem: Any? {
        didSet {
            switch selectedItem {
            case let item as BankListResponse.Bank:
                selectedLabel.text = item.INSTITUTION_NAMES
            case let account as AccountResponse.Account:
                selectedLabel.text = account.ACCT_NUMBER
            case let product as ExtendDepositProductResponse.Product:
                selectedLabel.text = product.RENEW_INSTRUCTION_NAME_ARRAY
            case let month as ExtendDepositMonthResponse.Month:
                selectedLabel.text = month.FD_RENEW_MONTHS_ARRAY.orEmpty
            case let data as DataListResponse.Item:
                selectedLabel.text = data.CD_DESC.orEmpty
            case let currency as CurrencyConstants:
                selectedLabel.text = currency.rawValue
            case let number as Int:
                selectedLabel.text = number.toString
            case let type as (index: Int, text: String):
                selectedLabel.text = type.text
            case nil:
                selectedLabel.text = nil
            default:
                break
            }
            onItemSelect?(selectedItem)
            setCheck()
        }
    }
    
    func hasError() -> Bool {
        switch selectedItem {
        case nil:
            separator.backgroundColor = .defaultError
            errorLabel.text = "error_must_pick".localized(with: title)
            imageButton?.imageView?.setTint(color: .defaultError)
            arrowDownImage.setTint(color: .defaultError)
            checkField?.changeCheckState(checked: nil)
            onStateChange?()
            return true
        default:
            separator.backgroundColor = .defaultSeparator
            imageButton?.imageView?.image = imageButton?.currentImage?.renderOriginal
            arrowDownImage.setTint(color: .defaultPrimaryText)
            errorLabel.text = nil
            onStateChange?()
            return false
        }
    }
    
    func hideLine() {
        checkField?.checkLine.isHidden = true
    }
    
    func showLine() {
        checkField?.checkLine.isHidden = false
    }
    
    private func initComponent() {
        selectionStyle = .none
        arrowDownImage.setTint(color: .defaultPrimaryText)
        titleLabel.textColor = .defaultSeparator
        titleLabel.useSmallFont()
        titleLabel.makeBold()
        selectedLabel.textColor = .defaultPrimaryText
        selectedLabel.useLargeFont()
        separator.backgroundColor = .defaultSeparator
        errorLabel.textColor = .red
        errorLabel.useSmallFont()
        pickerStackView.addTapGesture(tapNumber: 1, target: self, action: #selector(handlePicker))
    }
    
    @objc private func handlePicker() {
        onPickerClick?()
    }
    
    private func setCheck() {
        if hasError() {
            checkField?.changeCheckState(checked: nil)
        } else {
            checkField?.changeCheckState(checked: true)
        }
    }
}
