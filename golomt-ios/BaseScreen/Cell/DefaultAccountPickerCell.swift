//
//  DefaultAccountPicker.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/4/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class DefaultAccountPickerCell: UITableViewCell {
    @IBOutlet var containerView: UIView!
    @IBOutlet var containerBackgroundView: UIView!
    @IBOutlet var nicknameLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var balanceLabel: UILabel!
    @IBOutlet var arrowDownImage: UIImageView!
    
    var onPickerClick: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }

    var account: AccountResponse.Account? {
        didSet {
            if account != nil {
                nicknameLabel.text = account?.ACCT_NICKNAME
                numberLabel.text = account?.ACCT_NUMBER
                balanceLabel.text = account?.ACCT_BALANCE.toAmountWithCurrencySymbol
            } else {
                nicknameLabel.text = defaultNicknameText
                numberLabel.text = defaultNumberText
            }
        }
    }

    func hasError() -> Bool {
        if account == nil {
            return true
        }
        return false
    }

    var defaultNicknameText: String = "account_picker_nickname_label".localized() {
        didSet {
            nicknameLabel.text = defaultNicknameText
        }
    }
    
    var defaultNumberText: String = "account_picker_number_label".localized() {
        didSet {
            numberLabel.text = defaultNumberText
        }
    }
    
    private func initComponent() {
        containerBackgroundView.backgroundColor = .defaultHeader
        containerView.addTapGesture(tapNumber: 1, target: self, action: #selector(onClick))
        arrowDownImage.setTint(color: .defaultPrimaryText)
        nicknameLabel.textColor = .defaultPrimaryText
        numberLabel.textColor = .defaultSecondaryText
        balanceLabel.textColor = .defaultPrimaryText
        nicknameLabel.useMediumFont()
        numberLabel.useMediumFont()
        balanceLabel.useLargeFont()
        balanceLabel.makeBold()
        nicknameLabel.adjustsFontSizeToFitWidth = true
        numberLabel.adjustsFontSizeToFitWidth = true
        balanceLabel.adjustsFontSizeToFitWidth = true
        containerView.corner(cornerRadius: 10)
        account = nil
        selectionStyle = .none
    }
    
    @objc private func onClick() {
        onPickerClick?()
    }
}
