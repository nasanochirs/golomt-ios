//
//  BaseHeaderCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/16/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class BaseHeaderCell: UITableViewHeaderFooterView {
    @IBOutlet var headerLabel: UILabel!
    @IBOutlet var headerButton: UIButton!
    @IBAction func headerAction(_ sender: Any) {
        onButtonClick?()
    }
    
    var onButtonClick: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }

    func setupLabel(_ label: String) {
        headerLabel.text = label
        headerLabel.allCaps()
    }
    
    var buttonImage: String = "" {
        didSet {
            headerButton.isHidden = false
            let image = UIImage(named: buttonImage)?.withRenderingMode(.alwaysTemplate)
            headerButton.tintColor = .defaultPrimaryText
            headerButton.setImage(image, for: .normal)
        }
    }

    private func initComponent() {
        headerLabel.textColor = .defaultPrimaryText
        headerLabel.useMediumFont()
        headerLabel.makeBold()
    }
}
