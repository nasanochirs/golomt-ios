//
//  DefaultAmountFieldCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/4/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import IQKeyboardManagerSwift
import UIKit

extension DefaultAmountFieldCell: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if !canEdit {
            return false
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        collectionView.performBatchUpdates(nil, completion: { _ in
            if self.selectedCurrency != nil {
                let index = self.currencyList.firstIndex(of: self.selectedCurrency!)
                if let unwrappedIndex = index {
                    self.collectionView.layoutIfNeeded()
                    self.collectionView.scrollToItem(at: IndexPath(row: unwrappedIndex + 1, section: 0), at: .centeredHorizontally, animated: true)
                }
            }
        })
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.keyboardType == .decimalPad {
            var repString = string

            if string == "," {
                repString = "."
            }

            let newText = textField.text.orEmpty as NSString?

            var proposedNewString = newText?.replacingCharacters(in: range, with: repString)
            proposedNewString = getNumber(proposedNewString)

            textField.text = proposedNewString

            textField.sendActions(for: .editingChanged)

            return false
        }

        return true
    }

    func getNumber(_ amountString: String?) -> String? {
        var amountString = amountString.orZero
        amountString = amountString.replacingOccurrences(of: ",", with: "")

        let amount = Double(amountString) ?? 0.0

        let numberFormatter = NumberFormatter()
        numberFormatter.formatterBehavior = .behavior10_4
        numberFormatter.numberStyle = .decimal
        numberFormatter.roundingMode = .down
        numberFormatter.decimalSeparator = "."
        numberFormatter.groupingSeparator = ","
        numberFormatter.usesGroupingSeparator = true

        if amountString.contains(".") {
            let rangeOfDot = amountString.range(of: ".")
            let rangeLowerBound = rangeOfDot?.lowerBound

            if let rangeLowerBound = rangeLowerBound {
                let location = textField.text.orEmpty.distance(from: amountString.startIndex, to: rangeLowerBound)
                numberFormatter.minimumFractionDigits = amountString.count - location - 1
                numberFormatter.maximumFractionDigits = amountString.count - location - 1
                if location == amountString.count - 1 {
                    return "\(numberFormatter.string(from: NSNumber(value: amount)) ?? "")."
                } else if location > amountString.count - 3 {
                    return numberFormatter.string(from: NSNumber(value: amount))
                } else {
                    numberFormatter.minimumFractionDigits = 2
                    numberFormatter.maximumFractionDigits = 2
                    return numberFormatter.string(from: NSNumber(value: amount))
                }
            }
        } else {
            numberFormatter.minimumFractionDigits = 0
            numberFormatter.maximumFractionDigits = 0

            if amount == 0 {
                return nil
            }

            return numberFormatter.string(from: NSNumber(value: amount))
        }

        return numberFormatter.string(from: NSNumber(value: amount))
    }
}

extension DefaultAmountFieldCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        currencyList.count + 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
            let label = UILabel()
            label.text = "transaction_choose_currency_keyboard_label".localized()
            label.textColor = .defaultPrimaryText
            label.useXSmallFont()
            cell.addSubView(view: label, left: 10, top: 0, right: -10, bottom: 0)
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CurrencyCollectionViewCell", for: indexPath) as! CurrencyCollectionViewCell
        cell.currency = currencyList[indexPath.row - 1]
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            return
        }
        let cell = collectionView.cellForItem(at: indexPath) as! CurrencyCollectionViewCell
        cell.isSelected = true
        selectedCurrency = CurrencyConstants(rawValue: cell.currencyLabel.text ?? "")
    }
}

class DefaultAmountFieldCell: UITableViewCell {
    @IBOutlet var checkField: DefaultCheckField?
    @IBOutlet var currencyStackView: UIStackView!
    @IBOutlet var currencyLabel: UILabel!
    @IBOutlet var arrowDownImage: UIImageView!
    @IBOutlet var subTitleLabel: UILabel?
    @IBOutlet var subInfoLabel: UILabel?
    @IBOutlet var subInfoContainerView: UIView?
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var textField: UITextField!
    @IBOutlet var separator: UIView!
    @IBOutlet var errorLabel: UILabel!

    var onCurrencyClick: (() -> Void)?
    var onStateChange: (() -> Void)?

    lazy var collectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        flowLayout.minimumInteritemSpacing = 0.0
        flowLayout.minimumLineSpacing = 0.0
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: textField.bounds.size.width, height: 80), collectionViewLayout: flowLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isScrollEnabled = true
        collectionView.backgroundColor = .defaultSecondaryBackground
        collectionView.registerCell(nibName: "CurrencyCollectionViewCell")
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        return collectionView

    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }

    func hasError() -> Bool {
        if hasAmountError() {
            errorLabel.text = "error_amount_not_zero".localized()
            separator.backgroundColor = .defaultError
            errorLabel.textColor = .defaultError
            checkField?.changeCheckState(checked: nil, color: .defaultError)
            onStateChange?()
            return true
        }
        if hasCurrencyError() {
            errorLabel.text = "error_pick_currency".localized()
            separator.backgroundColor = .defaultError
            errorLabel.textColor = .defaultError
            currencyLabel.textColor = .defaultError
            arrowDownImage.setTint(color: .defaultError)
            checkField?.changeCheckState(checked: nil, color: .defaultError)
            onStateChange?()
            return true
        }
        return false
    }

    var selectedCurrency: CurrencyConstants? {
        set {
            if newValue == nil {
                currencyLabel.text = "currency_title".localized()
            } else {
                currencyLabel.text = newValue?.rawValue
                if !inputText.isEmpty {
                    textField.resignFirstResponder()
                }
                let index = currencyList.firstIndex(of: newValue!)
                if let unwrappedIndex = index {
                    collectionView.selectItem(at: IndexPath(row: unwrappedIndex + 1, section: 0), animated: true, scrollPosition: .centeredHorizontally)
                }
            }
            setCurrencyCheck()
        }
        get {
            CurrencyConstants(rawValue: currencyLabel.text.orEmpty)
        }
    }

    var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }

    var inputText: String {
        get {
            textField.text.orEmpty
        }
        set {
            textField.text = newValue
            textField.sendActions(for: .editingChanged)
        }
    }

    var subTitle: String = "" {
        didSet {
            subTitleLabel?.text = subTitle
        }
    }

    var subInfo: String? = "" {
        didSet {
            subInfoLabel?.text = subInfo
            onStateChange?()
        }
    }

    var currencyList: [CurrencyConstants] = [CurrencyConstants]() {
        didSet {
            selectedCurrency = nil
            if !currencyList.isEmpty {
                collectionView.reloadData()
                textField.inputAccessoryView = collectionView
            } else {
                textField.inputAccessoryView = nil
            }
        }
    }

    var canEdit: Bool = true
    
    func focusTextField() {
        textField.becomeFirstResponder()
    }

    func hideLine() {
        checkField?.checkLine.isHidden = true
    }
    
    private func initComponent() {
        selectionStyle = .none
        textField.keyboardType = .decimalPad
        textField.placeholder = "0.0"
        currencyLabel.text = "currency_title".localized()
        currencyLabel.textColor = .defaultPrimaryText
        currencyLabel.useLargeFont()
        currencyLabel.makeBold()
        currencyLabel.adjustsFontSizeToFitWidth = true
        currencyStackView.addTapGesture(tapNumber: 1, target: self, action: #selector(handleCurrencyClick))
        arrowDownImage.setTint(color: .defaultPrimaryText)

        titleLabel.textColor = .defaultSeparator
        titleLabel.useSmallFont()
        titleLabel.makeBold()

        errorLabel.textColor = .red
        errorLabel.useSmallFont()

        textField.borderStyle = .none
        textField.addTarget(self, action: #selector(setAmountCheck), for: .editingChanged)
        textField.delegate = self
        textField.textColor = .defaultPrimaryText
        textField.useLargeFont()
        textField.makeBold()

        separator.backgroundColor = .defaultSeparator

        initSubView()
    }

    private func initSubView() {
        subInfoContainerView?.corner(cornerRadius: 10)
        subTitleLabel?.useXSmallFont()
        subInfoLabel?.useSmallFont()
        subTitleLabel?.adjustsFontSizeToFitWidth = true
        subInfoLabel?.adjustsFontSizeToFitWidth = true
        subTitleLabel?.textColor = .defaultSecondaryText
        subInfoLabel?.textColor = .defaultPrimaryText
    }

    @objc private func setAmountCheck() {
        switch true {
        case hasAmountError():
            errorLabel.text = "error_amount_not_zero".localized()
            separator.backgroundColor = .defaultError
            checkField?.changeCheckState(checked: nil, color: .defaultError)
        case hasCurrencyError():
            errorLabel.text = "error_pick_currency".localized()
            checkField?.changeCheckState(checked: nil, color: .defaultWarning)
        default:
            errorLabel.text = nil
            separator.backgroundColor = .defaultSeparator
            checkField?.changeCheckState(checked: true)
        }
        onStateChange?()
    }

    private func setCurrencyCheck() {
        if hasCurrencyError(), !hasAmountError() {
            errorLabel.text = "error_pick_currency".localized()
            checkField?.changeCheckState(checked: nil, color: .defaultWarning)
        } else if !hasCurrencyError(), !hasAmountError() {
            errorLabel.text = nil
            checkField?.changeCheckState(checked: true)
        } else if !hasCurrencyError(), hasAmountError() {
            errorLabel.text = nil
            checkField?.changeCheckState(checked: false)
        } else {
            separator.backgroundColor = .defaultSeparator
            currencyLabel.textColor = .defaultPrimaryText
            arrowDownImage.setTint(color: .defaultPrimaryText)
        }
        onStateChange?()
    }

    private func hasAmountError() -> Bool {
        if inputText.isEmpty, inputText.isZero {
            errorLabel.textColor = .defaultError
            return true
        }
        separator.backgroundColor = .defaultSeparator
        return false
    }

    private func hasCurrencyError() -> Bool {
        if selectedCurrency == nil {
            errorLabel.textColor = .defaultWarning
            currencyLabel.textColor = .defaultWarning
            separator.backgroundColor = .defaultWarning
            arrowDownImage.setTint(color: .defaultWarning)
            return true
        }
        errorLabel.textColor = .defaultError
        currencyLabel.textColor = .defaultPrimaryText
        separator.backgroundColor = .defaultSeparator
        arrowDownImage.setTint(color: .defaultPrimaryText)
        return false
    }

    @objc private func handleCurrencyClick() {
        onCurrencyClick?()
    }
}
