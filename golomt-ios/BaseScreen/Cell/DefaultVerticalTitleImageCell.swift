//
//  DefaultVerticalTitleImageCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/10/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class DefaultVerticalTitleImageCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var labelImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.textColor = .defaultSeparator
        titleLabel.useSmallFont()
    }
    
    var titleText: String = "" {
        didSet {
            titleLabel.text = titleText.localized()
        }
    }
    
    var labelImage: UIImage? = nil {
        didSet {
            labelImageView.image = labelImage
        }
    }
}
