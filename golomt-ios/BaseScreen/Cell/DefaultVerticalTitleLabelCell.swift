//
//  DefaultVerticalTitleLabelCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/10/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class DefaultVerticalTitleLabelCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var infoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }
    
    var titleText: String = "" {
        didSet {
            titleLabel.text = titleText.localized()
        }
    }
    
    var labelText: String = "" {
        didSet {
            infoLabel.text = labelText.localized()
        }
    }
    
    func setData(_ data: TableViewModel.Row) {
        titleLabel.text = data.title
        infoLabel.text = data.info
        switch data.infoProperty {
        case .Income:
            setInfoIncome()
        case .Expense:
            setInfoExpense()
        case .DefaultAmount:
            setInfoDefaultAmount()
        default:
            setInfoDefault()
        }
    }
    
    private func initComponent() {
        titleLabel.useSmallFont()
        titleLabel.textColor = .defaultSeparator
        infoLabel.useLargeFont()
        infoLabel.textColor = .defaultPrimaryText
        infoLabel.wordWrap()
        selectionStyle = .none
    }
    
    private func setInfoDefault() {
           infoLabel.textColor = .defaultPrimaryText
           infoLabel.useLargeFont()
       }
       
       private func setInfoIncome() {
           infoLabel.textColor = .defaultGreen
           infoLabel.useXXLargeFont()
           infoLabel.makeBold()
       }
       
       private func setInfoExpense() {
           infoLabel.textColor = .defaultRed
           infoLabel.useXXLargeFont()
           infoLabel.makeBold()
       }
       
       private func setInfoDefaultAmount() {
           infoLabel.textColor = .defaultPrimaryText
           infoLabel.useXXLargeFont()
           infoLabel.makeBold()
       }
}
