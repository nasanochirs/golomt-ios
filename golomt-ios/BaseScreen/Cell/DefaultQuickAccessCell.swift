//
//  DefaultQuickAccessCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 8/7/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class DefaultQuickAccessCell: UICollectionViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var accountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        descriptionLabel.useMediumFont()
        descriptionLabel.textColor = .defaultSecondaryText
        descriptionLabel.sizeToFit()
        descriptionLabel.adjustsFontSizeToFitWidth = true
        titleLabel.useMediumFont()
        titleLabel.textColor = .defaultPrimaryText
        titleLabel.makeBold()
        titleLabel.sizeToFit()
        accountLabel.useMediumFont()
        accountLabel.textColor = .defaultSecondaryText
        accountLabel.sizeToFit()
        accountLabel.adjustsFontSizeToFitWidth = true
        contentView.backgroundColor = .defaultSecondaryBackground
        contentView.corner(cornerRadius: 15)
    }
    
    var titleText: String = "" {
        didSet {
            titleLabel.isHidden = false
            titleLabel.text = titleText
        }
    }
    
    var descriptionText: String = "" {
        didSet {
            descriptionLabel.text = descriptionText
        }
    }
    
    var accountText: String = "" {
        didSet {
            accountLabel.isHidden = false
            accountLabel.text = accountText
        }
    }
}
