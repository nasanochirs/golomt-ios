//
//  DefaultSegmentedGroupCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/12/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class DefaultSegmentedGroupCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var separator: UIView?
    @IBOutlet var segmentedGroup: UISegmentedControl!
    @IBOutlet var containerView: UIView?
    
    var onSegmentChange: ((Int?) -> Void)?
    var onStateChange: (() -> Void)?
    
    struct SegmentModel {
        var code = ""
        var value = ""
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }
    
    var title: String = "" {
        didSet {
            titleLabel?.text = title
        }
    }
    
    var segments: [SegmentModel] = [] {
        didSet {
            for (index, segment) in segments.enumerated() {
                segmentedGroup.insertSegment(withTitle: segment.value, at: index, animated: true)
                for segmentItem: UIView in segmentedGroup.subviews {
                    for item: Any in segmentItem.subviews {
                        if let i = item as? UILabel {
                            i.numberOfLines = 0
                        }
                    }
                }
            }
        }
    }
    
    var selectedSegmentIndex: Int {
        get {
            segmentedGroup.selectedSegmentIndex
        }
        set {
            segmentedGroup.selectedSegmentIndex = newValue
            onSegmentChange?(newValue)
        }
    }
    
    var selectedSegment: SegmentModel {
        segments[selectedSegmentIndex]
    }
    
    private func initComponent() {
        selectionStyle = .none
        titleLabel?.textColor = .defaultSeparator
        titleLabel?.useSmallFont()
        titleLabel?.makeBold()
        separator?.backgroundColor = .defaultSeparator
        containerView?.backgroundColor = .defaultHeader
        let selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.defaultPrimaryText]
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.defaultSecondaryText]
        segmentedGroup.setTitleTextAttributes(titleTextAttributes, for: .normal)
        segmentedGroup.setTitleTextAttributes(selectedTitleTextAttributes, for: .selected)
        segmentedGroup.removeAllSegments()
        segmentedGroup.addTarget(self, action: #selector(segmentChanged), for: .valueChanged)
    }
    
    @objc private func segmentChanged() {
        onSegmentChange?(segmentedGroup?.selectedSegmentIndex)
    }
}
