//
//  DefaultTitleLabelCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/28/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class DefaultTitleLabelCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var titleWidthConstraint: NSLayoutConstraint!
    @IBOutlet var containerStackView: UIStackView!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }
    
    func setData(_ data: TableViewModel.Row) {
        titleLabel.text = data.title
        let currencySymbol = data.currency?.rawValue.toCurrencySymbol
        infoLabel.text = "amount_with_currency_symbol".localized(with: data.info.orEmpty, currencySymbol.orEmpty)
        switch data.infoProperty {
        case .Income:
            setInfoIncome()
        case .Expense:
            setInfoExpense()
        case .DefaultAmount:
            setInfoDefaultAmount()
        case .InterestRate:
            setInterestRate()
        default:
            setInfoDefault()
        }
    }
    
    func setTitleWidth(_ width: CGFloat) {
        titleWidthConstraint.constant = width
        titleLabel.updateConstraints()
        titleLabel.layoutIfNeeded()
    }
    
    func setConstraint(_ constant: CGFloat) {
        bottomConstraint.constant = constant
        topConstraint.constant = constant
        self.contentView.layoutIfNeeded()
        self.containerStackView.layoutIfNeeded()
    }
    
    private func initComponent() {
        selectionStyle = .none
        titleLabel.textColor = .defaultSecondaryText
        titleLabel.useMediumFont()
        titleLabel.adjustsFontSizeToFitWidth = true
        setInfoDefault()
        infoLabel.wordWrap()
        if isIPhoneX() {
            setConstraint(10)
        }
        if isIPhoneXS() {
            setConstraint(15)
        }
    }
    
    
    private func setInfoDefault() {
        infoLabel.textColor = .defaultPrimaryText
        infoLabel.useLargeFont()
    }
    
    private func setInfoIncome() {
        infoLabel.textColor = .defaultGreen
        infoLabel.useXXLargeFont()
        infoLabel.makeBold()
    }
    
    private func setInfoExpense() {
        infoLabel.textColor = .defaultRed
        infoLabel.useXXLargeFont()
        infoLabel.makeBold()
    }
    
    private func setInfoDefaultAmount() {
        infoLabel.textColor = .defaultPrimaryText
        infoLabel.useXXLargeFont()
        infoLabel.makeBold()
    }
    
    private func setInterestRate() {
        infoLabel.text = "interest_rate".localized(with: infoLabel.text.orEmpty)
    }
}
