//
//  DefaultLabelCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/10/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class DefaultLabelCell: UITableViewCell {
    @IBOutlet var labelLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }

    var labelText: String = "" {
        didSet {
            labelLabel.text = labelText.localized()
        }
    }
    
    private func initComponent() {
        labelLabel.textColor = .defaultPrimaryText
        labelLabel.useMediumFont()
        selectionStyle = .none
    }
    
}
