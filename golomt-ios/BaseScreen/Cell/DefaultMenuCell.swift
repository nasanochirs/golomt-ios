//
//  DefaultMenuCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/23/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class DefaultMenuCell: UITableViewCell {
    @IBOutlet var containerView: UIView!
    @IBOutlet var menuImageView: UIImageView!
    @IBOutlet var menuTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.corner(cornerRadius: 15)
        menuTitleLabel.textColor = .defaultPrimaryText
        menuTitleLabel.useMediumFont()
        menuTitleLabel.makeBold()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData( data: MenuTableViewModel.Row) {
        menuImageView.image = data.icon
        menuTitleLabel.text = data.title?.localized()
    }
}
