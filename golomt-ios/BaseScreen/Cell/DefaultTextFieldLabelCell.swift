//
//  DefaultTextFieldLabelCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/5/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class DefaultTextFieldLabelCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var textField: UITextField!
    @IBOutlet var separator: UIView!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var imageButton: UIButton?
    @IBOutlet var subInfoLabel: UILabel?
    @IBOutlet var subInfoContainerView: UIView?
    @IBOutlet var checkField: DefaultCheckField!
    @IBAction func handleImageClick(_ sender: Any) {
        onImageClick?()
    }
    
    var onImageClick: (() -> Void)?
    var onTextChanged: (() -> Void)?
    var onStateChange: (() -> Void)?
    
    var textLength: Int = 255
    var lengthValidation: LengthValidation?
    
    enum LengthValidation {
        case Less, Equal, Greater
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }
    
    func hideLine() {
        checkField.checkLine.isHidden = true
    }
    
    func showLine() {
        checkField.checkLine.isHidden = false
    }

    func setData1( data: TableViewModel.Row) {
           titleLabel.text = data.title
    }
    
    func setData(_ data: Any) {
        switch data {
        case let accountBook as AccountBookResponse.AccountBook:
            textField.text = accountBook.BNIF_ACCNT_NUMBER.orEmpty
            subInfo = ""
        default:
            break
        }
    }
    
    func setTag(_ tag: Int, delegate: UITextFieldDelegate) {
        textField.tag = tag
        textField.delegate = delegate
    }
    
    func hasError() -> Bool {
        errorLabel.text = nil
        separator.backgroundColor = .defaultSeparator
        errorLabel.textColor = .defaultError
        if inputText.isEmpty {
            separator.backgroundColor = .defaultError
            errorLabel.text = "error_not_empty".localized(with: title)
            checkField.changeCheckState(checked: nil)
            onStateChange?()
            return true
        }
        switch lengthValidation {
        case .Equal:
            if inputText.count < textLength {
                separator.backgroundColor = .defaultError
                errorLabel.text = "error_must_be_equal_to".localized(with: textLength)
                checkField.changeCheckState(checked: nil)
                onStateChange?()
                return true
            }
        case .Greater:
            if inputText.count <= textLength {
                separator.backgroundColor = .defaultError
                errorLabel.text = "error_must_be_greater_than".localized(with: textLength)
                checkField.changeCheckState(checked: nil)
                onStateChange?()
                return true
            }
        default:
            return false
        }
        onStateChange?()
        return false
    }
    
    func checkError() -> Bool? {
        errorLabel.text = nil
        separator.backgroundColor = .defaultSeparator
        if inputText.isEmpty {
            separator.backgroundColor = .defaultError
            errorLabel.textColor = .defaultError
            errorLabel.text = "error_not_empty".localized(with: title)
            onStateChange?()
            return nil
        }
        onStateChange?()
        return false
    }
    
    var keyboardType: UIKeyboardType = .default {
        didSet {
            textField.keyboardType = keyboardType
        }
    }
    
    var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }
    
    var subInfo: String? = "" {
        didSet {
            subInfoLabel?.text = subInfo
        }
    }
    
    var inputText: String {
        get {
            textField.text.orEmpty
        }
        set {
            textField.text = newValue
            if !newValue.isEmpty {
                textField.sendActions(for: .editingChanged)
            } else {
                checkField.changeCheckState(checked: false)
            }
        }
    }
    
    var hasImage: Bool = true {
        didSet {
            if !hasImage {
                imageButton?.isHidden = true
            }
        }
    }
    
    var buttonImage: String = "account_book" {
        didSet {
            let image = UIImage(named: buttonImage)?.withRenderingMode(.alwaysTemplate)
            imageButton?.setImage(image, for: .normal)
        }
    }
    
    var buttonColor: UIColor = UIColor() {
        didSet {
            imageButton?.imageView?.setTint(color: buttonColor)
            imageButton?.tintColor = buttonColor
        }
    }
    
    var maxLines: Int = 1 {
        didSet {
            
        }
    }
    
    func focusTextField() {
        textField.becomeFirstResponder()
    }
    
    private func initComponent() {
        selectionStyle = .none
        
        titleLabel.textColor = .defaultSeparator
        titleLabel.useSmallFont()
        titleLabel.makeBold()
        
        errorLabel.textColor = .red
        errorLabel.useSmallFont()
        
        textField.borderStyle = .none
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        textField.textColor = .defaultPrimaryText
        textField.delegate = self
        textField.useLargeFont()
        
        
        separator.backgroundColor = .defaultSeparator
        initSubView()
    }
    
    private func initSubView() {
        subInfoContainerView?.corner(cornerRadius: 10)
        subInfoLabel?.font = .italicSystemFont(ofSize: 13)
        subInfoLabel?.adjustsFontSizeToFitWidth = true
        subInfoLabel?.textColor = .defaultSeparator
    }
    
    @objc private func textFieldDidChange() {
        onTextChanged?()
        setCheck()
    }
    
    private func setCheck() {
        switch checkError() {
        case nil:
            checkField.changeCheckState(checked: nil)
        case false:
            checkField.changeCheckState(checked: true)
        case true:
            checkField.changeCheckState(checked: false)
        default:
            break
        }
        validateLength()
    }
    
    private func validateLength() {
        switch lengthValidation {
        case .Equal:
            if inputText.count < textLength {
                separator.backgroundColor = .defaultWarning
                errorLabel.textColor = .defaultWarning
                errorLabel.text = "error_must_be_equal_to".localized(with: textLength)
                checkField.changeCheckState(checked: nil, color: .defaultWarning)
                onStateChange?()
            }
        case .Greater:
            if inputText.count <= textLength {
                separator.backgroundColor = .defaultWarning
                errorLabel.textColor = .defaultWarning
                errorLabel.text = "error_must_be_greater_than".localized(with: textLength)
                checkField.changeCheckState(checked: nil, color: .defaultWarning)
                onStateChange?()
            }
        default:
            break
        }
    }
    
    
    func getNumber(_ amountString: String?) -> String? {
        var amountString = amountString.orZero
        amountString = amountString.replacingOccurrences(of: ",", with: "")

        let amount = Double(amountString) ?? 0.0

        let numberFormatter = NumberFormatter()
        numberFormatter.formatterBehavior = .behavior10_4
        numberFormatter.numberStyle = .decimal
        numberFormatter.roundingMode = .down
        numberFormatter.decimalSeparator = "."
        numberFormatter.groupingSeparator = ","
        numberFormatter.usesGroupingSeparator = true

        if amountString.contains(".") {
            let rangeOfDot = amountString.range(of: ".")
            let rangeLowerBound = rangeOfDot?.lowerBound

            if let rangeLowerBound = rangeLowerBound {
                let location = textField.text.orEmpty.distance(from: amountString.startIndex, to: rangeLowerBound)
                numberFormatter.minimumFractionDigits = amountString.count - location - 1
                numberFormatter.maximumFractionDigits = amountString.count - location - 1
                if location == amountString.count - 1 {
                    return "\(numberFormatter.string(from: NSNumber(value: amount)) ?? "")."
                } else if location > amountString.count - 3 {
                    return numberFormatter.string(from: NSNumber(value: amount))
                } else {
                    numberFormatter.minimumFractionDigits = 2
                    numberFormatter.maximumFractionDigits = 2
                    return numberFormatter.string(from: NSNumber(value: amount))
                }
            }
        } else {
            numberFormatter.minimumFractionDigits = 0
            numberFormatter.maximumFractionDigits = 0

            if amount == 0 {
                return nil
            }

            return numberFormatter.string(from: NSNumber(value: amount))
        }

        return numberFormatter.string(from: NSNumber(value: amount))
    }
}

extension DefaultTextFieldLabelCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.keyboardType == .decimalPad {
             var repString = string

             if string == "," {
                 repString = "."
             }

             let newText = textField.text.orEmpty as NSString?

             var proposedNewString = newText?.replacingCharacters(in: range, with: repString)
             proposedNewString = getNumber(proposedNewString)

             textField.text = proposedNewString

             textField.sendActions(for: .editingChanged)

             return false
         }
        guard let stringRange = Range(range, in: inputText) else {

            return false
        }
        let newString = inputText.replacingCharacters(in: stringRange, with: string)
        switch lengthValidation {
            case .Equal:
                return newString.count <= textLength
            case .Less:
                return newString.count < textLength
            default:
                return true
        }

    }
    
}
