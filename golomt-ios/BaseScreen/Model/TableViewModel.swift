//
//  DefaultTitleLabelCellModel.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/28/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class TableViewModel {
    var sections = [Section]()

    class Section {
        internal init(title: String? = nil, image: String? = nil, rows: [TableViewModel.Row]) {
            self.title = title
            self.image = image
            self.rows = rows
        }
        
        var title: String?
        var image: String?
        var rows: [Row]
    }

    enum InfoProperty {
        case Income, Expense, DefaultAmount, Default, InterestRate
    }

    class Row {
        internal init(title: String? = nil, info: String? = nil, infoProperty: TableViewModel.InfoProperty? = nil, cell: UITableViewCell? = nil, currency: CurrencyConstants? = nil) {
            self.title = title
            self.info = info
            self.infoProperty = infoProperty
            self.cell = cell
            self.currency = currency
        }
        
        var title: String?
        var info: String?
        var infoProperty: InfoProperty?
        var cell: UITableViewCell?
        var currency: CurrencyConstants?
    }
    
}
