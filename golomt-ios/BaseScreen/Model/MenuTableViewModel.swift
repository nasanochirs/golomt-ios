//
//  MenuTableViewModel.swift
//  golomt-ios
//
//  Created by Khulan Odkhuu on 7/4/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class MenuTableViewModel {

    var sections = [Section]()
    var rows = [Row]()
    
    class Section {
        internal init(title: String? = nil, rows: [MenuTableViewModel.Row]) {
            self.title = title
            self.rows = rows
        }
        
        var title: String?
        var rows: [Row]
    }

    class Row {
        internal init(icon: UIImage? = nil, title: String? = nil, cell: UITableViewCell? = nil) {
            self.icon = icon
            self.title = title
            self.cell = cell
        }
        
        var icon: UIImage?
        var title: String?
        var cell: UITableViewCell?

    
    }
    
}
