//
//  LoanCollateralModel.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/30/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LoanCollateralModel {
    var depositAccount: AccountResponse.Account? = nil
    var depositAccountDetails: DEPDetailResponse? = nil
    var beneficiaryAccount: AccountResponse.Account? = nil
    var loanAmount = "MNT|0.0"
    var fee = "MNT|0.0"
    var loanDuration: Int?
}
