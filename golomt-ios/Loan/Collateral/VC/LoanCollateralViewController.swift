//
//  LoanCollateralViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/30/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class LoanCollateralViewController: TableViewController {
    var filteredList = [AccountResponse.Account]()
    var minAmount = 0.0
    var maxAmount = 0.0
    let loanModel = LoanCollateralModel()
    
    lazy var depositAccountField: DefaultAccountPickerCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAccountPickerCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAccountPickerCell
        guard let unwrappedCell = cell else {
            return DefaultAccountPickerCell()
        }
        unwrappedCell.onPickerClick = {
            self.showSenderAccountPicker(self.filteredList)
        }
        unwrappedCell.defaultNicknameText = "loan_collateral_deposit_account_title".localized()
        unwrappedCell.defaultNumberText = "loan_collateral_deposit_account_label".localized()
        self.onAccountSelect = { account in
            self.handleAccountSelect(account)
        }
        self.accountPickerTitle = "loan_collateral_deposit_account_title".localized()
        return unwrappedCell
    }()
    
    lazy var depositSection: TableViewModel.Section = {
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(
                    cell: self.depositAccountField
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_available_balance_title".localized(), infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "loan_collateral_deposit_loan_balance_title".localized(), infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    cell: self.amountField
                )
            ]
        )
    }()
    
    lazy var loanSection: TableViewModel.Section = {
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(
                    cell: self.beneficiaryAccountField
                )
            ]
        )
    }()
    
    lazy var beneficiaryAccountField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerPlainCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultPickerButtonCell
        guard let unwrappedCell = cell else {
            return DefaultPickerButtonCell()
        }
        unwrappedCell.title = "loan_collateral_beneficiary_account_title".localized()
        unwrappedCell.onPickerClick = {
            let controller = AccountPickerController()
            controller.onAccountSelect = { account in
                unwrappedCell.selectedItem = account
            }
            controller.selectedAccount = unwrappedCell.selectedItem as? AccountResponse.Account
            controller.title = unwrappedCell.title
            controller.accounts = operativeAccountList
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()
    
    lazy var amountField: DefaultAmountFieldCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAmountPlainFieldCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAmountFieldCell
        guard let unwrappedCell = cell else {
            return DefaultAmountFieldCell()
        }
        unwrappedCell.title = "loan_collateral_amount_title".localized()
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Continue)
        title = "loan_collateral_title".localized()
        model.sections = [self.depositSection, self.loanSection]
        tableView.reloadData()
        
        onContinue = {
            let amount = self.amountField.inputText.toDouble
            if amount < self.minAmount {
                let dialog = errorDialog(message: "loan_collateral_minimum_amount_label".localized(with: self.minAmount))
                self.present(dialog, animated: true, completion: nil)
                return
            }
            if amount > self.maxAmount {
                let dialog = errorDialog(message: "loan_collateral_maximum_amount_label".localized(with: self.maxAmount))
                self.present(dialog, animated: true, completion: nil)
                return
            }
            if self.depositAccountField.hasError() {
                self.showSenderAccountPicker(self.filteredList)
                return
            }
            if self.beneficiaryAccountField.hasError() {
                return
            }
            self.loanModel.beneficiaryAccount = self.beneficiaryAccountField.selectedItem as? AccountResponse.Account
            let currency = self.loanModel.depositAccount?.ACCT_CURRENCY ?? ""
            self.loanModel.loanAmount = "format_with_slash".localized(with: currency, self.amountField.inputText.formattedWithoutComma)
            let confirmVC = LoanCollateralConfirmViewController(nibName: "TableViewController", bundle: nil)
            confirmVC.loanModel = self.loanModel
            self.navigationController?.pushViewController(confirmVC, animated: true)
        }
        
        self.fetchAccountProducts()
    }
    
    private func fetchAccountProducts() {
        showLoader()
        ConnectionFactory.fetchParameterValue(
            parameterName: "SBL_PRODUCT_LIST",
            success: { response in
                self.hideLoader()
                self.filteredList = []
                response.parameter?.P_VAL.orEmpty.components(separatedBy: ",").forEach { product in
                    self.filteredList.append(
                        contentsOf: depositAccountList.filter {
                            $0.PRODUCT_CATEGORY.orEmpty.contains(product)
                        }
                    )
                }
                self.fetchFee()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
    
    private func handleAccountSelect(_ account: AccountResponse.Account) {
        self.depositAccountField.account = account
        self.selectedAccount = account
        self.fetchAccountDetail(account)
    }
    
    private func fetchFee() {
        self.showLoader()
        ConnectionFactory.fetchParameterValue(
            parameterName: "SBL_FEE",
            success: { response in
                self.hideLoader()
                let fee = response.parameter?.P_VAL ?? "0.0"
                self.loanModel.fee = fee
                if self.selectedAccount != nil {
                    self.handleAccountSelect(self.selectedAccount!)
                }
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
    
    private func fetchAccountDetail(_ account: AccountResponse.Account) {
        self.showLoader()
        ConnectionFactory.fetchDEPDetail(
            account: account,
            isLoan: true,
            success: { response in
                self.hideLoader()
                let currency = response.detail?.CURRENCY_CODE ?? ""
                let loanBalance = response.balance?.OTHER_BAL?.toAmount.formattedWithComma
                self.depositSection.rows[1].info = response.balance?.CURRENT_BAL?.toAmountWithCurrencySymbol
                self.depositSection.rows[2].info = "amount_with_currency_symbol".localized(with: loanBalance.orEmpty, currency.toCurrencySymbol)
                self.amountField.selectedCurrency = CurrencyConstants(rawValue: currency)
                self.fetchMinAmount()
                self.loanModel.depositAccount = account
                self.loanModel.depositAccountDetails = response
                self.tableView.reloadData()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
    
    private func fetchMinAmount() {
        self.showLoader()
        ConnectionFactory.fetchParameterValue(
            parameterName: "SBL_MIN_AMOUNT",
            success: { response in
                self.hideLoader()
                self.minAmount = response.parameter?.P_VAL?.toDouble ?? 0.0
                self.fetchMaxAmount()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
    
    private func fetchMaxAmount() {
        self.showLoader()
        ConnectionFactory.fetchParameterValue(
            parameterName: "SBL_MAX_AMOUNT",
            success: { response in
                self.hideLoader()
                self.maxAmount = response.parameter?.P_VAL?.toDouble ?? 0.0
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
}
