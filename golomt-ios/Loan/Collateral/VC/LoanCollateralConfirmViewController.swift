//
//  LoanCollateralConfirmViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/30/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class LoanCollateralConfirmViewController: TableViewController {
    var loanModel = LoanCollateralModel()

    lazy var accountSection: TableViewModel.Section = {
        let number = loanModel.depositAccount?.ACCT_NUMBER ?? ""
        let currency = loanModel.depositAccount?.ACCT_CURRENCY ?? ""
        let loanBalance = self.loanModel.depositAccountDetails?.balance?.OTHER_BAL?.toAmount.formattedWithComma
        return TableViewModel.Section(
            title: "loan_collateral_deposit_account_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "loan_collateral_deposit_account_title".localized(), info: self.loanModel.depositAccount?.ACCT_NUMBER
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_available_balance_title".localized(), info: self.loanModel.depositAccountDetails?.balance?.CURRENT_BAL?.toAmountWithCurrencySymbol
                ),
                TableViewModel.Row(
                    title: "loan_collateral_deposit_loan_balance_title".localized(), info: "amount_with_currency_symbol".localized(with: loanBalance.orEmpty, currency.toCurrencySymbol)
                ),
            ]
        )
    }()

    lazy var loanSection: TableViewModel.Section = {
        let section = TableViewModel.Section(
            title: "loan_collateral_loan_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "dep_detail_account_yearly_interest_title".localized(), info: self.loanModel.depositAccountDetails?.balance?.SBL_INTEREST_RATE, infoProperty: .InterestRate
                ),
                TableViewModel.Row(
                    title: "loan_collateral_maturity_date_title".localized(), info: self.loanModel.depositAccountDetails?.detail?.MATURITY_DATE
                ),
                TableViewModel.Row(
                    title: "loan_collateral_duration_day_title".localized()
                ),
                TableViewModel.Row(
                    title: "loan_collateral_calculated_interest_title".localized()
                ),
                TableViewModel.Row(
                    title: "loan_collateral_amount_title".localized(), infoProperty: .DefaultAmount
                ),
            ]
        )
        let currency = self.loanModel.depositAccount?.ACCT_CURRENCY ?? ""
        let amount = self.loanModel.loanAmount
        let maturityDay = self.loanModel.depositAccountDetails?.detail?.MATURITY_DATE?.toDate
        if let maturityDay = maturityDay {
            let calendar = Calendar.current
            let todayDate = calendar.startOfDay(for: Date())
            let maturityDate = calendar.startOfDay(for: maturityDay)
            let components = calendar.dateComponents([.day], from: todayDate, to: maturityDate)
            self.loanModel.loanDuration = components.day
            section.rows[2].info = String(components.day ?? 0)
            if let interestRate = self.loanModel.depositAccountDetails?.balance?.SBL_INTEREST_RATE?.toDouble {
                let interestAmount = amount.toAmount * interestRate / 365 * Double(components.day ?? 0) / 100
                section.rows[3].info = "amount_with_currency_symbol".localized(with: interestAmount.formattedWithComma, currency.toCurrencySymbol)
            }
        }
        section.rows[4].info = amount.toAmountWithCurrencySymbol
        return section
    }()

    lazy var beneficiarySection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "loan_collateral_beneficiary_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "loan_collateral_beneficiary_account_title".localized(), info: loanModel.beneficiaryAccount?.ACCT_NUMBER
                ),
                TableViewModel.Row(
                    title: "loan_collateral_fee_title".localized(), info: "amount_with_currency_symbol".localized(with: self.loanModel.fee.formattedWithComma, "MNT".toCurrencySymbol)
                ),
            ]
        )
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Confirm)
        title = "transaction_confirm_button_title".localized()
        model.sections = [self.accountSection, self.loanSection, self.beneficiarySection]
        tableView.reloadData()
        onContinue = {
            self.handleContinue()
        }
    }

    private func handleContinue() {
        let loaderViewController = TransactionLoadingController(nibName: "TransactionLoadingController", bundle: nil)
        navigationController?.pushViewController(loaderViewController, animated: true)
        let detailViewController = TransactionDetailViewController(nibName: "TableViewController", bundle: nil)
        detailViewController.model.sections = [self.accountSection, self.loanSection, self.beneficiarySection]
        detailViewController.controllerTitle = "loan_collateral_title".localized()
        detailViewController.onFinish = {
            self.navigationController?.popToRootViewController(animated: true)
            if detailViewController.isSuccessful {
                NotificationCenter.default.post(name: Notification.Name(NotificationConstants.REFRESH_ACCOUNTS), object: nil)
            }
        }
        ConnectionFactory.getCollateralLoan(
            model: self.loanModel,
            success: { response in
                let resultMessage = response.getMessage()
                detailViewController.transactionMessage = resultMessage
                loaderViewController.setResult(
                    isSuccessful: true,
                    message: resultMessage,
                    completion: {
                        detailViewController.isSuccessful = true
                        detailViewController.model.sections.insert(
                            TableViewModel.Section(
                                rows: [
                                    TableViewModel.Row(
                                        title: "loan_collateral_account_number_title".localized(),
                                        info: response.collateral?.SBL_LOAN_ACNT
                                    ),
                                ]
                            ),
                            at: 0
                        )
                        self.navigationController?.pushViewController(detailViewController, animated: false)
                    }
                )
            },
            failed: { reason in
                let resultMessage = reason?.MESSAGE_DESC ?? ""
                detailViewController.transactionMessage = resultMessage
                loaderViewController.setResult(isSuccessful: false, message: resultMessage, completion: {
                    detailViewController.isSuccessful = false
                    self.navigationController?.pushViewController(detailViewController, animated: false)
                })
            }
        )
    }
}
