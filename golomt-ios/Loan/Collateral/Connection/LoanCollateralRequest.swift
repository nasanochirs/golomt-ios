//
//  LoanCollateralRequest.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/2/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LoanCollateralRequest: BaseRequest {
    var body = Body()

    private enum CodingKeys: String, CodingKey {
        case body
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(body, forKey: .body)
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    override init() {
        super.init()
        header.__SRVCID__ = "CSBLR"
    }

    struct Body: Codable {
        var SBL_OPR_ACNT = ""
        var SBL_CURRENCY_CODE = ""
        var SBL_LOAN_AMOUNT = ""
        var SBL_DEP_ACNT = ""
        var SBL_DAYS: Int?
        var SBL_FEE_ACNT = ""
        var BRANCH_ID = ""
        var SBL_INTEREST = ""
        var SBL_FEE = ""
        var SBL_OPR_CURR = ""
        var SBL_MATURITY_DATE = ""
    }
}

