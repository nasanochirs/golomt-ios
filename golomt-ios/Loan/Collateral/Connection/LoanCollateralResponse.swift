//
//  LoanCollateralResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/2/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LoanCollateralResponse: BaseResponse {
    let collateral: CollateralResponse?
    
    private enum CodingKeys: String, CodingKey {
        case `default`
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(collateral, forKey: .default)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.collateral = try container.decodeIfPresent(CollateralResponse.self, forKey: .default)
        try super.init(from: decoder)
    }
    
    struct CollateralResponse: Codable {
        var FORM_REQ_STATE: String?
        var SBL_DEP_ACNT: String?
        var SBL_OPR_ACNT: String?
        var SBL_LOAN_ACNT: String?
    }
}
