//
//  LoanGraphicResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/4/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LoanGraphicResponse: BaseResponse {
    let amortizationList: [Amortization]?
    private var historyList = [Amortization]()
    private var scheduleList = [Amortization]()
    
    private enum CodingKeys: String, CodingKey {
        case AmortizationListing_REC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(amortizationList, forKey: .AmortizationListing_REC)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.amortizationList = try container.decodeIfPresent([Amortization].self, forKey: .AmortizationListing_REC)
        try super.init(from: decoder)
    }
    
    struct Amortization: Codable {
        var INSTALLMENT_AMOUNT: String?
        var OPENING_LIABILITY_AMOUNT: String?
        var DUE_DATE: String?
        var CLOSING_LIABILITY_AMOUNT: String?
        var INTEREST_AMOUNT: String?
        var PRINCIPAL_AMOUNT: String?
    }
    
    func getHistoryList() -> [Amortization] {
        switch historyList.isEmpty {
        case true:
            historyList =  amortizationList?.filter {
                if let date = $0.DUE_DATE?.toBackwardDate {
                    return date < Date()
                }
                return false
            } ?? []
        case false:
            break
        }
        return historyList
    }
    
    func getScheduleList() -> [Amortization] {
        switch scheduleList.isEmpty {
        case true:
            scheduleList =  amortizationList?.filter {
                if let date = $0.DUE_DATE?.toBackwardDate {
                    return date > Date()
                }
                return false
            } ?? []
        case false:
            break
        }
        return scheduleList
    }
}
