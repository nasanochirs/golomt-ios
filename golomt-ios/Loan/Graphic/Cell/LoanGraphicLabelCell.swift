//
//  LoanGraphicLabelCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/4/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class LoanGraphicLabelCell: UITableViewCell {
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var installmentLabel: UILabel!
    @IBOutlet var balanceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        dateLabel.useMediumFont()
        dateLabel.textColor = .defaultSecondaryText
        dateLabel.adjustsFontSizeToFitWidth = true
        installmentLabel.useMediumFont()
        installmentLabel.textColor = .defaultPrimaryText
        installmentLabel.makeBold()
        installmentLabel.adjustsFontSizeToFitWidth = true
        balanceLabel.useMediumFont()
        balanceLabel.textColor = .defaultPrimaryText
        balanceLabel.makeBold()
        balanceLabel.adjustsFontSizeToFitWidth = true
    }

    func setLabel(_ amortization: LoanGraphicResponse.Amortization) {
        dateLabel.text = amortization.DUE_DATE.backwardDateFormatted
        installmentLabel.text = amortization.INSTALLMENT_AMOUNT.toAmountWithCurrencySymbol
        balanceLabel.text = amortization.CLOSING_LIABILITY_AMOUNT.toAmountWithCurrencySymbol
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
