//
//  LoanGraphicTitleCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/4/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class LoanGraphicTitleCell: UITableViewCell {
    @IBOutlet var leftTitleLabel: UILabel!
    @IBOutlet var centerTitleLabel: UILabel!
    @IBOutlet var rightTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        leftTitleLabel.text = "loan_graphic_left_title".localized();
        centerTitleLabel.text = "loan_graphic_center_title".localized();
        rightTitleLabel.text = "loan_graphic_right_title".localized();
        leftTitleLabel.setLabelFormat()
        centerTitleLabel.setLabelFormat()
        rightTitleLabel.setLabelFormat()
    }
}

fileprivate extension UILabel {
    func setLabelFormat() {
        self.useLargeFont()
        self.textColor = .defaultPrimaryText
    }
}
