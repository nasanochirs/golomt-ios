//
//  LoanGraphicViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/4/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class LoanGraphicViewController: BaseUIViewController {
    @IBOutlet var tableView: UITableView!
    
    var account: AccountResponse.Account?
    var amortizationList = [LoanGraphicResponse.Amortization]()
    var graphicResponse: LoanGraphicResponse?
    
    lazy var dateField: DefaultSegmentedGroupCell = {
        let nib = Bundle.main.loadNibNamed("DefaultSegmentedGroupCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultSegmentedGroupCell else {
            return DefaultSegmentedGroupCell()
        }
        cell.title = "loan_graphic_segment_title".localized()
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.segments = [DefaultSegmentedGroupCell.SegmentModel(code: "", value: "loan_graphic_first_segment_title".localized()), DefaultSegmentedGroupCell.SegmentModel(code: "", value: "loan_graphic_second_segment_title".localized())]
        cell.selectedSegmentIndex = 0
        cell.onSegmentChange = { segment in
            switch segment {
            case 0:
                self.amortizationList = self.graphicResponse?.getHistoryList().sorted { $0.DUE_DATE?.toBackwardDate ?? Date() > $1.DUE_DATE?.toBackwardDate ?? Date() } ?? []
            case 1:
                self.amortizationList = self.graphicResponse?.getScheduleList() ?? []
            default:
                break
            }
            self.tableView.reloadData()
        }
        return cell
    }()
    
    lazy var titleField: LoanGraphicTitleCell = {
        let nib = Bundle.main.loadNibNamed("LoanGraphicTitleCell", owner: self, options: nil)
        guard let cell = nib?.first as? LoanGraphicTitleCell else {
            return LoanGraphicTitleCell()
        }
        return cell
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "loan_graphic_title".localized()
        
        self.tableView.contentInsetAdjustmentBehavior = .never
        self.tableView.registerCell(nibName: "LoanGraphicLabelCell")
        self.tableView.tableFooterView = UIView()
        
        self.fetchGraphic()
    }
    
    private func fetchGraphic() {
        guard let account = account else {
            return
        }
        self.showLoader()
        ConnectionFactory.fetchAmortizationList(
            account: account,
            success: { response in
                self.hideLoader()
                self.graphicResponse = response
                self.dateField.selectedSegmentIndex = 0
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
}

extension LoanGraphicViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return amortizationList.count + 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return self.dateField
        case 1:
            return self.titleField
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "LoanGraphicLabelCell") as? LoanGraphicLabelCell else {
                return UITableViewCell()
            }
            let amortization = amortizationList[indexPath.row - 2]
            cell.setLabel(amortization)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {}
}
