//
//  LoanDigitalDetailResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/16/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LoanDigitalDetailResponse: BaseResponse {
    let detail: Detail?
    
    private enum CodingKeys: String, CodingKey {
        case FetchDataEventArray
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(detail, forKey: .FetchDataEventArray)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.detail = try container.decodeIfPresent(Detail.self, forKey: .FetchDataEventArray)
        try super.init(from: decoder)
    }
    
    struct Detail: Codable {
        var LOAN_FEE: String?
        var TOTAL_BALANCE: String?
        var INSTALLMENT_START_DATE: String?
        var LOAN_INT: String?
        var DEDUCTION_DAY: String?
        var DEDUCTION_TYPE: String?
        var LOAN_AMOUNT: String?
    }
}
