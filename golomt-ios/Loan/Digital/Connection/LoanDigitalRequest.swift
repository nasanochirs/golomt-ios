//
//  LoanDigitalRequest.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/16/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LoanDigitalRequest: BaseRequest {
    var body = Body()

    private enum CodingKeys: String, CodingKey {
        case body
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(body, forKey: .body)
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    override init() {
        super.init()
        header.__SRVCID__ = "RQOLN"
    }

    enum Event: String, Codable {
        case LOAD, FETCH_DATA, FETCH_INSTALLMENT_AMOUNT, CONFIRM
    }
    
    struct Body: Codable {
        var EVENT: Event = .LOAD
        var LOAN_AMOUNT = 0.0
        var MAX_AMOUNT = ""
        var MIN_AMOUNT = ""
        var MM_AMOUNT = ""
        var IB_AVAILABLE_AMOUNT = ""
        var ISPREAPPROVED = ""
        var MASTER_FLG = ""
        var NPL_FLG = ""
        var SAL_FLG = ""
        var IS_TOPUP = ""
        var LOAN_ACCT  = ""
        var LOAN_ACCT_INT = ""
        var CLR_BALANCE = ""
        var SALARY_ACCT = ""
        var MISPURPOSECODE = ""
        var MISSUBPURPOSECODE = ""
        var MISGROUPCODE = ""
        var SOLID = ""
        var ACCT_MGR_DOMAIN = ""
        var LOAN_FEE_PERCENT = ""
        var SAL_DATE = ""
        
        var EMP_FLG = ""
        
        var LOAN_PERIOD = ""
        var LOAN_FEE = ""
        var TOTAL_BALANCE = ""
        var INSTALLMENT_START_DATE = ""
        var LOAN_INT = ""
        var DEDUCTION_DAY = ""
        var DEDUCTION_TYPE = ""
        
        var OPERATIVE_ACCT = ""
    }
}
