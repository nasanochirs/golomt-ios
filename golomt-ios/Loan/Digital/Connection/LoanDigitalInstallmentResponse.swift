//
//  LoanDigitalInstallmentResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/16/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LoanDigitalInstallmentResponse: BaseResponse {
    let detail: Detail?
    
    private enum CodingKeys: String, CodingKey {
        case FetchExpiryDateEventArray
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(detail, forKey: .FetchExpiryDateEventArray)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.detail = try container.decodeIfPresent(Detail.self, forKey: .FetchExpiryDateEventArray)
        try super.init(from: decoder)
    }
    
    struct Detail: Codable {
        var LOAN_PERIOD: String?
        var LOAN_EXP_DATE: String?
        var INSTALLMENT_AMOUNT: String?
    }
}
