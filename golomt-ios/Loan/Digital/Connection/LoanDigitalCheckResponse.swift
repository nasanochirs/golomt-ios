//
//  LoanDigitalCheckResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/4/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LoanDigitalCheckResponse: BaseResponse {
    let error: Error?
    let detail: Detail?
    
    private enum CodingKeys: String, CodingKey {
        case ErrorArray, LoadEventArray
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(error, forKey: .ErrorArray)
        try container.encode(detail, forKey: .LoadEventArray)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.error = try container.decodeIfPresent(Error.self, forKey: .ErrorArray)
        self.detail = try container.decodeIfPresent(Detail.self, forKey: .LoadEventArray)
        try super.init(from: decoder)
    }
    
    struct Error: Codable {
        var ERROR_FLG: String?
        var ERROR_DESCRIPTION: String?
    }
    
    struct Detail: Codable {
        var MAX_AMOUNT: String?
        var MIN_AMOUNT: String?
        var MM_AMOUNT: String?
        var IB_AVAILABLE_AMOUNT: String?
        var ISPREAPPROVED: String?
        var MASTER_FLG: String?
        var NPL_FLG: String?
        var SAL_FLG: String?
        var IS_TOPUP: String?
        var LOAN_ACCT: String?
        var LOAN_ACCT_INT: String?
        var CLR_BALANCE: String?
        var SALARY_ACCT: String?
        var MISPURPOSECODE: String?
        var MISSUBPURPOSECODE: String?
        var MISGROUPCODE: String?
        var SOLID: String?
        var ACCT_MGR_DOMAIN: String?
        var LOAN_FEE_PERCENT: String?
        var SAL_DATE: String?
        var EMP_FLG: String?
    }
}
