//
//  LoanDigitalViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/4/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class LoanDigitalViewController: TableViewController {
    var monthList = [DataListResponse.Item]()
    var errorList = [DataListResponse.Item]()
    var loanModel = LoanDigitalModel()

    lazy var beneficiaryField: DefaultAccountPickerCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAccountPickerCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultAccountPickerCell else {
            return DefaultAccountPickerCell()
        }
        cell.onPickerClick = {
            self.showSenderAccountPicker()
        }
        cell.defaultNicknameText = "Зээл авах данс".localized()
        cell.defaultNumberText = "Сонгоно уу!".localized()
        self.onAccountSelect = { account in
            cell.account = account
            self.selectedAccount = account
        }
        self.accountPickerTitle = "Зээл авах данс".localized()
        return cell
    }()

    lazy var monthField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultPickerButtonCell else {
            return DefaultPickerButtonCell()
        }
        cell.title = "Зээлийн хугацаа".localized()
        cell.onPickerClick = {
            let controller = PickerController()
            controller.list = self.monthList
            controller.onItemSelect = { month in
                controller.popupController?.dismiss()
                cell.selectedItem = month
            }
            controller.selectedItem = cell.selectedItem
            controller.title = cell.title
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()

    lazy var amountField: DefaultAmountFieldCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAmountFieldCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultAmountFieldCell else {
            return DefaultAmountFieldCell()
        }
        cell.title = "Зээлийн дүн".localized()
        cell.selectedCurrency = CurrencyConstants.MNT
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.hideLine()
        return cell
    }()

    lazy var amountButtonsField: TransactionAmountButtonsCell = {
        let nib = Bundle.main.loadNibNamed("TransactionAmountButtonsCell", owner: self, options: nil)
        guard let cell = nib?.first as? TransactionAmountButtonsCell else {
            return TransactionAmountButtonsCell()
        }
        cell.setButtonTitles("Зээлийн доод дүн", "Зээлийн дээд дүн")
        cell.onButtonClick = { _ in
        }
        cell.hideLine()
        return cell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Continue)
        title = "loan_digital_title".localized()
        self.fetchLoanStatus()
        onContinue = {
            self.handleContinue()
        }
    }

    private func fetchLoanStatus() {
        showLoader()
        ConnectionFactory.fetchDigitalLoanStatus(
            success: { response in
                self.hideLoader()
                self.loanModel.initialDetails = response.detail
                self.fetchMonthList()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }

    private func fetchMonthList() {
        showLoader()
        ConnectionFactory.fetchList(
            code: "OLP",
            success: { response in
                self.hideLoader()
                self.monthList = response.list ?? []
                self.fetchErrorList()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }

    private func fetchErrorList() {
        showLoader()
        ConnectionFactory.fetchList(
            code: "VERT",
            success: { response in
                self.hideLoader()
                self.errorList = response.list ?? []
                self.model.sections = [
                    TableViewModel.Section(
                        rows: [
                            TableViewModel.Row(cell: self.beneficiaryField),
                            TableViewModel.Row(cell: self.monthField),
                            TableViewModel.Row(cell: self.amountField),
                            TableViewModel.Row(cell: self.amountButtonsField),
                        ]
                    ),
                ]
                let minAmount = self.loanModel.initialDetails?.MIN_AMOUNT
                let maxAmount = self.loanModel.initialDetails?.IB_AVAILABLE_AMOUNT
                self.amountButtonsField.setButtonLabels(minAmount.toAmountWithCurrencySymbol, maxAmount.toAmountWithCurrencySymbol)
                self.tableView.reloadData()
            }, failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }

    private func handleContinue() {
        if self.beneficiaryField.hasError() {
            self.showSenderAccountPicker()
            return
        }
        let monthError = self.monthField.hasError()
        let amountError = self.amountField.hasError()
        if monthError || amountError {
            return
        }
//        let detail = self.loanModel.initialDetails
//        if detail == nil { return }
        let amount = self.amountField.inputText.toDouble
        self.loanModel.beneficiaryAccount = selectedAccount
        self.loanModel.amount = amount
        self.loanModel.duration = monthField.selectedItem as? DataListResponse.Item
        let confirmVC = LoanDigitalConfirmViewController(nibName: "TableViewController", bundle: nil)
        confirmVC.loanModel = self.loanModel
        self.navigationController?.pushViewController(confirmVC, animated: true)
//        let maxAmount = detail!.MAX_AMOUNT?.toDouble
//        if maxAmount.orZero < amount {
//            self.present(errorDialog(message: "HEHE"), animated: true, completion: nil)
//            return
//        }
//        let mmAmount = detail!.MM_AMOUNT?.toDouble
//        if mmAmount.orZero < amount {
//            self.present(errorDialog(message: "HOHO"), animated: true, completion: nil)
//            return
//        }
//        let minAmount = detail!.MIN_AMOUNT?.toDouble
//        if minAmount.orZero > amount {
//            self.present(errorDialog(message: "HIHI"), animated: true, completion: nil)
//            return
//        }
//        let availableAmount = detail!.IB_AVAILABLE_AMOUNT?.toDouble
//        if availableAmount.orZero < amount {
//            self.present(errorDialog(message: "LEL"), animated: true, completion: nil)
//            return
//        }
    }
}
