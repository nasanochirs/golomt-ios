//
//  LoanDigitalConfirmViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/16/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class LoanDigitalConfirmViewController: TableViewController {
    var loanModel = LoanDigitalModel()

    lazy var loanSection: TableViewModel.Section = {
        let balance = self.loanModel.initialDetails?.CLR_BALANCE
        let totalBalance = self.loanModel.detail?.TOTAL_BALANCE
        let fee = self.loanModel.detail?.LOAN_FEE
        let installmentAmount = self.loanModel.installmentDetail?.INSTALLMENT_AMOUNT
        let section = TableViewModel.Section(
            title: "Зээлийн мэдээлэл".localized(),
            rows: [
                TableViewModel.Row(
                    title: "Авах зээлийн дүн".localized(), info: self.loanModel.amount.formattedWithComma, infoProperty: .DefaultAmount, currency: CurrencyConstants.MNT
                ),
                TableViewModel.Row(
                    title: "Өмнөх зээлийн үлдэгдэл".localized(), info: abs(balance.toDouble).formattedWithComma, infoProperty: .DefaultAmount, currency: CurrencyConstants.MNT
                ),
                TableViewModel.Row(
                    title: "Нийт зээлийн дүн".localized(), info: totalBalance?.formattedWithComma, infoProperty: .DefaultAmount, currency: CurrencyConstants.MNT
                ),
                TableViewModel.Row(
                    title: "Сард төлөх дүн".localized(), info: installmentAmount?.formattedWithComma, infoProperty: .DefaultAmount, currency: CurrencyConstants.MNT
                ),
                TableViewModel.Row(
                    title: "Шимтгэлийн дүн".localized(), info: fee?.formattedWithComma, infoProperty: .DefaultAmount, currency: CurrencyConstants.MNT
                ),
            ]
        )
        return section
    }()

    lazy var durationSection: TableViewModel.Section = {
        let installmentDate = self.loanModel.detail?.INSTALLMENT_START_DATE
        let section = TableViewModel.Section(
            title: "Хугацааны мэдээлэл".localized(),
            rows: [
                TableViewModel.Row(
                    title: "Эхний төлөлт хийх огноо".localized(), info: installmentDate
                ),
                TableViewModel.Row(
                    title: "Зээлийн хугацаа".localized(), info: self.loanModel.installmentDetail?.LOAN_PERIOD
                ),
                TableViewModel.Row(
                    title: "Зээл дуусах хугацаа".localized(), info: self.loanModel.installmentDetail?.LOAN_EXP_DATE
                ),
                TableViewModel.Row(
                    title: "Зээлийн төлөлт хийх өдрүүд".localized(), info: self.loanModel.detail?.DEDUCTION_DAY
                ),
            ]
        )
        return section
    }()

    lazy var accountSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "Дансны мэдээлэл".localized(),
            rows: [
                TableViewModel.Row(
                    title: "Зээл олгох данс".localized(), info: self.loanModel.beneficiaryAccount?.ACCT_NUMBER
                ),
            ]
        )
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Confirm)
        fetchDetail()
        onContinue = {
            self.handleContinue()
        }
    }

    private func fetchDetail() {
        showLoader()
        ConnectionFactory.fetchDigitalLoanDetail(
            model: loanModel,
            success: { response in
                self.hideLoader()
                self.loanModel.detail = response.detail
                self.fetchInstallmentAmount()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }

    private func fetchInstallmentAmount() {
        showLoader()
        ConnectionFactory.fetchDigitalLoanInstallmentAmount(
            model: loanModel,
            success: { response in
                self.hideLoader()
                self.loanModel.installmentDetail = response.detail
                self.model.sections = [self.loanSection, self.durationSection, self.accountSection]
                self.tableView.reloadData()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }

    private func handleContinue() {
        let contractVC = ContractViewController(nibName: "ContractViewController", bundle: nil)
        switch getLanguage() {
        case "en":
            contractVC.contractLink = "https://m.egolomt.mn/social/contract_digital_loan_eng.html"
        case "mn":
            contractVC.contractLink = "https://m.egolomt.mn/social/contract_digital_loan_mng.html"
        default:
            break
        }
        contractVC.controllerTitle = "contract_title".localized()
        contractVC.buttonTitle = "accept_button_title".localized()
        contractVC.checkBoxLabel = "contract_accept_label".localized()
        contractVC.onButtonClick = {
            let loaderViewController = TransactionLoadingController(nibName: "TransactionLoadingController", bundle: nil)
            self.navigationController?.pushViewController(loaderViewController, animated: true)
            let detailViewController = TransactionDetailViewController(nibName: "TableViewController", bundle: nil)
            detailViewController.model.sections = [self.loanSection, self.durationSection, self.accountSection]
            detailViewController.controllerTitle = "loan_digital_title".localized()
            detailViewController.onFinish = {
                self.navigationController?.popToRootViewController(animated: true)
                if detailViewController.isSuccessful {
                    NotificationCenter.default.post(name: Notification.Name(NotificationConstants.REFRESH_ACCOUNTS), object: nil)
                }
            }
            ConnectionFactory.getDigitalLoan(
                model: self.loanModel,
                success: { response in
                    let resultMessage = response.getMessage()
                    detailViewController.transactionMessage = resultMessage
                    loaderViewController.setResult(
                        isSuccessful: true,
                        message: resultMessage,
                        completion: {
                            detailViewController.isSuccessful = true
                            self.navigationController?.pushViewController(detailViewController, animated: false)
                        }
                    )
                },
                failed: { reason in
                    let resultMessage = reason?.MESSAGE_DESC ?? ""
                    detailViewController.transactionMessage = resultMessage
                    loaderViewController.setResult(
                        isSuccessful: false,
                        message: resultMessage,
                        completion: {
                            detailViewController.isSuccessful = false
                            self.navigationController?.pushViewController(detailViewController, animated: false)
                        }
                    )
                }
            )
        }
        navigationController?.pushViewController(contractVC, animated: true)
    }
}
