//
//  LoanDigitalModel.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/7/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LoanDigitalModel {
    var initialDetails: LoanDigitalCheckResponse.Detail?
    var detail: LoanDigitalDetailResponse.Detail?
    var installmentDetail: LoanDigitalInstallmentResponse.Detail?
    var duration: DataListResponse.Item?
    var beneficiaryAccount: AccountResponse.Account?
    var amount = 0.0
}
