//
//  LoanCloseModel.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/3/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LoanCloseModel {
    var detail: LoanDetailResponse.Detail?
    var remitterAccount: AccountResponse.Account?
    var loanAccount: AccountResponse.Account?
}
