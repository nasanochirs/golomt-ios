//
//  LoanCloseRequest.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/4/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LoanCloseRequest: BaseRequest {
    var body = Body()

    private enum CodingKeys: String, CodingKey {
        case body
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(body, forKey: .body)
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    override init() {
        super.init()
        header.__SRVCID__ = "CLACR"
    }

    struct Body: Codable {
        var USRCHOSN_LNACCNT_ID = ""
        var USRCHOSN_LNACCT_CRN = ""
        var USRCHOSN_OPACCT_ID = ""
        var USRCHOSN_OPACCT_CRN = ""
    }
}
