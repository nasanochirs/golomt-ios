//
//  LoanDetailResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/2/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LoanDetailResponse: BaseResponse {
    let detail: Detail?
    
    private enum CodingKeys: String, CodingKey {
        case CustomLAC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(detail, forKey: .CustomLAC)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.detail = try container.decodeIfPresent(Detail.self, forKey: .CustomLAC)
        try super.init(from: decoder)
    }
    
    struct Detail: Codable {
        var LNPAYOFF_AMT: String?
        var LNPAYOFF_CRN: String?
        var LNDEPCOLL_GLAG: String?
    }
}
