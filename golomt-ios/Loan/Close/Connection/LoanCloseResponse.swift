//
//  LoanCloseResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/4/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LoanCloseResponse: BaseResponse {
    let close: Response?
    
    private enum CodingKeys: String, CodingKey {
        case `default`
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(close, forKey: .default)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.close = try container.decodeIfPresent(Response.self, forKey: .default)
        try super.init(from: decoder)
    }
    
    struct Response: Codable {
        var LN_CLS_RESULT_MSG: String?
        var FORM_REQ_STATE: String?
    }
}
