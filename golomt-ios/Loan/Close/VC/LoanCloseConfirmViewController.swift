//
//  LoanCloseConfirmViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/3/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class LoanCloseConfirmViewController: TableViewController {
    var loanModel = LoanCloseModel()

    lazy var loanSection: TableViewModel.Section = {
        let loanAccountNumber = self.loanModel.loanAccount?.ACCT_NUMBER
        let loanAccountCurrency = self.loanModel.loanAccount?.ACCT_CURRENCY
        return TableViewModel.Section(
            title: "loan_close_confirm_loan_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "loan_close_confirm_account_number_title".localized(), info: "amount_with_currency".localized(with: loanAccountNumber.orEmpty, loanAccountCurrency.orEmpty)
                ),
                TableViewModel.Row(
                    title: "loan_close_confirm_account_nickname_title".localized(), info: self.loanModel.loanAccount?.ACCT_NICKNAME
                ),
            ]
        )
    }()

    lazy var remitterSection: TableViewModel.Section = {
        let beneficiaryAccountNumber = self.loanModel.remitterAccount?.ACCT_NUMBER
        let beneficiaryAccountCurrency = self.loanModel.remitterAccount?.ACCT_CURRENCY
        return TableViewModel.Section(
            title: "loan_close_confirm_remitter_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "loan_close_confirm_account_number_title".localized(), info: "amount_with_currency".localized(with: beneficiaryAccountNumber.orEmpty, beneficiaryAccountCurrency.orEmpty)
                ),
                TableViewModel.Row(
                    title: "loan_close_confirm_account_nickname_title".localized(), info: self.loanModel.remitterAccount?.ACCT_NICKNAME
                ),
            ]
        )
    }()

    lazy var amountSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "loan_close_confirm_amount_details_title",
            rows: [
                TableViewModel.Row(
                    title: "loan_close_confirm_amount_title".localized(), info: self.loanModel.detail?.LNPAYOFF_AMT.toAmountWithCurrencySymbol, infoProperty: .DefaultAmount
                ),
            ]
        )
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Confirm)
        title = "transaction_confirm_button_title".localized()
        model.sections = [self.loanSection, self.remitterSection, self.amountSection]
        tableView.reloadData()
        onContinue = {
            self.handleContinue()
        }
    }

    private func handleContinue() {
        let loaderViewController = TransactionLoadingController(nibName: "TransactionLoadingController", bundle: nil)
        navigationController?.pushViewController(loaderViewController, animated: true)
        let detailViewController = TransactionDetailViewController(nibName: "TableViewController", bundle: nil)
        detailViewController.model.sections = [self.loanSection, self.remitterSection, self.amountSection]
        detailViewController.controllerTitle = "loan_close_title".localized()
        detailViewController.onFinish = {
            self.navigationController?.popToRootViewController(animated: true)
            if detailViewController.isSuccessful {
                NotificationCenter.default.post(name: Notification.Name(NotificationConstants.REFRESH_ACCOUNTS), object: nil)
            }
        }
        ConnectionFactory.closeLoan(
            model: self.loanModel,
            success: { response in
                let resultMessage = response.getMessage()
                detailViewController.transactionMessage = response.close?.LN_CLS_RESULT_MSG ?? ""
                loaderViewController.setResult(
                    isSuccessful: true,
                    message: resultMessage,
                    completion: {
                        detailViewController.isSuccessful = true
                        self.navigationController?.pushViewController(detailViewController, animated: false)
                    }
                )
            },
            failed: { reason in
                let resultMessage = reason?.MESSAGE_DESC ?? ""
                detailViewController.transactionMessage = resultMessage
                loaderViewController.setResult(isSuccessful: false, message: resultMessage, completion: {
                    detailViewController.isSuccessful = false
                    self.navigationController?.pushViewController(detailViewController, animated: false)
                })
            }
        )
    }
}
