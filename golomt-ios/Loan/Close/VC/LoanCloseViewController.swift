//
//  LoanCloseViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/2/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class LoanCloseViewController: TableViewController {
    var filteredList = [AccountResponse.Account]()
    var loanModel = LoanCloseModel()
    
    lazy var loanAccountField: DefaultAccountPickerCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAccountPickerCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAccountPickerCell
        guard let unwrappedCell = cell else {
            return DefaultAccountPickerCell()
        }
        unwrappedCell.onPickerClick = {
            self.showSenderAccountPicker(self.filteredList)
        }
        unwrappedCell.defaultNicknameText = "loan_close_account_to_close_title".localized()
        unwrappedCell.defaultNumberText = "loan_close_account_to_close_label".localized()
        self.onAccountSelect = { account in
            self.handleAccountSelect(account)
        }
        self.accountPickerTitle = "loan_close_account_to_close_title".localized()
        return unwrappedCell
    }()
    
    lazy var remitterAccountField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerPlainCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultPickerButtonCell else {
            return DefaultPickerButtonCell()
        }
        cell.title = "loan_close_remitter_title".localized()
        cell.onPickerClick = {
            let controller = AccountPickerController()
            controller.onAccountSelect = { account in
                cell.selectedItem = account
            }
            controller.selectedAccount = self.loanModel.remitterAccount
            controller.title = cell.title
            controller.accounts = operativeAccountList
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var loanSection: TableViewModel.Section = {
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(
                    cell: self.loanAccountField
                ),
                TableViewModel.Row(
                    title: "loan_close_amount_title".localized(), infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "loan_close_has_collateral_title".localized()
                ),
                TableViewModel.Row(
                    cell: self.remitterAccountField
                )
            ]
        )
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Continue)
        title = "loan_close_title".localized()
        model.sections = [self.loanSection]
        tableView.reloadData()
        onContinue = {
            self.handleContinue()
        }
        self.fetchAccountProducts()
    }
    
    private func handleAccountSelect(_ account: AccountResponse.Account) {
        self.loanAccountField.account = account
        self.selectedAccount = account
        self.showLoader()
        ConnectionFactory.fetchLoanDetail(
            account: account,
            success: { response in
                self.hideLoader()
                let amount = response.detail?.LNPAYOFF_AMT
                self.loanSection.rows[1].info = amount.toAmountWithCurrencySymbol
                self.loanSection.rows[2].info = response.detail?.LNDEPCOLL_GLAG.toFullAnswer
                self.loanModel.loanAccount = account
                self.loanModel.detail = response.detail
                self.tableView.reloadData()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
    
    private func fetchAccountProducts() {
        self.showLoader()
        ConnectionFactory.fetchParameterValue(
            parameterName: "LCA_PRODUCT_LIST",
            success: { response in
                self.hideLoader()
                self.filteredList = []
                response.parameter?.P_VAL.orEmpty.components(separatedBy: "|").forEach { product in
                    self.filteredList.append(
                        contentsOf: loanAccountList.filter {
                            $0.PRODUCT_CATEGORY.orEmpty.contains(product)
                        }
                    )
                }
                self.loanAccountField.account = self.loanModel.loanAccount
                self.remitterAccountField.selectedItem = self.loanModel.remitterAccount
                if self.selectedAccount != nil {
                    self.handleAccountSelect(self.selectedAccount!)
                }
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
    
    private func handleContinue() {
        if self.loanAccountField.hasError() {
            showSenderAccountPicker(self.filteredList)
            return
        }

        if self.remitterAccountField.hasError() || self.loanModel.detail == nil {
            return
        }
        
        let confirmVC = LoanCloseConfirmViewController(nibName: "TableViewController", bundle: nil)
        loanModel.remitterAccount = self.remitterAccountField.selectedItem as? AccountResponse.Account
        confirmVC.loanModel = self.loanModel
        self.navigationController?.pushViewController(confirmVC, animated: true)
    }
}
