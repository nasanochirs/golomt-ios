//
//  LoanMenuViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/30/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class LoanMenuViewController: MenuTableViewController {
    lazy var detailSection: MenuTableViewModel.Section = {
        var section = MenuTableViewModel.Section(
            rows: [
                MenuTableViewModel.Row(
                    icon: UIImage(named: "loan"), title: "loan_digital_title"
                ),
                MenuTableViewModel.Row(
                    icon: UIImage(named: "loan"), title: "loan_collateral_title"
                ),
                MenuTableViewModel.Row(
                    icon: UIImage(named: "loan"), title: "loan_payment_title"
                ),
                MenuTableViewModel.Row(
                    icon: UIImage(named: "loan"), title: "loan_close_title"
                ),
                MenuTableViewModel.Row(
                    icon: UIImage(named: "loan"), title: "loan_graphic_title"
                )
            ]
        )
        return section
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "loan_title".localized()
        model.sections = [detailSection]
        tableView.reloadData()

        onClick = { row in
            switch row {
                case 0:
                    let digitalVC = LoanDigitalViewController(nibName: "TableViewController", bundle: nil)
                    self.navigationController?.pushViewController(digitalVC, animated: true)
                case 1:
                    let collateralVC = LoanCollateralViewController(nibName: "TableViewController", bundle: nil)
                    self.navigationController?.pushViewController(collateralVC, animated: true)
                case 2:
                    let transactionVC = LoanTransactionViewController(nibName: "TableViewController", bundle: nil)
                    self.navigationController?.pushViewController(transactionVC, animated: true)
                case 3:
                    let closeVC = LoanCloseViewController(nibName: "TableViewController", bundle: nil)
                    self.navigationController?.pushViewController(closeVC, animated: true)
                case 4:
                    let controller = AccountPickerController()
                    controller.accounts = loanAccountList
                    controller.title = "loan_graphic_account_choose_title".localized()
                    controller.onAccountSelect = { account in
                        let statementStoryBoard: UIStoryboard = UIStoryboard(name: "Loan", bundle: nil)
                        let viewController = statementStoryBoard.instantiateViewController(withIdentifier: "LoanGraphicID")
                        guard let graphicVC = viewController as? LoanGraphicViewController else {
                            return
                        }
                        graphicVC.account = account
                        self.navigationController?.pushViewController(graphicVC, animated: true)
                    }
                    let popupController = STPopupController(rootViewController: controller)
                    popupController.style = .bottomSheet
                    popupController.present(in: self)
                default:
                    break
            }
        }
    }
}
