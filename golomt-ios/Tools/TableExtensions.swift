//
//  TableExtensions.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/9/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    func hideScrollIndicators() {
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false
    }

    func registerCell(nibName: String) {
        self.register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: nibName)
    }
}

extension UICollectionView {
    func registerCell(nibName: String) {
        self.register(UINib(nibName: nibName, bundle: nil), forCellWithReuseIdentifier: nibName)
    }
}
