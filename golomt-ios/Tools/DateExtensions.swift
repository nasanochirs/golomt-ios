//
//  DateExtensions.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/27/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

extension Date {
    func getMonth() -> Int {
        let calendar = Calendar.current
        let month = calendar.component(.month, from: self)
        return month
    }
    
    func getYear() -> Int {
        let calendar = Calendar.current
        let year = calendar.component(.year, from: self)
        return year
    }
    
    var toFormatted: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.locale = Locale(identifier: getLanguage())
        formatter.timeZone = .current
        return formatter.string(from: self)
    }
    
    var toDateMonthFormatted: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd"
        formatter.locale = Locale(identifier: getLanguage())
        return formatter.string(from: self)
    }
    
    var toSlashFormatted: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        formatter.locale = Locale(identifier: getLanguage())
        return formatter.string(from: self)
    }
    
    func getMonthLabel() -> String {
        let month = self.getMonth()
        switch getLanguage() {
        case "en":
            let monthName = DateFormatter().monthSymbols[month - 1]
            return monthName
        case "mn":
            let monthName = "statement_month_picker_mn_month".localized(with: month)
            return monthName
        default:
            return ""
        }
    }
    
    func getMonthYearLabel() -> String {
        let month = self.getMonth()
        let year = self.getYear()
        switch getLanguage() {
        case "en":
            let monthName = DateFormatter().monthSymbols[month - 1]
            return "statement_month_picker_row_title".localized(with: monthName, year)
        case "mn":
            let monthName = "statement_month_picker_mn_month".localized(with: month)
            return "statement_month_picker_row_title".localized(with: year, monthName)
        default:
            return ""
        }
    }
    
    var startOfMonth: Date {
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.year, .month], from: self)

        return calendar.date(from: components)!
    }

    var endOfMonth: Date {
        var components = DateComponents()
        components.month = 1
        components.second = -1
        return Calendar(identifier: .gregorian).date(byAdding: components, to: startOfMonth)!
    }
}

extension String {
    var toBackwardDate: Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        return formatter.date(from: self)
    }
    
    var backwardDateFormatted: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let date = formatter.date(from: self)
        guard let unwrappedDate = date else {
            return ""
        }
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: unwrappedDate)
    }
    
    var backwardTimeFormatted: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let date = formatter.date(from: self)
        guard let unwrappedDate = date else {
            return ""
        }
        formatter.dateFormat = "HH:mm:ss"
        return formatter.string(from: unwrappedDate)
    }
    
    var backwardDateTimeFormatted: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let date = formatter.date(from: self)
        guard let unwrappedDate = date else {
            return ""
        }
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        return formatter.string(from: unwrappedDate)
    }
    
    var toDate: Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.date(from: self)
    }
    
    var dateFormatted: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let date = self.toDate else {
            return ""
        }
        return formatter.string(from: date)
    }
}

extension Optional where Wrapped == String {
    var backwardDateFormatted: String {
        return self.orEmpty.backwardDateFormatted
    }
    
    var backwardTimeFormatted: String {
        return self.orEmpty.backwardTimeFormatted
    }
    
    var dateFormatted: String {
        return self.orEmpty.dateFormatted
    }
}
