//
//  Extensions.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 2/21/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

extension Array where Element: Hashable {
  func uniqueValues() -> [Element] {
    return Array(Set(self))
  }
}

extension UILabel {
  func useXSmallFont() {
    self.font = .systemFont(ofSize: 11)
  }

  func useSmallFont() {
    self.font = .systemFont(ofSize: 13)
  }

  func useMediumFont() {
    self.font = .systemFont(ofSize: 15)
  }

  func useLargeFont() {
    self.font = .systemFont(ofSize: 17)
  }

  func useXLargeFont() {
    self.font = .systemFont(ofSize: 19)
  }

  func useXXLargeFont() {
    self.font = .systemFont(ofSize: 21)
  }

  func useHeaderFont() {
    self.font = .boldSystemFont(ofSize: 34)
  }

  func makeBold() {
    self.font = .boldSystemFont(ofSize: self.font.pointSize)
  }
}

extension UITextField {
  func useSmallFont() {
    self.font = .systemFont(ofSize: 13)
  }

  func useMediumFont() {
    self.font = .systemFont(ofSize: 15)
  }

  func useLargeFont() {
    self.font = .systemFont(ofSize: 17)
  }

  func useXLargeFont() {
    self.font = .systemFont(ofSize: 19)
  }

  func makeBold() {
    self.font = .boldSystemFont(ofSize: self.font?.pointSize ?? 15)
  }
}

extension UIView {
  var safeTopAnchor: NSLayoutYAxisAnchor {
    if #available(iOS 11.0, *) {
      return self.safeAreaLayoutGuide.topAnchor
    }
    return self.topAnchor
  }

  var safeLeftAnchor: NSLayoutXAxisAnchor {
    if #available(iOS 11.0, *) {
      return self.safeAreaLayoutGuide.leftAnchor
    }
    return self.leftAnchor
  }

  var safeRightAnchor: NSLayoutXAxisAnchor {
    if #available(iOS 11.0, *) {
      return self.safeAreaLayoutGuide.rightAnchor
    }
    return self.rightAnchor
  }

  var safeBottomAnchor: NSLayoutYAxisAnchor {
    if #available(iOS 11.0, *) {
      return self.safeAreaLayoutGuide.bottomAnchor
    }
    return self.bottomAnchor
  }

  func addSubView(view: UIView, left: CGFloat, top: CGFloat, right: CGFloat, bottom: CGFloat) {
    view.translatesAutoresizingMaskIntoConstraints = false
    self.addSubview(view)

    view.leftAnchor.constraint(equalTo: self.leftAnchor, constant: left).isActive = true
    view.topAnchor.constraint(equalTo: self.topAnchor, constant: top).isActive = true
    view.rightAnchor.constraint(equalTo: self.rightAnchor, constant: right).isActive = true
    view.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: bottom).isActive = true
  }

  func setDefaultPurpleGradient() {
    self.setGradient(startColor: UIColor.defaultPurpleGradientStart.cgColor, endColor: UIColor.defaultPurpleGradientEnd.cgColor)
  }

  func setDefaultBlueGradient() {
    self.setGradient(startColor: UIColor.defaultBlueGradientStart.cgColor, endColor: UIColor.defaultBlueGradientEnd.cgColor)
  }

  func setGradient(startColor: CGColor, endColor: CGColor) {
    self.layer.sublayers?.forEach { layer in
      if layer is CAGradientLayer {
        layer.removeFromSuperlayer()
      }
    }
    let gradientLayer = CAGradientLayer()
    gradientLayer.colors = [startColor, endColor]
    gradientLayer.startPoint = CGPoint(x: 0, y: 0)
    gradientLayer.endPoint = CGPoint(x: 1, y: 0)
    gradientLayer.frame = self.bounds
    gradientLayer.locations = [0.0, 1.0]
    self.clipsToBounds = true
    self.layer.insertSublayer(gradientLayer, at: 0)
  }
}

extension Int {
  var toString: String {
    return String(self)
  }
}

extension UILabel {
  func allCaps() {
    self.text = self.text?.uppercased()
  }

  func wordWrap() {
    self.numberOfLines = 0
    self.lineBreakMode = .byWordWrapping
  }

  func getSize() -> CGSize {
    return systemLayoutSizeFitting(CGSize(width: bounds.size.width, height: UIView.layoutFittingCompressedSize.height), withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel)
  }
}

extension Array {
  func chunked(into size: Int) -> [[Element]] {
    return stride(from: 0, to: count, by: size).map {
      Array(self[$0 ..< Swift.min($0 + size, count)])
    }
  }
}

extension Hashable where Self: AnyObject {
  func hash(into hasher: inout Hasher) {
    hasher.combine(ObjectIdentifier(self))
  }
}

extension UIView {
  func addTapGesture(tapNumber: Int, target: Any, action: Selector) {
    let tap = UITapGestureRecognizer(target: target, action: action)
    tap.numberOfTapsRequired = tapNumber
    addGestureRecognizer(tap)
    isUserInteractionEnabled = true
  }
}

extension UIView {
  enum ViewSide {
    case Left, Right, Top, Bottom
  }

  func corner(cornerRadius: CGFloat = 0) {
    self.layer.cornerRadius = cornerRadius
    self.layer.masksToBounds = true
  }

  func sideCorner(cornerRadius: CGFloat = 0, side: ViewSide) {
    self.clipsToBounds = true
    self.layer.cornerRadius = cornerRadius
    switch side {
    case ViewSide.Left:
      self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
    case ViewSide.Right:
      self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
    case ViewSide.Top:
      self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    case ViewSide.Bottom:
      self.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
  }
}

extension Encodable {
  var toJsonString: String {
    let jsonEncoder = JSONEncoder()
    jsonEncoder.outputFormatting = .prettyPrinted
    guard let jsonData = try? jsonEncoder.encode(self) else { return "" }
    return String(data: jsonData, encoding: .utf8)!
  }
}

extension Bundle {
  private static var bundle: Bundle!

  public static func localizedBundle() -> Bundle! {
    if self.bundle == nil {
      let path = Bundle.main.path(forResource: getLanguage(), ofType: "lproj")
      self.bundle = Bundle(path: path!)
    }
    return self.bundle
  }

  public static func setBundleLanguage(language: String) {
    setLanguage(language: language)
    let path = Bundle.main.path(forResource: language, ofType: "lproj")
    self.bundle = Bundle(path: path!)
  }
}

extension String {
  subscript(_ i: Int) -> String {
    let idx1 = index(startIndex, offsetBy: i)
    let idx2 = index(idx1, offsetBy: 1)
    return String(self[idx1 ..< idx2])
  }

  subscript(r: Range<Int>) -> String {
    let start = index(startIndex, offsetBy: r.lowerBound)
    let end = index(startIndex, offsetBy: r.upperBound)
    return String(self[start ..< end])
  }

  subscript(r: CountableClosedRange<Int>) -> String {
    let startIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
    let endIndex = self.index(startIndex, offsetBy: r.upperBound - r.lowerBound)
    return String(self[startIndex ... endIndex])
  }

  var toDigits: String {
    return components(separatedBy: CharacterSet.decimalDigits.inverted)
      .joined()
  }

  func trim() -> String {
    return self.trimmingCharacters(in: .whitespaces)
  }

  func localized() -> String {
    #if TARGET_INTERFACE_BUILDER
      return NSLocalizedString(self, tableName: nil, bundle: .main, value: "", comment: "")
    #else
      return NSLocalizedString(self, tableName: nil, bundle: .localizedBundle(), value: "", comment: "")
    #endif
  }

  func localized(with arguments: CVarArg...) -> String {
    return String(format: self.localized(), locale: nil, arguments: arguments)
  }

  var toFlag: String {
    let base: UInt32 = 127397
    var flagString = ""
    for v in String(self.prefix(2)).uppercased().unicodeScalars {
      flagString.unicodeScalars.append(UnicodeScalar(base + v.value)!)
    }
    return flagString
  }

  var toInt: Int {
    return Int(self) ?? 0
  }

  var toAmountFormatted: String {
    let amountString = self.replacingOccurrences(of: ",", with: "")
    let amount = amountString.toDouble
    let numberFormatter = NumberFormatter()
    numberFormatter.formatterBehavior = .behavior10_4
    numberFormatter.numberStyle = .decimal
    numberFormatter.roundingMode = .down
    numberFormatter.decimalSeparator = "."
    numberFormatter.groupingSeparator = ","
    numberFormatter.usesGroupingSeparator = true
    return numberFormatter.string(from: NSNumber(value: amount)).orEmpty
  }

  var isZero: Bool {
    let formatted = self.replacingOccurrences(of: ",", with: "")
    let amount = formatted.toDouble
    switch true {
    case amount > 0:
      return false
    default:
      return true
    }
  }
}

extension Optional where Wrapped == String {
  var isZero: Bool {
    return self.orZero.isZero
  }

  var orEmpty: String {
    switch self {
    case let .some(value):
      return String(describing: value)
    case _:
      return ""
    }
  }

  var toInt: Int {
    switch self {
    case let .some(value):
      return String(describing: value).toInt
    case _:
      return 0
    }
  }

  var toFullAnswer: String {
    switch self?.lowercased() {
    case "y", "Y":
      return "yes_label".localized()
    case "n", "N":
      return "no_label".localized()
    default:
      return ""
    }
  }
}
