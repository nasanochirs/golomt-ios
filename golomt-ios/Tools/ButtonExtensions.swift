//
//  ButtonExtensions.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/27/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    enum ViewSide {
        case Left, Right, Top, Bottom
    }

//    func setDefaultPurpleGradient() {
//        self.setGradient(startColor: UIColor.defaultPurpleGradientStart.cgColor, endColor: UIColor.defaultPurpleGradientEnd.cgColor)
//    }
//
//    func setDefaultBlueGradient() {
//        self.setGradient(startColor: UIColor.defaultBlueGradientStart.cgColor, endColor: UIColor.defaultBlueGradientEnd.cgColor)
//    }
//
//    func setGradient(startColor: CGColor, endColor: CGColor) {
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.colors = [startColor, endColor]
//        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
//        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
//        gradientLayer.frame = self.bounds
//        gradientLayer.locations = [0.0, 1.0]
//        self.clipsToBounds = true
//        self.layer.insertSublayer(gradientLayer, at: 0)
//    }

    func wordWrapLabel() {
        self.titleLabel?.numberOfLines = 0
        self.titleLabel?.lineBreakMode = .byWordWrapping
        self.titleLabel?.textAlignment = .center
    }

    func addBorder(toSide side: ViewSide, withColor color: UIColor, andThickness thickness: CGFloat) {
        if let previousView = viewWithTag(side.hashValue) {
            previousView.backgroundColor = color
            return
        }
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.autoresizingMask = [.flexibleTopMargin, .flexibleWidth]
        lineView.tag = side.hashValue
        switch side {
        case .Left: lineView.frame = CGRect(x: frame.minX, y: frame.minY, width: thickness, height: frame.height)
        case .Right: lineView.frame = CGRect(x: frame.maxX, y: frame.minY, width: thickness, height: frame.height)
        case .Top: lineView.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: thickness)
        case .Bottom: lineView.frame = CGRect(x: frame.minX, y: frame.maxY, width: frame.width, height: thickness)
        }
        addSubview(lineView)
    }

    func setRightImage(name: String?, tint: UIColor) {
        guard let name = name else {
            self.setImage(nil, for: .normal)
            return
        }
        let image = UIImage(named: name)
        self.setImage(image, for: .normal)
        self.tintColor = tint
        self.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        self.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        self.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        self.imageEdgeInsets.left = -10
        self.imageEdgeInsets.right = 10
        if let imageView = self.imageView {
            self.bringSubviewToFront(imageView)
        }
    }

    func setLeftImage(name: String?) {
        guard let name = name else {
            self.setImage(nil, for: .normal)
            return
        }
        let image = UIImage(named: name)
        self.setImage(image, for: .normal)
        self.tintColor = .white
        self.imageEdgeInsets.right = 10
        self.imageEdgeInsets.left = -10
        if let imageView = self.imageView {
            self.bringSubviewToFront(imageView)
        }
    }
}
