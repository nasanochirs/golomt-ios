//
//  Constants.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/6/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

@objc class UserPreferencesConstants: NSObject {
    static let imageKey = "golomt_image_key"
    static let balanceKey = "golomt_balance_key"
    static let languageKey = "golomt_language"
    static let publicKey = "golomt_public_key"
    static let usernameKey = "golomt_username"
    static let loginNameKey = "golomt_login_name"
    static let locationUrl = "https://e.golomtbank.com/api/location/"
    static let rateUrl = "https://egolomt.mn/corp/RateInfoServlet"
    static let imageApiUrl = "https://pfm.golomtbank.com:8443/smartapi/image/"
    static let financesApiUrl = "https://pfm.golomtbank.com:8443/smartapi/pfm/v1/"
    static let productApiUrl = "https://p2p.golomtbank.com/ImageUploadServerApp/img/product/"
    static let locationKey = "golomt_location_key"

//    // PROD
//    static let apiUrl = "https://www.egolomt.mn/corp/XService"
//    static let xDestUrl = ""
//    static let userAgent = "SmartBank42"
    // UAT
    static let apiUrl = "http://103.51.60.125:8580/corp/XService"
    static let xDestUrl = "http://192.168.1.127:8080/z-http-cache/repo/api"
    static let userAgent = "Iphone"

    @objc static func y8w6oh(l: [UInt8]) -> [UInt8] {
        var g2nj4m = [Int]()
        var lo6h3u = [Int]()
        var k6v67f = l
        let hihyqf = Bundle.main.object(forInfoDictionaryKey: "0ri3te") as? String
        for (index, element) in hihyqf.orEmpty.components(separatedBy: ",").enumerated() {
            if index % 2 == 0 {
                guard let vo7xn2 = Int(element) else {
                    continue
                }
                g2nj4m.append(vo7xn2)
            } else {
                if lo6h3u.count < 4 {
                    guard let evef89 = Int(element) else {
                        continue
                    }
                    lo6h3u.append(evef89)
                }
            }
        }

        for i in stride(from: 0, to: g2nj4m.count, by: 2) {
            let temp = k6v67f[g2nj4m[i]]
            k6v67f[g2nj4m[i]] = k6v67f[g2nj4m[i + 1]] ^ 0x67
            k6v67f[g2nj4m[i + 1]] = temp ^ 0xf4
        }
        var fkgoer = [UInt8]()
        for (index, element) in k6v67f.enumerated() {
            fkgoer.append(element ^ UInt8(lo6h3u[index % 4]) ^ 0xc6 ^ UInt8(index))
        }
        return fkgoer
    }
}

struct AccountConstants {
    static let operative = "OPR"
    static let credit = "CCD"
    static let deposit = "DEP"
    static let loan = "LON"
}

struct LoginConstants {
    static let etoken = "SECD"
}

enum TransactionNetworkConstants: String {
    case golomt = "WIB"
    case bank = "OUB"
    case swift = "SWI"
}

enum TransactionTypeConstants: String {
    case own = "XFR"
    case creditCard = "CCP"
    case loan = "LAP"
    case golomt = "INB"
    case bank = "OSB"
    case swift = "SWI"
}

struct StatementConstants {
    static let income = "04"
    static let expense = "05"
}

enum CurrencyConstants: String {
    case MNT
    case USD
    case CNY
    case JPY
    case EUR
    case GBP
    case CHF
    case RUB
    case KRW
    case AUD
    case HKD
    case SEK
    case SGD
    case CAD
}

struct NotificationConstants {
    static let REFRESH_ACCOUNTS = "REFRESH_ACCOUNTS"
    static let REFRESH_BILLS = "REFRESH_BILLS"
    static let BILLS_REFRESHED = "BILLS_REFRESHED"
    static let REFRESH_CARD_LIST = "REFRESH_CARD_LIST"
    static let REFRESH_PACKET_LIST = "REFRESH_PACKET_LIST"
    static let SESSION_DESTROYED = "SESSION_DESTROYED"
    static let SESSION_EXPIRED = "SESSION_EXPIRED"
    static let REFRESH_PROFILE = "REFRESH_PROFILE"
    static let LOGOUT = "LOGOUT"
}

struct ComponentConstants {
    static let INFO_TAG = 100
}
