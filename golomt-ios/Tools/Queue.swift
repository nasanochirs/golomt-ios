//
//  CustomObjects.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/26/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

struct Queue {
    var items: [String] = []

    mutating func enqueue(element: String) {
        items.append(element)
    }

    mutating func dequeue() -> String? {
        if items.isEmpty {
            return nil
        }
        else {
            let tempElement = items.first
            items.remove(at: 0)
            return tempElement
        }
    }
}

func initAccountQueue() -> Queue {
    var queue = Queue()
    queue.enqueue(element: AccountConstants.operative)
    queue.enqueue(element: AccountConstants.credit)
    queue.enqueue(element: AccountConstants.deposit)
    queue.enqueue(element: AccountConstants.loan)
    return queue
}

func initAccountBookQueue() -> Queue {
    var queue = Queue()
    queue.enqueue(element: TransactionNetworkConstants.golomt.rawValue)
    queue.enqueue(element: TransactionNetworkConstants.bank.rawValue)
    return queue
}
