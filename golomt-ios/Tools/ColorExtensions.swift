//
//  ColorExtensions.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/25/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(hex: String) {
        var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }

        if cString.count != 6 {
            self.init(red: 255, green: 255, blue: 255, alpha: CGFloat(1.0))
        }

        var rgbValue: UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    static var defaultPrimaryText: UIColor {
        #if TARGET_INTERFACE_BUILDER
        return .black
        #else
        if #available(iOS 13, *) {
            return UIColor { (traitCollection: UITraitCollection) -> UIColor in
                if traitCollection.userInterfaceStyle == .dark {
                    return UIColor(hex: "FFFFFF")
                } else {
                    return UIColor(hex: "243B53")
                }
            }
        } else {
            return UIColor(hex: "243B53")
        }
        #endif
    }

    static var defaultSecondaryText: UIColor {
        if #available(iOS 13, *) {
            return UIColor { (traitCollection: UITraitCollection) -> UIColor in
                if traitCollection.userInterfaceStyle == .dark {
                    return UIColor(hex: "D5D5D5")
                } else {
                    return UIColor(hex: "9AA5B1")
                }
            }
        } else {
            return UIColor(hex: "9AA5B1")
        }
    }

    static var defaultHeader: UIColor {
        #if TARGET_INTERFACE_BUILDER
        return .defaultBlueGradientStart
        #else
        if #available(iOS 13, *) {
            return UIColor { (traitCollection: UITraitCollection) -> UIColor in
                if traitCollection.userInterfaceStyle == .dark {
                    return UIColor(hex: "000000")
                } else {
                    return UIColor(patternImage: UIImage(named: "header")!)
                }
            }
        } else {
            return UIColor(patternImage: UIImage(named: "header")!)
        }
        #endif
    }

    static var defaultComponentSubInfo: UIColor {
        if #available(iOS 13, *) {
            return UIColor { (traitCollection: UITraitCollection) -> UIColor in
                if traitCollection.userInterfaceStyle == .dark {
                    return UIColor(hex: "999999")
                } else {
                    return UIColor(hex: "EEEEEE")
                }
            }
        } else {
            return UIColor(hex: "EEEEEE")
        }
    }

    static var defaultError: UIColor {
        if #available(iOS 13, *) {
            return UIColor { (traitCollection: UITraitCollection) -> UIColor in
                if traitCollection.userInterfaceStyle == .dark {
                    return UIColor(hex: "f62a4c")
                } else {
                    return UIColor(hex: "F32E50")
                }
            }
        } else {
            return UIColor(hex: "F32E50")
        }
    }

    static var defaultWarning: UIColor {
        if #available(iOS 13, *) {
            return UIColor { (traitCollection: UITraitCollection) -> UIColor in
                if traitCollection.userInterfaceStyle == .dark {
                    return UIColor(hex: "fba409")
                } else {
                    return UIColor(hex: "f0bc27")
                }
            }
        } else {
            return UIColor(hex: "f0bc27")
        }
    }

    static var defaultPurpleGradientStart: UIColor {
        return UIColor(hex: "a234ff")
    }

    static var defaultPurpleGradientEnd: UIColor {
        return UIColor(hex: "6773fa")
    }

    static var defaultBlueGradientStart: UIColor {
        return UIColor(hex: "000c4a")
    }

    static var defaultBlueGradientEnd: UIColor {
        return UIColor(hex: "4570b2")
    }

    static var defaultGreen: UIColor {
        return UIColor(hex: "93D847")
    }

    static var defaultRed: UIColor {
        return UIColor(hex: "DE2900")
    }

    static var defaultPrimaryBackground: UIColor {
        #if TARGET_INTERFACE_BUILDER
        return .gray
        #else
        guard let color = UIColor(named: "defaultPrimaryBackgroundColor") else {
            return UIColor(hex: "F1F1F3")
        }
        return color
        #endif
    }

    static var defaultSecondaryBackground: UIColor {
        #if TARGET_INTERFACE_BUILDER
        return .white
        #else
        guard let color = UIColor(named: "defaultSecondaryBackgroundColor") else {
            return UIColor(hex: "FFFFFF")
        }
        return color
        #endif
    }

    static var defaultTertiaryBackground: UIColor {
        if #available(iOS 13, *) {
            return UIColor { (traitCollection: UITraitCollection) -> UIColor in
                if traitCollection.userInterfaceStyle == .dark {
                    return UIColor(hex: "6C6C71")
                } else {
                    return UIColor(hex: "E2E2E4")
                }
            }
        } else {
            return UIColor(hex: "E2E2E4")
        }
    }
    
    static var defaultSelectionBackground: UIColor {
        if #available(iOS 13, *) {
            return UIColor { (traitCollection: UITraitCollection) -> UIColor in
                if traitCollection.userInterfaceStyle == .dark {
                    return UIColor(hex: "6C6C71")
                } else {
                    return .defaultBlueGradientEnd
                }
            }
        } else {
            return .defaultBlueGradientEnd
        }
    }

    static var defaultSeparator: UIColor {
        if #available(iOS 13, *) {
            return UIColor { (traitCollection: UITraitCollection) -> UIColor in
                if traitCollection.userInterfaceStyle == .dark {
                    return UIColor(hex: "8b8b8b")
                } else {
                    return UIColor(hex: "707070")
                }
            }
        } else {
            return UIColor(hex: "707070")
        }
    }

    static var operativeAccount: UIColor {
        return UIColor(hex: "3c5ea2")
    }

    static var depositAccount: UIColor {
        return UIColor(hex: "7bb811")
    }

    static var creditAccount: UIColor {
        return UIColor(hex: "f2c529")
    }

    static var loanAccount: UIColor {
        return UIColor(hex: "6773fa")
    }
}
