//
//  AmountExtensions.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/27/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

extension Double {
    var formattedWithComma: String {
        return Formatter.withComma.string(for: self).orZero
    }
}

extension String {
    var toAmount: Double {
        let amountArray = self.components(separatedBy: "|")
        let amount = amountArray.last
        return amount.orEmpty.toDouble
    }

    var toAmountWithCurrency: String {
        let amountArray = self.components(separatedBy: "|")
        let currency = amountArray.first
        let amount = amountArray.last
        return String(format: "%@ %@", amount.orEmpty.formattedWithComma, currency.orEmpty)
    }

    var toAmountWithCurrencySymbol: String {
        let amountArray = self.components(separatedBy: "|")
        let currency = amountArray.first
        let amount = amountArray.last
        return String(format: "%@ %@", amount.orEmpty.formattedWithComma, currency.orEmpty.toCurrencySymbol)
    }

    var toAmountTest: NSMutableAttributedString {
        let amountArray = self.components(separatedBy: "|")
        let currency = amountArray.first
        let amount = amountArray.last
        let text = NSMutableAttributedString()
        text.append(amount.orEmpty.formattedWithComma2)
        text.append(NSAttributedString(string: " "))
        text.append(NSAttributedString(string: currency.orEmpty.toCurrencySymbol))
        return text
    }
    
    var toCurrency: String {
        let amountArray = self.components(separatedBy: "|")
        let currency = amountArray.first
        return currency.orEmpty
    }

    var toCurrencySymbol: String {
        switch self {
        case CurrencyConstants.MNT.rawValue: return "₮"
        case CurrencyConstants.USD.rawValue: return "$"
        case CurrencyConstants.CNY.rawValue: return "￥"
        case CurrencyConstants.JPY.rawValue: return "￥"
        case CurrencyConstants.EUR.rawValue: return "€"
        case CurrencyConstants.GBP.rawValue: return "₮"
        case CurrencyConstants.CHF.rawValue: return "CHF"
        case CurrencyConstants.RUB.rawValue: return "₽"
        case CurrencyConstants.KRW.rawValue: return "₩"
        case CurrencyConstants.AUD.rawValue: return "A$"
        case CurrencyConstants.HKD.rawValue: return "H$"
        case CurrencyConstants.SEK.rawValue: return "kr"
        case CurrencyConstants.SGD.rawValue: return "S$"
        case CurrencyConstants.CAD.rawValue: return "C$"
        default: return ""
        }
    }

    var toDouble: Double {
        let amount = self.replacingOccurrences(of: ",", with: "")
        return Double(amount).orZero
    }

    var formattedWithComma: String {
        let doubleValue = self.toDouble
        return Formatter.withComma.string(for: doubleValue).orZero
    }
    
    var formattedWithComma2: NSMutableAttributedString {
        let text = NSMutableAttributedString()
        let doubleValue = self.toDouble
        let amount = Formatter.withComma.string(for: doubleValue).orZero
        let splittedAmount = amount.components(separatedBy: ".")
        text.append(NSAttributedString(string: splittedAmount.first.orEmpty, attributes: [NSAttributedString.Key.foregroundColor: UIColor.defaultPrimaryText]))
        if splittedAmount.count > 1 {
            text.append(NSAttributedString(string: "."))
            text.append(NSAttributedString(string: splittedAmount.last.orEmpty, attributes: [NSAttributedString.Key.foregroundColor: UIColor.defaultPrimaryText.withAlphaComponent(0.5)]))
        }
        return text
    }
    
    var formattedWithoutComma: String {
        let doubleValue = self.toDouble
        return Formatter.withoutComma.string(for: doubleValue).orZero
    }
}

extension Formatter {
    static let withComma: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        return formatter
    }()
    
    static let withoutComma: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ""
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        return formatter
    }()
}

extension Optional where Wrapped == String {
    var toAmountWithCurrency: String {
        return self.orEmpty.toAmountWithCurrency
    }

    var toAmountWithCurrencySymbol: String {
        return self.orEmpty.toAmountWithCurrencySymbol
    }

    var toCurrency: String {
        return self.orEmpty.toCurrency
    }

    var orZero: String {
        switch self {
        case let .some(value):
            return String(describing: value)
        case _:
            return "0.00"
        }
    }

    var toDouble: Double {
        let amount = self.orEmpty.replacingOccurrences(of: ",", with: "")
        return Double(amount).orZero
    }

    var toAmount: Double {
        return self.orEmpty.toAmount
    }
}

extension Optional where Wrapped == Double {
    var orZero: Double {
        switch self {
        case let .some(value):
            return value
        case _:
            return 0.00
        }
    }
}

