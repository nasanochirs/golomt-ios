//
//  LocalAuthManager.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 2/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import LocalAuthentication

public class LocalAuthManager: NSObject {
    public static let shared = LocalAuthManager()
    private var context = LAContext()
    private var error: NSError?

    enum BiometricType: String {
        case none
        case touchID
        case faceID
    }

    private override init() {}

    // check type of local authentication device currently support
    var biometricType: BiometricType {
        context = LAContext()
        guard context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            return .none
        }
        guard context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) else {
               return .none
        }
        switch context.biometryType {
        case .none:
            return .none
        case .touchID:
            return .touchID
        case .faceID:
            return .faceID
        @unknown default:
            return .none
        }
    }

    func setupBiometric(reason: String, isSuccessful: @escaping ((Bool) -> Void)) {
        context = LAContext()
        context.localizedFallbackTitle = ""
        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason.localized()) { success, _ in
            if success {
                let contextKey = self.context.evaluatedPolicyDomainState?.base64EncodedString()
                setPublicKey(contextKey)
                isSuccessful(true)
            } else {
                isSuccessful(false)
            }
        }
    }

    func checkBiometric(reason: String, isSuccessful: @escaping ((Bool) -> Void)) {
        context = LAContext()
        context.localizedFallbackTitle = ""
        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason.localized()) { success, _ in
            if success {
                isSuccessful(true)
            } else {
                isSuccessful(false)
            }
        }
    }

    func getPublicKey() -> String? {
        return context.evaluatedPolicyDomainState?.base64EncodedString()
    }
}
