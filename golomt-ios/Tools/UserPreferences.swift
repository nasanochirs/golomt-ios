//
//  UserDefaults.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/2/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

var formId = "0"
var userId = ""
var sessionId = ""
var profileName = ""
var loanAccountList = [AccountResponse.Account]()
var creditAccountList = [AccountResponse.Account]()
var operativeAccountList = [AccountResponse.Account]()
var depositAccountList = [AccountResponse.Account]()
var golomtAccountBookList = [AccountBookResponse.AccountBook]()
var bankAccountBookList = [AccountBookResponse.AccountBook]()
var billPaymentList = [PaymentResponse.Payment]()
var imageKey = ""
var imageBearerToken = ""
var financesBearerToken = ""
var userDetails: LoginResponse.UserDetails?
var incomeCategoryList = [FinancesCategoryResponse]()
var expenseCategoryList = [FinancesCategoryResponse]()
var transferCategoryList = [FinancesCategoryResponse]()
var categoryList = [FinancesCategoryResponse]()
var shortcutList = [LoginResponse.Shortcut]()

func isEToken() -> Bool {
  return userDetails?.IS_TOKEN == "ETOKEN"
}

func resetAll() {
    formId = "0"
    userId = ""
    sessionId = ""
    loanAccountList = []
    creditAccountList = []
    operativeAccountList = []
    depositAccountList = []
    golomtAccountBookList = []
    bankAccountBookList = []
    billPaymentList = []
    imageKey = ""
    imageBearerToken = ""
    userDetails = nil
}

func setProfileImage(_ image: Data?) {
    UserDefaults.standard.set(image, forKey: UserPreferencesConstants.imageKey)
}

func getProfileImage() -> Data? {
    UserDefaults.standard.data(forKey: UserPreferencesConstants.imageKey)
}

func setBalanceVisibility(_ visibility: Bool) {
    UserDefaults.standard.set(visibility, forKey: UserPreferencesConstants.balanceKey)
}

func getBalanceVisibility() -> Bool {
    UserDefaults.standard.bool(forKey: UserPreferencesConstants.balanceKey)
}

func setLanguage(language: String) {
    UserDefaults.standard.set(language, forKey: UserPreferencesConstants.languageKey)
}

func getLanguage() -> String {
    return UserDefaults.standard.string(forKey: UserPreferencesConstants.languageKey) ?? "mn"
}

func setLocation(location: [LocationResponse.LocationData.Location]) throws {
    let locations = try JSONEncoder().encode(location)
    UserDefaults.standard.set(locations, forKey: UserPreferencesConstants.locationKey)
}

func getLocation() throws -> [LocationResponse.LocationData.Location] {
    guard let locations = UserDefaults.standard.data(forKey: UserPreferencesConstants.locationKey) else { return [] }
    return try JSONDecoder().decode([LocationResponse.LocationData.Location].self, from: locations)
}

func getRequestUrl() -> String {
    return UserPreferencesConstants.apiUrl + ";jsessionid=" + sessionId
}

func getLocationUrl() -> String {
    return UserPreferencesConstants.locationUrl
}

func getRateUrl() -> String {
    return UserPreferencesConstants.rateUrl
}

func getImageRequestUrl() -> String {
    return UserPreferencesConstants.imageApiUrl
}

func getFinancesRequestUrl() -> String {
    return UserPreferencesConstants.financesApiUrl
}

func getProductRequestUrl() -> String {
    return UserPreferencesConstants.productApiUrl
}

func hasBiometric() -> Bool {
    let savedKey = getPublicKey()
    let authManager = LocalAuthManager.shared
    let localKey = authManager.getPublicKey()
    guard let _ = getUsername() else {
        return false
    }
    return localKey == savedKey
}

func setPublicKey(_ key: String?) {
    guard let key = key else {
        UserDefaults.standard.set(nil, forKey: UserPreferencesConstants.publicKey)
        return
    }
    UserDefaults.standard.set(key, forKey: UserPreferencesConstants.publicKey)
}

func getPublicKey() -> String {
    let key = UserDefaults.standard.string(forKey: UserPreferencesConstants.publicKey)
    return key.orEmpty
}

func setUsername(_ username: String) {
    UserDefaults.standard.set(username, forKey: UserPreferencesConstants.usernameKey)
}

func getUsername() -> String? {
    let username = UserDefaults.standard.string(forKey: UserPreferencesConstants.usernameKey)
    return username
}

func setLoginName(_ loginName: String) {
    UserDefaults.standard.set(loginName, forKey: UserPreferencesConstants.loginNameKey)
}

func getLoginName() -> String? {
    let loginName = UserDefaults.standard.string(forKey: UserPreferencesConstants.loginNameKey)
    return loginName
}

func setSession(response: LoginResponse) {
    formId = response.header?.SESSION?.FORM_ID ?? "0"
    userId = response.userDetails?.USER_ID ?? ""
    sessionId = response.header?.SESSION?.SESSION_ID ?? ""
    profileName = response.userDetails?.USER_FIRST_NAME ?? ""
    userDetails = response.userDetails
    shortcutList = response.shortcutList ?? []
}

func isIPhoneX() -> Bool {
    return UIScreen.main.bounds.height / UIScreen.main.bounds.width >= 812.0 / 375.0
}

func isIPhoneXS() -> Bool {
    return UIScreen.main.bounds.height / UIScreen.main.bounds.width >= 896.0 / 414.0
}

func isIPhoneSE() -> Bool {
    return UIScreen.main.bounds.height / UIScreen.main.bounds.width >= 667.0 / 375.0
}
