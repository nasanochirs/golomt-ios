//
//  StandingOrderTransactionViewController.swift
//  golomt-ios
//
//  Created by Khulan on 8/5/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class StandingOrderTransactionViewController: TableViewController {
    var standingOrderModel = StandingOrderModel()
    var selectedBeneficiaryAccount: AccountResponse.Account?
    var datePickerTransfer = UIDatePicker()
    var datePickerMature = UIDatePicker()
    var selectedAccountBook: AccountBookResponse.AccountBook?
    var accountList = [AccountResponse.Account]()

    lazy var senderAccountField: DefaultAccountPickerCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAccountPickerCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAccountPickerCell
        guard let unwrappedCell = cell else {
            return DefaultAccountPickerCell()
        }
        self.onAccountSelect = { account in
            self.senderAccountField.account = account
            self.selectedAccount = account
            self.beneficiaryAccountField.selectedItem = nil
            self.selectedBeneficiaryAccount = nil
            self.setCurrency()
        }
        unwrappedCell.onPickerClick = {
            self.showSenderAccountPicker()
        }
        return unwrappedCell
    }()

    lazy var beneficiaryAccountField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerButtonCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultPickerButtonCell
        guard let unwrappedCell = cell else {
            return DefaultPickerButtonCell()
        }
        unwrappedCell.title = "standing_order_transaction_beneficiary_title".localized()
        unwrappedCell.hasImage = false
        unwrappedCell.onPickerClick = {
            let controller = PickerController()
            controller.title = "Дансны төрөл".localized()
            controller.list = [(0, "standing_order_transaction_any_account_label".localized()), (1, "standing_order_transaction_own_account_label".localized()), (2, "standing_order_transaction_account_book_label".localized())]
            controller.onItemSelect = { type in
                controller.popupController?.dismiss()
                unwrappedCell.selectedItem = type
                let type = type as? (index: Int, text: String)
                switch type?.index {
                case 0:
                    self.beneficiaryAccountNumberField.hasImage = false
                    unwrappedCell.hasImage = false
                    self.beneficiaryAccountNumberField.isUserInteraction = true
                    self.beneficiaryAccountNumberField.focusTextField()
                case 1:
                    self.beneficiaryAccountNumberField.hasImage = true
                    unwrappedCell.hasImage = false
                    self.beneficiaryAccountNumberField.isUserInteraction = false
                    self.beneficiaryAccountNumberField.focusTextField()
                case 2:
                    unwrappedCell.hasImage = true
                    self.beneficiaryAccountNumberField.hasImage = false
                default:
                    break
                }
            }
            controller.selectedItem = self.beneficiaryAccountField.selectedItem
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onImageClick = {
            self.beneficiaryAccountNumberField.focusTextField()
            let controller = AccountBookPickerController()
            controller.accountBooks = golomtAccountBookList
            controller.onAccountBookSelect = { accountBook in
                self.beneficiaryAccountNumberField.setData(accountBook)
                self.selectedAccountBook = accountBook
                controller.popupController?.dismiss(completion: {
                    self.transferDateField.focusTextField()
                })
            }
            controller.selectedAccountBook = self.selectedAccountBook
            controller.type = TransactionNetworkConstants.golomt.rawValue
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }

        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var beneficiaryAccountNumberField: AccountNumberCell = {
        let nib = Bundle.main.loadNibNamed("AccountNumberCell", owner: self, options: nil)
        let cell = nib?.first as? AccountNumberCell
        guard let unwrappedCell = cell else {
            return AccountNumberCell()
        }
        unwrappedCell.keyboardType = .numberPad
        unwrappedCell.title = "standing_order_transaction_beneficiary_account_number_title".localized()
        unwrappedCell.hasImage = false
        unwrappedCell.setTag(ComponentConstants.INFO_TAG, delegate: self)
        unwrappedCell.onPickerClick = {
            let controller = AccountPickerController()
            controller.selectedAccount = self.selectedBeneficiaryAccount
            controller.onAccountSelect = { account in
                self.beneficiaryAccountNumberField.selectedItem = account
                unwrappedCell.subInfo = account.ACCT_NICKNAME.orEmpty
                self.selectedBeneficiaryAccount = account
                self.setCurrency()
            }
            controller.title = "transaction_between_own_beneficiary_title".localized()
            var filterList = [AccountResponse.Account]()
            if self.selectedAccount != nil {
                filterList.append(self.selectedAccount!)
            }
            controller.accounts = Array(Set(operativeAccountList).subtracting(filterList))
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
//        unwrappedCell.onTextChanged = {
//            self.selectedAccountBook = nil
//        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        unwrappedCell.customErrorText = "transaction_between_golomt_error_account_number_wrong".localized()
        return unwrappedCell

    }()

    lazy var frequencyTypeField: DefaultTextFieldInfoCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultTextFieldInfoCell
        guard let unwrappedCell = cell else {
            return DefaultTextFieldInfoCell()
        }
        unwrappedCell.title = "standing_order_transaction_frequency_type_label".localized()
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell?.inputText = "standing_order_transaction_every_month_label".localized()
        cell?.textField.isUserInteractionEnabled = false
        return unwrappedCell
    }()

    lazy var transferDateField: DefaultTextFieldInfoCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultTextFieldInfoCell
        guard let unwrappedCell = cell else {
            return DefaultTextFieldInfoCell()
        }
        unwrappedCell.title = "standing_order_transaction_transfer_date_label".localized()

        unwrappedCell.textField.inputView = self.datePickerTransfer
        self.datePickerTransfer.datePickerMode = .date
        self.datePickerTransfer.addTarget(self, action: #selector(dateChangedTransfer), for: .valueChanged)
        let weekAgo = Calendar.current.date(byAdding: .day, value: 0, to: Date())
        self.datePickerTransfer.minimumDate = weekAgo
        unwrappedCell.onTextChanged = {
            self.datePickerMature.minimumDate = self.datePickerTransfer.date
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var monthlyDateField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultPickerButtonCell
        guard let unwrappedCell = cell else {
            return DefaultPickerButtonCell()
        }
        unwrappedCell.title = "standing_order_transaction_monthly_date_label".localized()
        unwrappedCell.onPickerClick = {
            let controller = PickerController()
            controller.title = "Сар бүр хийгдэх өдөр".localized()
            controller.list = Array(stride(from: 1, to: 31, by: 1))
            controller.onItemSelect = { number in
                controller.popupController?.dismiss()
                unwrappedCell.selectedItem = number
            }
            controller.selectedItem = self.monthlyDateField.selectedItem
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }

        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var matureDateField: DefaultTextFieldInfoCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultTextFieldInfoCell
        guard let unwrappedCell = cell else {
            return DefaultTextFieldInfoCell()
        }
        unwrappedCell.title = "standing_order_transaction_mature_date_label".localized()
        unwrappedCell.textField.inputView = self.datePickerMature
        self.datePickerMature.datePickerMode = .date
        self.datePickerMature.addTarget(self, action: #selector(dateChangedMature), for: .valueChanged)
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var amountField: DefaultAmountFieldCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAmountFieldInfoCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAmountFieldCell
        guard let unwrappedCell = cell else {
            return DefaultAmountFieldCell()
        }
        unwrappedCell.title = "standing_order_transaction_amount_title".localized()
        unwrappedCell.subTitle = ""
        unwrappedCell.onCurrencyClick = {
            let controller = PickerController()
            controller.list = self.amountField.currencyList
            controller.onItemSelect = { currency in
                controller.popupController?.dismiss()
                self.amountField.selectedCurrency = currency as? CurrencyConstants
            }
            controller.selectedItem = self.amountField.selectedCurrency
            controller.title = "currency_title".localized()
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var remarkField: DefaultTextFieldInfoCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultTextFieldInfoCell
        guard let unwrappedCell = cell else {
            return DefaultTextFieldInfoCell()
        }
        unwrappedCell.title = "standing_order_transaction_remark_title".localized()
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        unwrappedCell.hideLine()
        self.returnKeyHandler?.addResponderFromView(unwrappedCell.contentView)
        return unwrappedCell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "standing_order_transaction_title".localized()
        navigationItem.largeTitleDisplayMode = .never

        setInitialRows()
        hasFooterButton = true

        if selectedAccount != nil {
            senderAccountField.account = selectedAccount!
            setCurrency()
        }
        if selectedBeneficiaryAccount != nil {
            beneficiaryAccountField.selectedItem = selectedBeneficiaryAccount!
            selectedBeneficiaryAccount = selectedBeneficiaryAccount!
            setCurrency()
        }
        onContinue = {
            self.handleContinue()
        }
        if selectedAccount == nil {
            showSenderAccountPicker()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
//        if !didAppear, selectedBeneficiaryAccount != nil {
//            amountField.focusTextField()
//        }
        super.viewDidAppear(animated)
    }

    private func setCurrency() {
        let currency = CurrencyConstants(rawValue: selectedAccount?.ACCT_CURRENCY.orEmpty ?? "")
        var currencyList = [CurrencyConstants]()
        if currency != nil {
            currencyList.append(currency!)
        }
        let beneficiaryCurrency = CurrencyConstants(rawValue: selectedBeneficiaryAccount?.ACCT_CURRENCY.orEmpty ?? "")
        if beneficiaryCurrency != nil {
            currencyList.append(beneficiaryCurrency!)
            currencyList = currencyList.uniqueValues()
        }
        amountField.currencyList = currencyList
    }

    private func setInitialRows() {
        model.sections = [
            TableViewModel.Section(
                rows: [
                    TableViewModel.Row(cell: senderAccountField),
                    TableViewModel.Row(cell: beneficiaryAccountField),
                    TableViewModel.Row(cell: beneficiaryAccountNumberField),
                    TableViewModel.Row(cell: frequencyTypeField),
                    TableViewModel.Row(cell: transferDateField),
                    TableViewModel.Row(cell: monthlyDateField),
                    TableViewModel.Row(cell: matureDateField),
                    TableViewModel.Row(cell: amountField),
                    TableViewModel.Row(cell: remarkField),
                ]
            ),
        ]
        tableView.reloadData()
    }
    
    private func setupAccountBook() {
        if selectedAccountBook != nil {
            beneficiaryAccountNumberField.setData(selectedAccountBook!)
            fetchAccountName()
        }
    }

    private func fetchAccountName() {
        showLoader()
        ConnectionFactory.fetchAccountName(
            accountNumber: beneficiaryAccountNumberField.inputText,
            success: { response in
                self.hideLoader()
                self.beneficiaryAccountNumberField.subInfo = response.detail?.getMaskedName()
                self.standingOrderModel.beneficiaryAccountInfo = StandingOrderModel.BeneficiaryAccountInfo(
                    number: self.selectedAccountBook?.PAYEE_LIST_ID ?? response.detail?.ACCOUNT_ID,
                    displayNumber: response.detail?.ACCOUNT_ID,
                    name: response.detail?.ACCOUNT_NAME,
                    displayName: response.detail?.getMaskedName(),
                    currency: response.detail?.ACCOUNT_CURRENCY,
                    bank: response.detail?.ACCOUNT_BRANCH,
                    displayBank: "transaction_confirm_beneficiary_golomt_label".localized()
                )
                self.transferDateField.focusTextField()
            },
            failed: { _ in
                self.beneficiaryAccountNumberField.subInfo = nil
                self.hideLoader()
            }
        )
    }
    
    private func handleContinue() {
        let contractVC = ContractViewController(nibName: "ContractViewController", bundle: nil)
        switch getLanguage() {
        case "en":
            contractVC.contractLink = "https://m.egolomt.mn/social/contract_opr_eng.html"
        case "mn":
            contractVC.contractLink = "https://m.egolomt.mn/social/contract_opr_mng.html"
        default:
            break
        }
        contractVC.controllerTitle = "contract_title".localized()
        contractVC.buttonTitle = "accept_button_title".localized()
        contractVC.checkBoxLabel = "contract_accept_label".localized()
        contractVC.onButtonClick = {
            let confirmationVC = StandingOrderConifrmationViewController(nibName: "TableViewController", bundle: nil)
            let account = self.senderAccountField.account!
            confirmationVC.transactionModel.senderAccountInfo = account
            self.standingOrderModel.beneficiaryAccountInfo.usedAccountBook = self.selectedAccountBook != nil
            if self.selectedBeneficiaryAccount != nil {
                let beneficiaryAccount = self.selectedBeneficiaryAccount!
                confirmationVC.transactionModel.beneficiaryAccountInfo = StandingOrderModel.BeneficiaryAccountInfo(
                    number: beneficiaryAccount.ACCT_NUMBER.orEmpty,
                    displayNumber: beneficiaryAccount.ACCT_NUMBER.orEmpty,
                    name: beneficiaryAccount.ACCT_NICKNAME.orEmpty,
                    displayName: beneficiaryAccount.ACCT_NICKNAME.orEmpty,
                    currency: beneficiaryAccount.ACCT_CURRENCY.orEmpty,
                    bank: beneficiaryAccount.BRANCH_ID.orEmpty,
                    displayBank: "transaction_confirm_beneficiary_golomt_label".localized()
                )
            }
            else {
                confirmationVC.transactionModel.beneficiaryAccountInfo = StandingOrderModel.BeneficiaryAccountInfo(
                    displayNumber: self.beneficiaryAccountNumberField.inputText,
                    displayName: self.beneficiaryAccountNumberField.subInfo
                )
            }

            var remark = self.remarkField.inputText
            if remark.isEmpty {
                remark = "transaction_confirm_own_remark".localized()
            }
            confirmationVC.transactionModel.transactionInfo = StandingOrderModel.TransactionInfo(
                amount: self.amountField.inputText.toDouble,
                displayAmount: self.amountField.inputText,
                remark: remark,
                currency: self.amountField.selectedCurrency?.rawValue,
                fee: "300.00 MNT",
                type: .bank,
                networkType: .bank
            )

            confirmationVC.transactionModel.frequencyInfo = StandingOrderModel.FrequencyInfo(
                type: self.frequencyTypeField.inputText,
                firstDate: self.transferDateField.inputText,
                monthlyDate: self.monthlyDateField.selectedItem as? Int,
                endDate: self.matureDateField.inputText
            )

            self.navigationController?.pushViewController(confirmationVC, animated: true)
        }
        navigationController?.pushViewController(contractVC, animated: true)
    }

    @objc private func dateChangedTransfer() {
        getdatefrompickerTransfer()
    }
    
    @objc private func dateChangedMature() {
        getdatefrompickerMature()
    }

    func getdatefrompickerMature() {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        matureDateField.inputText = formatter.string(from: datePickerMature.date)
    }

    func getdatefrompickerTransfer() {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        transferDateField.inputText = formatter.string(from: datePickerTransfer.date)
    }
}

extension StandingOrderTransactionViewController: UITextFieldDelegate, UITextViewDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case ComponentConstants.INFO_TAG:
            if beneficiaryAccountNumberField.checkError() == true, !beneficiaryAccountNumberField.inputText.isEmpty {
                fetchAccountName()
            }
        default:
            break
        }
    }
}
