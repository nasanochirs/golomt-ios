//
//  StandingOrderViewController.swift
//  golomt-ios
//
//  Created by Khulan on 8/5/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.


import Foundation
import UIKit

class StandingOrderViewController: TableViewController {
    var standingOrderList = [StandingOrderListResponse.List]()

    lazy var registerOrder: TableViewModel.Section = {
        var section = TableViewModel.Section(
            rows: [
                TableViewModel.Row(cell: addOrderField)
            ]
        )
        return section
    }()

    lazy var addOrderField: AddStandingOrderCell = {
        let nib = Bundle.main.loadNibNamed("AddStandingOrderCell", owner: self, options: nil)
        let cell = nib?.first as? AddStandingOrderCell
        guard let unwrappedCell = cell else {
            return AddStandingOrderCell()
        }
        cell?.labelText = "standing_order_add_label".localized()
        cell?.labelImage = UIImage(named: "plus")
        return unwrappedCell
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        title = "standing_order_title".localized()
        hasButton = false
        fetchOrderList()
        model.sections = [registerOrder, TableViewModel.Section(rows: [])]
        tableView.registerCell(nibName: "StandingOrderCell")
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        fetchOrderList()
        tableView.reloadData()
    }

    private func fetchOrderList() {
        showLoader()
        ConnectionFactory.fetchStandingOrderList(success: { response in
            self.hideLoader()
            response.orderList?.forEach { order in
               guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "StandingOrderCell") as? StandingOrderCell else {
                    return
                }
                cell.remitterLabel.text = "standing_order_remitter_title".localized()
                cell.nameRemitterLabel.text = order.INITIATOR_ACCOUNT_NAME
                cell.accNumberRemitterLabel.text = order.INITIATOR_ACCOUNT_ID
                cell.beneficiaryLabel.text = "standing_order_beneficiary_title".localized()
                cell.nameBeneficiaryLabel.text = order.CP_ACCOUNT_NAME
                cell.accNumberBeneficiaryLabel.text = order.CP_ACCOUNT_ID
                cell.amountLabel.text = order.TOTAL_AMOUNT?.formattedWithComma
                cell.currencyLabel.text = order.TRAN_CRN
                self.model.sections[1].rows.append(
                    TableViewModel.Row(
                        cell: cell
                    )
                )
            }
            self.tableView.reloadData()
            self.standingOrderList = response.orderList ?? []
        }, failed: { _ in
            self.hideLoader()
        })
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.section {
        case 0:
            let transVC = StandingOrderTransactionViewController(nibName: "TableViewController", bundle: nil)
            navigationController?.pushViewController(transVC, animated: true)
        case 1:
            let detailVC = StandingOrderDetailViewController(nibName: "TableViewController", bundle: nil)
            detailVC.selectedOrder = standingOrderList[indexPath.row]
            navigationController?.pushViewController(detailVC, animated: true)
        default:
            break
        }
    }
}

