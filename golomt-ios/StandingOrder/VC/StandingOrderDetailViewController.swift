//
//  StandingOrderDetailViewController.swift
//  golomt-ios
//
//  Created by Khulan on 8/10/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class StandingOrderDetailViewController: TableViewController {
    var selectedOrder: StandingOrderListResponse.List? = nil
    var account: AccountResponse.Account?

    lazy var senderSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "transaction_confirm_sender_info_label".localized(),
            rows: [
                TableViewModel.Row(
                    title: "transaction_confirm_sender_account_number_title".localized(), info: "transaction_confirm_sender_account_number_label".localized(with: self.selectedOrder?.INITIATOR_ACCOUNT_ID ?? "", self.selectedOrder?.TRAN_CRN ?? "")
                ),
                TableViewModel.Row(
                    title: "transaction_confirm_sender_account_nickname_title".localized(), info: self.selectedOrder?.INITIATOR_ACCOUNT_NAME
                )
            ]
        )
    }()

    lazy var beneficiarySection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "transaction_confirm_beneficiary_info_label".localized(),
            rows: [
                TableViewModel.Row(
                    title: "transaction_confirm_beneficiary_account_number_title".localized(), info: "transaction_confirm_beneficiary_account_number_label".localized(with: self.selectedOrder?.CP_ACCOUNT_ID ?? "", self.selectedOrder?.TRAN_CRN ?? "")
                ),
                TableViewModel.Row(
                    title: "transaction_confirm_beneficiary_account_name_title".localized(), info: self.selectedOrder?.CP_ACCOUNT_NAME
                )
            ]
        )
    }()

    lazy var transactionSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "transaction_confirm_transaction_info_label".localized(),
            rows: [
                TableViewModel.Row(
                    title: "transaction_confirm_transaction_amount_title".localized(),
                    info: "transaction_confirm_transaction_amount_label".localized(
                        with: self.selectedOrder?.TOTAL_AMOUNT ?? "", self.selectedOrder?.TRAN_CRN ?? ""
                    ),
                    infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "transaction_confirm_transaction_remark_title".localized(), info: self.selectedOrder?.TRAN_REMARKS
                ),
                TableViewModel.Row(
                    title: "standing_order_frequency_transaction_id_label".localized(), info: self.selectedOrder?.TRAN_ID_ARRAY
                )
                
            ]
        )
    }()

    lazy var frequencySection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "standing_order_frequency_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "standing_order_frequency_type_label".localized(), info: self.selectedOrder?.TRANSACTION_TYPE_INS
                ),
                TableViewModel.Row(
                    title: "standing_order_frequency_first_date_label".localized(), info: self.selectedOrder?.REC_DATE
                ),
                TableViewModel.Row(
                    title: "standing_order_frequency_monthly_date_label".localized(), info: self.selectedOrder?.FREQ_START_DD
                ),
                TableViewModel.Row(
                    title: "standing_order_frequency_end_date_label".localized(), info: self.selectedOrder?.END_DATE_INS
                )
            ]
        )
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        hasButton = false
        navigationItem.largeTitleDisplayMode = .never
        model.sections = [
            senderSection,
            beneficiarySection,
            transactionSection,
            frequencySection
        ]
        let deleteButtonItem = UIBarButtonItem(image: UIImage(named: "trash"), style: .plain, target: self, action: #selector(deleteOrder))
        navigationItem.rightBarButtonItem = deleteButtonItem
        tableView.reloadData()
    }

    private func navigateToViewController() {
        self.navigationController?.popToRootViewController(animated: true)
      }
    
    @objc private func deleteOrder() {
        showLoader()
        ConnectionFactory.deleteStandingOrder(tranId: self.selectedOrder?.TRAN_ID_ARRAY ?? "", success: { response in
            self.hideLoader()
            self.present(successDialog(message: response.getMessage(), dismiss: self.navigateToViewController), animated: true, completion: nil)
        }, failed: { reason in
            self.hideLoader()
            self.handleRequestFailure(reason)
        })

    }
    
}
