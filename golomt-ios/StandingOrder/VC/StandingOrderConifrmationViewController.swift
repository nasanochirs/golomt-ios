//
//  StandingOrderConifrmationViewController.swift
//  golomt-ios
//
//  Created by Khulan on 8/9/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class StandingOrderConifrmationViewController: TableViewController {
    var transactionModel = StandingOrderModel()

    lazy var senderSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "transaction_confirm_sender_info_label".localized(),
            rows: [
                TableViewModel.Row(
                    title: "transaction_confirm_sender_account_number_title".localized(), info: "transaction_confirm_sender_account_number_label".localized(with: transactionModel.senderAccountInfo.ACCT_NUMBER ?? "", transactionModel.senderAccountInfo.ACCT_CURRENCY ?? "")
                ),
                TableViewModel.Row(
                    title: "transaction_confirm_sender_account_nickname_title".localized(), info: transactionModel.senderAccountInfo.ACCT_NICKNAME
                )
            ]
        )
    }()

    lazy var beneficiarySection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "transaction_confirm_beneficiary_info_label".localized(),
            rows: [
                TableViewModel.Row(
                    title: "transaction_confirm_beneficiary_account_number_title".localized(), info: "transaction_confirm_beneficiary_account_number_label".localized(with: transactionModel.beneficiaryAccountInfo.displayNumber ?? "", transactionModel.beneficiaryAccountInfo.currency ?? "")
                ),
                TableViewModel.Row(
                    title: "transaction_confirm_beneficiary_account_name_title".localized(), info: transactionModel.beneficiaryAccountInfo.displayName ?? ""
                ),
                TableViewModel.Row(
                    title: "transaction_confirm_beneficiary_bank_title".localized(), info: transactionModel.beneficiaryAccountInfo.displayBank ?? ""
                )
            ]
        )
    }()

    lazy var transactionSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "transaction_confirm_transaction_info_label".localized(),
            rows: [
                TableViewModel.Row(
                    title: "transaction_confirm_transaction_amount_title".localized(),
                    info: "transaction_confirm_transaction_amount_label".localized(
                        with: transactionModel.transactionInfo.displayAmount ?? "", transactionModel.transactionInfo.currency ?? ""
                    ),
                    infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "transaction_confirm_transaction_remark_title".localized(), info: transactionModel.transactionInfo.remark ?? ""
                ),
                TableViewModel.Row(
                    title: "transaction_confirm_transaction_fee_title".localized(), info: transactionModel.transactionInfo.fee ?? ""
                )
            ]
        )
    }()

    lazy var frequencySection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "standing_order_frequency_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "standing_order_frequency_type_label".localized(), info: transactionModel.frequencyInfo.type ?? ""
                ),
                TableViewModel.Row(
                    title: "standing_order_frequency_first_date_label".localized(), info: transactionModel.frequencyInfo.firstDate ?? ""
                ),
                TableViewModel.Row(
                    title: "standing_order_frequency_monthly_date_label".localized(), info: transactionModel.frequencyInfo.monthlyDate?.toString
                ),
                TableViewModel.Row(
                    title: "standing_order_frequency_end_date_label".localized(), info: transactionModel.frequencyInfo.endDate ?? ""
                )
            ]
        )
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Confirm)
        model.sections = [
            senderSection,
            beneficiarySection,
            transactionSection,
            frequencySection
        ]
        onContinue = {
            self.handleContinue()
        }
    }

    @objc private func deleteOrder() {}

    private func handleContinue() {
        let loaderViewController = TransactionLoadingController(nibName: "TransactionLoadingController", bundle: nil)
        navigationController?.pushViewController(loaderViewController, animated: true)
        let detailViewController = TransactionDetailViewController(nibName: "TableViewController", bundle: nil)
        detailViewController.model.sections = [senderSection, beneficiarySection, transactionSection, frequencySection]

        detailViewController.onFinish = {
            self.navigationController?.popToRootViewController(animated: true)
            if detailViewController.isSuccessful {
                NotificationCenter.default.post(name: Notification.Name(NotificationConstants.REFRESH_ACCOUNTS), object: nil)
            }
        }
        ConnectionFactory.registerStandingOrder(
            model: transactionModel,
            success: {response in
            let resultMessage = response.getMessage()
            detailViewController.transactionMessage = resultMessage
            loaderViewController.setResult(
                isSuccessful: true,
                message: resultMessage,
                completion: {
                    detailViewController.isSuccessful = true
                    self.navigationController?.pushViewController(detailViewController, animated: false)
                })
        },
        failed: { reason in
            let resultMessage = reason?.MESSAGE_DESC ?? ""
            detailViewController.transactionMessage = resultMessage
            loaderViewController.setResult(isSuccessful: false, message: resultMessage, completion: {
                detailViewController.isSuccessful = false
                self.navigationController?.pushViewController(detailViewController, animated: false)
            })
        })
    }
}
