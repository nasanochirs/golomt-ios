//
//  StandingOrderListResponse.swift
//  golomt-ios
//
//  Created by Khulan on 8/7/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class StandingOrderListResponse: BaseResponse {
    let orderList: [List]?

    private enum CodingKeys: String, CodingKey {
        case StandingInstructionList_REC
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(orderList, forKey: .StandingInstructionList_REC)
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.orderList = try container.decodeIfPresent([List].self, forKey: .StandingInstructionList_REC)
        try super.init(from: decoder)
    }
    
    struct List: Codable {
        var INITIATOR_ACCOUNT_ID: String?
        var INITIATOR_ACCOUNT_NAME: String?
        var CP_ACCOUNT_ID: String?
        var CP_ACCOUNT_NAME: String?
        var TOTAL_AMOUNT: String?
        var TRAN_CRN: String?
        var FREQ_START_DD: String?
        var REC_DATE: String?
        var END_DATE_INS: String?
        var TRANSACTION_TYPE_INS: String?
        var TRAN_REMARKS: String?
        var TRAN_ID_ARRAY: String?
    }
    
}
