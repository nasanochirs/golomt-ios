//
//  StandingOrderAddRequest.swift
//  golomt-ios
//
//  Created by Khulan on 8/9/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class StandingOrderAddRequest: BaseRequest {
    var body = Body()

    private enum CodingKeys: String, CodingKey {
        case body
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(body, forKey: .body)
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    override init() {
        super.init()
        header.__SRVCID__ = "CSDINS"
    }

    struct Body: Codable {
        var EVENT = ""
        var INITOR_ACCOUNT = ""
        var CP_ACCOUNT_NUMBER = ""
        var FREQ_TYPE = ""
        var TXN_DAY: Int?
        var END_DATE = ""
        var FIRST_TXN_DATE = ""
        var AMT_ONE: Double?
        var CRN_ONE = ""
    }
}
