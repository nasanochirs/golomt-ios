//
//  StandingOrderDeleteRequest.swift
//  golomt-ios
//
//  Created by Khulan on 8/10/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class StandingOrderDeleteRequest: BaseRequest {
    var body = Body()

    private enum CodingKeys: String, CodingKey {
        case body
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(body, forKey: .body)
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    override init() {
        super.init()
        header.__SRVCID__ = "CSDINS"
    }

    struct Body: Codable {
        var EVENT = ""
        var TRAN_ID = ""
    }
}
