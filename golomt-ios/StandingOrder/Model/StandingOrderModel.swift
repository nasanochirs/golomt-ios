//
//  StandingOrderModel.swift
//  golomt-ios
//
//  Created by Khulan on 8/9/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class StandingOrderModel: NSObject {
    var senderAccountInfo = AccountResponse.Account()
    var beneficiaryAccountInfo = BeneficiaryAccountInfo()
    var transactionInfo = TransactionInfo()
    var frequencyInfo = FrequencyInfo()

    
    struct BeneficiaryAccountInfo {
        var number: String?
        var displayNumber: String?
        var name: String?
        var displayName: String?
        var currency: String?
        var bank: String?
        var displayBank: String?
        var usedAccountBook: Bool = false
    }

    struct TransactionInfo {
        var amount: Double?
        var displayAmount: String?
        var remark: String?
        var currency: String?
        var fee: String?
        var type: TransactionTypeConstants?
        var networkType: TransactionNetworkConstants?
    }
    
    struct FrequencyInfo {
        var type: String?
        var firstDate: String?
        var monthlyDate: Int?
        var endDate: String?
    }
}
