//
//  AccountNumberCell.swift
//  golomt-ios
//
//  Created by Khulan on 8/7/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class AccountNumberCell: UITableViewCell {
    @IBOutlet var checkField: DefaultCheckField!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var accNumberTextfield: UITextField!
    @IBOutlet var arrowDownImage: UIImageView!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var separator: UIView!
    @IBOutlet var pickerStackView: UIStackView!
    @IBOutlet var subInfoContainerView: UIStackView!
    @IBOutlet var subInfoTitle: UILabel!
    @IBOutlet var subInfoLabel: UILabel!
    
    var onImageClick: (() -> Void)?
    var onPickerClick: (() -> Void)?
    var onItemSelect: ((Any?) -> Void)?
    var pickerList = [Any]()
    var onStateChange: (() -> Void)?
    var onTextChanged: (() -> Void)?
        
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }
    
    func focusTextField() {
        accNumberTextfield.becomeFirstResponder()
    }
    
    func setData(_ data: Any) {
        switch data {
        case let accountBook as AccountBookResponse.AccountBook:
            accNumberTextfield.text = accountBook.BNIF_ACCNT_NUMBER.orEmpty
            subInfo = ""
        default:
            break
        }
    }

    func setTag(_ tag: Int, delegate: UITextFieldDelegate) {
        accNumberTextfield.tag = tag
        accNumberTextfield.delegate = delegate
    }
    
    var hasImage: Bool = true {
        didSet {
            switch hasImage {
            case true:
                arrowDownImage?.isHidden = false
            case false:
                arrowDownImage?.isHidden = true
            }
        }
    }
    
    var isUserInteraction: Bool = true {
        didSet {
            switch isUserInteraction {
            case true:
                accNumberTextfield.isUserInteractionEnabled = true
            case false:
                accNumberTextfield.isUserInteractionEnabled = false
            }
        }
    }
    
    var pickerIsUserInteraction: Bool = true {
        didSet {
            switch pickerIsUserInteraction {
            case true:
                pickerStackView.isUserInteractionEnabled = true
            case false:
                pickerStackView.isUserInteractionEnabled = false
            }
        }
    }
    
    var keyboardType: UIKeyboardType = .default {
        didSet {
            accNumberTextfield.keyboardType = keyboardType
        }
    }
        
    var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }
    
    var inputText: String {
        get {
            accNumberTextfield.text.orEmpty
        }
        set {
            accNumberTextfield.text = newValue
            if !newValue.isEmpty {
                accNumberTextfield.sendActions(for: .editingChanged)
            } else {
                checkField.changeCheckState(checked: false)
            }
        }
    }
    
    var subInfo: String? = "" {
        didSet {
            subInfoLabel?.text = subInfo
            if subInfo == nil || subInfo.orEmpty.trim().isEmpty {
                errorLabel.text = customErrorText
                errorLabel.textColor = .defaultWarning
                separator.backgroundColor = .defaultWarning
                checkField.changeCheckState(checked: nil, color: .defaultWarning)
            } else {
                setCheck()
            }
        }
    }
        
    var selectedItem: Any? {
        didSet {
            switch selectedItem {
            case let account as AccountResponse.Account:
                accNumberTextfield.text = account.ACCT_NUMBER
            case nil:
                accNumberTextfield.text = nil
            default:
                break
            }
            onItemSelect?(selectedItem)
            setCheck()
        }
    }
    
    var customErrorText: String = ""
        
    func hasError() -> Bool {
        errorLabel.text = nil
        separator.backgroundColor = .defaultSeparator
        errorLabel.textColor = .defaultError
        if inputText.isEmpty {
            separator.backgroundColor = .defaultError
            errorLabel.text = "error_not_empty".localized(with: title)
            checkField.changeCheckState(checked: nil)
            onStateChange?()
            return true
        }
        if subInfoLabel != nil, subInfoLabel?.text == nil || subInfoLabel?.text?.isEmpty ?? false {
            separator.backgroundColor = .defaultError
            errorLabel.text = customErrorText
            checkField.changeCheckState(checked: nil)
            onStateChange?()
            return true
        }
        switch selectedItem {
        case nil:
            separator.backgroundColor = .defaultError
            errorLabel.text = "error_must_pick".localized(with: title)
            arrowDownImage.setTint(color: .defaultError)
            checkField?.changeCheckState(checked: nil)
            onStateChange?()
            return true
        default:
            separator.backgroundColor = .defaultSeparator
            arrowDownImage.setTint(color: .defaultPrimaryText)
            errorLabel.text = nil
            onStateChange?()
            return false
        }
    }
    
    func checkError() -> Bool? {
        errorLabel.text = nil
        separator.backgroundColor = .defaultSeparator
        if inputText.isEmpty {
            separator.backgroundColor = .defaultError
            errorLabel.textColor = .defaultError
            errorLabel.text = "error_not_empty".localized(with: title)
            onStateChange?()
            return nil
        }
        if subInfoLabel != nil, subInfoLabel?.text == nil || subInfoLabel?.text?.isEmpty ?? false {
            return true
        }
        return false
    }
        
    func hideLine() {
        checkField?.checkLine.isHidden = true
    }
        
    func showLine() {
        checkField?.checkLine.isHidden = false
    }
        
    private func initComponent() {
        selectionStyle = .none
        arrowDownImage.setTint(color: .defaultPrimaryText)
        
        titleLabel.textColor = .defaultSeparator
        titleLabel.useSmallFont()
        titleLabel.makeBold()
        
        accNumberTextfield.textColor = .defaultPrimaryText
        accNumberTextfield.useLargeFont()
        accNumberTextfield.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        
        separator.backgroundColor = .defaultSeparator
        errorLabel.textColor = .red
        errorLabel.useSmallFont()
        
        pickerStackView.addTapGesture(tapNumber: 1, target: self, action: #selector(handlePicker))
        initSubView()
    }
    
    private func initSubView() {
        subInfoContainerView?.corner(cornerRadius: 10)
        subInfoTitle.text = "transaction_between_golomt_beneficiary_name_label".localized()
        subInfoTitle?.useXSmallFont()
        subInfoLabel?.useSmallFont()
        subInfoTitle?.adjustsFontSizeToFitWidth = true
        subInfoLabel?.adjustsFontSizeToFitWidth = true
        subInfoTitle?.textColor = .defaultSecondaryText
        subInfoLabel?.textColor = .defaultPrimaryText
    }
        
    @objc private func handlePicker() {
        onPickerClick?()
    }
    
    @objc private func textFieldDidChange() {
        subInfo = ""
        onTextChanged?()
        setCheckTextield()
    }
    
    private func setCheckTextield() {
        switch checkError() {
        case nil:
            checkField.changeCheckState(checked: nil)
        case false:
            checkField.changeCheckState(checked: true)
        case true:
            checkField.changeCheckState(checked: false)
        default:
            break
        }
    }
    
    private func setCheck() {
        switch checkError() {
        case nil:
            checkField.changeCheckState(checked: nil)
        case false:
            checkField.changeCheckState(checked: true)
        case true:
            checkField.changeCheckState(checked: false)
        default:
            break
        }
        onStateChange?()
    }
}
