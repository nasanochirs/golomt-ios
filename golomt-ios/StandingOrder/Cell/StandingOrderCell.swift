//
//  StandingOrderCell.swift
//  golomt-ios
//
//  Created by Khulan on 8/5/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class StandingOrderCell: UITableViewCell {
    @IBOutlet var containerView: UIView!
    @IBOutlet var remitterLabel: UILabel!
    @IBOutlet var nameRemitterLabel: UILabel!
    @IBOutlet var accNumberRemitterLabel: UILabel!
    @IBOutlet var nameBeneficiaryLabel: UILabel!
    @IBOutlet var accNumberBeneficiaryLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var currencyLabel: UILabel!
    @IBOutlet var beneficiaryLabel: UILabel!
    @IBOutlet var separator: UIView!
    
    var onStateChange: (() -> Void)?
    
    var standingOrder = [StandingOrderListResponse.List]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        fetchOrderList()
        initComponent()
    }
    
    private func initComponent() {
        selectionStyle = .none
        contentView.corner(cornerRadius: 10)
        containerView.corner(cornerRadius: 10)
        
        remitterLabel.textColor = .defaultPrimaryText
        remitterLabel.useSmallFont()
        beneficiaryLabel.textColor = .defaultPrimaryText
        beneficiaryLabel.useSmallFont()
        
        nameRemitterLabel.textColor = .defaultSecondaryText
        nameRemitterLabel.useSmallFont()
        nameBeneficiaryLabel.textColor = .defaultSecondaryText
        nameBeneficiaryLabel.useSmallFont()
        
        accNumberRemitterLabel.textColor = .defaultSecondaryText
        accNumberRemitterLabel.useXSmallFont()
        accNumberBeneficiaryLabel.textColor = .defaultSecondaryText
        accNumberBeneficiaryLabel.useXSmallFont()
        
        amountLabel.textColor = .defaultPrimaryText
        amountLabel.useLargeFont()
        currencyLabel.textColor = .defaultPrimaryText
        currencyLabel.useLargeFont()
        separator.backgroundColor = .defaultSeparator
        containerView.backgroundColor = .defaultSecondaryBackground
        
        
    }
    
//    private func fetchOrderList () {
//        ConnectionFactory.fetchStandingOrderList(success: { response in
//            response.orderList?.forEach { order in
//                self.remitterLabel.text = "standing_order_remitter_title".localized()
//                self.nameRemitterLabel.text = order.INITIATOR_ACCOUNT_NAME
//                self.accNumberRemitterLabel.text = order.INITIATOR_ACCOUNT_ID
//                self.beneficiaryLabel.text = "standing_order_beneficiary_title".localized()
//                self.nameBeneficiaryLabel.text = order.CP_ACCOUNT_NAME
//                self.accNumberBeneficiaryLabel.text = order.CP_ACCOUNT_ID
//                self.amountLabel.text = order.TOTAL_AMOUNT?.formattedWithComma
//                self.currencyLabel.text = order.TRAN_CRN
//            }
//        }, failed: { _ in
//        })
//    }
}
