//
//  AddStandingOrderCell.swift
//  golomt-ios
//
//  Created by Khulan on 8/5/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class AddStandingOrderCell: UITableViewCell {
    @IBOutlet var labelLabel: UILabel!
    @IBOutlet var addImage: UIImageView!
    @IBOutlet var containerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none

        labelLabel.useMediumFont()
        labelLabel.textColor = .defaultSecondaryText

        let shapeLayer = CAShapeLayer()
        let frameSize = containerView.bounds.size
        let shapeRect = CGRect(x:0 , y: 0, width: frameSize.width, height: frameSize.height)
        shapeLayer.frame = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor.defaultSecondaryText.cgColor
        shapeLayer.lineWidth = 2
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: containerView.bounds, cornerRadius: 10).cgPath
        self.containerView.layer.addSublayer(shapeLayer)
    }

    var labelImage: UIImage? {
        didSet {
            addImage.image = labelImage?.withColor(.defaultPrimaryText)
        }
    }

    var labelText: String = "" {
        didSet {
            labelLabel.text = labelText
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

