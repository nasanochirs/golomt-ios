//
//  AccountSettingsViewController.swift
//  golomt-ios
//
//  Created by Khulan on 7/28/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class AccountSettingsViewController: TableViewController {
    var accList = [AccountSettingsResponse.Account]()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "account_settings_title".localized()
        fetchAccountList()
        hasButton = false
        model.sections = [TableViewModel.Section(rows: [])]
        tableView.registerCell(nibName: "AccountSettingsCell")
        tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    private func fetchAccountList() {
        showLoader()
        ConnectionFactory.fetchAccountList(userId: userDetails?.USER_ID ?? "", custId: userDetails?.CUSTOMER_ID ?? "", corporateId: userDetails?.CORPORATE_ID ?? "", success: {
            response in
            self.hideLoader()
            self.accList = response.accountList ?? []
            response.accountList?.forEach { account in
                guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "AccountSettingsCell") as? AccountSettingsCell else {
                    return
                }
                cell.accountNumber.text = account.ACCOUNT_NUMBER
                cell.profileName.text = account.ACCOUNT_NICKNAME
                if account.INQUIRY_ACCESS == "Y" || account.TRANSACT_ACCESS == "Y" {
                    cell.switchControl.isOn = true
                }
                else {
                    cell.switchControl.isOn = false
                }
                cell.onValueChanged = {
                    self.showLoader()
                    if cell.switchControl.isOn == false {
                        ConnectionFactory.fetchAccountClose(branchId: account.ACC_BRANCH_ID ?? "", accId: account.ACCOUNT_NUMBER ?? "", success: { _ in
                            self.hideLoader()
                        }, failed: { _ in
                            self.hideLoader()
                            cell.switchControl.isOn = false
                        })
                    }
                    else {
                        self.showLoader()
                        ConnectionFactory.fetchAccountCreate(branchId: account.ACC_BRANCH_ID ?? "", accId: account.ACCOUNT_NUMBER ?? "", success: { _ in
                            self.hideLoader()
                        }, failed: { _ in
                            self.hideLoader()
                            cell.switchControl.isOn = true
                        })
                    }
                }
                self.model.sections[0].rows.append(
                    TableViewModel.Row(
                        cell: cell
                    )
                )
            }
            self.tableView.reloadData()
        }, failed: { _ in
            self.hideLoader()
        })
    }
}
