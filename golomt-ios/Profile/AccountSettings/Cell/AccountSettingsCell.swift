//
//  AccountSettingsCell.swift
//  golomt-ios
//
//  Created by Khulan on 7/28/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class AccountSettingsCell: UITableViewCell {
    @IBOutlet var imageAcc: UIImageView!
    @IBOutlet var accountContainerView: UIView!
    @IBOutlet var profileName: UILabel!
    @IBOutlet var switchControl: UISwitch!
    @IBOutlet var accountNumber: UILabel!
    @IBAction func clickedSwitch(_ sender: Any) {}
    
    var onValueChanged: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        accountContainerView.corner(cornerRadius: 10)
        contentView.corner(cornerRadius: 10)
        
        profileName.useMediumFont()
        profileName.textColor = .defaultPrimaryText
        
        accountNumber.textColor = .defaultSecondaryText
        accountNumber.useSmallFont()
        
        switchControl.onTintColor = .defaultBlueGradientEnd
        switchControl.addTarget(self, action: #selector(switchChangeValue), for: .valueChanged)
    }

    @objc private func switchChangeValue() {
        onValueChanged?()
    }
}
