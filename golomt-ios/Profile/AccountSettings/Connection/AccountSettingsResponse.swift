//
//  AccountSettingsResponse.swift
//  golomt-ios
//
//  Created by Khulan on 7/28/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class AccountSettingsResponse: BaseResponse {
    let accountList: [Account]?

    private enum CodingKeys: String, CodingKey {
        case AccountAccessList_REC
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(accountList, forKey: .AccountAccessList_REC)
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.accountList = try container.decodeIfPresent([Account].self, forKey: .AccountAccessList_REC)
        try super.init(from: decoder)
    }
    struct Account: Codable {
        var ACC_BRANCH_ID: String?
        var ACCOUNT_NICKNAME: String?
        var ACC_TYPE: String?
        var IS_JOINT: String?
        var ACCOUNT_NUMBER: String?
        var INQUIRY_ACCESS: String?
        var CUSTOMER_ID: String?
        var TRANSACT_ACCESS: String?
        var ACC_BANK_ID: String?
    }
    
}
