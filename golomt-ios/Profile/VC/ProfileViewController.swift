//
//  ProfileViewController.swift
//  golomt-ios
//
//  Created by Khulan Odkhuu on 7/2/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Alamofire
import AlamofireImage
import Foundation
import UIKit

class ProfileViewController: BaseUIViewController {
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var imageContainerView: UIView!
    @IBOutlet var profileNameLabel: UILabel!

    lazy var horizontalButtonField: ProfileHorizontalButtonCell = {
        let nib = Bundle.main.loadNibNamed("ProfileHorizontalButtonCell", owner: self, options: nil)
        guard let cell = nib?.first as? ProfileHorizontalButtonCell else {
            return ProfileHorizontalButtonCell()
        }
        cell.passwordView.addTapGesture(tapNumber: 1, target: self, action: #selector(changePass))
        cell.personalInfoView.addTapGesture(tapNumber: 1, target: self, action: #selector(personalInfo))
        return cell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "profile_title".localized()
//        navigationItem.largeTitleDisplayMode = .never
        setupTableView()
        tableView.tableFooterView = UIView()

        var name = userDetails?.FULLNAME
        switch getLanguage() {
        case "mn":
            break
        default:
            name = userDetails?.FULLNAME_EN
        }
        profileNameLabel.text = name
        profileNameLabel.useXXLargeFont()
        profileNameLabel.makeBold()
        profileNameLabel.textColor = .defaultPrimaryText
        profileImageView.round(borderColor: .defaultBlueGradientEnd)
        guard let pImage = getProfileImage() else {
            return
        }
        profileImageView.image = UIImage(data: pImage)
        imageContainerView.addTapGesture(tapNumber: 1, target: self, action: #selector(handleProfileImageClick))
    }
 
    private func setupTableView() {
        tableView.registerCell(nibName: "ProfileButtonCell")
        tableView.separatorColor = .defaultSeparator
        tableView.hideScrollIndicators()
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.reloadData()
    }

    @objc private func handleProfileImageClick() {
        let imagePickerDialog = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        imagePickerDialog.popoverPresentationController?.barButtonItem = navigationItem.leftBarButtonItem
        let cameraPickerAction = UIAlertAction(title: "profile_image_camera_picker".localized(), style: .default, handler: { _ in
            let vc = UIImagePickerController()
            vc.sourceType = .camera
            vc.allowsEditing = true
            vc.delegate = self
            self.present(vc, animated: true)
           })
        let libraryPickerAction = UIAlertAction(title: "profile_image_library_picker".localized(), style: .default, handler: { _ in
            let vc = UIImagePickerController()
            vc.sourceType = .photoLibrary
            vc.allowsEditing = true
            vc.delegate = self
            self.present(vc, animated: true)
           })
        let cancelAction = UIAlertAction(title: "cancel_action".localized(), style: .cancel, handler: nil)
        imagePickerDialog.addAction(cameraPickerAction)
        imagePickerDialog.addAction(libraryPickerAction)
        imagePickerDialog.addAction(cancelAction)
        present(imagePickerDialog, animated: true)
    }
    
    @objc private func changePass() {
        let changeVC = ChangePassViewController(nibName: "TableViewController", bundle: nil)
        self.navigationController?.pushViewController(changeVC, animated: true)
    }
    
    @objc private func personalInfo() {
           let personalVC = PersonalInfoViewController(nibName: "TableViewController", bundle: nil)
           self.navigationController?.pushViewController(personalVC, animated: true)
       }

    private func fetchImage() {
        ImageConnectionFactory.fetchList(
            success: { response in
                response.body?.forEach { image in
                    if image.imageCategory == "PRO" {
                        ImageResponseSerializer.addAcceptableImageContentTypes([])
                        var request = URLRequest(url: URL(string: getImageRequestUrl() + "download?imageName=" + image.imageName)!)
                        request.httpMethod = HTTPMethod.get.rawValue
                        request.setValue("Bearer " + imageBearerToken, forHTTPHeaderField: "Authorization")
                        let downloader = ImageDownloader()
                        downloader.download(request) { response in
                            if case .success(let image) = response.result {
                                setProfileImage(image.jpegData(compressionQuality: 1.0)!)
                                self.profileImageView.image = image
                            }
                        }
                    }
                }
            },
            failed: { _ in }
        )
    }
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return horizontalButtonField
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileButtonCell") as? ProfileButtonCell else {
                return UITableViewCell()
            }
            switch indexPath.row {
            case 1:
                cell.labelText = "profile_account_settings_label".localized()
            case 2:
                cell.labelText = "profile_change_login_label".localized()
            default:
                break
            }
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
              case 0:
                    break
            default:
                  switch indexPath.row {
                  case 1:
                      let accountVC = AccountSettingsViewController(nibName: "TableViewController", bundle: nil)
                      self.navigationController?.pushViewController(accountVC, animated: true)
                  case 2:
                      let loginVC = ChangeNameViewController(nibName: "TableViewController", bundle: nil)
                      self.navigationController?.pushViewController(loginVC, animated: true)
                    break
                  default:
                      break
                  }
            }
    }
}

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.editedImage] as? UIImage else {
            return
        }

        let base64Image = image.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
        ImageConnectionFactory.uploadImage(image: base64Image, success: { _ in
            self.fetchImage()
            NotificationCenter.default.post(name: Notification.Name(NotificationConstants.REFRESH_PROFILE), object: nil)
        }, failed: { _ in
        })
    }
}
