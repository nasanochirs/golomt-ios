//
//  PersonalInfoEditCell.swift
//  golomt-ios
//
//  Created by Khulan on 7/30/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class PersonalInfoEditCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var textField: UITextField!
    @IBOutlet var separator: UIView!
    @IBOutlet var errorLabel: UILabel!

    var onImageClick: (() -> Void)?
    var onTextChanged: (() -> Void)?
    var onStateChange: (() -> Void)?
    
    var textLength: Int = 255
    var lengthValidation: LengthValidation?
    
    enum LengthValidation {
        case Less, Equal, Greater
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }
    
    func setTag(_ tag: Int, delegate: UITextFieldDelegate) {
        textField.tag = tag
        textField.delegate = delegate
    }
    
    func hasError() -> Bool {
        errorLabel.text = nil
        separator.backgroundColor = .defaultSeparator
        errorLabel.textColor = .defaultError
        if inputText.isEmpty {
            separator.backgroundColor = .defaultError
            errorLabel.text = "error_not_empty".localized(with: title)
            onStateChange?()
            return true
        }
        switch lengthValidation {
        case .Equal:
            if inputText.count < textLength {
                separator.backgroundColor = .defaultError
                errorLabel.text = "error_must_be_equal_to".localized(with: textLength)
                onStateChange?()
                return true
            }
        case .Greater:
            if inputText.count <= textLength {
                separator.backgroundColor = .defaultError
                errorLabel.text = "error_must_be_greater_than".localized(with: textLength)
                onStateChange?()
                return true
            }
        default:
            return false
        }
        onStateChange?()
        return false
    }
    
    func checkError() -> Bool? {
        errorLabel.text = nil
        separator.backgroundColor = .defaultSeparator
        if inputText.isEmpty {
            separator.backgroundColor = .defaultError
            errorLabel.textColor = .defaultError
            errorLabel.text = "error_not_empty".localized(with: title)
            onStateChange?()
            return nil
        }
        onStateChange?()
        return false
    }
    
    var keyboardType: UIKeyboardType = .default {
        didSet {
            textField.keyboardType = keyboardType
        }
    }
    
    override var isUserInteractionEnabled: Bool {
        didSet {
            textField.isUserInteractionEnabled = isUserInteractionEnabled
        }
    }

    var title: String = "" {
        didSet {
            titleLabel.text = title.localized()
        }
    }
    
    var inputText: String {
        get {
            textField.text.orEmpty
        }
        set {
            textField.text = newValue
            if !newValue.isEmpty {
                textField.sendActions(for: .editingChanged)
            } else {}
        }
    }
    
    var customErrorText: String = ""
    
    func focusTextField() {
        textField.becomeFirstResponder()
    }
    
    private func initComponent() {
        selectionStyle = .none
        
        titleLabel.textColor = .defaultSeparator
        titleLabel.useSmallFont()
        
        errorLabel.textColor = .red
        errorLabel.useSmallFont()
        
        textField.borderStyle = .none
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        textField.textColor = .defaultPrimaryText
        textField.makeBold()
        textField.delegate = self
        textField.useLargeFont()
        
        separator.backgroundColor = .defaultSeparator
    }
    
    @objc private func textFieldDidChange() {
        onTextChanged?()
    }
    
    private func validateLength() {
        switch lengthValidation {
        case .Equal:
            if inputText.count < textLength {
                separator.backgroundColor = .defaultWarning
                errorLabel.textColor = .defaultWarning
                errorLabel.text = "error_must_be_equal_to".localized(with: textLength)
                onStateChange?()
            }
        case .Greater:
            if inputText.count <= textLength {
                separator.backgroundColor = .defaultWarning
                errorLabel.textColor = .defaultWarning
                errorLabel.text = "error_must_be_greater_than".localized(with: textLength)
                onStateChange?()
            }
        default:
            break
        }
    }
}

extension PersonalInfoEditCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let stringRange = Range(range, in: inputText) else {
            return false
        }
        let newString = inputText.replacingCharacters(in: stringRange, with: string)
        switch lengthValidation {
        case .Equal:
            return newString.count <= textLength
        case .Less:
            return newString.count < textLength
        default:
            return true
        }
    }
}
