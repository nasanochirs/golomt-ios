//
//  ProfileInfoViewController.swift
//  golomt-ios
//
//  Created by Khulan on 7/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class PersonalInfoViewController: TableViewController {
    lazy var personalInfoFields: TableViewModel.Section = {
        var section = TableViewModel.Section(
            rows: [
                TableViewModel.Row(cell: lastName),
                TableViewModel.Row(cell: firstName),
                TableViewModel.Row(cell: registerNumber),
            ]
        )
        return section
    }()
    
    lazy var personalInfoEditFields: TableViewModel.Section = {
        var section = TableViewModel.Section(
            rows: [
                TableViewModel.Row(cell: phoneNumber),
                TableViewModel.Row(cell: email),
            ]
        )
        return section
    }()
    
    lazy var lastName: DefaultVerticalTitleLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultVerticalTitleLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultVerticalTitleLabelCell else {
            return DefaultVerticalTitleLabelCell()
        }
        cell.setData(TableViewModel.Row(title: "personal_info_last_name_label".localized(), info: userDetails?.USER_LAST_NAME))
        return cell
    }()
    
    lazy var firstName: DefaultVerticalTitleLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultVerticalTitleLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultVerticalTitleLabelCell else {
            return DefaultVerticalTitleLabelCell()
        }
        cell.setData(TableViewModel.Row(title: "personal_info_first_name_label".localized(), info: userDetails?.USER_FIRST_NAME))
        return cell
    }()
    
    lazy var registerNumber: DefaultVerticalTitleLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultVerticalTitleLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultVerticalTitleLabelCell else {
            return DefaultVerticalTitleLabelCell()
        }
        cell.setData(TableViewModel.Row(title: "personal_info_register_label".localized(), info: userDetails?.REGISTER_NUM))
        return cell
    }()
    
    lazy var phoneNumber: PersonalInfoEditCell = {
        let nib = Bundle.main.loadNibNamed("PersonalInfoEditCell", owner: self, options: nil)
        guard let cell = nib?.first as? PersonalInfoEditCell else {
            return PersonalInfoEditCell()
        }
        cell.title = "personal_info_phone_label".localized()
        cell.inputText = userDetails?.MOBILE_NUMBER ?? ""
        cell.isUserInteractionEnabled = false
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var email: PersonalInfoEditCell = {
        let nib = Bundle.main.loadNibNamed("PersonalInfoEditCell", owner: self, options: nil)
        guard let cell = nib?.first as? PersonalInfoEditCell else {
            return PersonalInfoEditCell()
        }
        cell.title = "personal_info_email_label".localized()
        cell.inputText = userDetails?.USER_EMAIL ?? ""
        cell.isUserInteractionEnabled = false
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "profile_personal_info_label".localized()
        hasButton = false
        model.sections = [personalInfoFields, personalInfoEditFields]
        let barButton = UIBarButtonItem(image: UIImage(named: "edit"), landscapeImagePhone: nil, style: .done, target: self, action: #selector(rightButton))
        navigationItem.rightBarButtonItem = barButton
        tableView.reloadData()
    }
    
    @objc func rightButton() {
        hasFooterButton = true
        email.isUserInteractionEnabled = true
        phoneNumber.isUserInteractionEnabled = true
        phoneNumber.textField.becomeFirstResponder()
    }
}
