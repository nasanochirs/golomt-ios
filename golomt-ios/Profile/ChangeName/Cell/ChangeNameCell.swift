//
//  ChangeNameCell.swift
//  golomt-ios
//
//  Created by Khulan on 7/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class ChangeNameCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var nameContainerView: UIView!
    @IBOutlet var bankName: UILabel!
    @IBOutlet var separator: UIView!
    @IBOutlet var nameView: UIView!
    @IBOutlet var nameTextfield: UITextField!
    @IBOutlet var changeButton: UIButton!
    
    var changeButtonClicked: (() -> Void)?
    
    @IBAction func changeButtonClicked(_ sender: Any) {
        changeButtonClicked?()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }
    
    var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }
    
    var bank: String = "" {
        didSet {
            bankName.text = bank
        }
    }
    
    var inputText: String {
        get {
            nameTextfield.text.orEmpty
        }
        set {
            nameTextfield.text = newValue
            if !newValue.isEmpty {
                nameTextfield.sendActions(for: .editingChanged)
            }
        }
    }
         
    private func initComponent() {
        selectionStyle = .none
        nameContainerView.corner(cornerRadius: 10)
        contentView.corner(cornerRadius: 10)
        
        titleLabel.textColor = .defaultSecondaryText
        titleLabel.useSmallFont()
        
        bankName.textColor = .defaultPrimaryText
        bankName.makeBold()
        bankName.useMediumFont()
        
        separator.backgroundColor = .defaultSeparator
        
        nameTextfield.textColor = .defaultPurpleGradientStart
        nameTextfield.makeBold()
    
        nameView.layer.borderWidth = 0.5
        nameView.layer.borderColor = UIColor.defaultSeparator.cgColor
        nameView.layer.cornerRadius = 10
    }
}
