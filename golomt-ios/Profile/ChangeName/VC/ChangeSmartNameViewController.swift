//
//  ChangeSmartNameViewController.swift
//  golomt-ios
//
//  Created by Khulan on 7/31/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class ChangeSmartNameViewController: DialogTableViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.registerCell(nibName: "ChangeNameCell")
        tableView.rowHeight = 70
        
        configContentSize()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChangeNameCell") as? ChangeNameCell
        guard let unwrappedCell = cell else {
            return UITableViewCell()
        }
        unwrappedCell.changeButton.isHidden = true
        unwrappedCell.title = "change_login_channel_label".localized()
        unwrappedCell.bank = "change_login_smart_bank_label".localized()
        return unwrappedCell
    }
    
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let account = accounts[indexPath.row]
//        onAccountSelect?(account)
//        popupController?.dismiss()
//    }
}
