//
//  ChangeNameViewController.swift
//  golomt-ios
//
//  Created by Khulan on 7/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class ChangeNameViewController: TableViewController {
    lazy var text = [
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(cell: internetBankField),
                TableViewModel.Row(cell: smartBankField),
            ]
        ),
    ]
    
    lazy var internetBankField: ChangeNameCell = {
        let nib = Bundle.main.loadNibNamed("ChangeNameCell", owner: self, options: nil)
        let cell = nib?.first as? ChangeNameCell
        guard let unwrappedCell = cell else {
            return ChangeNameCell()
        }
        unwrappedCell.title = "change_login_channel_label".localized()
        unwrappedCell.bank = "change_login_internet_bank_label".localized()
        unwrappedCell.nameTextfield.isUserInteractionEnabled = false
        unwrappedCell.inputText = getLoginName().orEmpty
        return unwrappedCell
    }()
    
    lazy var smartBankField: ChangeNameCell = {
        let nib = Bundle.main.loadNibNamed("ChangeNameCell", owner: self, options: nil)
        let cell = nib?.first as? ChangeNameCell
        guard let unwrappedCell = cell else {
            return ChangeNameCell()
        }
        unwrappedCell.nameTextfield.isUserInteractionEnabled = false
        unwrappedCell.title = "change_login_channel_label".localized()
        unwrappedCell.bank = "change_login_smart_bank_label".localized()
        return unwrappedCell
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "profile_change_login_label".localized()
        setInitialRows()
        hasButton = false
        tableView.reloadData()
    }

    private func setInitialRows() {
        model.sections = text
        tableView.reloadData()
    }
}
