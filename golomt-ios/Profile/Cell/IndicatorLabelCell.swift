//
//  IndicatorLabelCell.swift
//  golomt-ios
//
//  Created by Khulan Odkhuu on 7/2/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class IndicatorLabelCell: UITableViewCell {
    @IBOutlet var textlabel: UILabel!
    @IBOutlet var containerView: UIView!
    
    override func awakeFromNib() {
          super.awakeFromNib()
          initComponent()
      }
    
    private func initComponent() {
        textlabel.textColor = .defaultPrimaryText
        textlabel.useSmallFont()
        
    }
 
}
