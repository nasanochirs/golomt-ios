//
//  ProfileButtonCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/10/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class ProfileButtonCell: UITableViewCell {
    @IBOutlet var buttonLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        buttonLabel.useLargeFont()
        buttonLabel.textColor = .defaultPrimaryText
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var labelText: String = "" {
        didSet {
            buttonLabel.text = labelText
        }
    }
}
