//
//  ProfileHorizontalButtonCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/10/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class ProfileHorizontalButtonCell: UITableViewCell {
    @IBOutlet var notificationLabel: UILabel!
    @IBOutlet var personalInfoLabel: UILabel!
    @IBOutlet var passwordLabel: UILabel!
    @IBOutlet var personalInfoView: UIStackView!
    @IBOutlet var passwordView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        notificationLabel.text = "profile_notification_label".localized()
        personalInfoLabel.text = "profile_personal_info_label".localized()
        passwordLabel.text = "profile_change_password_label".localized()
        notificationLabel.setFormat()
        personalInfoLabel.setFormat()
        passwordLabel.setFormat()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

fileprivate extension UILabel {
    func setFormat() {
        self.useLargeFont()
        self.wordWrap()
    }
}
