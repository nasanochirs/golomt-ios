//
//  ChangePassViewController.swift
//  golomt-ios
//
//  Created by Khulan on 7/28/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class ChangePassViewController: TableViewController {
    lazy var text = [
        TableViewModel.Section(
            title: "",
            rows: [
                TableViewModel.Row(cell: firstField),
                TableViewModel.Row(cell: secondField),
                TableViewModel.Row(cell: thirdField),
            ]
        ),
    ]
    
    lazy var firstField: DefaultLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLabelCell
        guard let unwrappedCell = cell else {
            return DefaultLabelCell()
        }
        unwrappedCell.labelText = "change_password_first_label".localized()
        return unwrappedCell
    }()
    
    lazy var secondField: DefaultLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLabelCell
        guard let unwrappedCell = cell else {
            return DefaultLabelCell()
        }
        unwrappedCell.labelText = "change_password_second_label".localized()
        return unwrappedCell
    }()
    
    lazy var thirdField: DefaultLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLabelCell
        guard let unwrappedCell = cell else {
            return DefaultLabelCell()
        }
        unwrappedCell.labelText = "change_password_third_label".localized()
        return unwrappedCell
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "change_password_title".localized()
        setButtonTitle(.Continue)
        onContinue = {
            self.handleChangeClick()
        }
        setInitialRows()
        tableView.reloadData()
    }
    
    private func setInitialRows() {
        model.sections = text
        tableView.reloadData()
    }
    
    @objc private func handleChangeClick() {
        let profileActionDialog = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        profileActionDialog.popoverPresentationController?.barButtonItem = navigationItem.leftBarButtonItem
        let loginPass = UIAlertAction(title: "change_password_login_title".localized(), style: .default, handler: { _ in
            let loginVC = ChangeLoginPassViewController(nibName: "TableViewController", bundle: nil)
            self.navigationController?.pushViewController(loginVC, animated: true)
        })
        let confirmPass = UIAlertAction(title: "change_password_confirm_title".localized(), style: .default, handler: { _ in
            let confirmVC = ChangeConfirmPassViewController(nibName: "TableViewController", bundle: nil)
            self.navigationController?.pushViewController(confirmVC, animated: true)
        })
        let cancelAction = UIAlertAction(title: "cancel_action".localized(), style: .cancel, handler: nil)
        profileActionDialog.addAction(loginPass)
        profileActionDialog.addAction(confirmPass)
        profileActionDialog.addAction(cancelAction)
        present(profileActionDialog, animated: true)
    }
}
