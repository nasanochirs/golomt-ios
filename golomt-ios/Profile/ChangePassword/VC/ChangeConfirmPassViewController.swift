//
//  ChangeConfirmPassViewController.swift
//  golomt-ios
//
//  Created by Khulan on 7/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class ChangeConfirmPassViewController: TableViewController {
    lazy var passFields = [
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(cell: textField),
                TableViewModel.Row(cell: oldPass),
                TableViewModel.Row(cell: newPass),
                TableViewModel.Row(cell: repeatNewPass),
            ]
        ),
    ]
    
    lazy var textField: DefaultLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLabelCell
        guard let unwrappedCell = cell else {
            return DefaultLabelCell()
        }
        unwrappedCell.labelLabel.textColor = .defaultSecondaryText
        unwrappedCell.labelText = "change_password_confirm_label".localized()
        return unwrappedCell
    }()
    
    lazy var oldPass: ChangePasswordCell = {
        let nib = Bundle.main.loadNibNamed("ChangePasswordCell", owner: self, options: nil)
        guard let cell = nib?.first as? ChangePasswordCell else {
            return ChangePasswordCell()
        }
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.title = "change_password_old_pass_title".localized()
        return cell
    }()
    
    lazy var newPass: ChangePasswordErrorCell = {
        let nib = Bundle.main.loadNibNamed("ChangePasswordErrorCell", owner: self, options: nil)
        guard let cell = nib?.first as? ChangePasswordErrorCell else {
            return ChangePasswordErrorCell()
        }
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.title = "change_password_new_pass_title".localized()
        return cell
    }()
    
    lazy var repeatNewPass: ChangePasswordCell = {
        let nib = Bundle.main.loadNibNamed("ChangePasswordCell", owner: self, options: nil)
        guard let cell = nib?.first as? ChangePasswordCell else {
            return ChangePasswordCell()
        }
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        cell.title = "change_password_repeat_new_pass_title".localized()
        cell.hideLine()
        return cell
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "change_password_confirm_title".localized()
        continueButton?.setTitle("change_password_title".localized(), for: .normal)
        self.setInitialRows()
        tableView.reloadData()
    }
    
    private func setInitialRows() {
        model.sections = self.passFields
        tableView.reloadData()
    }
    
    private func changePass() {
        showLoader()
        ConnectionFactory.changeConfirmPass(
            oldPass: self.oldPass.inputText,
            newPass: self.newPass.inputText,
            newPassRepeat: self.repeatNewPass.inputText, success: { response in
                self.hideLoader()
                let infoDialog = UIAlertController(title: nil, message: response.getMessage(), preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok".localized(), style: .cancel, handler: { _ in
                    let storyboard = UIStoryboard(name: "Profile", bundle: nil)
                    let viewController = storyboard.instantiateInitialViewController()
                    guard let VC = viewController else {
                        return
                    }
                    self.navigationController?.pushViewController(VC, animated: true)
                })
                infoDialog.addAction(okAction)
                self.present(infoDialog, animated: true, completion: nil)
            }, failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
}
