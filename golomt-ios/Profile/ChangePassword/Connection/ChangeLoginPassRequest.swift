//
//  ChangeLoginPassRequest.swift
//  golomt-ios
//
//  Created by Khulan on 7/30/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class ChangeLoginPassRequest: BaseRequest {
    var body = Body()

    private enum CodingKeys: String, CodingKey {
        case body
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(body, forKey: .body)
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    override init() {
        super.init()
        header.__SRVCID__ = "PWDCHG"
    }

    struct Body: Codable {
        var SIGNON_PWD = ""
        var SIGNON_NEW_PWD = ""
        var RETYPE_SIGNON_PWD = ""
        var SIGNON_FLAG = ""
    }
}
