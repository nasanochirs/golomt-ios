//
//  ChangePasswordErrorCell.swift
//  golomt-ios
//
//  Created by Khulan on 7/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class ChangePasswordErrorCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var passwordTextfield: UITextField!
    @IBOutlet var separator: UIView!
    @IBOutlet var checkField: DefaultCheckField!
    @IBOutlet var errorView: UIStackView!
    @IBOutlet var errorLettersLabel: UILabel!
    @IBOutlet var errorNumberLabel: UILabel!
    @IBOutlet var errorSpaceLabel: UILabel!
    @IBOutlet var errorMaxminLabel: UILabel!
    @IBOutlet var errorNameLabel: UILabel!
    var onImageClick: (() -> Void)?
    var onTextChanged: (() -> Void)?
    var onStateChange: (() -> Void)?
    
    var textLength: Int = 28
    var lengthValidation: LengthValidation?
    
    enum LengthValidation {
        case Less, Equal, Greater
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }
    
    func hideLine() {
        checkField.checkLine.isHidden = true
    }
    
    func showLine() {
        checkField.checkLine.isHidden = false
    }
    
    func setTag(_ tag: Int, delegate: UITextFieldDelegate) {
        passwordTextfield.tag = tag
        passwordTextfield.delegate = delegate
    }
    
    func checkError() -> Bool? {
        separator.backgroundColor = .defaultSeparator

        let number = CharacterSet.decimalDigits
        let decimalRange = inputText.rangeOfCharacter(from: number)
        let charset = CharacterSet(charactersIn: " ")
        let letter = CharacterSet.letters
        let letterRange = inputText.rangeOfCharacter(from: letter)
        
        let letterError = letterRange == nil
        let decimalError = decimalRange == nil
        let spaceError = inputText.rangeOfCharacter(from: charset) != nil
        let lengthError = inputText.count < 8 || inputText.count > 28
        let loginNameError = inputText.elementsEqual(getLoginName().orEmpty)
        
        errorLettersLabel.textColor = letterError ? .defaultError : .defaultSeparator
        errorNumberLabel.textColor = decimalError ? .defaultError : .defaultSeparator
        errorSpaceLabel.textColor = spaceError ? .defaultError : .defaultSeparator
        errorMaxminLabel.textColor = lengthError ? .defaultError : .defaultSeparator
        errorNameLabel.textColor = loginNameError ? .defaultError : .defaultSeparator
        
        if letterError || decimalError || spaceError || lengthError || loginNameError {
            separator.backgroundColor = .defaultError
            onStateChange?()
            return nil
        }
        
        return false
    }
    
    var keyboardType: UIKeyboardType = .default {
        didSet {
            passwordTextfield.keyboardType = keyboardType
        }
    }
    
    var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }
    
    var inputText: String {
        get {
            passwordTextfield.text.orEmpty
        }
        set {
            passwordTextfield.text = newValue
            if !newValue.isEmpty {
                passwordTextfield.sendActions(for: .editingChanged)
            } else {
                checkField.changeCheckState(checked: false)
            }
        }
    }
    
    var customErrorText: String = ""
    
    func focusTextField() {
        passwordTextfield.becomeFirstResponder()
    }
    
    private func initComponent() {
        selectionStyle = .none
        
        titleLabel.textColor = .defaultSeparator
        titleLabel.useSmallFont()
        titleLabel.makeBold()
        
        errorLettersLabel.text = "change_password_letters_label".localized()
        errorNumberLabel.text = "change_password_number_label".localized()
        errorMaxminLabel.text = "change_password_max_min_label".localized()
        errorSpaceLabel.text = "change_password_not_space_label".localized()
        errorNameLabel.text = "change_password_name_label".localized()
        
        errorNameLabel.textColor = .defaultSeparator
        errorSpaceLabel.textColor = .defaultSeparator
        errorMaxminLabel.textColor = .defaultSeparator
        errorNumberLabel.textColor = .defaultSeparator
        errorLettersLabel.textColor = .defaultSeparator
        
        errorNameLabel.useXSmallFont()
        errorSpaceLabel.useXSmallFont()
        errorMaxminLabel.useXSmallFont()
        errorNumberLabel.useXSmallFont()
        errorLettersLabel.useXSmallFont()
        
        passwordTextfield.borderStyle = .none
        passwordTextfield.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        passwordTextfield.textColor = .defaultPrimaryText
        passwordTextfield.delegate = self
        passwordTextfield.useLargeFont()
        
        separator.backgroundColor = .defaultSeparator
    }
    
    @objc private func textFieldDidChange() {
        onTextChanged?()
        setCheck()
    }
    
    private func setCheck() {
        switch checkError() {
        case nil:
            checkField.changeCheckState(checked: nil)
        case false:
            checkField.changeCheckState(checked: true)
        case true:
            checkField.changeCheckState(checked: false)
        default:
            break
        }
    }
}

extension ChangePasswordErrorCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let stringRange = Range(range, in: inputText) else {
            return false
        }
        let newString = inputText.replacingCharacters(in: stringRange, with: string)
        switch lengthValidation {
        case .Equal:
            return newString.count <= textLength
        case .Less:
            return newString.count < textLength
        default:
            return true
        }
    }
}
