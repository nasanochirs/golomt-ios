//
//  ChangePasswordCell.swift
//  golomt-ios
//
//  Created by Khulan on 7/30/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class ChangePasswordCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var textField: UITextField!
    @IBOutlet var separator: UIView!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var checkField: DefaultCheckField!
    
    var onImageClick: (() -> Void)?
    var onTextChanged: (() -> Void)?
    var onStateChange: (() -> Void)?
    
    var textLength: Int = 255
    var lengthValidation: LengthValidation?
    
    enum LengthValidation {
        case Less, Equal, Greater
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }
    
    func hideLine() {
        checkField.checkLine.isHidden = true
    }
    
    func showLine() {
        checkField.checkLine.isHidden = false
    }
    
    func setTag(_ tag: Int, delegate: UITextFieldDelegate) {
        textField.tag = tag
        textField.delegate = delegate
    }
    
    func hasError() -> Bool {
        errorLabel.text = nil
        separator.backgroundColor = .defaultSeparator
        errorLabel.textColor = .defaultError
        if inputText.isEmpty {
            separator.backgroundColor = .defaultError
            errorLabel.text = "error_not_empty".localized(with: title)
            checkField.changeCheckState(checked: nil)
            onStateChange?()
            return true
        }
        switch lengthValidation {
        case .Equal:
            if inputText.count < textLength {
                separator.backgroundColor = .defaultError
                errorLabel.text = "error_must_be_equal_to".localized(with: textLength)
                checkField.changeCheckState(checked: nil)
                onStateChange?()
                return true
            }
        case .Greater:
            if inputText.count <= textLength {
                separator.backgroundColor = .defaultError
                errorLabel.text = "error_must_be_greater_than".localized(with: textLength)
                checkField.changeCheckState(checked: nil)
                onStateChange?()
                return true
            }
        default:
            return false
        }
        onStateChange?()
        return false
    }
    
    func checkError() -> Bool? {
        errorLabel.text = nil
        separator.backgroundColor = .defaultSeparator
        if inputText.isEmpty {
            separator.backgroundColor = .defaultError
            errorLabel.textColor = .defaultError
            errorLabel.text = "error_not_empty".localized(with: title)
            onStateChange?()
            return nil
        }
        onStateChange?()
        return false
    }
    
    var keyboardType: UIKeyboardType = .default {
        didSet {
            textField.keyboardType = keyboardType
        }
    }
    
    var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }
    
    var inputText: String {
        get {
            textField.text.orEmpty
        }
        set {
            textField.text = newValue
            if !newValue.isEmpty {
                textField.sendActions(for: .editingChanged)
            } else {
                checkField.changeCheckState(checked: false)
            }
        }
    }

    var customErrorText: String = ""
    
    func focusTextField() {
        textField.becomeFirstResponder()
    }
    
    private func initComponent() {
        selectionStyle = .none
        
        titleLabel.textColor = .defaultSeparator
        titleLabel.useSmallFont()
        titleLabel.makeBold()
        
        errorLabel.textColor = .red
        errorLabel.useSmallFont()
        
        textField.borderStyle = .none
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        textField.textColor = .defaultPrimaryText
        textField.delegate = self
        textField.useLargeFont()
        textField.isSecureTextEntry = true
        
        separator.backgroundColor = .defaultSeparator
    }
    
    @objc private func textFieldDidChange() {
        onTextChanged?()
        setCheck()
    }
    
    private func setCheck() {
        switch checkError() {
        case nil:
            checkField.changeCheckState(checked: nil)
        case false:
            checkField.changeCheckState(checked: true)
        case true:
            checkField.changeCheckState(checked: false)
        default:
            break
        }
        validateLength()
    }
    
    private func validateLength() {
        switch lengthValidation {
        case .Equal:
            if inputText.count < textLength {
                separator.backgroundColor = .defaultWarning
                errorLabel.textColor = .defaultWarning
                errorLabel.text = "error_must_be_equal_to".localized(with: textLength)
                checkField.changeCheckState(checked: nil, color: .defaultWarning)
                onStateChange?()
            }
        case .Greater:
            if inputText.count <= textLength {
                separator.backgroundColor = .defaultWarning
                errorLabel.textColor = .defaultWarning
                errorLabel.text = "error_must_be_greater_than".localized(with: textLength)
                checkField.changeCheckState(checked: nil, color: .defaultWarning)
                onStateChange?()
            }
        default:
            break
        }
    }
}

extension ChangePasswordCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let stringRange = Range(range, in: inputText) else {
            return false
        }
        let newString = inputText.replacingCharacters(in: stringRange, with: string)
        switch lengthValidation {
        case .Equal:
            return newString.count <= textLength
        case .Less:
            return newString.count < textLength
        default:
            return true
        }
    }
}
