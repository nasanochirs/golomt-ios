//
//  ExtendDepositModel.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class ExtendDepositModel {
    var depositAccount: AccountResponse.Account? = nil
    var depositAccountDetails: DEPDetailResponse? = nil
    var beneficiaryAccount: AccountResponse.Account? = nil
    var product: ExtendDepositProductResponse.Product? = nil
    var month: ExtendDepositMonthResponse.Month? = nil
}
