//
//  ExtendDepositViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/27/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class ExtendDepositViewController: TableViewController {
    var filteredList = [AccountResponse.Account]()
    var productList = [ExtendDepositProductResponse.Product]()
    var monthList = [ExtendDepositMonthResponse.Month]()
    var extendModel = ExtendDepositModel()

    lazy var extendAccountField: DefaultAccountPickerCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAccountPickerCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAccountPickerCell
        guard let unwrappedCell = cell else {
            return DefaultAccountPickerCell()
        }
        unwrappedCell.defaultNicknameText = "deposit_extend_remitter_account_title".localized()
        unwrappedCell.defaultNumberText = "deposit_extend_remitter_account_label".localized()
        unwrappedCell.onPickerClick = {
            self.showSenderAccountPicker(self.filteredList)
        }
        self.onAccountSelect = { account in
            self.handleAccountSelect(account)
        }
        self.accountPickerTitle = "deposit_extend_remitter_account_title".localized()
        return unwrappedCell
    }()

    lazy var productField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerPlainCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultPickerButtonCell
        guard let unwrappedCell = cell else {
            return DefaultPickerButtonCell()
        }
        unwrappedCell.title = "deposit_extend_product_title".localized()
        unwrappedCell.onPickerClick = {
            let controller = PickerController()
            controller.onItemSelect = { item in
                unwrappedCell.selectedItem = item
                controller.popupController?.dismiss(completion: {
                    guard let item = item as? ExtendDepositProductResponse.Product else {
                        return
                    }
                    self.fetchProductMonths(of: item, with: self.selectedAccount!.ACCT_CURRENCY.orEmpty)
                })
            }
            controller.selectedItem = unwrappedCell.selectedItem
            controller.title = unwrappedCell.title
            controller.list = self.productList
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var monthField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerPlainCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultPickerButtonCell
        guard let unwrappedCell = cell else {
            return DefaultPickerButtonCell()
        }
        unwrappedCell.title = "deposit_extend_month_title".localized()
        unwrappedCell.onPickerClick = {
            let controller = PickerController()
            controller.onItemSelect = { item in
                unwrappedCell.selectedItem = item
                controller.popupController?.dismiss()
            }
            controller.selectedItem = unwrappedCell.selectedItem
            controller.title = unwrappedCell.title
            controller.list = self.monthList
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var extendSection: TableViewModel.Section = {
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(
                    cell: self.extendAccountField
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_category_title".localized()
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_maturity_date".localized()
                ),
                TableViewModel.Row(
                    title: "deposit_extend_currency_title".localized(), info: nil, infoProperty: .DefaultAmount, cell: nil
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_available_balance_title".localized(), info: nil, infoProperty: .DefaultAmount, cell: nil
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_accumulated_interest_title".localized(), info: nil, infoProperty: .DefaultAmount, cell: nil
                ),
                TableViewModel.Row(
                    cell: self.productField
                ),
                TableViewModel.Row(
                    cell: self.monthField
                ),
            ]
        )
    }()

    override func viewDidLoad() {
        title = "deposit_extend_title".localized()
//        table = tableView

        super.viewDidLoad()
        setButtonTitle(.Continue)
//        accountControllerTitle = "deposit_extend_remitter_account_title".localized()
        self.fetchAccountList()

        onContinue = {
            if self.extendAccountField.hasError() {
                self.showSenderAccountPicker(self.filteredList)
            }
            if self.productField.hasError() || self.monthField.hasError() {
                return
            }
            let confirmVC = ExtendDepositConfirmViewController(nibName: "TableViewController", bundle: nil)
            self.extendModel.depositAccount = self.selectedAccount
            self.extendModel.product = self.productField.selectedItem as? ExtendDepositProductResponse.Product
            self.extendModel.month = self.monthField.selectedItem as? ExtendDepositMonthResponse.Month
            confirmVC.extendModel = self.extendModel
            self.navigationController?.pushViewController(confirmVC, animated: true)
        }
    }

    private func handleAccountSelect(_ account: AccountResponse.Account) {
        self.extendAccountField.account = account
        self.selectedAccount = account
        self.fetchAccountDetail(account)
    }

    private func fetchAccountList() {
        showLoader()
        ConnectionFactory.fetchDepositExtendAccounts(
            success: { response in
                self.hideLoader()
                response.accountList?.forEach { account in
                    let tmpAccount = depositAccountList.filter {
                        $0.ACCT_NUMBER.orEmpty == account.ACCOUNT_ID
                    }
                    self.filteredList.append(contentsOf: tmpAccount)
                }
                if self.filteredList.isEmpty {
                    self.model.sections = []
                } else {
                    self.model.sections = [self.extendSection]
                }
                self.tableView.reloadData()
                if self.selectedAccount != nil {
                    self.handleAccountSelect(self.selectedAccount!)
                }
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }

    private func fetchAccountDetail(_ account: AccountResponse.Account) {
        self.showLoader()
        ConnectionFactory.fetchDEPDetail(
            account: account,
            isLoan: false,
            success: { response in
                self.extendSection.rows[1].info = response.description?.ACCOUNT_CATEGORY_DESC
                self.extendSection.rows[2].info = response.detail?.MATURITY_DATE
                self.extendSection.rows[3].info = account.ACCT_CURRENCY
                self.extendSection.rows[4].info = response.balance?.LEDGER_BAL?.toAmountWithCurrencySymbol
                self.extendSection.rows[5].info = response.balance?.NRML_BOOKED_AMOUNT_CR?.toAmountWithCurrencySymbol
                self.tableView.reloadData()
                self.fetchProductList(of: account)
                self.extendModel.depositAccountDetails = response
                self.hideLoader()

            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }

    private func fetchProductList(of account: AccountResponse.Account) {
        self.showLoader()
        ConnectionFactory.fetchDepositExtendProducts(
            account: account,
            success: { response in
                self.productList = response.productList ?? []
                self.hideLoader()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }

    private func fetchProductMonths(of product: ExtendDepositProductResponse.Product, with currency: String) {
        self.showLoader()
        ConnectionFactory.fetchDepositExtendMonths(
            product: product.RENEW_INSTRUCTION_CODE_ARRAY.orEmpty,
            currency: currency,
            success: { response in
                self.monthList = response.months ?? []
                self.hideLoader()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
}
