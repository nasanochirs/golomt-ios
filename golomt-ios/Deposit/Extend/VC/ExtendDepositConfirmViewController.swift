//
//  ExtendDepositConfirmViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class ExtendDepositConfirmViewController: TableViewController {
    var extendModel = ExtendDepositModel()

    lazy var accountSection: TableViewModel.Section = {
        let number = extendModel.depositAccount?.ACCT_NUMBER ?? ""
        let currency = extendModel.depositAccount?.ACCT_CURRENCY ?? ""
        return TableViewModel.Section(
            title: "deposit_extend_confirm_account_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "deposit_extend_confirm_account_to_extend_title".localized(), info: number + " " + currency
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_available_balance_title".localized(), info: self.extendModel.depositAccountDetails?.balance?.LEDGER_BAL.toAmountWithCurrencySymbol, infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_accumulated_interest_title".localized(), info: self.extendModel.depositAccountDetails?.detail?.MATURITY_AMOUNT.toAmountWithCurrencySymbol, infoProperty: .DefaultAmount
                  ),
            ]
        )
    }()

    lazy var productSection: TableViewModel.Section = {
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(
                    title: "deposit_extend_product_title".localized(), info: extendModel.product?.RENEW_INSTRUCTION_NAME_ARRAY
                ),
                TableViewModel.Row(
                    title: "deposit_extend_month_title".localized(), info: extendModel.month?.FD_RENEW_MONTHS_ARRAY
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_maturity_date".localized(), info: extendModel.depositAccountDetails?.detail?.MATURITY_DATE
                ),
                TableViewModel.Row(
                    title: "deposit_extend_confirm_extend_date_title".localized(), info: Date().toFormatted
                ),
            ]
        )
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Confirm)
        title = "transaction_confirm_button_title".localized()
        model.sections = [accountSection, productSection]
        tableView.reloadData()
        onContinue = {
            self.handleContinue()
        }
    }
    
    private func handleContinue() {
        let loaderViewController = TransactionLoadingController(nibName: "TransactionLoadingController", bundle: nil)
        navigationController?.pushViewController(loaderViewController, animated: true)
        let detailViewController = TransactionDetailViewController(nibName: "TableViewController", bundle: nil)
        detailViewController.model.sections = [accountSection, productSection]
        detailViewController.controllerTitle = "deposit_extend_title".localized()
        detailViewController.onFinish = {
            self.navigationController?.popToRootViewController(animated: true)
            if detailViewController.isSuccessful {
                NotificationCenter.default.post(name: Notification.Name(NotificationConstants.REFRESH_ACCOUNTS), object: nil)
            }
        }
        ConnectionFactory.extendDeposit(
            model: extendModel,
            success: { response in
                let resultMessage = response.getMessage()
                detailViewController.transactionMessage = resultMessage
                loaderViewController.setResult(
                    isSuccessful: true,
                    message: resultMessage,
                    completion: {
                        detailViewController.isSuccessful = true
                        self.navigationController?.pushViewController(detailViewController, animated: false)
                    }
                )
            },
            failed: { reason in
                let resultMessage = reason?.MESSAGE_DESC ?? ""
                detailViewController.transactionMessage = resultMessage
                loaderViewController.setResult(isSuccessful: false, message: resultMessage, completion: {
                    detailViewController.isSuccessful = false
                    self.navigationController?.pushViewController(detailViewController, animated: false)
                })
            }
        )
    }
}
