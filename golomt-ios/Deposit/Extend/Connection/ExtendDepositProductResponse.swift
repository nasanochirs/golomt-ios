//
//  ExtendDepositProductResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class ExtendDepositProductResponse: BaseResponse {
    let productList: [Product]?
    
    private enum CodingKeys: String, CodingKey {
        case SchemeList_REC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(productList, forKey: .SchemeList_REC)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.productList = try container.decodeIfPresent([Product].self, forKey: .SchemeList_REC)
        try super.init(from: decoder)
    }
    
    struct Product: Codable {
        var RENEW_INSTRUCTION_CODE_ARRAY: String?
        var RENEW_INSTRUCTION_NAME_ARRAY: String?
    }
}
