//
//  ExtendDepositMonthResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class ExtendDepositMonthResponse: BaseResponse {
    let months: [Month]?
    
    private enum CodingKeys: String, CodingKey {
        case PeriodList_REC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(months, forKey: .PeriodList_REC)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.months = try container.decodeIfPresent([Month].self, forKey: .PeriodList_REC)
        try super.init(from: decoder)
    }
    
    struct Month: Codable {
        var FD_RENEW_MONTHS_ARRAY: String?
    }
}
