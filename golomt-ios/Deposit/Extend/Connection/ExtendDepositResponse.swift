//
//  ExtendDepositResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class ExtendDepositResponse: BaseResponse {
    let deposit: DepositExtendResponse?
    
    private enum CodingKeys: String, CodingKey {
        case `default`
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(deposit, forKey: .default)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.deposit = try container.decodeIfPresent(DepositExtendResponse.self, forKey: .default)
        try super.init(from: decoder)
    }
    
    struct DepositExtendResponse: Codable {
        var FORM_REQ_STATE: String?
    }
}
