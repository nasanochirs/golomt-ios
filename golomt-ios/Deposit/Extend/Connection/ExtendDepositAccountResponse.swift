//
//  ExtendDepositAccountResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/27/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class ExtendDepositAccountResponse: BaseResponse {
    let accountList: [Account]?
    
    private enum CodingKeys: String, CodingKey {
        case GeneralDetails_REC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(accountList, forKey: .GeneralDetails_REC)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.accountList = try container.decodeIfPresent([Account].self, forKey: .GeneralDetails_REC)
        try super.init(from: decoder)
    }
    
    struct Account: Codable {
        var ACC_CURRENCY: String?
        var ACCOUNT_ID: String?
        var ACCOUNT_NAME: String?
    }
}
