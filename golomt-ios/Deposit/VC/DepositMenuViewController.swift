//
//  CreateDepositMenuViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/23/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class DepositMenuViewController: MenuTableViewController {
    lazy var detailSection: MenuTableViewModel.Section = {
        var section = MenuTableViewModel.Section(
            rows: [
                MenuTableViewModel.Row(
                    icon: UIImage(named: "deposit"), title: "deposit_create_deposit_title"
                ),
                MenuTableViewModel.Row(
                    icon: UIImage(named: "deposit"), title: "deposit_close_deposit_title"
                ),
                MenuTableViewModel.Row(
                    icon: UIImage(named: "deposit"), title: "deposit_extend_title"
                )
            ]
        )
        return section
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "deposit_title".localized()
        tableView.registerCell(nibName: "DefaultMenuCell")
        tableView.rowHeight = 70
        model.sections = [detailSection]
        tableView.reloadData()

        onClick = { row in
            switch row {
                case 0:
                    let depositStoryBoard: UIStoryboard = UIStoryboard(name: "Deposit", bundle: nil)

                    let viewController = depositStoryBoard.instantiateViewController(withIdentifier: "DepositTypeID")
                    let typeVC = viewController as? CreateDepositTypeViewController
                    guard let unwrappedVC = typeVC else {
                        return
                    }
                    self.navigationController?.pushViewController(unwrappedVC, animated: true)
                case 1:
                    let closeVC = CloseDepositViewController(nibName: "TableViewController", bundle: nil)
                    self.navigationController?.pushViewController(closeVC, animated: true)
                case 2:
                    let extendVC = ExtendDepositViewController(nibName: "TableViewController", bundle: nil)
                    self.navigationController?.pushViewController(extendVC, animated: true)
                default:
                    break
            }
        }
    }
}
