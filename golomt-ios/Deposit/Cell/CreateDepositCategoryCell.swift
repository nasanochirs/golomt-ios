//
//  CreateDepositCategoryCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/24/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class CreateDepositCategoryCell: UICollectionViewCell {
    @IBOutlet var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = .defaultSecondaryBackground
        contentView.corner(cornerRadius: 15)
        descriptionLabel.makeBold()
        descriptionLabel.useXLargeFont()
        descriptionLabel.textColor = .defaultPrimaryText
        descriptionLabel.adjustsFontSizeToFitWidth = true
    }

    override var isSelected: Bool {
        didSet {
            if isSelected {
                contentView.backgroundColor = .defaultSelectionBackground
                descriptionLabel.textColor = .white
            } else {
                contentView.backgroundColor = .defaultSecondaryBackground
                descriptionLabel.textColor = .defaultPrimaryText
            }
        }
    }
    
    func setDescription(_ description: String) {
        descriptionLabel.text = description
    }
}
