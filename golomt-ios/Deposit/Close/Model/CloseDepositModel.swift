//
//  CloseDepositModel.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/26/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class CloseDepositModel {
    var depositAccount: AccountResponse.Account? = nil
    var depositAccountDetails: DEPDetailResponse? = nil
    var beneficiaryAccount: AccountResponse.Account? = nil
}
