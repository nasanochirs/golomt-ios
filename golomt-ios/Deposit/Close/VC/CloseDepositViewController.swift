//
//  CloseDepositViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/26/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class CloseDepositViewController: TableViewController {
    var filteredList = [AccountResponse.Account]()
    var closeModel = CloseDepositModel()

    lazy var closeAccountField: DefaultAccountPickerCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAccountPickerCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAccountPickerCell
        guard let unwrappedCell = cell else {
            return DefaultAccountPickerCell()
        }
        unwrappedCell.onPickerClick = {
            self.showSenderAccountPicker(self.filteredList)
        }
        unwrappedCell.defaultNicknameText = "deposit_close_account_to_close_title".localized()
        unwrappedCell.defaultNumberText = "deposit_close_account_to_close_label".localized()
        self.onAccountSelect = { account in
            self.handleAccountSelect(account)
        }
        self.accountPickerTitle = "deposit_close_account_to_close_title".localized()
        return unwrappedCell
    }()

    lazy var warningField: DefaultLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLabelCell
        guard let unwrappedCell = cell else {
            return DefaultLabelCell()
        }
        unwrappedCell.labelText = "deposit_close_confirm_warning_label".localized()
        return unwrappedCell
    }()

    lazy var remitterAccountField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerPlainCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultPickerButtonCell
        guard let unwrappedCell = cell else {
            return DefaultPickerButtonCell()
        }
        unwrappedCell.title = "deposit_close_beneficiary_account_title".localized()
        unwrappedCell.onPickerClick = {
            let controller = AccountPickerController()
            controller.onAccountSelect = { account in
                self.remitterAccountField.selectedItem = account
                self.selectedAccount = account
                self.closeModel.beneficiaryAccount = account
            }
            controller.selectedAccount = self.selectedAccount
            controller.title = unwrappedCell.title
            controller.accounts = operativeAccountList
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var closeSection: TableViewModel.Section = {
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(
                    title: nil, info: nil, infoProperty: nil, cell: self.closeAccountField
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_available_balance_title".localized(), info: nil, infoProperty: .DefaultAmount, cell: nil
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_maturity_date".localized(), info: nil, infoProperty: nil, cell: nil
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_yearly_interest_title".localized(), info: nil, infoProperty: nil, cell: nil
                ),
                TableViewModel.Row(
                    title: nil, info: nil, infoProperty: nil, cell: self.warningField
                ),
                TableViewModel.Row(
                    title: nil, info: nil, infoProperty: nil, cell: self.remitterAccountField
                ),
            ]
        )
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Continue)
        title = "deposit_close_deposit_title".localized()
//        accountControllerTitle = "deposit_close_account_to_close_title".localized()
        
        onContinue = {
            self.handleContinue()
        }
        model.sections = [self.closeSection]
        tableView.reloadData()

        self.fetchAccountProducts()
    }

    private func fetchAccountProducts() {
        showLoader()
            ConnectionFactory.fetchParameterValue(
                parameterName: "BREAK_DEP_PROD",
                success: { response in
                    self.hideLoader()
                    self.filteredList = []
                    response.parameter?.P_VAL.orEmpty.components(separatedBy: "|").forEach { product in
                        self.filteredList.append(contentsOf: depositAccountList.filter {
                            $0.PRODUCT_CATEGORY.orEmpty.contains(product)
                        })
                    }
                    if self.selectedAccount != nil {
                        self.handleAccountSelect(self.selectedAccount!)
                    }
                },
                failed: { reason in
                    self.hideLoader()
                    self.handleRequestFailure(reason)
                }
            )
    }
    
    private func handleContinue() {
        if self.closeAccountField.hasError() {
            showSenderAccountPicker(filteredList)
            return
        }
        if self.remitterAccountField.hasError() {
            return
        }
        let confirmVC = CloseDepositConfirmViewController(nibName: "TableViewController", bundle: nil)
        confirmVC.closeModel = closeModel
        navigationController?.pushViewController(confirmVC, animated: true)
    }

    private func handleAccountSelect(_ account: AccountResponse.Account) {
        self.closeAccountField.account = account
        self.selectedAccount = account
        self.showLoader()
        ConnectionFactory.fetchDEPDetail(
            account: account,
            isLoan: false,
            success: { response in
                self.closeSection.rows[1].info = response.balance?.LEDGER_BAL?.toAmountWithCurrencySymbol
                self.closeSection.rows[2].info = response.detail?.MATURITY_DATE
                self.closeSection.rows[3].info = response.detail?.INTEREST_RATE
                self.closeModel.depositAccount = account
                self.closeModel.depositAccountDetails = response
                self.tableView.reloadData()
                self.hideLoader()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
}
