//
//  CloseDepositConfirmViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/26/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class CloseDepositConfirmViewController: TableViewController {
    var closeModel = CloseDepositModel()

    lazy var accountSection: TableViewModel.Section = {
        let number = closeModel.depositAccount?.ACCT_NUMBER ?? ""
        let currency = closeModel.depositAccount?.ACCT_CURRENCY ?? ""
        return TableViewModel.Section(
            title: "deposit_close_confirm_account_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "deposit_close_confirm_account_to_close_title".localized(), info: number + " " + currency
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_available_balance_title".localized(), info: self.closeModel.depositAccountDetails?.balance?.LEDGER_BAL.toAmountWithCurrencySymbol, infoProperty: .DefaultAmount
                ),
            ]
        )
    }()

    lazy var productSection: TableViewModel.Section = {
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(
                    title: "dep_detail_account_category_title".localized(), info: closeModel.depositAccountDetails?.description?.ACCOUNT_CATEGORY_DESC
                ),
                TableViewModel.Row(
                    title: "dep_detail_account_maturity_date".localized(), info: closeModel.depositAccountDetails?.detail?.MATURITY_DATE
                ),
                TableViewModel.Row(
                    title: "deposit_close_confirm_closing_date_title".localized(), info: Date().toFormatted
                ),
            ]
        )
    }()

    lazy var beneficiarySection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "deposit_close_confirm_beneficiary_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "deposit_close_confirm_beneficiary_account_title".localized(), info: closeModel.beneficiaryAccount?.ACCT_NUMBER
                ),
                TableViewModel.Row(
                    title: "deposit_close_confirm_fee_title".localized(), info: "3000 ₮", infoProperty: .DefaultAmount
                ),
            ]
        )
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Confirm)
        title = "transaction_confirm_button_title".localized()
        model.sections = [accountSection, productSection, beneficiarySection]
        tableView.reloadData()
        onContinue = {
            self.handleContinue()
        }
    }
    
    private func handleContinue() {
        let loaderViewController = TransactionLoadingController(nibName: "TransactionLoadingController", bundle: nil)
        navigationController?.pushViewController(loaderViewController, animated: true)
        let detailViewController = TransactionDetailViewController(nibName: "TableViewController", bundle: nil)
        detailViewController.model.sections = [accountSection, productSection, beneficiarySection]
        detailViewController.controllerTitle = "deposit_close_deposit_title".localized()
        detailViewController.onFinish = {
            self.navigationController?.popToRootViewController(animated: true)
            if detailViewController.isSuccessful {
                NotificationCenter.default.post(name: Notification.Name(NotificationConstants.REFRESH_ACCOUNTS), object: nil)
            }
        }
        ConnectionFactory.closeDeposit(
            model: closeModel,
            success: { response in
                let resultMessage = response.getMessage()
                detailViewController.transactionMessage = resultMessage
                loaderViewController.setResult(
                    isSuccessful: true,
                    message: resultMessage,
                    completion: {
                        detailViewController.isSuccessful = true
                        self.navigationController?.pushViewController(detailViewController, animated: false)
                    }
                )
            },
            failed: { reason in
                let resultMessage = reason?.MESSAGE_DESC ?? ""
                detailViewController.transactionMessage = resultMessage
                loaderViewController.setResult(isSuccessful: false, message: resultMessage, completion: {
                    detailViewController.isSuccessful = false
                    self.navigationController?.pushViewController(detailViewController, animated: false)
                })
            }
        )
    }
}
