//
//  CloseDepositRequest.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/26/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class CloseDepositRequest: BaseRequest {
    var body = Body()

    private enum CodingKeys: String, CodingKey {
        case body
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(body, forKey: .body)
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    override init() {
        super.init()
        header.__SRVCID__ = "CFDB"
    }

    struct Body: Codable {
        var USERCHOSENACCID = ""
        var LEDGER_BAL = ""
        var ACCOUNTID = ""
        var BREAKING_DATE = ""
        var USERENTEREDAMOUNT = ""
    }
}
