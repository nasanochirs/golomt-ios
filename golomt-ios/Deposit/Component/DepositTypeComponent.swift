//
//  DepositTypeComponent.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/23/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class DepositTypeComponent: UIView {
    @IBOutlet var containerView: UIView!
    @IBOutlet var typeImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        initComponent()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initComponent()
    }
    
    func setData(title: String, description: String, image: String) {
        titleLabel.text = title.localized()
        descriptionLabel.text = description.localized()
    }

    private func initComponent() {
        let bundle = Bundle(for: DepositTypeComponent.self)
        bundle.loadNibNamed("DepositTypeComponent", owner: self, options: nil)
        addSubview(containerView)
        containerView.frame = bounds
        containerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        containerView.corner(cornerRadius: 15)
        if traitCollection.userInterfaceStyle == .dark {
            containerView.backgroundColor = UIColor.gray.withAlphaComponent(0.4)
        } else {
            containerView.backgroundColor = .defaultBlueGradientEnd
        }
        titleLabel.useXXLargeFont()
        titleLabel.makeBold()
        titleLabel.textColor = .white
        titleLabel.sizeToFit()
        titleLabel.adjustsFontSizeToFitWidth = true
        descriptionLabel.useMediumFont()
        descriptionLabel.textColor = .white
        descriptionLabel.adjustsFontSizeToFitWidth = true
        typeImageView.isHidden = !isIPhoneSE()
    }
}
