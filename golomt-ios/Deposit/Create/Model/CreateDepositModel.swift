//
//  CreateDepositModel.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/24/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class CreateDepositModel {
    var type: AccountType? = nil
    var categoryList = [Category]()
    var selectedCategory: Category? = nil
    
    enum AccountType: String {
        case Savings = "SDD"
        case Term = "SDN"
        case Demand = "SBA"
    }
    
    class Category {
        var type: String = ""
        var description: String = ""
        var currency: String = ""
        var minimumAmount: Double = 0.0
        var monthlyAmount: Double = 0.0
        var duration: String = ""
        var remitterAccount: AccountResponse.Account? = nil
        
        init(type: String, description: String, currency: String, minimumAmount: Double, monthlyAmount: Double, duration: String) {
            self.type = type
            self.description = description
            self.currency = currency
            self.minimumAmount = minimumAmount
            self.monthlyAmount = monthlyAmount
            self.duration = duration
        }
        
        init(type: String, description: String, currency: String, minimumAmount: Double, duration: String) {
            self.type = type
            self.description = description
            self.currency = currency
            self.minimumAmount = minimumAmount
            self.duration = duration
        }
        
        init(type: String, description: String, currency: String, minimumAmount: Double) {
            self.type = type
            self.description = description
            self.currency = currency
            self.minimumAmount = minimumAmount
        }
    }
}
