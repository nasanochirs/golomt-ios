//
//  TermSavingsDepositResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/25/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class CreateDepositResponse: BaseResponse {
    let deposit: DepositCreateResponse?
    
    private enum CodingKeys: String, CodingKey {
        case `default`
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(deposit, forKey: .default)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.deposit = try container.decodeIfPresent(DepositCreateResponse.self, forKey: .default)
        try super.init(from: decoder)
    }
    
    struct DepositCreateResponse: Codable {
        var BACKEND_REF_ID: String?
        var CRN_ONE: String?
        var AMT_ONE: String?
        var PRODUCTTYPE: String?
        var FORM_REQ_STATE: String?
    }
}
