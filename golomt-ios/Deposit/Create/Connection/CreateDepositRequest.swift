//
//  TermDemandDepositRequest.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/25/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class CreateDepositRequest: BaseRequest {
    var body = Body()

    private enum CodingKeys: String, CodingKey {
        case body
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(body, forKey: .body)
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    override init() {
        super.init()
        header.__SRVCID__ = "CNTAR"
    }

    struct Body: Codable {
        var PRODUCTTYPE = ""
        var CRN_ONE = ""
        var REMITTER_ACCOUNT = ""
        var AMT_ONE = ""
        var DEBIT_ACCT_CRN = ""
        var PRIMARY_SOLID = ""
        var SHORTNAME = ""
        var USERCHOSENMONTHS = ""
        var FULLNAME = ""
    }
}
