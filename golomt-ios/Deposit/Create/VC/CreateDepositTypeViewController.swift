//
//  CreateDepositTypeViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/23/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class CreateDepositTypeViewController: BaseUIViewController {
    @IBOutlet var firstTypeComponent: DepositTypeComponent!
    @IBOutlet var secondTypeComponent: DepositTypeComponent!
    @IBOutlet var thirdTypeComponent: DepositTypeComponent!

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "deposit_create_type_title".localized()
        firstTypeComponent.setData(title: "deposit_create_type_savings_title", description: "deposit_create_type_savings_label", image: "")
        firstTypeComponent.tag = 0
        secondTypeComponent.setData(title: "deposit_create_type_term_title", description: "deposit_create_type_term_label", image: "")
        secondTypeComponent.tag = 1
        thirdTypeComponent.setData(title: "deposit_create_type_demand_title", description: "deposit_create_type_demand_label", image: "")
        thirdTypeComponent.tag = 2

        firstTypeComponent.addTapGesture(tapNumber: 1, target: self, action: #selector(onComponentChoose(_:)))
        secondTypeComponent.addTapGesture(tapNumber: 1, target: self, action: #selector(onComponentChoose(_:)))
        thirdTypeComponent.addTapGesture(tapNumber: 1, target: self, action: #selector(onComponentChoose(_:)))
    }

    @objc private func onComponentChoose(_ target: UITapGestureRecognizer) {
        guard let tag = target.view?.tag else {
            return
        }
        let model = CreateDepositModel()
        switch tag {
        case 0:
            model.type = .Savings
        case 1:
            model.type = .Term
        case 2:
            model.type = .Demand
        default:
            break
        }
        let contractVC = ContractViewController(nibName: "ContractViewController", bundle: nil)
        contractVC.contractLink = model.type?.getDepositInfo() ?? ""
        contractVC.controllerTitle = "deposit_create_info_title".localized()
        contractVC.buttonTitle = "contract_button_continue_title".localized()
        contractVC.onButtonClick = {
            let depositStoryBoard: UIStoryboard = UIStoryboard(name: "Deposit", bundle: nil)
            let viewController = depositStoryBoard.instantiateViewController(withIdentifier: "DepositCategoryID")
            let categoryVC = viewController as? CreateDepositCategoryViewController
            guard let unwrappedVC = categoryVC else {
                return
            }
            unwrappedVC.createModel = model
            self.navigationController?.pushViewController(unwrappedVC, animated: true)
        }
        navigationController?.pushViewController(contractVC, animated: true)
    }
}

private extension RawRepresentable where RawValue == String {
    func getDepositInfo() -> String {
        switch rawValue {
        case "SDD":
            switch getLanguage() {
            case "en":
                return "https://m.egolomt.mn/social/prod_savingsdeposit_eng.html"
            case "mn":
                return "https://m.egolomt.mn/social/prod_savingsdeposit_mng.html"
            default:
                return ""
            }
        case "SDN":
            switch getLanguage() {
            case "en":
                return "https://m.egolomt.mn/social/prod_termdeposit_eng.html"
            case "mn":
                return "https://m.egolomt.mn/social/prod_termdeposit_mng.html"
            default:
                return ""
            }
        case "SBA":
            switch getLanguage() {
            case "en":
                return "https://m.egolomt.mn/social/prod_demanddeposit_eng.html"
            case "mn":
                return "https://m.egolomt.mn/social/prod_demanddeposit_mng.html"
            default:
                return ""
            }
        default:
            return ""
        }
    }
}
