//
//  CreateDepositCategoryViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/24/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class CreateDepositCategoryViewController: BaseUIViewController {
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var containerStackView: UIStackView!
    @IBOutlet var questionLabel: UILabel!
    var createModel = CreateDepositModel()

    lazy var continueButton: DefaultGradientButton = {
        let button = DefaultGradientButton()
        button.titleText = "card_order_type_button_title"
        button.addTarget(self, action: #selector(continueAction), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        createModel.categoryList = []
        switch createModel.type {
        case .Savings:
            title = "deposit_create_type_savings_title".localized()
            questionLabel.text = "deposit_create_type_savings_question_label".localized()
        case .Term:
            title = "deposit_create_type_term_title".localized()
            questionLabel.text = "deposit_create_type_term_question_label".localized()
        case .Demand:
            title = "deposit_create_type_demand_title".localized()
            questionLabel.text = "deposit_create_type_demand_question_label".localized()
        default:
            break
        }
        questionLabel.useXXLargeFont()
        questionLabel.makeBold()
        questionLabel.textColor = .defaultSecondaryText
        collectionView.registerCell(nibName: "CreateDepositCategoryCell")

        showLoader()
        ConnectionFactory.fetchList(
            code: createModel.type?.rawValue ?? "",
            success: { response in
                self.hideLoader()
                response.list?.sorted { $0.CM_CODE.orEmpty.toDigits < $1.CM_CODE.orEmpty.toDigits }.forEach { item in
                    let splitted = item.CD_DESC.orEmpty.components(separatedBy: "|")
                    switch self.createModel.type {
                    case .Term:
                        var code = item.CM_CODE.orEmpty
                        guard let index = code.index(code.startIndex, offsetBy: 1, limitedBy: code.endIndex) else {
                            return
                        }
                        code.remove(at: index)
                        self.showLoader()
                        ConnectionFactory.fetchList(
                            code: code,
                            success: { response in
                                self.hideLoader()
                                response.list?.sorted { $0.CM_CODE.orEmpty.toInt < $1.CM_CODE.orEmpty.toInt }.forEach { month in
                                    let duration = month.CD_DESC.orEmpty.components(separatedBy: "-")
                                    self.createModel.categoryList.append(
                                        CreateDepositModel.Category(
                                            type: item.CM_CODE.orEmpty,
                                            description: month.CD_DESC.orEmpty,
                                            currency: splitted[1],
                                            minimumAmount: splitted[2].toDigits.toDouble,
                                            duration: duration.first.orEmpty
                                        )
                                    )
                                }
                                self.collectionView.reloadData()
                            },
                            failed: { reason in
                                self.hideLoader()
                                self.handleRequestFailure(reason)
                            }
                        )
                    case .Savings:
                        self.createModel.categoryList.append(
                            CreateDepositModel.Category(
                                type: item.CM_CODE.orEmpty,
                                description: splitted[0],
                                currency: splitted[1],
                                minimumAmount: splitted[2].toDigits.toDouble,
                                monthlyAmount: splitted[0].toDigits.toDouble,
                                duration: "deposit_create_type_savings_duration_label".localized()
                            )
                        )
                        self.collectionView.reloadData()
                    case .Demand:
                        ConnectionFactory.fetchList(
                            code: "SBAC",
                            success: { response in
                                self.hideLoader()
                                response.list?.forEach { currency in
                                    self.createModel.categoryList.append(
                                        CreateDepositModel.Category(
                                            type: item.CM_CODE.orEmpty,
                                            description: currency.CM_CODE.orEmpty,
                                            currency: currency.CM_CODE.orEmpty,
                                            minimumAmount: 0.0
                                        )
                                    )
                                }
                                self.collectionView.reloadData()
                            },
                            failed: { reason in
                                self.hideLoader()
                                self.handleRequestFailure(reason)
                            }
                        )
                    default:
                        break
                    }
                }
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }

    @objc private func continueAction() {
        let confirmVC = CreateDepositConfirmViewController(nibName: "TableViewController", bundle: nil)
        confirmVC.createModel = createModel
        navigationController?.pushViewController(confirmVC, animated: true)
    }
}

extension CreateDepositCategoryViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        createModel.categoryList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CreateDepositCategoryCell", for: indexPath) as! CreateDepositCategoryCell
        let category = createModel.categoryList[indexPath.row]
        cell.setDescription(category.description)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        createModel.selectedCategory = createModel.categoryList[indexPath.row]
        continueButton.widthAnchor.constraint(equalToConstant: containerStackView.bounds.size.width - 40).isActive = true
        continueButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        containerStackView.addArrangedSubview(continueButton)
        self.collectionView.layoutIfNeeded()
        self.collectionView.layoutSubviews()
        self.collectionView.scrollToItem(at: indexPath, at: .bottom, animated: true)
    }
}

extension CreateDepositCategoryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch createModel.type {
//        case .Term:
//            let width = (view.frame.size.width - 42)
//            let height = (view.frame.size.width - 42) / 3
//            return CGSize(width: width, height: height)
        default:
            let width = (view.frame.size.width - 30)
            let height = (view.frame.size.width - 30) / 5
            return CGSize(width: width, height: height)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
}
