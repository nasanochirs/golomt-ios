//
//  CreateDepositConfirmViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/25/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class CreateDepositConfirmViewController: TableViewController {
    var createModel = CreateDepositModel()

    lazy var accountField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerPlainCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultPickerButtonCell
        guard let unwrappedCell = cell else {
            return DefaultPickerButtonCell()
        }
        unwrappedCell.title = "deposit_create_confirm_account_title".localized()
        unwrappedCell.onPickerClick = {
            let controller = AccountPickerController()
            controller.onAccountSelect = { account in
                self.accountField.selectedItem = account
                self.selectedAccount = account
            }
            controller.selectedAccount = self.selectedAccount
            controller.title = unwrappedCell.title
            controller.accounts = operativeAccountList
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var savingsSection: TableViewModel.Section = {
        let category = self.createModel.selectedCategory
        return TableViewModel.Section(
            title: "deposit_create_confirm_new_deposit_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "deposit_create_confirm_type_title".localized(), info: "deposit_create_type_savings_title".localized()
                ),
                TableViewModel.Row(
                    title: "deposit_create_confirm_currency_title".localized(), info: category?.currency
                ),
                TableViewModel.Row(
                    title: "deposit_create_confirm_duration_title".localized(), info: category?.duration
                ),
                TableViewModel.Row(
                    title: "deposit_create_confirm_monthly_amount_title".localized(), info: category?.monthlyAmount.formattedWithComma
                ),
                TableViewModel.Row(
                    title: "deposit_create_confirm_minimum_amount_title".localized(), info: category?.minimumAmount.formattedWithComma
                ),
            ]
        )
    }()

    lazy var termSection: TableViewModel.Section = {
        let category = self.createModel.selectedCategory
        return TableViewModel.Section(
            title: "deposit_create_confirm_new_deposit_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "deposit_create_confirm_type_title".localized(), info: "deposit_create_type_term_title".localized()
                ),
                TableViewModel.Row(
                    title: "deposit_create_confirm_currency_title".localized(), info: category?.currency
                ),
                TableViewModel.Row(
                    title: "deposit_create_confirm_duration_title".localized(), info: category?.duration
                ),
                TableViewModel.Row(
                    title: "deposit_create_confirm_minimum_amount_title".localized(), info: category?.minimumAmount.formattedWithComma
                ),
            ]
        )
    }()

    lazy var demandSection: TableViewModel.Section = {
        let category = self.createModel.selectedCategory
        return TableViewModel.Section(
            title: "deposit_create_confirm_new_deposit_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "deposit_create_confirm_type_title".localized(), info: "deposit_create_type_demand_title".localized()
                ),
                TableViewModel.Row(
                    title: "deposit_create_confirm_currency_title".localized(), info: category?.currency
                ),
                TableViewModel.Row(
                    title: "deposit_create_confirm_minimum_amount_title".localized(), info: category?.minimumAmount.formattedWithComma
                ),
            ]
        )
    }()

    lazy var accountSection: TableViewModel.Section = {
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(
                    title: nil, info: nil, infoProperty: nil, cell: self.accountField
                ),
            ]
        )
    }()

    lazy var accountInfoSection: TableViewModel.Section = {
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(
                    title: "deposit_detail_account_title".localized(), info: self.selectedAccount?.ACCT_NUMBER, infoProperty: nil, cell: nil
                ),
                TableViewModel.Row(
                    title: "deposit_detail_new_account_title".localized(), info: nil, infoProperty: nil, cell: nil
                ),
            ]
        )
    }()

    lazy var warningField: DefaultLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLabelCell
        guard let unwrappedCell = cell else {
            return DefaultLabelCell()
        }
        unwrappedCell.labelText = "deposit_detail_warning_title".localized()
        return unwrappedCell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Confirm)
        title = "deposit_create_confirm_title".localized()
        switch createModel.type {
        case .Savings:
            model.sections = [savingsSection, accountSection]
        case .Term:
            model.sections = [termSection, accountSection]
        case .Demand:
            model.sections = [demandSection, accountSection]
            let type = createModel.selectedCategory?.type ?? ""
            let currency = createModel.selectedCategory?.currency ?? ""
            showLoader()
            ConnectionFactory.fetchParameterValue(
                parameterName: type + "_" + currency + "_MIN",
                success: { response in
                    self.hideLoader()
                    self.createModel.selectedCategory?.minimumAmount = response.parameter?.P_VAL?.toDouble ?? 0.0
                    self.demandSection.rows[2].info = self.createModel.selectedCategory?.minimumAmount.formattedWithComma
                    self.tableView.reloadData()
                },
                failed: { reason in
                    self.hideLoader()
                    self.handleRequestFailure(reason)
                }
            )
        default:
            break
        }
        tableView.reloadData()
        onContinue = {
            self.handleContinue()
        }
    }
    
    private func handleContinue() {
        if accountField.hasError() {
            return
        }
        if createModel.selectedCategory?.minimumAmount ?? 0.0 > selectedAccount!.ACCT_BALANCE.toAmount {
            let dialog = errorDialog(message: "deposit_create_confirm_error_balance_label".localized())
            present(dialog, animated: true, completion: nil)
            return
        }
        let contractVC = ContractViewController(nibName: "ContractViewController", bundle: nil)
        contractVC.contractLink = createModel.type?.getDepositContract() ?? ""
        contractVC.controllerTitle = "contract_title".localized()
        contractVC.buttonTitle = "accept_button_title".localized()
        contractVC.checkBoxLabel = "deposit_create_confirm_accept_contract_label".localized()
        contractVC.onButtonClick = {
            self.createModel.selectedCategory?.remitterAccount = self.selectedAccount
            let loaderViewController = TransactionLoadingController(nibName: "TransactionLoadingController", bundle: nil)
            self.navigationController?.pushViewController(loaderViewController, animated: true)
            let detailViewController = TransactionDetailViewController(nibName: "TableViewController", bundle: nil)
            detailViewController.controllerTitle = "deposit_detail_title".localized()
            detailViewController.onFinish = {
                if let cardListVC = self.navigationController?.viewControllers[1] {
                    self.navigationController?.popToViewController(cardListVC, animated: true)
                }
                if detailViewController.isSuccessful {
                    NotificationCenter.default.post(name: Notification.Name(NotificationConstants.REFRESH_ACCOUNTS), object: nil)
                }
            }
            detailViewController.model.sections = [
                self.model.sections[0],
            ]
            ConnectionFactory.createDeposit(
                model: self.createModel,
                success: { response in
                    self.accountInfoSection.rows[1].info = response.deposit?.BACKEND_REF_ID
                    let resultMessage = response.getMessage()
                    detailViewController.transactionMessage = resultMessage
                    detailViewController.model.sections.append(contentsOf: [
                        self.accountInfoSection,
                        TableViewModel.Section(
                            rows: [
                                TableViewModel.Row(
                                    cell: self.warningField
                                ),
                            ]
                        )
                    ])
                    loaderViewController.setResult(isSuccessful: true, message: resultMessage, completion: {
                        detailViewController.isSuccessful = true
                        self.navigationController?.pushViewController(detailViewController, animated: false)
                    })
                },
                failed: { reason in
                    let resultMessage = reason?.MESSAGE_DESC ?? ""
                    detailViewController.transactionMessage = resultMessage
                    loaderViewController.setResult(isSuccessful: false, message: resultMessage, completion: {
                        detailViewController.isSuccessful = false
                        self.navigationController?.pushViewController(detailViewController, animated: false)
                    })
                }
            )
        }
        navigationController?.pushViewController(contractVC, animated: true)
    }
}

private extension RawRepresentable where RawValue == String {
    func getDepositContract() -> String {
        switch rawValue {
        case "SDD":
            switch getLanguage() {
            case "en":
                return "https://m.egolomt.mn/social/contract_tua_eng.html"
            case "mn":
                return "https://m.egolomt.mn/social/contract_tua_mng.html"
            default:
                return ""
            }
        case "SDN":
            switch getLanguage() {
            case "en":
                return "https://m.egolomt.mn/social/contract_tua_eng.html"
            case "mn":
                return "https://m.egolomt.mn/social/contract_tua_mng.html"
            default:
                return ""
            }
        case "SBA":
            switch getLanguage() {
            case "en":
                return "https://m.egolomt.mn/social/contract_sba_eng.html"
            case "mn":
                return "https://m.egolomt.mn/social/contract_sba_mng.html"
            default:
                return ""
            }
        default:
            return ""
        }
    }
}
