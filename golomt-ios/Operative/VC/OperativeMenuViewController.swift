//
//  OperativeMenuViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class OperativeMenuViewController: MenuTableViewController {
    lazy var detailSection: MenuTableViewModel.Section = {
        var section = MenuTableViewModel.Section(
            rows: [
                MenuTableViewModel.Row(
                    icon: UIImage(named: "operative"), title: "operative_create_title"
                ),
                MenuTableViewModel.Row(
                    icon: UIImage(named: "operative"), title: "operative_close_title"
                )
            ]
        )
        return section
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "operative_title".localized()
        tableView.registerCell(nibName: "DefaultMenuCell")
        tableView.rowHeight = 70
        model.sections = [detailSection]

        onClick = { row in
            switch row {
                case 0:
                    let createVC = OperativeCreateViewController(nibName: "TableViewController", bundle: nil)
                    self.navigationController?.pushViewController(createVC, animated: true)
                case 1:
                    let closeVC = OperativeCloseViewController(nibName: "TableViewController", bundle: nil)
                    self.navigationController?.pushViewController(closeVC, animated: true)
                default:
                    break
            }
        }
        tableView.reloadData()
    }
}
