//
//  OperativeCreateModel.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class OperativeCreateModel {
    var product: DataListResponse.Item? = nil
    var remitter: AccountResponse.Account? = nil
    var currency: CurrencyConstants? = nil
    var minimumAmount: String = ""
}
