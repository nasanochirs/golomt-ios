//
//  OperativeCreateViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class OperativeCreateViewController: TableViewController {
    var createModel = OperativeCreateModel()
    var productList = [DataListResponse.Item]()
    var currencyList = [CurrencyConstants]()
    
    lazy var productField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerPlainCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultPickerButtonCell
        guard let unwrappedCell = cell else {
            return DefaultPickerButtonCell()
        }
        unwrappedCell.title = "operative_create_product_title".localized()
        unwrappedCell.onPickerClick = {
            let controller = PickerController()
            controller.onItemSelect = { item in
                unwrappedCell.selectedItem = item
                controller.popupController?.dismiss(completion: {
                    guard let item = item as? DataListResponse.Item else {
                        return
                    }
                    self.fetchCurrency(of: item)
                })
            }
            controller.selectedItem = unwrappedCell.selectedItem
            controller.title = unwrappedCell.title
            controller.list = self.productList
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()
    
    lazy var currencyField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerPlainCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultPickerButtonCell
        guard let unwrappedCell = cell else {
            return DefaultPickerButtonCell()
        }
        unwrappedCell.title = "operative_create_currency_title".localized()
        unwrappedCell.onPickerClick = {
            let controller = PickerController()
            controller.onItemSelect = { item in
                unwrappedCell.selectedItem = item
                controller.popupController?.dismiss(completion: {
                    let product = self.productField.selectedItem as? DataListResponse.Item
                    let currency = item as? CurrencyConstants
                    self.fetchMinimumAmount(of: product, with: currency)
                })
            }
            controller.selectedItem = unwrappedCell.selectedItem
            controller.title = unwrappedCell.title
            controller.list = self.currencyList
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()
    
    lazy var remitterAccountField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerPlainCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultPickerButtonCell
        guard let unwrappedCell = cell else {
            return DefaultPickerButtonCell()
        }
        unwrappedCell.title = "operative_create_account_for_minimum_amount_title".localized()
        unwrappedCell.onPickerClick = {
            let controller = AccountPickerController()
            controller.selectedAccount = self.selectedAccount
            controller.onAccountSelect = { account in
                self.selectedAccount = account
                unwrappedCell.selectedItem = account
            }
            controller.title = unwrappedCell.title
            controller.accounts = operativeAccountList
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Continue)
        title = "operative_create_title".localized()
        self.model.sections = [
            TableViewModel.Section(
                title: "",
                rows: [
                    TableViewModel.Row(
                        cell: self.productField
                    ),
                    TableViewModel.Row(
                        cell: self.currencyField
                    ),
                    TableViewModel.Row(
                        title: "operative_create_minimum_amount_title".localized()
                    ),
                    TableViewModel.Row(
                        cell: self.remitterAccountField
                    ),
                ]
            ),
        ]
        tableView.reloadData()
        onContinue = {
            self.handleContinue()
        }
        self.fetchProducts()
    }
    
    private func fetchProducts() {
        self.showLoader()
        ConnectionFactory.fetchList(
            code: "DCA",
            success: { response in
                self.hideLoader()
                self.productList = response.list ?? []
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
    
    private func fetchCurrency(of item: DataListResponse.Item) {
        var codeType = ""
        switch item.CM_CODE {
        case "CA655":
            codeType = "DCO"
        case "CA601":
            codeType = "DCD"
        default:
            break
        }
        self.showLoader()
        self.currencyList = []
        ConnectionFactory.fetchList(
            code: codeType,
            success: { response in
                self.hideLoader()
                response.list?.forEach { item in
                    let currency = CurrencyConstants(rawValue: item.CM_CODE.orEmpty)
                    if currency != nil {
                        self.currencyList.append(currency!)
                    }
                }
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
    
    private func fetchMinimumAmount(of item: DataListResponse.Item?, with currency: CurrencyConstants?) {
        guard let item = item else {
            return
        }
        guard let currency = currency else {
            return
        }
        let productCode = item.CM_CODE.orEmpty
        self.showLoader()
        ConnectionFactory.fetchParameterValue(
            parameterName: productCode + "_" + currency.rawValue + "_MIN",
            success: { response in
                self.hideLoader()
                let minimumAmount = response.parameter?.P_VAL?.toDouble ?? 0.0
                self.createModel.minimumAmount = currency.rawValue + "|" + String(minimumAmount)
                self.createModel.currency = currency
                let currencySymbol = currency.rawValue.toCurrencySymbol
                self.model.sections[0].rows[2].info = minimumAmount.formattedWithComma + " " + currencySymbol
                self.tableView.reloadData()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
    
    private func handleContinue() {
        if self.productField.hasError() || self.currencyField.hasError() || self.remitterAccountField.hasError() {
            return
        }
        self.createModel.product = productField.selectedItem as? DataListResponse.Item
        self.createModel.remitter = remitterAccountField.selectedItem as? AccountResponse.Account
        let contractVC = ContractViewController(nibName: "ContractViewController", bundle: nil)
        switch getLanguage() {
        case "en":
            contractVC.contractLink = "https://m.egolomt.mn/social/contract_opr_eng.html"
        case "mn":
            contractVC.contractLink = "https://m.egolomt.mn/social/contract_opr_mng.html"
        default:
            break
        }
        contractVC.controllerTitle = "contract_title".localized()
        contractVC.buttonTitle = "accept_button_title".localized()
        contractVC.checkBoxLabel = "contract_accept_label".localized()
        contractVC.onButtonClick = {
            let confirmVC = OperativeCreateConfirmViewController(nibName: "TableViewController", bundle: nil)
            confirmVC.createModel = self.createModel
            self.navigationController?.pushViewController(confirmVC, animated: true)
        }
        navigationController?.pushViewController(contractVC, animated: true)
    }
}
