//
//  OperativeCreateConfirmViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class OperativeCreateConfirmViewController: TableViewController {
    var createModel = OperativeCreateModel()

    lazy var warningField: DefaultLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLabelCell
        guard let unwrappedCell = cell else {
            return DefaultLabelCell()
        }
        unwrappedCell.labelText = "operative_detail_warning_label".localized()
        return unwrappedCell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Confirm)
        title = "transaction_confirm_button_title".localized()
        self.model.sections = [
            TableViewModel.Section(
                title: "",
                rows: [
                    TableViewModel.Row(
                        title: "operative_create_product_title".localized(),
                        info: self.createModel.product?.CD_DESC
                    ),
                    TableViewModel.Row(
                        title: "operative_create_currency_title".localized(),
                        info: self.createModel.currency?.rawValue
                    ),
                    TableViewModel.Row(
                        title: "operative_create_minimum_amount_title".localized(),
                        info: self.createModel.minimumAmount.formattedWithComma
                    ),
                    TableViewModel.Row(
                        title: "operative_create_account_for_minimum_amount_title".localized(),
                        info: self.createModel.remitter?.ACCT_NUMBER
                    ),
                ]
            ),
        ]
        tableView.reloadData()
        onContinue = {
            self.handleContinue()
        }
    }

    private func handleContinue() {
        let loaderViewController = TransactionLoadingController(nibName: "TransactionLoadingController", bundle: nil)
        self.navigationController?.pushViewController(loaderViewController, animated: true)
        let detailViewController = TransactionDetailViewController(nibName: "TableViewController", bundle: nil)
        detailViewController.controllerTitle = "operative_title".localized()
        detailViewController.onFinish = {
            if let cardListVC = self.navigationController?.viewControllers[1] {
                self.navigationController?.popToViewController(cardListVC, animated: true)
            }
            if detailViewController.isSuccessful {
                NotificationCenter.default.post(name: Notification.Name(NotificationConstants.REFRESH_ACCOUNTS), object: nil)
            }
        }
        detailViewController.model.sections = self.model.sections
        ConnectionFactory.createOperative(
            model: self.createModel,
            success: { response in
                let resultMessage = response.getMessage()
                detailViewController.transactionMessage = resultMessage
                detailViewController.model.sections.append(
                    contentsOf: [
                        TableViewModel.Section(
                            rows: [
                                TableViewModel.Row(
                                    title: "operative_detail_new_account_title".localized(), info: response.operative?.NEW_ACCOUNT
                                ),
                            ]
                        ),
                        TableViewModel.Section(
                            rows: [
                                TableViewModel.Row(
                                    cell: self.warningField
                                ),
                            ]
                        ),
                    ]
                )
                loaderViewController.setResult(isSuccessful: true, message: resultMessage, completion: {
                    detailViewController.isSuccessful = true
                    self.navigationController?.pushViewController(detailViewController, animated: false)
                })
            },
            failed: { reason in
                let resultMessage = reason?.MESSAGE_DESC ?? ""
                detailViewController.transactionMessage = resultMessage
                loaderViewController.setResult(isSuccessful: false, message: resultMessage, completion: {
                    detailViewController.isSuccessful = false
                    self.navigationController?.pushViewController(detailViewController, animated: false)
                })
            }
        )
    }
}
