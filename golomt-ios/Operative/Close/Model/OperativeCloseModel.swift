//
//  OperativeCloseModel.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class OperativeCloseModel {
    var minimumAmount = "MNT|0.0"
    var operativeAccount: AccountResponse.Account? = nil
    var beneficiaryAccount: AccountResponse.Account? = nil
    var reason: DataListResponse.Item? = nil
}
