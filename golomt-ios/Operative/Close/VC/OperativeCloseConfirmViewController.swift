//
//  OperativeCloseConfirmViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class OperativeCloseConfirmViewController: TableViewController {
    var closeModel = OperativeCloseModel()

    lazy var accountSection: TableViewModel.Section = {
        let number = closeModel.operativeAccount?.ACCT_NUMBER ?? ""
        let currency = closeModel.operativeAccount?.ACCT_CURRENCY ?? ""
        return TableViewModel.Section(
            title: "operative_close_confirm_account_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "operative_close_confirm_account_to_close_title".localized(), info: number + " " + currency
                ),
                TableViewModel.Row(
                    title: "operative_close_confirm_reason_title".localized(), info: self.closeModel.reason?.CD_DESC
                ),
            ]
        )
    }()

    lazy var beneficiarySection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "operative_close_confirm_beneficiary_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "operative_close_confirm_beneficiary_account_title".localized(), info: closeModel.beneficiaryAccount?.ACCT_NUMBER
                ),
                TableViewModel.Row(
                    title: "operative_close_fee_title".localized(), info: self.closeModel.minimumAmount.toAmountWithCurrencySymbol, infoProperty: .DefaultAmount
                ),
            ]
        )
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Confirm)
        title = "transaction_confirm_button_title".localized()
        model.sections = [accountSection, beneficiarySection]
        tableView.reloadData()
        onContinue = {
            self.handleContinue()
        }
    }

    private func handleContinue() {
        let loaderViewController = TransactionLoadingController(nibName: "TransactionLoadingController", bundle: nil)
        navigationController?.pushViewController(loaderViewController, animated: true)
        let detailViewController = TransactionDetailViewController(nibName: "TableViewController", bundle: nil)
        detailViewController.model.sections = [accountSection, beneficiarySection]
        detailViewController.controllerTitle = "operative_close_title".localized()
        detailViewController.onFinish = {
            self.navigationController?.popToRootViewController(animated: true)
            if detailViewController.isSuccessful {
                NotificationCenter.default.post(name: Notification.Name(NotificationConstants.REFRESH_ACCOUNTS), object: nil)
            }
        }
        ConnectionFactory.closeOperative(
            model: closeModel,
            success: { response in
                let resultMessage = response.getMessage()
                detailViewController.transactionMessage = resultMessage
                loaderViewController.setResult(
                    isSuccessful: true,
                    message: resultMessage,
                    completion: {
                        detailViewController.isSuccessful = true
                        self.navigationController?.pushViewController(detailViewController, animated: false)
                    }
                )
            },
            failed: { reason in
                let resultMessage = reason?.MESSAGE_DESC ?? ""
                detailViewController.transactionMessage = resultMessage
                loaderViewController.setResult(isSuccessful: false, message: resultMessage, completion: {
                    detailViewController.isSuccessful = false
                    self.navigationController?.pushViewController(detailViewController, animated: false)
                })
            }
        )
    }
}
