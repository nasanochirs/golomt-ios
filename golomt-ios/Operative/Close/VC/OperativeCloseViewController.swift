//
//  OperativeCloseViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class OperativeCloseViewController: TableViewController {
    var filteredList = [AccountResponse.Account]()
    var reasonList = [DataListResponse.Item]()
    var closeModel = OperativeCloseModel()

    lazy var closeAccountField: DefaultAccountPickerCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAccountPickerCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAccountPickerCell
        guard let unwrappedCell = cell else {
            return DefaultAccountPickerCell()
        }
        unwrappedCell.onPickerClick = {
            self.showSenderAccountPicker(self.filteredList)
        }
        unwrappedCell.defaultNicknameText = "operative_close_account_title".localized()
        unwrappedCell.defaultNumberText = "operative_close_account_label".localized()
        self.onAccountSelect = { account in
            self.handleAccountSelect(account)
        }
        self.accountPickerTitle = "operative_close_account_title".localized()
        return unwrappedCell
    }()

    lazy var beneficiaryAccountField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerPlainCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultPickerButtonCell
        guard let unwrappedCell = cell else {
            return DefaultPickerButtonCell()
        }
        unwrappedCell.title = "operative_close_account_for_remainder_title".localized()
        unwrappedCell.onPickerClick = {
            let controller = AccountPickerController()
            controller.onAccountSelect = { account in
                unwrappedCell.selectedItem = account
                self.closeModel.beneficiaryAccount = account
            }
            controller.selectedAccount = self.closeModel.beneficiaryAccount
            controller.title = unwrappedCell.title
            controller.accounts = operativeAccountList.filter {
                $0.ACCT_NUMBER != self.selectedAccount?.ACCT_NUMBER
            }
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var reasonField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerPlainCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultPickerButtonCell
        guard let unwrappedCell = cell else {
            return DefaultPickerButtonCell()
        }
        unwrappedCell.title = "operative_close_reason_title".localized()
        unwrappedCell.onPickerClick = {
            let controller = PickerController()
            controller.onItemSelect = { item in
                unwrappedCell.selectedItem = item
                controller.popupController?.dismiss()
            }
            controller.selectedItem = unwrappedCell.selectedItem
            controller.title = unwrappedCell.title
            controller.list = self.reasonList
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var closeSection: TableViewModel.Section = {
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(
                    cell: self.closeAccountField
                ),
                TableViewModel.Row(
                    title: "operative_close_fee_title".localized(), info: self.closeModel.minimumAmount.toAmountWithCurrencySymbol, infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    cell: self.beneficiaryAccountField
                ),
                TableViewModel.Row(
                    cell: self.reasonField
                ),
            ]
        )
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Continue)
        title = "operative_close_title".localized()
//        accountControllerTitle = "deposit_close_account_to_close_title".localized()

        if selectedAccount != nil {
            handleAccountSelect(selectedAccount!)
        }
        
        onContinue = {
            self.handleContinue()
        }
        model.sections = [self.closeSection]
        tableView.reloadData()
        self.fetchAccountList()
    }

    private func fetchAccountList() {
        showLoader()
        ConnectionFactory.fetchParameterValue(
            parameterName: "ACC_CLOSE_PROD",
            success: { response in
                self.hideLoader()
                self.filteredList = []
                response.parameter?.P_VAL.orEmpty.components(separatedBy: "|").forEach { product in
                    self.filteredList.append(contentsOf: operativeAccountList.filter {
                        $0.PRODUCT_CATEGORY.orEmpty.contains(product)
                    })
                }
                self.fetchReasonList()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }

    private func fetchReasonList() {
        showLoader()
        ConnectionFactory.fetchList(
            code: "RSN",
            success: { response in
                self.hideLoader()
                self.reasonList = response.list ?? []
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }

    private func handleAccountSelect(_ account: AccountResponse.Account) {
        self.selectedAccount = account
        self.closeAccountField.account = account
        self.beneficiaryAccountField.selectedItem = nil
        self.closeModel.beneficiaryAccount = nil
        var code = ""
        switch account.ACCT_TYPE {
        case "SB701":
            code = "ACC_DEP"
        case "QC651", "QC652", "CA653", "CA654":
            return
        default:
            code = "ACC_OPR"
        }
        self.showLoader()
        ConnectionFactory.fetchParameterValue(
            parameterName: code,
            success: { response in
                self.hideLoader()
                self.closeModel.minimumAmount = response.parameter?.P_VAL ?? "MNT|0"
                self.closeSection.rows[1].info = self.closeModel.minimumAmount.toAmountWithCurrencySymbol
                self.tableView.reloadData()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }

    private func handleContinue() {
        if self.closeAccountField.hasError() {
            showSenderAccountPicker(self.filteredList)
            return
        }
        if self.beneficiaryAccountField.hasError() || self.reasonField.hasError() {
            return
        }
        let confirmVC = OperativeCloseConfirmViewController(nibName: "TableViewController", bundle: nil)
        closeModel.operativeAccount = self.selectedAccount
        closeModel.beneficiaryAccount = self.beneficiaryAccountField.selectedItem as? AccountResponse.Account
        closeModel.reason = self.reasonField.selectedItem as? DataListResponse.Item
        confirmVC.closeModel = self.closeModel
        navigationController?.pushViewController(confirmVC, animated: true)
    }
}
