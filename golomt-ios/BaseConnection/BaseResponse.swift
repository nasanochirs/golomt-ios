//
//  BaseResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/4/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class BaseResponse: Codable {
    var header: Header?

    struct Header: Codable {
        let MESSAGE: [HeaderMessage]?
        let SESSION: HeaderSession?
    }

    struct HeaderMessage: Codable {
        let MESSAGE_TYPE: String
        let MESSAGE_DESC: String
        let MESSAGE_CODE: String
    }

    struct HeaderSession: Codable {
        let SESSION_ID: String?
        let FORM_ID: String?
    }

    func getMessage() -> String {
        var message = ""
        header?.MESSAGE?.forEach { headerMessage in
            message = message + " " + headerMessage.MESSAGE_DESC
        }
        return message
    }
}
