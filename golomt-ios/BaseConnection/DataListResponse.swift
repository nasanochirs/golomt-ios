//
//  DataListResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class DataListResponse: BaseResponse {
    let list: [Item]?

    private enum CodingKeys: String, CodingKey {
        case resultList_REC
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(list, forKey: .resultList_REC)
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.list = try container.decodeIfPresent([Item].self, forKey: .resultList_REC)
        try super.init(from: decoder)
    }

    struct Item: Codable {
        var CM_CODE: String?
        var CD_DESC: String?
    }
}
