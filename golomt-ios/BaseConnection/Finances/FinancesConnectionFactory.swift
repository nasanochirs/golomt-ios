//
//  FinancesConnectionFactory.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class FinancesConnectionFactory {
    static func fetchToken(sessionId: String, success: @escaping (FinancesTokenResponse) -> Void, failed: @escaping ConnectionError) {
        let request = FinancesTokenRequest()
        request.body.sid = sessionId[4...26]
        FinancesConnectionManager<FinancesTokenResponse>.post(
            params: request,
            requestUrl: "https://pfm.golomtbank.com:8443/smartapi/auth/login?language=mn",
            success: success,
            failed: failed
        )
    }
    
    static func fetchMonthlyTransactionList(date: Date, success: @escaping ([FinancesMonthlyTransactionResponse]) -> Void, failed: @escaping ConnectionError) {
        let endOfMonth = date.endOfMonth
        FinancesConnectionManager<FinancesMonthlyTransactionResponse>.getArray(
            requestUrl: getFinancesRequestUrl() + "transaction/raw/monthly_total/?date_before=" + endOfMonth.toFormatted,
            success: success,
            failed: failed
        )
    }
    
    static func fetchCategoryList(success: @escaping ([FinancesCategoryResponse]) -> Void, failed: @escaping ConnectionError) {
        FinancesConnectionManager<FinancesCategoryResponse>.getArray(
            requestUrl: getFinancesRequestUrl() + "transaction/category/",
            success: success,
            failed: failed
        )
    }
    
    static func fetchUserCategoryList(date: Date, success: @escaping ([FinancesUserCategoryResponse]) -> Void, failed: @escaping ConnectionError) {
        let startOfMonth = date.startOfMonth
        let endOfMonth = date.endOfMonth
        FinancesConnectionManager<FinancesUserCategoryResponse>.getArray(
            requestUrl: getFinancesRequestUrl() + "transaction/category/total/?date_after=\(startOfMonth.toFormatted)&date_before=\(endOfMonth.toFormatted)",
            success: success,
            failed: failed
        )
    }
    
    static func fetchTotalTransactionList(date: Date, success: @escaping ([FinancesTotalTransactionResponse]) -> Void, failed: @escaping ConnectionError) {
        let startOfMonth = date.startOfMonth
        let endOfMonth = date.endOfMonth
        FinancesConnectionManager<FinancesTotalTransactionResponse>.getArray(
            requestUrl: getFinancesRequestUrl() + "transaction/raw/total/?date_after=\(startOfMonth.toFormatted)&date_before=\(endOfMonth.toFormatted)",
            success: success,
            failed: failed
        )
    }
    
    static func fetchTransactionReport(date: Date, success: @escaping (FinancesTransactionReportResponse) -> Void, failed: @escaping ConnectionError) {
        let endOfMonth = date.endOfMonth
        FinancesConnectionManager<FinancesTransactionReportResponse>.get(
            requestUrl: getFinancesRequestUrl() + "transaction/raw/report/?date_before=\(endOfMonth.toFormatted)",
            success: success,
            failed: failed
        )
    }
    
    static func fetchTransactionList(date: Date, page: Int, success: @escaping (FinancesTransactionResponse) -> Void, failed: @escaping ConnectionError) {
        let startOfMonth = date.startOfMonth
        let endOfMonth = date.endOfMonth
        FinancesConnectionManager<FinancesTransactionResponse>.get(
            requestUrl: getFinancesRequestUrl() + "transaction/raw/?date_after=\(startOfMonth.toFormatted)&date_before=\(endOfMonth.toFormatted)&page=\(page)&page_size=10000",
            success: success,
            failed: failed
        )
    }
    
    static func fetchBudgetList(date: Date, success: @escaping ([FinancesBudgetResponse]) -> Void, failed: @escaping ConnectionError) {
        let startOfMonth = date.startOfMonth
        let endOfMonth = date.endOfMonth
        FinancesConnectionManager<FinancesBudgetResponse>.getArray(
            requestUrl: getFinancesRequestUrl() + "finance/budget_shared/?date_after=\(startOfMonth.toFormatted)&date_before=\(endOfMonth.toFormatted)",
            success: success,
            failed: failed
        )
    }
}
