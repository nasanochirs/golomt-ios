//
//  FinancesConnectionManager.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Alamofire
import Foundation

class FinancesConnectionManager<T: Codable>: NSObject {
    static func get(requestUrl: String, success: @escaping (T) -> Void, failed: @escaping ConnectionError) {
        var request = URLRequest(url: URL(string: requestUrl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer " + financesBearerToken, forHTTPHeaderField: "Authorization")
        print("REQUEST: ", request)
        AF.request(request).response { response in
            switch response.result {
            case .success:
                guard let jsonData = response.data else {
                    return
                }
                do {
                    let jsonDecoder = JSONDecoder()
                    let decodedData = try jsonDecoder.decode(T.self, from: jsonData)
                    print("RESPONSE: ", decodedData.toJsonString)
                    success(decodedData)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    static func getArray(requestUrl: String, success: @escaping ([T]) -> Void, failed: @escaping ConnectionError) {
        var request = URLRequest(url: URL(string: requestUrl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer " + financesBearerToken, forHTTPHeaderField: "Authorization")
        print("REQUEST: ", request)
        AF.request(request).response { response in
            switch response.result {
            case .success:
                guard let jsonData = response.data else {
                    return
                }
                do {
                    let jsonDecoder = JSONDecoder()
                    let decodedData = try jsonDecoder.decode([T].self, from: jsonData)
                    print("RESPONSE: ", decodedData.toJsonString)
                    success(decodedData)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    static func post(params: Codable, requestUrl: String, success: @escaping (T) -> Void, failed: @escaping ConnectionError) {
        var request = URLRequest(url: URL(string: requestUrl)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer " + financesBearerToken, forHTTPHeaderField: "Authorization")
        request.httpBody = params.toJsonString.data(using: .utf8, allowLossyConversion: false)
        print("REQUEST: ", params.toJsonString)
        AF.request(request).response { response in
            switch response.result {
            case .success:
                guard let jsonData = response.data else {
                    return
                }
                do {
                    let jsonDecoder = JSONDecoder()
                    let decodedData = try jsonDecoder.decode(T.self, from: jsonData)
                    print("RESPONSE: ", decodedData.toJsonString)
                    success(decodedData)
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
}
