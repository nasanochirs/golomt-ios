//
//  ConnectionManager.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/2/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. Al l rights reserved.
//

import Alamofire
import Foundation

typealias ConnectionError = ((BaseResponse.HeaderMessage?) -> Void)

class ConnectionManager<T: BaseResponse>: NSObject {
    static func get(url: String, success: @escaping (T) -> Void, failed: @escaping ConnectionError) {
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.timeoutInterval = 120
        AF.request(request).response { response in
            switch response.result {
            case .success:
                 guard let jsonData = response.data else {
                                    return
                                }
                                let responseString = String(decoding: jsonData, as: UTF8.self)
                                do {
                                    let jsonDecoder = JSONDecoder()
                                    let convertedData = responseString.data(using: .utf8, allowLossyConversion: false)
                                    guard let data = convertedData else {
                                        return
                                    }
                                    let decodedData = try jsonDecoder.decode(T.self, from: data)
                                    print("RESPONSE: ", decodedData.toJsonString)
                                    success(decodedData)
                                }
                                catch {
                                    print(error)
                                }
            case .failure(_):
                failed(BaseResponse.HeaderMessage(MESSAGE_TYPE: "", MESSAGE_DESC: "connection_failure_label".localized(), MESSAGE_CODE: ""))
            }
    }
    }
    
    static func post(params: BaseRequest, success: @escaping (T) -> Void, failed: @escaping ConnectionError) {
        var request = URLRequest(url: URL(string: getRequestUrl())!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.headers = [
            "Accept": "application/json;q=0.9,*/*;q=0.8",
            "IPTYPE": "JSON",
            "X-CACHE-KEY": UserPreferencesConstants.xDestUrl,
            "X-DESTINATION": "",
            "User-Agent": UserPreferencesConstants.userAgent,
        ]
        let encrypted = encryptString(request: params.toJsonString)
        request.httpBody = encrypted.data(using: .utf8, allowLossyConversion: false)
        request.timeoutInterval = 120
        print("REQUEST: ", params.toJsonString)
        AF.request(request).response { response in
            switch response.result {
            case .success:
                guard let headers = response.response?.allHeaderFields else {
                    failed(nil)
                    return
                }
                if let headers = headers as? [String: Any] {
                    if let isFatal = headers["isFatal"] {
                        if (isFatal as AnyObject).boolValue == true {
                            LoadingDialog.shared.hideLoader()
                            let loginStoryBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
                            guard let loginVC = loginStoryBoard.instantiateInitialViewController() else {
                                return
                            }
                            UIApplication.shared.windows.first?.rootViewController = loginVC
                            UIApplication.shared.windows.first?.makeKeyAndVisible()
                            NotificationCenter.default.post(name: Notification.Name(NotificationConstants.SESSION_DESTROYED), object: nil)
                        }
                    }
                }
                guard let jsonData = response.data else {
                    return
                }
                let responseString = String(decoding: jsonData, as: UTF8.self)
                guard let decryptedData = decryptString(request: responseString) else {
                    return
                }

//                print(decryptedData)
                do {
                    let jsonDecoder = JSONDecoder()
                    let convertedData = decryptedData.data(using: .utf8, allowLossyConversion: false)
                    guard let data = convertedData else {
                        return
                    }
                    let decodedData = try jsonDecoder.decode(T.self, from: data)
                    print("RESPONSE: ", decodedData.toJsonString)
                    guard let message = decodedData.header?.MESSAGE?[0] else {
                        return
                    }
                    switch message.MESSAGE_TYPE {
                    case "SU", "BC": success(decodedData)
                    default: failed(message)
                    }
                }
                catch {
                    print(error)
                }
            case .failure:
                failed(BaseResponse.HeaderMessage(MESSAGE_TYPE: "", MESSAGE_DESC: "connection_failure_label".localized(), MESSAGE_CODE: ""))
            }
        }
    }

    static func postWithUrl(params: Encodable, url: String, success: @escaping (T) -> Void, failed: @escaping ConnectionError) {
        print(url)
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = params.toJsonString.data(using: .utf8, allowLossyConversion: false)
        request.timeoutInterval = 120
        print("REQUEST: ", params.toJsonString)
        AF.request(request).response { response in
            switch response.result {
            case .success:
                guard let jsonData = response.data else {
                    return
                }
                let responseString = String(decoding: jsonData, as: UTF8.self)
                do {
                    let jsonDecoder = JSONDecoder()
                    let convertedData = responseString.data(using: .utf8, allowLossyConversion: false)
                    guard let data = convertedData else {
                        return
                    }
                    let decodedData = try jsonDecoder.decode(T.self, from: data)
                    print("RESPONSE: ", "SUCCESS")
                    success(decodedData)
                }
                catch {
                    print(error)
                }
            case .failure:
                failed(BaseResponse.HeaderMessage(MESSAGE_TYPE: "", MESSAGE_DESC: "connection_failure_label".localized(), MESSAGE_CODE: ""))
            }
        }
    }
}
