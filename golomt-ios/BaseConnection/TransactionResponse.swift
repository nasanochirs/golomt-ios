//
//  TransactionResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/23/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class TransactionResponse: BaseResponse {
    let description: Description?
    let transaction: Transaction?
    
    private enum CodingKeys: String, CodingKey {
        case CodeDescription, InitiateTransaction
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(transaction, forKey: .InitiateTransaction)
        try container.encode(description, forKey: .CodeDescription)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.description = try container.decodeIfPresent(Description.self, forKey: .CodeDescription)
        self.transaction = try container.decodeIfPresent(Transaction.self, forKey: .InitiateTransaction)
        try super.init(from: decoder)
    }
    
    struct Description: Codable {
        var BANK_REF_NO: String?
        var INSTITUTION_NAMES: String?
        var ROUTING_NOS: String?
    }
    
    struct Transaction: Codable {
        var TXN_ID: String?
        var TOTAL_TXN_AMT: String?
        var TOTAL_AMT: String?
        var TXN_DATE: String?
        var REQUEST_ID: String?
        var TXN_STATUS: String?
    }
}
