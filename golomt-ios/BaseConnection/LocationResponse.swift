//
//  LocationResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LocationResponse: BaseResponse {
    let locationData: LocationData?
    
    private enum CodingKeys: String, CodingKey {
        case data
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(locationData, forKey: .data)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.locationData = try container.decodeIfPresent(LocationData.self, forKey: .data)
        try super.init(from: decoder)
    }

    enum BranchType: String {
      case CDM, ATM, BRANCH
    }
    
    struct LocationData: Codable {
        var locations: [Location]
        
        struct Location: Codable {
            var _id: String?
            var type: String?
            var info_en: Info?
            var info_mn: Info?
            var timetable: TimeTable?
            var coordinates: Coordinates?
            var solId: String?
            var imageBase64: String?
            
            func getName() -> String {
                switch getLanguage() {
                case "mn":
                    return info_mn?.name ?? ""
                default:
                    return info_en?.name ?? ""
                }
            }
            
            func getAddress() -> String {
                var address = info_mn?.address
                switch getLanguage() {
                case "mn":
                    address = info_mn?.address
                default:
                    address = info_en?.address
                }
                var value = ""
                value += (address?.city ?? "") + ", "
                value += (address?.district ?? "") + ", "
                value += (address?.khoroo ?? "") + ", "
                value += (address?.street ?? "") + ", "
                value += address?.other ?? ""
                return value
            }
            
            struct Info: Codable {
                var name: String?
                var contacts: [String]?
                var address: Address?
                
                struct Address: Codable {
                    var city: String?
                    var district: String?
                    var khoroo: String?
                    var street: String?
                    var other: String?
                    var what3words: String?
                }
            }
            
            struct TimeTable: Codable {
                var monday: String?
                var tuesday: String?
                var wednesday: String?
                var thursday: String?
                var friday: String?
                var saturday: String?
                var sunday: String?
                
                struct LocationModel {
                    var id: Int
                    var key: String
                    var value: String
                }
                
                func getDay(_ day: String) -> String {
                    switch day {
                    case "0":
                        return "ДА"
                    case "1":
                        return "МЯ"
                    case "2":
                        return "ЛХ"
                    case "3":
                        return "ПҮ"
                    case "4":
                        return "БА"
                    case "5":
                        return "БЯ"
                    case "6":
                        return "НЯ"
                    default:
                        return ""
                    }
                }
                
                func isOpen() -> Bool {
                    let dayList = [sunday, monday, tuesday, wednesday, thursday, friday, saturday]
                    let calendar = Calendar.current
                    let weekDay = calendar.component(.weekday, from: Date()) - 1
                    let schedule = dayList[weekDay]
                    if schedule == "24" {
                        return true
                    }
                    let times = schedule.orEmpty.components(separatedBy: "-")
                    let formatter = DateFormatter()
                    formatter.dateFormat = "HH:mm"
                    guard let openTime = formatter.date(from: times.first.orEmpty) else {
                        return false
                    }
                    guard let closeTime = formatter.date(from: times.last.orEmpty) else {
                        return false
                    }
                    guard let currentTime = formatter.date(from: formatter.string(from: Date())) else {
                        return false
                    }
                    return (openTime ... closeTime).contains(currentTime)
                }
                
                func getTimeLine() -> String {
                    var dictionary = [String: String]()
                    let dayList = [monday, tuesday, wednesday, thursday, friday, saturday, sunday]
                    for i in 0...6 {
                        let key = dayList[i].orEmpty
                        if dictionary.keys.contains(key) {
                            let value = dictionary[key]! + i.toString + ","
                            dictionary.updateValue(value, forKey: key)
                        } else {
                            dictionary[key] = i.toString + ","
                        }
                    }
                    var locationModels = [LocationModel]()
                    for key in dictionary.keys.sorted() {
                        let value = dictionary[key]!
                        let components = value.components(separatedBy: ",")
                        let string = components.first.orEmpty
                        switch string {
                        case "location_closed_label".localized():
                            locationModels.append(LocationModel(id: 100, key: key, value: value))
                        default:
                            locationModels.append(LocationModel(id: Int(string) ?? 200, key: key, value: value))
                        }
                    }
                    var schedule = ""
                    let sorted = locationModels.sorted { $0.id < $1.id }
                    sorted.forEach { location in
                        var value = ""
                        let components = location.value.components(separatedBy: ",")
                        components.forEach { component in
                            if getDay(component) != "" {
                                value += getDay(component) + ", "
                            }
                        }
                        value = String(value.dropLast(2))
                        var key = location.key
                        if location.key == "0" {
                            key = "location_closed_label".localized()
                        }
                        schedule += value + " /" + key + "/\n"
                    }
                    return schedule
                }
            }
            
            struct Coordinates: Codable {
                var longitude: String?
                var latitude: String?
                
                func getLongitude() -> String {
                    if longitude == "-" {
                        return ""
                    }
                    return longitude.orEmpty
                }
                
                func getLatitude() -> String {
                    if latitude == "-" {
                        return ""
                    }
                    return latitude.orEmpty
                }
            }
        }
    }
}
