//
//  ConnectionEncrypt.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/2/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

func encryptString(request: String) -> String {
    let encryptor = DataEncryptDecrypt.sharedENDEcryptor()
    let encrypted = encryptor?.encrypt(withAES256: request, withStringUID: "")
    return encrypted ?? ""
}

func encryptImageString(request: String) -> String {
    let encryptor = DataEncryptDecrypt.sharedENDEcryptor()
    let encrypted = encryptor?.encryptImage(withAES256: request, withStringUID: "")
    return encrypted ?? ""
}

func decryptString(request: String) -> String? {
    let encryptor = DataEncryptDecrypt.sharedENDEcryptor()
    let decrypted = encryptor?.decrypt(withAES256: request, withStringUID: "")
    return decrypted ?? nil
}

func decryptImageString(request: String) -> String? {
    let encryptor = DataEncryptDecrypt.sharedENDEcryptor()
    let decrypted = encryptor?.decryptImage(withAES256: request, withStringUID: "")
    return decrypted ?? nil
}
