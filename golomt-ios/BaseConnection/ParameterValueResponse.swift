//
//  ParameterValueResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class ParameterValueResponse: BaseResponse {
    let parameter: Parameter?

    private enum CodingKeys: String, CodingKey {
        case basic
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(parameter, forKey: .basic)
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.parameter = try container.decodeIfPresent(Parameter.self, forKey: .basic)
        try super.init(from: decoder)
    }

    struct Parameter: Codable {
        var P_NAME: String?
        var P_VAL: String?
    }
}
