//
//  RateRequest.swift
//  golomt-ios
//
//  Created by Khulan Odkhuu on 7/6/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class RateRequest: Codable {
    let query = "query {rate_list {non_cash_buy cash_avg cash_buy date non_cash_sell cash_sell currency}}"
}
