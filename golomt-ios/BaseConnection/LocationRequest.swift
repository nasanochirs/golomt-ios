//
//  LocationRequest.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class LocationRequest: Codable {
    let query = "query {locations {_id type info_en{name contacts address{city district khoroo street other what3words}} info_mn{name contacts address{ city district khoroo street other what3words}} timetable{monday tuesday wednesday thursday friday saturday sunday} coordinates{latitude longitude} solId imageBase64}}"
}
