//
//  RateResponse.swift
//  golomt-ios
//
//  Created by Khulan Odkhuu on 7/6/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class RateResponse: BaseResponse {
    let rateList: [Rate]?
    
    private enum CodingKeys: String, CodingKey {
        case rate_list
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(rateList, forKey: .rate_list)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.rateList = try container.decodeIfPresent([Rate].self, forKey: .rate_list)
        try super.init(from: decoder)
    }
    
    struct Rate: Codable {
        var non_cash_buy: Double?
        var cash_avg: Double?
        var cash_buy: Double?
        var date: String?
        var non_cash_sell: Double?
        var cash_sell: Double?
        var currency: String?
    }
}
