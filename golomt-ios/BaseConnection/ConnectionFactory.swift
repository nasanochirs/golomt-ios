//
//  ConnectionFactory.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/4/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Alamofire
import Foundation
import UIKit

class ConnectionFactory {
    static func login(username: String, password: String, success: @escaping (LoginResponse) -> Void, failed: @escaping ConnectionError) {
        let request = LoginRequest()
        request.header.USER_PRINCIPAL = username
        request.header.ACCESS_CODE = password
        ConnectionManager<LoginResponse>.post(
            params: request,
            success: success,
            failed: { reason in
                switch reason?.MESSAGE_CODE {
                case "108892":
                    removeBiometric(
                        username: username,
                        success: { _ in
                            login(username: username, password: password, success: success, failed: failed)
                        },
                        failed: failed
                    )
                default: failed(reason)
                }
            }
        )
    }

    static func loginWithBiometric(success: @escaping (LoginResponse) -> Void, failed: @escaping ConnectionError) {
        let request = LoginRequest()
        let authManager = LocalAuthManager.shared
        let publicKey = authManager.getPublicKey()
        let deviceId = UIDevice.current.identifierForVendor?.uuidString
        request.header.USER_PRINCIPAL = getLoginName().orEmpty
        request.header.AUTH_MODE = "FPAM"
        request.header.DEVICEID = deviceId.orEmpty
        request.header.PUBLICKEY = publicKey.orEmpty
        ConnectionManager<LoginResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func loginWithEToken(username: String, eToken: String, success: @escaping (LoginResponse) -> Void, failed: @escaping ConnectionError) {
        let request = LoginRequest()
        request.header.USER_PRINCIPAL = username
        request.header.AUTH_MODE = "SECD"
        request.header.VASCO_TOKEN = eToken
        ConnectionManager<LoginResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func registerBiometric(success: @escaping (BaseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = BiometricRequest()
        let authManager = LocalAuthManager.shared
        let publicKey = authManager.getPublicKey()
        let deviceId = UIDevice.current.identifierForVendor?.uuidString
        request.body.DEVICE_ID = deviceId.orEmpty
        request.body.PUBLIC_KEY = publicKey.orEmpty
        ConnectionManager<BaseResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func removeBiometric(username: String, success: @escaping (BaseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = LoginRequest()
        request.body.USER_ID = username
        request.header.USER_PRINCIPAL = "01.SMARTBANK"
        request.header.ACCESS_CODE = "ow390401"
        request.header.__SRVCID__ = "CFPR"
        request.header.LOGIN_FLAG = "2"
        request.header.STATEMODE = "N"
        request.header.CHANNEL_ID = "I"
        ConnectionManager<BaseResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchAccount(accountType: String, success: @escaping (AccountResponse) -> Void, failed: @escaping ConnectionError) {
        let request = AccountRequest()
        request.body.MAIN_ACCOUNT_TYPE = accountType
        ConnectionManager<AccountResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchAccountList(userId: String, custId: String, corporateId: String, success: @escaping (AccountSettingsResponse) -> Void, failed: @escaping ConnectionError) {
        let request = AccountSettingsRequest()
        request.body.USER_ID = userId
        request.body.CUST_ID = custId
        request.body.CORPORATE_ID = corporateId
        ConnectionManager<AccountSettingsResponse>.post(params: request, success: success, failed: failed)
    }

    static func fetchAccountClose(branchId: String, accId: String, success: @escaping (BaseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = AccountCloseRequest()
        request.body.USER_ID = userDetails?.USER_ID ?? ""
        request.body.CORPORATE_ID = userDetails?.CORPORATE_ID ?? ""
        request.body.AC_BRANCH_ID = branchId
        request.body.ACID = accId
        request.body.ACCESS = "N"
        request.body.INQUIRY_ACCESS = "N"
        ConnectionManager<BaseResponse>.post(params: request, success: success, failed: failed)
    }

    static func fetchAccountCreate(branchId: String, accId: String, success: @escaping (BaseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = AccountCreateRequest()
        request.body.USER_ID = userDetails?.USER_ID ?? ""
        request.body.CORPORATE_ID = userDetails?.CORPORATE_ID ?? ""
        request.body.AC_BRANCH_ID = branchId
        request.body.ACID = accId
        request.body.ACCESS = "Y"
        request.body.INQUIRY_ACCESS = "Y"
        ConnectionManager<BaseResponse>.post(params: request, success: success, failed: failed)
    }

    static func changeLoginPass(oldPass: String, newPass: String, newPassRepeat: String, success: @escaping (BaseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = ChangeLoginPassRequest()
        request.body.SIGNON_PWD = oldPass
        request.body.SIGNON_NEW_PWD = newPass
        request.body.RETYPE_SIGNON_PWD = newPassRepeat
        request.body.SIGNON_FLAG = "Y"
        ConnectionManager<BaseResponse>.post(params: request, success: success, failed: failed)
    }

    static func changeConfirmPass(oldPass: String, newPass: String, newPassRepeat: String, success: @escaping (BaseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = ChangeConfirmPassRequest()
        request.body.TRANSACTION_PWD = oldPass
        request.body.TRANSACTION_NEW_PWD = newPass
        request.body.RETYPE_TRANSACTION_PWD = newPassRepeat
        request.body.TRANSACTION_FLAG = "Y"
        ConnectionManager<BaseResponse>.post(params: request, success: success, failed: failed)
    }

    static func changeNickname(body: ChangeNicknameRequest.Body?, success: @escaping (BaseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = ChangeNicknameRequest()
        guard let unwrappedBody = body else {
            return
        }
        request.body = unwrappedBody
        ConnectionManager<BaseResponse>.post(params: request, success: success, failed: failed)
    }

    static func fetchCreditAccount(success: @escaping (CreditAccountResponse) -> Void, failed: @escaping ConnectionError) {
        let request = CreditAccountRequest()
        ConnectionManager<CreditAccountResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchAccountBook(accountBookType: String, success: @escaping (AccountBookResponse) -> Void, failed: @escaping ConnectionError) {
        let request = AccountBookRequest()
        request.body.NETWORK_ID = accountBookType
        ConnectionManager<AccountBookResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchPayments(success: @escaping (PaymentResponse) -> Void, failed: @escaping ConnectionError) {
        let request = PaymentRequest()
        ConnectionManager<PaymentResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchLienStatement(accountNumber: String, success: @escaping (LienStatementResponse) -> Void, failed: @escaping ConnectionError) {
        let request = LienStatementRequest()
        request.body.ACCOUNT_NUMBER = accountNumber
        ConnectionManager<LienStatementResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchOPRStatement(model: StatementModel, success: @escaping (OPRStatementResponse) -> Void, failed: @escaping ConnectionError) {
        let request = StatementRequest()
        let account = model.account
        request.body.ACCOUNT_ID = account.ACCT_NUMBER.orEmpty
        request.body.ACCOUNT_TYPE = account.ACCT_TYPE.orEmpty
        request.body.FROM_TXN_DATE = model.startDate
        request.body.TO_TXN_DATE = model.endDate
        request.body.AMOUNT_TYPE = model.filterType
        request.body.SET_NUMBER = model.pageNumber
        ConnectionManager<OPRStatementResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchDEPStatement(model: StatementModel, success: @escaping (DEPStatementResponse) -> Void, failed: @escaping ConnectionError) {
        let request = StatementRequest()
        request.header.__SRVCID__ = "CDTXNH"
        let account = model.account
        request.body.ACCOUNT_ID = account.ACCT_NUMBER.orEmpty
        request.body.ACCOUNT_TYPE = account.ACCT_TYPE.orEmpty
        request.body.FROM_TXN_DATE = model.startDate
        request.body.TO_TXN_DATE = model.endDate
        request.body.AMOUNT_TYPE = model.filterType
        request.body.SET_NUMBER = model.pageNumber
        request.body.MAIN_ACCOUNT_TYPE = AccountConstants.deposit
        ConnectionManager<DEPStatementResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchCCDStatement(model: StatementModel, success: @escaping (CCDStatementResponse) -> Void, failed: @escaping ConnectionError) {
        let request = CCDStatementRequest()
        let account = model.account

        request.body.ACCOUNT_TYPE = account.ACCT_TYPE
        request.body.CARD_NUMBER = account.ACCT_NUMBER
        request.body.ACCOUNT_NICKNAME = account.ACCT_NICKNAME
        request.body.SELECTED_CURRENCY_CODE = account.ACCT_CURRENCY
        request.body.SELECTED_MONTH = model.startDate
        request.body.SELECTED_YEAR = model.endDate

        switch model.startDate.toInt {
        case 0:
            request.header.__SRVCID__ = CCDStatementRequest.RequestType.current.rawValue
        default:
            request.header.__SRVCID__ = CCDStatementRequest.RequestType.previous.rawValue
        }

        ConnectionManager<CCDStatementResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchLONStatement(model: StatementModel, success: @escaping (LONStatementResponse) -> Void, failed: @escaping ConnectionError) {
        let request = StatementRequest()
        request.header.__SRVCID__ = "CDTXNH"
        let account = model.account
        request.body.ACCOUNT_ID = account.ACCT_NUMBER.orEmpty
        request.body.ACCOUNT_TYPE = account.ACCT_TYPE.orEmpty
        request.body.FROM_TXN_DATE = model.startDate
        request.body.TO_TXN_DATE = model.endDate
        request.body.AMOUNT_TYPE = model.filterType
        request.body.SET_NUMBER = model.pageNumber
        request.body.MAIN_ACCOUNT_TYPE = AccountConstants.loan
        ConnectionManager<LONStatementResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchAccountName(accountNumber: String, success: @escaping (AccountNameResponse) -> Void, failed: @escaping ConnectionError) {
        let request = AccountNameRequest()
        request.body.ACCOUNT_ID = accountNumber
        ConnectionManager<AccountNameResponse>.post(params: request, success: success, failed: failed)
    }

    static func fetchCreditAccountName(cardNumber: String, success: @escaping (CreditAccountNameResponse) -> Void, failed: @escaping ConnectionError) {
        let request = CreditAccountNameRequest()
        request.body.CARD_NUMBER = cardNumber
        ConnectionManager<CreditAccountNameResponse>.post(params: request, success: success, failed: failed)
    }

    static func fetchTransactionLimit(transactionType: String, success: @escaping (TransactionLimitResponse) -> Void, failed: @escaping ConnectionError) {
        let request = TransactionLimitRequest()
        request.body.EFFEVTIVE_TRANSACTION_TYPE = transactionType
        request.body.AS_ON_DATE = Date().toFormatted
        ConnectionManager<TransactionLimitResponse>.post(params: request, success: success, failed: failed)
    }

    static func fetchBankList(success: @escaping (BankListResponse) -> Void, failed: @escaping ConnectionError) {
        let request = BankListRequest()
        ConnectionManager<BankListResponse>.post(params: request, success: success, failed: failed)
    }

    static func fetchParameterValue(parameterName: String, success: @escaping (ParameterValueResponse) -> Void, failed: @escaping ConnectionError) {
        let request = ParameterValueRequest()
        request.body.P_NAME = parameterName
        ConnectionManager<ParameterValueResponse>.post(params: request, success: success, failed: failed)
    }

    static func fetchList(code: String, success: @escaping (DataListResponse) -> Void, failed: @escaping ConnectionError) {
        let request = DataListRequest()
        request.body.CODE_TYPE = code
        ConnectionManager<DataListResponse>.post(params: request, success: success, failed: failed)
    }

    static func fetchBankCurrencyList(bankCode: String, success: @escaping (DataListResponse) -> Void, failed: @escaping ConnectionError) {
        fetchParameterValue(
            parameterName: bankCode + "_CURS",
            success: { response in
                fetchList(
                    code: response.parameter?.P_VAL ?? "",
                    success: success,
                    failed: failed
                )
            },
            failed: failed
        )
    }

    static func makeTransaction(model: TransactionModel, success: @escaping (TransactionResponse) -> Void, failed: @escaping ConnectionError) {
        let request = TransactionRequest()
        request.body.TXN_TYPE = model.transactionInfo.type?.rawValue ?? ""
        request.body.TXN_CRN = model.transactionInfo.currency ?? ""
        request.body.ENTRY_AMT = model.transactionInfo.amount ?? 0.0
        request.body.NWK_TYP = model.transactionInfo.networkType?.rawValue ?? ""
        request.body.INIT_ACC_CURRENCY = model.senderAccountInfo.ACCT_CURRENCY ?? ""
        request.body.INIT_ACC_ENTITY_ID = "transaction_sender_account_format".localized(with: model.senderAccountInfo.ACCT_NUMBER ?? "", model.senderAccountInfo.BRANCH_ID ?? "")
        request.body.CP_NICKNAME = model.beneficiaryAccountInfo.name ?? ""
        request.body.CP_BANK_IDENTIFIER = model.beneficiaryAccountInfo.bank ?? ""
        request.body.CP_ENTITY_ID = model.beneficiaryAccountInfo.number ?? ""
        request.body.CP_TYPE = model.beneficiaryAccountInfo.usedAccountBook ? "P" : "H"
        request.body.ENT_REMARKS = model.transactionInfo.remark ?? ""
        switch model.transactionInfo.type {
        case .own:
            request.body.CP_TYPE = "O"
            request.body.CP_ENTITY_ID = "transaction_sender_account_format".localized(with: model.beneficiaryAccountInfo.number ?? "", model.beneficiaryAccountInfo.bank ?? "")
            request.body.CP_ENTITY_CURRENCY = model.beneficiaryAccountInfo.currency ?? ""
        case .creditCard:
            request.body.CP_TYPE = model.beneficiaryAccountInfo.usedAccountBook ? "H" : "O"
            request.body.CP_ENTITY_ID = model.beneficiaryAccountInfo.usedAccountBook ? model.beneficiaryAccountInfo.number ?? "" : "transaction_sender_account_format".localized(with: model.beneficiaryAccountInfo.number ?? "", model.beneficiaryAccountInfo.bank ?? "")
            request.body.CP_ENTITY_CURRENCY = model.beneficiaryAccountInfo.currency ?? ""
        case .loan:
            request.body.CP_TYPE = "L"
            request.body.CP_ENTITY_ID = "transaction_sender_account_format".localized(with: model.beneficiaryAccountInfo.number ?? "", model.beneficiaryAccountInfo.bank ?? "")
            request.body.CP_ENTITY_CURRENCY = model.beneficiaryAccountInfo.currency ?? ""
            request.body.LOAN_OVERDUE_AMT = model.extraInfo.overdueAmount
        default:
            break
        }
        ConnectionManager<TransactionResponse>.post(
            params: request,
            success: { response in
                if response.transaction?.TXN_STATUS == "FAL" {
                    failed(response.header?.MESSAGE?[0])
                } else {
                    success(response)
                }
            },
            failed: failed
        )
    }

    static func makeForeignTransaction(model: TransactionForeignModel, success: @escaping (TransactionResponse) -> Void, failed: @escaping ConnectionError) {
        let request = TransactionRequest()
        request.body.TXN_TYPE = TransactionTypeConstants.swift.rawValue
        request.body.TXN_CRN = model.transactionInfo.currency.orEmpty
        request.body.ENTRY_AMT = model.transactionInfo.amount.orZero
        request.body.NWK_TYP = TransactionNetworkConstants.swift.rawValue
        request.body.INIT_ACC_CURRENCY = model.senderAccountInfo.ACCT_CURRENCY.orEmpty
        request.body.INIT_ACC_ENTITY_ID = "transaction_sender_account_format".localized(with: model.senderAccountInfo.ACCT_NUMBER ?? "", model.senderAccountInfo.BRANCH_ID ?? "")
        request.body.CP_BANK_IDENTIFIER = model.beneficiaryBankInfo.swift?.ROUTING_NOS ?? nil
        request.body.CP_NICKNAME = model.beneficiaryAccountInfo.name.orEmpty
        request.body.BENEFICIARY_ADDRESS = model.beneficiaryAccountInfo.address.orEmpty
        request.body.CP_ENTITY_ID = model.beneficiaryAccountInfo.number.orEmpty
        switch model.isRuble {
        case true:
            request.body.BANK_IDENTIFIER_TYPE = "LOCAL"
        case false:
            request.body.BANK_IDENTIFIER_TYPE = "SWI"
        }
        request.body.BENEFICIARY_CODE = model.beneficiaryBankInfo.bankCode.orEmpty
        request.body.CP_TYPE = "H"
        request.body.ENT_REMARKS = model.transactionInfo.remark.orEmpty
        request.body.CP_ENTITY_CURRENCY = model.transactionInfo.currency.orEmpty
        request.body.PURPOSE_CODE = model.transactionInfo.purpose.orEmpty
        request.body.CHARGE_OPTION = model.transactionInfo.feeType.orEmpty
        request.body.BENEFICIARY_BANK_ADDRESS = model.beneficiaryBankInfo.bankAddress.orEmpty
        request.body.B_OTHER = model.beneficiaryBankInfo.otherField.orEmpty
        request.body.B_NAME = model.beneficiaryBankInfo.bankName.orEmpty
        request.body.B_INN = model.beneficiaryAccountInfo.inn.orEmpty
        request.body.B_KPP = model.beneficiaryAccountInfo.kpp.orEmpty
        request.body.B_ACCOUNT = model.beneficiaryBankInfo.bankNumber.orEmpty
        request.body.BO_CODE = model.beneficiaryAccountInfo.voCode.orEmpty
        ConnectionManager<TransactionResponse>.post(
            params: request,
            success: { response in
                if response.transaction?.TXN_STATUS == "FAL" {
                    failed(response.header?.MESSAGE?[0])
                } else {
                    success(response)
                }
            },
            failed: failed
        )
    }

    static func saveAccountBook(model: TransactionModel, success: @escaping (BaseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = AccountBookSaveRequest()
        let accountName = model.beneficiaryAccountInfo.name.orEmpty
        let accountNumber = model.beneficiaryAccountInfo.number.orEmpty
        request.body.BNF_NAME = accountName
        request.body.BNF_NICK_NAME = accountName.components(separatedBy: " ").first.orEmpty + " " + accountNumber.suffix(4)
        request.body.BNF_ACCT_NUMBER = accountNumber
        request.body.NETWORK_ID = model.transactionInfo.networkType?.rawValue ?? ""
        request.body.BANK_IDENTIFIER = model.beneficiaryAccountInfo.bank.orEmpty
        request.body.BNF_ACCT_CRN = model.transactionInfo.currency.orEmpty
        ConnectionManager<BaseResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchCardList(success: @escaping (CardListResponse) -> Void, failed: @escaping ConnectionError) {
        let request = CardListRequest()
        ConnectionManager<CardListResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func handlePinRequest(body: CardPinRequest.Body, success: @escaping (CardPinResponse) -> Void, failed: @escaping ConnectionError) {
        let request = CardPinRequest()
        request.body = body
        ConnectionManager<CardPinResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func handleECodeRequest(body: CardECodeRequest.Body, success: @escaping (BaseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = CardECodeRequest()
        request.body = body
        ConnectionManager<BaseResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func handeCardStatusRequest(body: CardStatusRequest.Body, success: @escaping (BaseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = CardStatusRequest()
        request.body = body
        ConnectionManager<BaseResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func getLocations(success: @escaping (LocationResponse) -> Void, failed: @escaping ConnectionError) {
        let request = LocationRequest()
        ConnectionManager<LocationResponse>.postWithUrl(
            params: request,
            url: getLocationUrl() + "graphql",
            success: success,
            failed: failed
        )
    }

    static func getBranchQueue(solId: String, success: @escaping (LocationQueueResponse) -> Void, failed: @escaping ConnectionError) {
        ConnectionManager<LocationQueueResponse>.get(
            url: getLocationUrl() + "queue?solId=\(solId)",
            success: success,
            failed: failed
        )
    }

    static func fetchRates(success: @escaping (RateResponse) -> Void, failed: @escaping ConnectionError) {
        ConnectionManager<RateResponse>.get(
            url: getRateUrl(),
            success: success,
            failed: failed
        )
    }

    static func cardOrderRequest(model: CardOrderModel, success: @escaping (BaseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = CardOrderRequest()
        request.body.FULLNAME = userDetails?.FULLNAME_EN ?? ""
        request.body.SHORTNAME = userDetails?.SHORTNAME ?? ""
        request.body.CARD_PLASTIC = model.getCard()
        request.body.CARDTYPE = model.typeValue
        request.body.USERCHOSENCURRENCY = model.currency
        if !model.newCardProduct.isEmpty {
            request.body.ISNEWACCOUNT = "Y"
            request.body.PRODUCTTYPE = model.newCardProduct
            request.body.FEE_ADDED = model.addedFee + model.fee
        } else {
            request.body.ACCOUNT = model.account?.ACCT_NUMBER ?? ""
        }
        request.body.CRN_ONE = model.currency
        if request.body.FULLNAME.trim().count <= 20 {
            request.body.NAME_ON_CARD = request.body.FULLNAME
        } else {
            let shortName = request.body.SHORTNAME.trim()
            request.body.NAME_ON_CARD = String(shortName.prefix(20))
        }
        request.body.OPERATIVE_SOLID = model.location?.solId ?? ""
        request.body.CUST_ID = userDetails?.CUSTOMER_ID ?? ""
        request.body.FEE_AMOUNT = model.fee
        request.body.PRIMARY_SOLID = userDetails?.PRIMARY_SOLID ?? ""
        request.body.REMITTER_ACCOUNT = model.account?.ACCT_NUMBER ?? ""
        request.body.DEBIT_ACCT_CRN = model.account?.ACCT_CURRENCY ?? ""
        request.body.AMT_ONE = model.currency + "|" + String(model.fee)
        ConnectionManager<BaseResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchExchangeRate(model: TransactionModel, success: @escaping (ExchangeRateResponse) -> Void, failed: @escaping ConnectionError) {
        let request = ExchangeRateRequest()
        request.body.TRAN_INI_CRN = model.senderAccountInfo.ACCT_CURRENCY.orEmpty
        request.body.TRAN_CP_CRN = model.beneficiaryAccountInfo.currency.orEmpty
        request.body.TXN_CRN = model.transactionInfo.currency.orEmpty
        request.body.INITOR_ACCOUNT = model.senderAccountInfo.ACCT_NUMBER.orEmpty
        request.body.TRAN_CP_ENTITY_ID = model.beneficiaryAccountInfo.number.orEmpty
        request.body.TXN_TYPE = model.transactionInfo.type?.rawValue ?? ""
        ConnectionManager<ExchangeRateResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchOPRDetail(number: String, success: @escaping (OPRDetailResponse) -> Void, failed: @escaping ConnectionError) {
        let request = OPRDetailRequest()
        request.body.ACCOUNT_ID = number
        ConnectionManager<OPRDetailResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchDEPDetail(account: AccountResponse.Account?, isLoan: Bool, success: @escaping (DEPDetailResponse) -> Void, failed: @escaping ConnectionError) {
        let request = DEPDetailRequest()
        request.body.ACCOUNT_ID = account?.ACCT_NUMBER ?? ""
        request.body.ACCOUNT_TYPE = account?.ACCT_TYPE ?? ""
        request.body.SBL_DEPOSIT = isLoan ? "Y" : "N"
        ConnectionManager<DEPDetailResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchLONDetail(account: AccountResponse.Account?, success: @escaping (LONDetailResponse) -> Void, failed: @escaping ConnectionError) {
        let request = LONDetailRequest()
        request.body.ACCOUNT_ID = account?.ACCT_NUMBER ?? ""
        request.body.ACCOUNT_TYPE = account?.ACCT_TYPE ?? ""
        ConnectionManager<LONDetailResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchCCDDetail(account: AccountResponse.Account?, success: @escaping (CCDDetailResponse) -> Void, failed: @escaping ConnectionError) {
        let request = CCDDetailRequest()
        request.body.CARD_NUMBER = account?.ACCT_NUMBER ?? ""
        ConnectionManager<CCDDetailResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func createDeposit(model: CreateDepositModel, success: @escaping (CreateDepositResponse) -> Void, failed: @escaping ConnectionError) {
        let request = CreateDepositRequest()
        request.body.PRODUCTTYPE = model.selectedCategory?.type ?? ""
        request.body.CRN_ONE = model.selectedCategory?.currency ?? ""
        request.body.REMITTER_ACCOUNT = model.selectedCategory?.remitterAccount?.ACCT_NUMBER ?? ""
        request.body.DEBIT_ACCT_CRN = model.selectedCategory?.remitterAccount?.ACCT_CURRENCY ?? ""
        request.body.PRIMARY_SOLID = userDetails?.PRIMARY_SOLID ?? ""
        request.body.SHORTNAME = userDetails?.SHORTNAME ?? ""
        request.body.FULLNAME = userDetails?.FULLNAME ?? ""
        switch model.type {
        case .Demand:
            request.header.__SRVCID__ = "CSBAR"
            let currency = model.selectedCategory?.currency ?? ""
            let amount = model.selectedCategory?.minimumAmount ?? 0.0
            request.body.AMT_ONE = currency + "|" + String(describing: amount)
        default:
            request.body.USERCHOSENMONTHS = model.selectedCategory?.duration.toDigits ?? ""
            request.body.AMT_ONE = String(describing: model.selectedCategory?.minimumAmount ?? 0.0)
        }
        ConnectionManager<CreateDepositResponse>.post(
            params: request,
            success: { response in
                switch response.deposit?.FORM_REQ_STATE {
                case SuccessTypes.SUB.rawValue, SuccessTypes.SUC.rawValue:
                    success(response)
                default:
                    failed(BaseResponse.HeaderMessage(MESSAGE_TYPE: "", MESSAGE_DESC: response.getMessage(), MESSAGE_CODE: ""))
                }
            },
            failed: failed
        )
    }

    static func closeDeposit(model: CloseDepositModel, success: @escaping (CloseDepositResponse) -> Void, failed: @escaping ConnectionError) {
        let request = CloseDepositRequest()
        request.body.USERCHOSENACCID = model.depositAccount?.ACCT_NUMBER ?? ""
        request.body.LEDGER_BAL = model.depositAccountDetails?.balance?.LEDGER_BAL ?? ""
        request.body.ACCOUNTID = model.beneficiaryAccount?.ACCT_NUMBER ?? ""
        request.body.BREAKING_DATE = Date().toFormatted
        request.body.USERENTEREDAMOUNT = model.depositAccountDetails?.balance?.LEDGER_BAL ?? ""
        ConnectionManager<CloseDepositResponse>.post(
            params: request,
            success: { response in
                switch response.deposit?.FORM_REQ_STATE {
                case SuccessTypes.SUB.rawValue, SuccessTypes.SUC.rawValue:
                    success(response)
                default:
                    failed(BaseResponse.HeaderMessage(MESSAGE_TYPE: "", MESSAGE_DESC: response.getMessage(), MESSAGE_CODE: ""))
                }
            },
            failed: failed
        )
    }

    static func fetchDepositExtendAccounts(success: @escaping (ExtendDepositAccountResponse) -> Void, failed: @escaping ConnectionError) {
        let request = BaseRequest()
        request.header.__SRVCID__ = "CMDAL"
        ConnectionManager<ExtendDepositAccountResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchDepositExtendProducts(account: AccountResponse.Account, success: @escaping (ExtendDepositProductResponse) -> Void, failed: @escaping ConnectionError) {
        let request = ExtendDepositProductRequest()
        request.body.ACCOUNT_ID = account.ACCT_NUMBER.orEmpty
        ConnectionManager<ExtendDepositProductResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchDepositExtendMonths(product: String, currency: String, success: @escaping (ExtendDepositMonthResponse) -> Void, failed: @escaping ConnectionError) {
        let request = ExtendDepositMonthRequest()
        request.body.RENEW_INSTRUCTION = product
        request.body.ACC_CURRENCY = currency
        ConnectionManager<ExtendDepositMonthResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func extendDeposit(model: ExtendDepositModel, success: @escaping (ExtendDepositResponse) -> Void, failed: @escaping ConnectionError) {
        let request = ExtendDepositRequest()
        request.body.RENEW_INSTRUCTION = model.product?.RENEW_INSTRUCTION_CODE_ARRAY ?? ""
        request.body.FD_RENEW_MONTHS = model.month?.FD_RENEW_MONTHS_ARRAY ?? ""
        request.body.CREDIT_ACCT_CRN = model.depositAccount?.ACCT_CURRENCY ?? ""
        request.body.USERCHOSENACCID_STRING = model.depositAccount?.ACCT_NUMBER ?? ""
        ConnectionManager<ExtendDepositResponse>.post(
            params: request,
            success: { response in
                switch response.deposit?.FORM_REQ_STATE {
                case SuccessTypes.SUB.rawValue, SuccessTypes.SUC.rawValue:
                    success(response)
                default:
                    failed(BaseResponse.HeaderMessage(MESSAGE_TYPE: "", MESSAGE_DESC: response.getMessage(), MESSAGE_CODE: ""))
                }
            },
            failed: failed
        )
    }

    static func createOperative(model: OperativeCreateModel, success: @escaping (OperativeCreateResponse) -> Void, failed: @escaping ConnectionError) {
        let request = OperativeCreateRequest()
        request.body.PRODUCTTYPE = model.product?.CM_CODE ?? ""
        request.body.CRN_ONE = model.currency?.rawValue ?? ""
        request.body.REMITTER_ACCOUNT = model.remitter?.ACCT_NUMBER ?? ""
        request.body.AMT_ONE = model.minimumAmount
        request.body.DEBIT_ACCT_CRN = model.remitter?.ACCT_CURRENCY ?? ""
        request.body.PRIMARY_SOLID = userDetails?.PRIMARY_SOLID ?? ""
        request.body.SHORTNAME = userDetails?.SHORTNAME ?? ""
        request.body.FULLNAME = userDetails?.FULLNAME ?? ""
        ConnectionManager<OperativeCreateResponse>.post(
            params: request,
            success: { response in
                switch response.operative?.FORM_REQ_STATE {
                case SuccessTypes.SUB.rawValue, SuccessTypes.SUC.rawValue:
                    success(response)
                default:
                    failed(BaseResponse.HeaderMessage(MESSAGE_TYPE: "", MESSAGE_DESC: response.getMessage(), MESSAGE_CODE: ""))
                }
            },
            failed: failed
        )
    }

    static func closeOperative(model: OperativeCloseModel, success: @escaping (OperativeCloseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = OperativeCloseRequest()
        request.body.ACCOUNT_ID = model.operativeAccount?.ACCT_NUMBER ?? ""
        request.body.CRN_ONE = model.operativeAccount?.ACCT_CURRENCY ?? ""
        request.body.REPAYACCTID = model.beneficiaryAccount?.ACCT_NUMBER ?? ""
        request.body.CRN_TWO = model.beneficiaryAccount?.ACCT_CURRENCY ?? ""
        request.body.PAY_AMOUNT = model.minimumAmount
        request.body.CLOSE_RES = model.reason?.CM_CODE ?? ""
        ConnectionManager<OperativeCloseResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func getCollateralLoan(model: LoanCollateralModel, success: @escaping (LoanCollateralResponse) -> Void, failed: @escaping ConnectionError) {
        let request = LoanCollateralRequest()
        let currency = model.depositAccount?.ACCT_CURRENCY ?? ""
        let depositNumber = model.depositAccount?.ACCT_NUMBER ?? ""
        let depositType = model.depositAccount?.ACCT_TYPE ?? ""
        request.body.SBL_OPR_ACNT = model.beneficiaryAccount?.ACCT_NUMBER ?? ""
        request.body.SBL_CURRENCY_CODE = currency
        request.body.SBL_LOAN_AMOUNT = model.loanAmount
        request.body.SBL_DEP_ACNT = "format_with_slash".localized(with: depositNumber, depositType)
        request.body.SBL_DAYS = model.loanDuration
        request.body.SBL_FEE_ACNT = depositNumber
        request.body.BRANCH_ID = model.depositAccount?.BRANCH_ID ?? ""
        request.body.SBL_INTEREST = model.depositAccountDetails?.balance?.SBL_INTEREST_RATE ?? ""
        request.body.SBL_FEE = model.fee
        request.body.SBL_OPR_CURR = model.beneficiaryAccount?.ACCT_CURRENCY ?? ""
        request.body.SBL_MATURITY_DATE = model.depositAccountDetails?.detail?.MATURITY_DATE ?? ""
        ConnectionManager<LoanCollateralResponse>.post(
            params: request,
            success: { response in
                switch response.collateral?.FORM_REQ_STATE {
                case SuccessTypes.SUB.rawValue, SuccessTypes.SUC.rawValue:
                    success(response)
                default:
                    failed(BaseResponse.HeaderMessage(MESSAGE_TYPE: "", MESSAGE_DESC: response.getMessage(), MESSAGE_CODE: ""))
                }
            },
            failed: failed
        )
    }

    static func fetchLoanDetail(account: AccountResponse.Account, success: @escaping (LoanDetailResponse) -> Void, failed: @escaping ConnectionError) {
        let request = LoanDetailRequest()
        request.body.ACCOUNT = account.ACCT_NUMBER ?? ""
        request.body.ACCOUNT_CUR = account.ACCT_CURRENCY ?? ""
        ConnectionManager<LoanDetailResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func closeLoan(model: LoanCloseModel, success: @escaping (LoanCloseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = LoanCloseRequest()
        request.body.USRCHOSN_LNACCNT_ID = model.loanAccount?.ACCT_NUMBER ?? ""
        request.body.USRCHOSN_LNACCT_CRN = model.loanAccount?.ACCT_CURRENCY ?? ""
        request.body.USRCHOSN_OPACCT_ID = model.remitterAccount?.ACCT_NUMBER ?? ""
        request.body.USRCHOSN_OPACCT_CRN = model.remitterAccount?.ACCT_CURRENCY ?? ""
        ConnectionManager<LoanCloseResponse>.post(
            params: request,
            success: { response in
                switch response.close?.FORM_REQ_STATE {
                case SuccessTypes.SUB.rawValue, SuccessTypes.SUC.rawValue:
                    success(response)
                default:
                    failed(BaseResponse.HeaderMessage(MESSAGE_TYPE: "", MESSAGE_DESC: response.getMessage(), MESSAGE_CODE: ""))
                }
            },
            failed: failed
        )
    }

    static func fetchAmortizationList(account: AccountResponse.Account, success: @escaping (LoanGraphicResponse) -> Void, failed: @escaping ConnectionError) {
        let request = LoanGraphicRequest()
        request.body.ACCOUNT_TYPE = account.ACCT_TYPE.orEmpty
        request.body.ACCOUNT_NUMBER = account.ACCT_NUMBER.orEmpty
        ConnectionManager<LoanGraphicResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchDigitalLoanStatus(success: @escaping (LoanDigitalCheckResponse) -> Void, failed: @escaping ConnectionError) {
        let request = LoanDigitalRequest()
        ConnectionManager<LoanDigitalCheckResponse>.post(
            params: request,
            success: { response in
                switch response.error?.ERROR_FLG {
                case DCTypes.N.rawValue:
                    success(response)
                default:
                    failed(BaseResponse.HeaderMessage(MESSAGE_TYPE: "", MESSAGE_DESC: response.error?.ERROR_DESCRIPTION ?? "", MESSAGE_CODE: ""))
                }
            },
            failed: failed
        )
    }

    static func fetchDigitalLoanDetail(model: LoanDigitalModel, success: @escaping (LoanDigitalDetailResponse) -> Void, failed: @escaping ConnectionError) {
        let request = LoanDigitalRequest()
        let details = model.initialDetails!
        request.body.EVENT = .FETCH_DATA
        request.body.LOAN_AMOUNT = model.amount
        request.body.MAX_AMOUNT = details.MAX_AMOUNT.orEmpty
        request.body.MIN_AMOUNT = details.MIN_AMOUNT.orEmpty
        request.body.MM_AMOUNT = details.MM_AMOUNT.orEmpty
        request.body.IB_AVAILABLE_AMOUNT = details.IB_AVAILABLE_AMOUNT.orEmpty
        request.body.ISPREAPPROVED = details.ISPREAPPROVED.orEmpty
        request.body.MASTER_FLG = details.MASTER_FLG.orEmpty
        request.body.NPL_FLG = details.SAL_FLG.orEmpty
        request.body.IS_TOPUP = details.IS_TOPUP.orEmpty
        request.body.LOAN_ACCT = details.LOAN_ACCT.orEmpty
        request.body.LOAN_ACCT_INT = details.LOAN_ACCT_INT.orEmpty
        request.body.CLR_BALANCE = details.CLR_BALANCE.orEmpty
        request.body.SALARY_ACCT = details.SALARY_ACCT.orEmpty
        request.body.MISPURPOSECODE = details.MISPURPOSECODE.orEmpty
        request.body.MISSUBPURPOSECODE = details.MISSUBPURPOSECODE.orEmpty
        request.body.MISGROUPCODE = details.MISGROUPCODE.orEmpty
        request.body.SOLID = details.SOLID.orEmpty
        request.body.ACCT_MGR_DOMAIN = details.ACCT_MGR_DOMAIN.orEmpty
        request.body.LOAN_FEE_PERCENT = details.LOAN_FEE_PERCENT.orEmpty
        request.body.SAL_DATE = details.SAL_DATE.orEmpty
        request.body.EMP_FLG = details.EMP_FLG.orEmpty
        ConnectionManager<LoanDigitalDetailResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchDigitalLoanInstallmentAmount(model: LoanDigitalModel, success: @escaping (LoanDigitalInstallmentResponse) -> Void, failed: @escaping ConnectionError) {
        let request = LoanDigitalRequest()
        let details = model.initialDetails!
        let detail = model.detail!
        request.body.EVENT = .FETCH_INSTALLMENT_AMOUNT
        request.body.LOAN_AMOUNT = model.amount
        request.body.MAX_AMOUNT = details.MAX_AMOUNT.orEmpty
        request.body.MIN_AMOUNT = details.MIN_AMOUNT.orEmpty
        request.body.MM_AMOUNT = details.MM_AMOUNT.orEmpty
        request.body.IB_AVAILABLE_AMOUNT = details.IB_AVAILABLE_AMOUNT.orEmpty
        request.body.ISPREAPPROVED = details.ISPREAPPROVED.orEmpty
        request.body.MASTER_FLG = details.MASTER_FLG.orEmpty
        request.body.NPL_FLG = details.SAL_FLG.orEmpty
        request.body.IS_TOPUP = details.IS_TOPUP.orEmpty
        request.body.LOAN_ACCT = details.LOAN_ACCT.orEmpty
        request.body.LOAN_ACCT_INT = details.LOAN_ACCT_INT.orEmpty
        request.body.CLR_BALANCE = details.CLR_BALANCE.orEmpty
        request.body.SALARY_ACCT = details.SALARY_ACCT.orEmpty
        request.body.MISPURPOSECODE = details.MISPURPOSECODE.orEmpty
        request.body.MISSUBPURPOSECODE = details.MISSUBPURPOSECODE.orEmpty
        request.body.MISGROUPCODE = details.MISGROUPCODE.orEmpty
        request.body.SOLID = details.SOLID.orEmpty
        request.body.ACCT_MGR_DOMAIN = details.ACCT_MGR_DOMAIN.orEmpty
        request.body.LOAN_FEE_PERCENT = details.LOAN_FEE_PERCENT.orEmpty
        request.body.SAL_DATE = details.SAL_DATE.orEmpty
        request.body.EMP_FLG = details.EMP_FLG.orEmpty

        request.body.LOAN_FEE = detail.LOAN_FEE.orEmpty
        request.body.TOTAL_BALANCE = detail.TOTAL_BALANCE.orEmpty
        request.body.INSTALLMENT_START_DATE = detail.INSTALLMENT_START_DATE.orEmpty
        request.body.LOAN_INT = detail.LOAN_INT.orEmpty
        request.body.DEDUCTION_DAY = detail.DEDUCTION_DAY.orEmpty
        request.body.DEDUCTION_TYPE = detail.DEDUCTION_TYPE.orEmpty
        request.body.LOAN_PERIOD = model.duration?.CM_CODE ?? ""

        ConnectionManager<LoanDigitalInstallmentResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func getDigitalLoan(model: LoanDigitalModel, success: @escaping (BaseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = LoanDigitalRequest()
        let details = model.initialDetails!
        let detail = model.detail!
        request.body.EVENT = .CONFIRM
        request.body.LOAN_AMOUNT = model.amount
        request.body.MAX_AMOUNT = details.MAX_AMOUNT.orEmpty
        request.body.MIN_AMOUNT = details.MIN_AMOUNT.orEmpty
        request.body.MM_AMOUNT = details.MM_AMOUNT.orEmpty
        request.body.IB_AVAILABLE_AMOUNT = details.IB_AVAILABLE_AMOUNT.orEmpty
        request.body.ISPREAPPROVED = details.ISPREAPPROVED.orEmpty
        request.body.MASTER_FLG = details.MASTER_FLG.orEmpty
        request.body.NPL_FLG = details.SAL_FLG.orEmpty
        request.body.IS_TOPUP = details.IS_TOPUP.orEmpty
        request.body.LOAN_ACCT = details.LOAN_ACCT.orEmpty
        request.body.LOAN_ACCT_INT = details.LOAN_ACCT_INT.orEmpty
        request.body.CLR_BALANCE = details.CLR_BALANCE.orEmpty
        request.body.SALARY_ACCT = details.SALARY_ACCT.orEmpty
        request.body.MISPURPOSECODE = details.MISPURPOSECODE.orEmpty
        request.body.MISSUBPURPOSECODE = details.MISSUBPURPOSECODE.orEmpty
        request.body.MISGROUPCODE = details.MISGROUPCODE.orEmpty
        request.body.SOLID = details.SOLID.orEmpty
        request.body.ACCT_MGR_DOMAIN = details.ACCT_MGR_DOMAIN.orEmpty
        request.body.LOAN_FEE_PERCENT = details.LOAN_FEE_PERCENT.orEmpty
        request.body.SAL_DATE = details.SAL_DATE.orEmpty
        request.body.EMP_FLG = details.EMP_FLG.orEmpty
        request.body.OPERATIVE_ACCT = model.beneficiaryAccount?.ACCT_NUMBER ?? ""
        request.body.LOAN_FEE = detail.LOAN_FEE.orEmpty
        request.body.TOTAL_BALANCE = detail.TOTAL_BALANCE.orEmpty
        request.body.INSTALLMENT_START_DATE = detail.INSTALLMENT_START_DATE.orEmpty
        request.body.LOAN_INT = detail.LOAN_INT.orEmpty
        request.body.DEDUCTION_DAY = detail.DEDUCTION_DAY.orEmpty
        request.body.DEDUCTION_TYPE = detail.DEDUCTION_TYPE.orEmpty
        request.body.LOAN_PERIOD = model.duration?.CM_CODE ?? ""

        ConnectionManager<BaseResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchSwiftBankList(searchValue: String, success: @escaping (SwiftListResponse) -> Void, failed: @escaping ConnectionError) {
        let request = SwiftListRequest()
        request.body.ROUTING_NO = searchValue
        ConnectionManager<SwiftListResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchChallengePacketList(success: @escaping (ChallengePacketResponse) -> Void, failed: @escaping ConnectionError) {
        let request = BaseRequest()
        request.header.__SRVCID__ = "CDCCP"
        ConnectionManager<ChallengePacketResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchChallengeGiftList(success: @escaping (ChallengeGiftResponse) -> Void, failed: @escaping ConnectionError) {
        let request = BaseRequest()
        request.header.__SRVCID__ = "CCGLI"
        ConnectionManager<ChallengeGiftResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchChallengeDetail(challengeId: String, success: @escaping (ChallengePacketDetailResponse) -> Void, failed: @escaping ConnectionError) {
        let request = ChallengePacketDetailRequest()
        request.body.CHALL_ID = challengeId
        ConnectionManager<ChallengePacketDetailResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func getChallengeGift(challengeId: String, giftId: String, giftOrg: String, success: @escaping (ChallengeReceiveGiftResponse) -> Void, failed: @escaping ConnectionError) {
        let request = ChallengeReceiveGiftRequest()
        request.body.CHALL_ID = challengeId
        request.body.GIFT_ID = giftId
        request.body.GIFT_ORG = giftOrg
        ConnectionManager<ChallengeReceiveGiftResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }
    
    static func fetchStandingOrderList(success: @escaping (StandingOrderListResponse) -> Void, failed: @escaping ConnectionError) {
        let request = StandingOrderListRequest()
        request.body.EVENT = "SEARCH"
        ConnectionManager<StandingOrderListResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }
    
    static func registerStandingOrder(model:StandingOrderModel, success: @escaping (BaseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = StandingOrderAddRequest()
        request.body.EVENT = "REGISTER"
        request.body.INITOR_ACCOUNT = model.senderAccountInfo.ACCT_NUMBER ?? ""
        request.body.CP_ACCOUNT_NUMBER = model.beneficiaryAccountInfo.displayNumber ?? ""
        request.body.FREQ_TYPE = "M"
        request.body.TXN_DAY = model.frequencyInfo.monthlyDate
        request.body.END_DATE = model.frequencyInfo.endDate ?? ""
        request.body.FIRST_TXN_DATE = model.frequencyInfo.firstDate ?? ""
        request.body.AMT_ONE = model.transactionInfo.amount
        request.body.CRN_ONE = model.transactionInfo.currency ?? ""
        ConnectionManager<BaseResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }
    
    static func deleteStandingOrder(tranId:String, success: @escaping (BaseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = StandingOrderDeleteRequest()
        request.body.EVENT = "DELETE"
        request.body.TRAN_ID = tranId
        ConnectionManager<BaseResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }
    
    static func fetchBillingOrganizationList(success: @escaping (BillPaymentOrganizationResponse) -> Void, failed: @escaping ConnectionError) {
        let request = BillPaymentOrganizationRequest()
        ConnectionManager<BillPaymentOrganizationResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func registerBillPayment(model: BillPaymentRegisterModel, success: @escaping (BaseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = BillPaymentRegisterRequest()
        guard let organization = model.organization else {
            return
        }
        request.body.BILLER_NAME = organization.BILLER_NAME_INFO.orEmpty
        request.body.LOCAL_BILLER_ID = organization.BILLER_ID_INFO.orEmpty
        request.body.CONSUMER_CODE = model.consumerCode
        request.body.BILLER_TYPE = organization.BILLER_TYPE_INFO.orEmpty
        request.body.BILLER_NICK_NAME = model.nickname
        request.body.SUBSCRIPTION_START_DATE = Date().toFormatted
        request.body.PAYMENT_START_DATE = model.subscriptionDay
        ConnectionManager<BaseResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func fetchBillPaymentInvoice(payment: PaymentResponse.Payment, success: @escaping (BillPaymentInvoiceResponse) -> Void, failed: @escaping ConnectionError) {
        let request = BillPaymentInvoiceRequest()
        request.body.MSISDN = payment.CONSUMER_CODE_DET.orEmpty
        request.body.BNF_ID = payment.BNF_ID.orEmpty
        request.body.OTP_FLG = payment.OTP_FLG.orEmpty
        ConnectionManager<BillPaymentInvoiceResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func editBillPayment(payment: PaymentResponse.Payment, success: @escaping (BaseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = BillPaymentEditRequest()
        request.body.SUBSCRIPTION_ID = payment.SUBSCRIPTION_IDS.orEmpty
        request.body.BILLER_NICK_NAME = payment.BILLERS_NICK_NAME.orEmpty
        request.body.OTP_FLG = "Y"
        ConnectionManager<BaseResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    static func payBillPayment(model: BillPaymentPayModel, success: @escaping (TransactionResponse) -> Void, failed: @escaping ConnectionError) {
        let request = TransactionRequest()
        let payment = model.payment
        let account = model.account
        let organization = model.organization
        request.body.TXN_TYPE = "IBP"
        request.body.TXN_CRN = organization?.BILLER_CURRENCY_INFO ?? ""
        request.body.ENTRY_AMT = model.amount
        request.body.NWK_TYP = TransactionNetworkConstants.golomt.rawValue
        request.body.INIT_ACC_CURRENCY = account?.ACCT_CURRENCY ?? ""
        request.body.INIT_ACC_ENTITY_ID = "transaction_sender_account_format".localized(with: account?.ACCT_NUMBER ?? "", account?.BRANCH_ID ?? "")
        request.body.CP_ENTITY_ID = payment?.SUBSCRIPTION_IDS ?? ""
        request.body.CP_TYPE = "S"
        ConnectionManager<TransactionResponse>.post(
            params: request,
            success: { response in
                if response.transaction?.TXN_STATUS == "FAL" {
                    failed(response.header?.MESSAGE?[0])
                } else {
                    success(response)
                }
            },
            failed: failed
        )
    }
    
    static func buyDataUnit(model: UnitModel, success: @escaping (BaseResponse) -> Void, failed: @escaping ConnectionError) {
        let request = UnitRequest()
        request.body.OPERATOR = model.type?.rawValue ?? ""
        request.body.MOBILE_NUM = model.phoneNumber
        request.body.CARDS = model.selectedUnit?.code ?? ""
        request.body.AMT_ONE = model.transactionInfo.amount
        request.body.REMITTER_ACCOUNT = model.senderAccountInfo.ACCT_NUMBER.orEmpty
        request.body.CRN_ONE = CurrencyConstants.MNT.rawValue
        request.body.MNBC_CARDNUMBER = ""
        request.body.MONTHS = ""
        ConnectionManager<BaseResponse>.post(
            params: request,
            success: success,
            failed: failed
        )
    }

    fileprivate enum SuccessTypes: String {
        case SUB, SUC
    }

    fileprivate enum DCTypes: String {
        case Y, N
    }
}
