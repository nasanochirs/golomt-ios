//
//  ExchangeRateRequest.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/17/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class ExchangeRateRequest: BaseRequest {
    var body = Body()

    private enum CodingKeys: String, CodingKey {
        case body
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(body, forKey: .body)
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    override init() {
        super.init()
        header.__SRVCID__ = "CXCHR"
    }

    struct Body: Codable {
        var TRAN_INI_TYPE = "O"
        var TRAN_INI_CRN = ""
        var TRAN_CP_TYPE = "H"
        var TRAN_CP_CRN = ""
        var TXN_TYPE = ""
        var TXN_CRN = ""
        var INITOR_ACCOUNT = ""
        var TRAN_CP_ENTITY_ID = ""
        var TXN_FREQ_TYPE = "O"
    }
}
