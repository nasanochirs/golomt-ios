//
//  ExchangeRateResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/17/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class ExchangeRateResponse: BaseResponse {
    let rateDetail: Detail?
    
    private enum CodingKeys: String, CodingKey {
        case ExchangeRateDetails
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(rateDetail, forKey: .ExchangeRateDetails)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.rateDetail = try container.decodeIfPresent(Detail.self, forKey: .ExchangeRateDetails)
        try super.init(from: decoder)
    }
    
    struct Detail: Codable {
        var RESULTANT_COUNTER_RATE: String?
        var RATE_MODE: String?
        var RATE_CODE: String?
        var VAR_CUR: String?
        var FIX_CUR: String?
        var FIX_CUR_CODE: String?
        var VAR_CUR_CODE: String?
    }
}
