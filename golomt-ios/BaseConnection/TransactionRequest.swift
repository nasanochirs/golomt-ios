//
//  TransactionRequest.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/23/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class TransactionRequest: BaseRequest {
    var body = Body()

    private enum CodingKeys: String, CodingKey {
        case body
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(body, forKey: .body)
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    override init() {
        super.init()
        header.__SRVCID__ = "INITXN"
    }

    struct Body: Codable {
        var TXN_TYPE = ""
        let FREQ_TYPE = "O"
        var TXN_CRN = ""
        let VAL_IND = "N"
        let TOT_NO_INST = "1"
        var ENTRY_AMT = 0.0
        var NWK_TYP = ""
        var INIT_ACC_CURRENCY = ""
        let INIT_ACC_ENTITY_TYPE = "O"
        var INIT_ACC_ENTITY_ID = ""
        var CP_NICKNAME = ""
        var CP_BANK_IDENTIFIER: String? = nil
        var CP_ENTITY_CURRENCY = ""
        var CP_ENTITY_ID = ""
        var CP_TYPE = ""
        var ENT_REMARKS = ""
        var LOAN_OVERDUE_AMT = 0.0
        
        // FOREIGN
        var BENEFICIARY_ADDRESS = ""
        var BANK_IDENTIFIER_TYPE = ""
        var BENEFICIARY_CODE = ""
        var BENEFICIARY_BANK_ADDRESS = ""
        var PURPOSE_CODE = ""
        var CHARGE_OPTION = ""
        var B_OTHER = ""
        var B_NAME = ""
        var B_INN = ""
        var B_KPP = ""
        var B_ACCOUNT = ""
        var BO_CODE = ""
    }
}
