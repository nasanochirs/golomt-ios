//
//  Header.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/2/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Alamofire
import Foundation

class BaseRequest: Codable {
    var header = Header()

    struct Header: Codable {
        var BANK_ID = "01"
        var LANGUAGE_ID = ""
        var CHANNEL_ID = "T"
        var LOGIN_FLAG = "1"
        var USER_PRINCIPAL = ""
        var OPFMT = "JSON"
        var IPFMT = "JSON"
        var ACCESS_CODE: String? = nil
        var AUTH_MODE = "SPWD"
        var __SRVCID__ = ""
        var STATEMODE = "Y"
        var FORM_ID = "0"
        var IP_ADDRESS: String? = "202.21.101.226"
        var VERSION = "s5.1.0"
        var DEVICEID: String? = nil
        var PUBLICKEY: String? = nil
        var VASCO_TOKEN: String? = nil
        init() {
            switch getLanguage() {
            case "mn":
                LANGUAGE_ID = "002"
            default:
                LANGUAGE_ID = "001"
            }
            FORM_ID = formId
            USER_PRINCIPAL = userId
        }
    }
}
