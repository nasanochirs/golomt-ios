//
//  BaseImageRequest.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class BaseImageRequest: Codable {
    var header = Header()

    struct Header: Codable {
        var key = imageKey
        var languageId = ""
        var sysId = "SMART"
        var channelId = "T"

        init() {
            switch getLanguage() {
            case "mn":
                languageId = "002"
            default:
                languageId = "001"
            }
        }
    }
}
