//
//  ImageConnectionManager.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Alamofire
import Foundation

class ImageConnectionManager<T: BaseImageResponse>: NSObject {
    static func imagePost(params: Codable, apiUrl: String, success: @escaping (T) -> Void, failed: @escaping ConnectionError) {
        var request = URLRequest(url: URL(string: getImageRequestUrl() + apiUrl)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer " + imageBearerToken, forHTTPHeaderField: "Authorization")
        request.httpBody = params.toJsonString.data(using: .utf8, allowLossyConversion: false)
        print("REQUEST: ", params.toJsonString)
        AF.request(request).response { response in
            switch response.result {
            case .success:
                guard let jsonData = response.data else {
                    return
                }
                do {
                    let jsonDecoder = JSONDecoder()
                    let decodedData = try jsonDecoder.decode(T.self, from: jsonData)
                    print("RESPONSE: ", decodedData.toJsonString)
                    if decodedData.header?.status == "SUCCESS" {
                        success(decodedData)
                    } else {
                        let message = BaseResponse.HeaderMessage(MESSAGE_TYPE: "", MESSAGE_DESC: "", MESSAGE_CODE: "")
                        failed(message)
                    }
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
}
