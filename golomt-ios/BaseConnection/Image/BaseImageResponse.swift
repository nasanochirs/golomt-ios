//
//  BaseImageResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class BaseImageResponse: Codable {
    var header: Header?

    struct Header: Codable {
        var code: String?
        var status: String?
        var date: String?
    }
}
