//
//  ImageConnectionFactory.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class ImageConnectionFactory {
    static func fetchToken(success: @escaping (ImageTokenResponse) -> Void, failed: @escaping ConnectionError) {
        let request = ImageTokenRequest()
        guard let registerNum = userDetails?.REGISTER_NUM else {
            return
        }
        request.body = encryptImageString(request: registerNum)
        ImageConnectionManager<ImageTokenResponse>.imagePost(
            params: request,
            apiUrl: "token",
            success: success,
            failed: failed
        )
    }

    static func fetchList(success: @escaping (ImageListResponse) -> Void, failed: @escaping ConnectionError) {
        let request = ImageListRequest()
        guard let registerNum = userDetails?.REGISTER_NUM else {
            return
        }
        request.body.key = encryptImageString(request: registerNum)
        ImageConnectionManager<ImageListResponse>.imagePost(
            params: request,
            apiUrl: "list",
            success: success,
            failed: failed
        )
    }

    static func uploadImage(image: String, success: @escaping (BaseImageResponse) -> Void, failed: @escaping ConnectionError) {
        let request = ImageUploadRequest()
        guard let registerNum = userDetails?.REGISTER_NUM else {
            return
        }
        guard let fullName = userDetails?.FULLNAME else {
            return
        }
        request.body.regNo = encryptImageString(request: registerNum)
        request.body.imgBase64 = "data:image/jpeg;base64," + image
        request.body.name = fullName
        request.body.userType = "1"
        request.body.imgName = fullName + ".jpeg"
        ImageConnectionManager<BaseImageResponse>.imagePost(
            params: request,
            apiUrl: "upload",
            success: success,
            failed: failed
        )
    }
}
