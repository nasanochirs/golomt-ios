//
//  ChallengeImageCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/19/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class ChallengeImageCell: UITableViewCell {
    @IBOutlet var challengeImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setImage(link: String) {
        ImageResponseSerializer.addAcceptableImageContentTypes([])
        guard let url = URL(string: link) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.get.rawValue
        let downloader = ImageDownloader()
        downloader.download(request) { response in
            if case .success(let image) = response.result {
                self.challengeImageView.image = image
            }
        }
    }
    
}
