//
//  ChallengePacketCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit
import UICircularProgressRing

class ChallengePacketCell: UITableViewCell {
    @IBOutlet var circularProgressView: UICircularProgressRing!
    @IBOutlet var giftImageView: UIImageView!
    @IBOutlet var packetDetailLabel: UILabel!
    @IBOutlet var containerView: UIView!
    @IBOutlet var packetCompletionRateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        packetDetailLabel.makeBold()
        packetDetailLabel.useLargeFont()
        packetDetailLabel.textColor = .defaultPrimaryText
        packetCompletionRateLabel.textColor = .defaultSecondaryText
        packetCompletionRateLabel.useSmallFont()
        containerView.corner(cornerRadius: 10)
        circularProgressView.font = .boldSystemFont(ofSize: 15)
        circularProgressView.fontColor = .defaultSecondaryText
        circularProgressView.innerRingColor = .defaultPurpleGradientStart
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func set(packet: ChallengePacketResponse.Packet) {
        circularProgressView.value = CGFloat(packet.CHALLPER_ARRAY.toInt)
        packetDetailLabel.text = packet.PACKETNAME_ARRAY
    }
    
    func set(tasks: [ChallengePacketDetailResponse.Task]) {
        var total = 0
        var completed = 0
        tasks.forEach { task in
            total += task.TASK_NUM.toInt
            completed += task.TASK_PER.toInt
        }
        packetCompletionRateLabel.text = "digital_challenge_completed_task_label".localized(with: String(completed), String(total))
    }
}
