//
//  ChallengeRewardCodeCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/19/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class ChallengeRewardCodeCell: UITableViewCell {
    @IBOutlet var rewardTitleLabel: UILabel!
    @IBOutlet var rewardCodeContainerView: UIView!
    @IBOutlet var rewardCodeCopyButton: DefaultGradientButton!
    @IBOutlet var rewardCodeLabel: UILabel!
    
    var onStateChange: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        rewardCodeContainerView.corner(cornerRadius: 10)
        rewardCodeCopyButton.titleText = "Хуулах"
        rewardTitleLabel.textColor = .defaultSeparator
        rewardTitleLabel.useSmallFont()
        rewardTitleLabel.text = "Таны код"
        rewardCodeLabel.useXXLargeFont()
        rewardCodeLabel.textColor = .defaultPrimaryText
        rewardCodeLabel.makeBold()
    }
    
    func setReward(gift: ChallengeGiftResponse.Gift) {
        rewardCodeLabel.text = gift.GL_CODE
        onStateChange?()
    }
    
    func setReward(_ reward: ChallengeReceiveGiftResponse.Reward) {
        rewardCodeLabel.text = reward.GL_CODE
        onStateChange?()
    }
}
