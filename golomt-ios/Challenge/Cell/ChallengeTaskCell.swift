//
//  ChallengeTaskCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/19/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class ChallengeTaskCell: UITableViewCell {
    @IBOutlet var completionImageView: UIImageView!
    @IBOutlet var taskNameLabel: UILabel!
    @IBOutlet var taskRateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        taskNameLabel.textColor = .defaultPrimaryText
        taskNameLabel.useMediumFont()
        taskRateLabel.textColor = .defaultSecondaryText
        taskRateLabel.useMediumFont()
        completionImageView.setTint(color: .defaultPurpleGradientStart)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func set(task: ChallengePacketDetailResponse.Task) {
        taskNameLabel.text = task.TASK_NAME
        taskRateLabel.text = String.init(format: "%@/%@", task.TASK_PER.orEmpty, task.TASK_NUM.orEmpty)
        if task.TASK_PER.orEmpty != task.TASK_NUM.orEmpty {
            completionImageView.image = UIImage(named: "tick_circle")
        } else {
            completionImageView.image = UIImage(named: "tick")
        }
    }
    
}
