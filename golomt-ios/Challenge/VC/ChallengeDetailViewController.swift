//
//  ChallengeDetailViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/19/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class ChallengeDetailViewController: TableViewController {
    var packet: ChallengePacketResponse.Packet?
    var gift: ChallengeGiftResponse.Gift?
    var counts = [ChallengeGiftResponse.Count]()

    lazy var packetField: ChallengePacketCell = {
        let nib = Bundle.main.loadNibNamed("ChallengePacketCell", owner: self, options: nil)
        guard let cell = nib?.first as? ChallengePacketCell else {
            return ChallengePacketCell()
        }
        if let packet = self.packet {
            cell.set(packet: packet)
        }
        return cell
    }()

    lazy var imageField: ChallengeImageCell = {
        let nib = Bundle.main.loadNibNamed("ChallengeImageCell", owner: self, options: nil)
        guard let cell = nib?.first as? ChallengeImageCell else {
            return ChallengeImageCell()
        }
        return cell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.model.sections = [TableViewModel.Section(rows: [TableViewModel.Row(cell: self.packetField), TableViewModel.Row(cell: self.imageField)])]
        self.tableView.registerCell(nibName: "ChallengeTaskCell")
        self.tableView.reloadData()
        self.fetchDetail()
        onContinue = {
            self.handleContinue()
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 1:
            switch self.packet?.CHALLPER_ARRAY.toInt {
            case 100:
                return tableView.bounds.size.height / 2
            default:
                return 0
            }
        default:
            return UITableView.automaticDimension
        }
    }

    private func fetchDetail() {
        showLoader()
        let challengeId = self.packet?.CHALLID_ARRAY
        ConnectionFactory.fetchChallengeDetail(
            challengeId: challengeId.orEmpty,
            success: { response in
                self.hideLoader()
                self.packetField.set(tasks: response.taskList ?? [])
                response.taskList?.forEach(
                    { task in
                        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "ChallengeTaskCell") as? ChallengeTaskCell else {
                            return
                        }
                        cell.set(task: task)
                        self.model.sections[0].rows.append(
                            TableViewModel.Row(
                                cell: cell
                            )
                        )
                    }
                )
                switch self.packet?.CHALLPER_ARRAY.toInt {
                case 100:
                    self.hasButton = true
                    self.imageField.setImage(link: getProductRequestUrl() + "challengeincomplete.png")
                default:
                    self.hasButton = false
                }
                self.tableView.reloadData()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }

    private func handleContinue() {
        let controller = PickerController()
        controller.list = self.counts
        controller.onItemSelect = { item in
            controller.popupController?.dismiss()
            self.handleGiftSelect(item as? ChallengeGiftResponse.Count)
        }
        controller.title = "Бэлгээ сонгоно уу".localized()
        let popupController = STPopupController(rootViewController: controller)
        popupController.style = .bottomSheet
        popupController.present(in: self)
    }

    private func handleGiftSelect(_ count: ChallengeGiftResponse.Count?) {
        guard let orgId = count?.GIFT_ORG else {
            return
        }
        guard let challengeId = packet?.CHALLID_ARRAY else {
            return
        }
        guard let giftId = gift?.GIFT_ID else {
            return
        }
        let giftActionDialog = UIAlertController(title: nil, message: "Та сонголтоо зөв хийнэ үү. Сонголтоо өөрчлөх боломжгүйг анхаарна уу", preferredStyle: .actionSheet)
        giftActionDialog.popoverPresentationController?.barButtonItem = navigationItem.leftBarButtonItem
        let chooseAction = UIAlertAction(title: "Сонгох".localized(), style: .destructive, handler: { _ in
            self.showLoader()
            ConnectionFactory.getChallengeGift(
                challengeId: challengeId,
                giftId: giftId,
                giftOrg: orgId,
                success: { response in
                    self.hideLoader()
                    let giftVC = ChallengeGiftViewController(nibName: "TableViewController", bundle: nil)
                    giftVC.packet = self.packet
                    giftVC.reward = response.reward
                    self.navigationController?.pushViewController(giftVC, animated: true)
                },
                failed: { reason in
                    self.hideLoader()
                    self.handleRequestFailure(reason)
                }
            )
        })
        let cancelAction = UIAlertAction(title: "Болих".localized(), style: .cancel, handler: { _ in

        })
        giftActionDialog.addAction(chooseAction)
        giftActionDialog.addAction(cancelAction)
        present(giftActionDialog, animated: true)
    }
}
