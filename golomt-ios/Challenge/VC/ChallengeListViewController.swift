//
//  ChallengeListViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class ChallengeListViewController: TableViewController {
    var packetList = [ChallengePacketResponse.Packet]()
    var giftList = [ChallengeGiftResponse.Gift]()
    var countList = [ChallengeGiftResponse.Count]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "digital_challenge_title".localized()
        hasButton = false
        tableView.registerCell(nibName: "ChallengePacketCell")
        NotificationCenter.default.addObserver(self, selector: #selector(fetchPacketList), name: Notification.Name(NotificationConstants.REFRESH_PACKET_LIST), object: nil)
        self.fetchPacketList()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        let packet = packetList[indexPath.row]
        
        let gift = giftList.filter({ $0.CHALL_ID == packet.CHALLID_ARRAY }).first
        
        let counts = countList.filter({ $0.GIFT_COUNT_ID == gift?.GIFT_ID })
        
        switch gift?.GIFT_STATUS.orEmpty {
        case "Y":
            let giftVC = ChallengeGiftViewController(nibName: "TableViewController", bundle: nil)
            giftVC.packet = packet
            giftVC.gift = gift
            self.navigationController?.pushViewController(giftVC, animated: true)
        default:
            let detailVC = ChallengeDetailViewController(nibName: "TableViewController", bundle: nil)
            detailVC.packet = packet
            detailVC.gift = gift
            detailVC.counts = counts
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
    }
    
    private func fetchGiftList() {
        showLoader()
        ConnectionFactory.fetchChallengeGiftList(
            success: { response in
                self.hideLoader()
                self.giftList = response.giftList ?? []
                self.countList = response.countList ?? []
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
    
    @objc private func fetchPacketList() {
        self.showLoader()
        ConnectionFactory.fetchChallengePacketList(
            success: { response in
                self.hideLoader()
                let section = TableViewModel.Section(
                    rows: []
                )
                response.packetList?.forEach(
                    { packet in
                        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "ChallengePacketCell") as? ChallengePacketCell else {
                            return
                        }
                        cell.set(packet: packet)
                        section.rows.append(
                            TableViewModel.Row(
                                cell: cell
                            )
                        )
                    }
                )
                if section.rows.count > 0 {
                    self.packetList = response.packetList ?? []
                    self.model.sections = [section]
                    self.fetchGiftList()
                    self.tableView.reloadData()
                }
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
}
