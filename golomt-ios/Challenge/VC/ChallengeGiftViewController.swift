//
//  ChallengeGiftViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/19/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class ChallengeGiftViewController: TableViewController {
    var packet: ChallengePacketResponse.Packet?
    var gift: ChallengeGiftResponse.Gift?
    var reward: ChallengeReceiveGiftResponse.Reward?

    lazy var packetField: ChallengePacketCell = {
        let nib = Bundle.main.loadNibNamed("ChallengePacketCell", owner: self, options: nil)
        guard let cell = nib?.first as? ChallengePacketCell else {
            return ChallengePacketCell()
        }
        if let packet = self.packet {
            cell.set(packet: packet)
        }
        return cell
    }()

    lazy var imageField: ChallengeImageCell = {
        let nib = Bundle.main.loadNibNamed("ChallengeImageCell", owner: self, options: nil)
        guard let cell = nib?.first as? ChallengeImageCell else {
            return ChallengeImageCell()
        }
        let giftKey = self.packet?.GIFTKEY_ARRAY
        cell.setImage(link: getProductRequestUrl() + giftKey.orEmpty + ".png")
        return cell
    }()

    lazy var rewardField: ChallengeRewardCodeCell = {
        let nib = Bundle.main.loadNibNamed("ChallengeRewardCodeCell", owner: self, options: nil)
        guard let cell = nib?.first as? ChallengeRewardCodeCell else {
            return ChallengeRewardCodeCell()
        }
        if let gift = self.gift {
            cell.setReward(gift: gift)
        }
        if let reward = self.reward {
            cell.setReward(reward)
        }
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        hasButton = false
        if self.reward != nil {
            self.hasButton = true
            self.onContinue = {
                self.handleContinue()
            }
        }
        self.model.sections = [
            TableViewModel.Section(
                rows: [
                    TableViewModel.Row(
                        cell: self.packetField
                    ),
                    TableViewModel.Row(
                        cell: self.imageField
                    ),
                    TableViewModel.Row(
                        cell: self.rewardField
                    )
                ]
            )
        ]
        self.tableView.registerCell(nibName: "ChallengeTaskCell")
        self.tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 1:
            return tableView.bounds.size.height / 2
        default:
            return UITableView.automaticDimension
        }
    }
    
    private func handleContinue() {
        if let listVC = self.navigationController?.viewControllers[1] {
              self.navigationController?.popToViewController(listVC, animated: true)
          }
          NotificationCenter.default.post(name: Notification.Name(NotificationConstants.REFRESH_PACKET_LIST), object: nil)
    }
}
