//
//  ChallengePacketResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/19/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class ChallengePacketResponse: BaseResponse {
    let packetList: [Packet]?
    
    private enum CodingKeys: String, CodingKey {
        case PacketList_REC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(packetList, forKey: .PacketList_REC)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.packetList = try container.decodeIfPresent([Packet].self, forKey: .PacketList_REC)
        try super.init(from: decoder)
    }
    
    struct Packet: Codable {
        var CHALLID_ARRAY: String?
        var PACKETID_ARRAY: String?
        var PACKETNAME_ARRAY: String?
        var CHALLPER_ARRAY: String?
        var PACKETTYPE_ARRAY: String?
        var GIFTKEY_ARRAY: String?
        var GIFTCOUNT_ARRAY: String?
        var GIFTID_ARRAY: String?
    }
}
