//
//  ChallengeReceiveGiftResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/19/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class ChallengeReceiveGiftResponse: BaseResponse {
    let reward: Reward?
    
    private enum CodingKeys: String, CodingKey {
        case CustGetGift
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(reward, forKey: .CustGetGift)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.reward = try container.decodeIfPresent(Reward.self, forKey: .CustGetGift)
        try super.init(from: decoder)
    }
    
    struct Reward: Codable {
        var GL_CODE: String?
        var GL_DESC: String?
        var GL_ID: String?
    }
}
