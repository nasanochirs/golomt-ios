//
//  ChallengePacketDetailResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/19/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class ChallengePacketDetailResponse: BaseResponse {
    let taskList: [Task]?
    let detail: Detail?
    
    private enum CodingKeys: String, CodingKey {
        case PacketDetail, PacketDetList_REC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(taskList, forKey: .PacketDetList_REC)
        try container.encode(detail, forKey: .PacketDetail)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.taskList = try container.decodeIfPresent([Task].self, forKey: .PacketDetList_REC)
        self.detail = try container.decodeIfPresent(Detail.self, forKey: .PacketDetail)
        try super.init(from: decoder)
    }
    
    struct Task: Codable {
        var CHALL_ID: String?
        var TASK_ID: String?
        var TASK_NAME: String?
        var TASK_PER: String?
        var TASK_NUM: String?
    }
    
    struct Detail: Codable {
        var CHALL_ID: String?
    }
}
