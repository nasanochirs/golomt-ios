//
//  ChallengeGiftResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/19/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class ChallengeGiftResponse: BaseResponse {
    let giftList: [Gift]?
    let countList: [Count]?
    
    private enum CodingKeys: String, CodingKey {
        case GiftList_REC, GiftCount_REC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(giftList, forKey: .GiftList_REC)
        try container.encode(countList, forKey: .GiftCount_REC)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.giftList = try container.decodeIfPresent([Gift].self, forKey: .GiftList_REC)
        self.countList = try container.decodeIfPresent([Count].self, forKey: .GiftCount_REC)
        try super.init(from: decoder)
    }
    
    struct Gift: Codable {
        var CHALL_ID: String?
        var PACKET_NAME: String?
        var GIFT_ID: String?
        var GIFT_EXP_DATE: String?
        var GIFT_TYPE: String?
        var GIFT_STATUS: String?
        var GL_CODE: String?
        var GIFT_TAILBAR: String?
        var GL_ORG: String?
    }
    
    struct Count: Codable {
        var GIFT_ORG: String?
        var GIFT_COUNT_ID: String?
        var GIFT_COUNT: String?
    }
}
