//
//  CardViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/1/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class ServicesViewController: BaseUIViewController {
    @IBOutlet var firstMenuBox: DefaultMenuBox!
    @IBOutlet var secondMenuBox: DefaultMenuBox!
    @IBOutlet var thirdMenuBox: DefaultMenuBox!
    @IBOutlet var fourthMenuBox: DefaultMenuBox!
    @IBOutlet var sixthMenuBox: DefaultMenuBox!
    @IBOutlet var collectionView: UICollectionView!
    
    lazy var helpButton: UIBarButtonItem? = {
        UIBarButtonItem(image: UIImage(named: "help"), style: .plain, target: self, action:  #selector(helpClicked))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.registerCell(nibName: "DefaultQuickAccessCell")
        
        firstMenuBox.onMenuTap = {
            let VC = OperativeMenuViewController(nibName: "MenuTableViewController", bundle: nil)
            self.navigationController?.pushViewController(VC, animated: true)
        }
        
        secondMenuBox.onMenuTap = {
            let VC = DepositMenuViewController(nibName: "MenuTableViewController", bundle: nil)
            self.navigationController?.pushViewController(VC, animated: true)
        }
        
        thirdMenuBox.onMenuTap = {
            let VC = LoanMenuViewController(nibName: "MenuTableViewController", bundle: nil)
            self.navigationController?.pushViewController(VC, animated: true)
        }
        
        fourthMenuBox.onMenuTap = {
            let storyboard = UIStoryboard(name: "CardList", bundle: nil)
            guard let VC = storyboard.instantiateInitialViewController() else {
                return
            }
            self.navigationController?.pushViewController(VC, animated: true)
        }
        
        sixthMenuBox.onMenuTap = {
            let VC = ChallengeListViewController(nibName: "TableViewController", bundle: nil)
            self.navigationController?.pushViewController(VC, animated: true)
        }
        
        tabBarController?.navigationItem.rightBarButtonItems = [helpButton!]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationItem.largeTitleDisplayMode = .always
    }
    
    @objc private func helpClicked() {
        let helpVC = HelpViewController(nibName: "TableViewController", bundle: nil)
        navigationController?.pushViewController(helpVC, animated: true)
    }
}

extension ServicesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        2
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DefaultQuickAccessCell", for: indexPath) as! DefaultQuickAccessCell
        switch indexPath.row {
        case 0:
            cell.descriptionText = "Захиалгат шилжүүлэг"
        case 1:
            cell.descriptionText = "Гүйлгээний лавлагаа"
        default:
            break
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            let standingOrderVC = StandingOrderViewController(nibName: "TableViewController", bundle: nil)
            self.navigationController?.pushViewController(standingOrderVC, animated: true)
        case 1:
            break
        default:
            break
        }
    }
    
}

extension ServicesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.bounds.size.height
        let width = height * 2
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
