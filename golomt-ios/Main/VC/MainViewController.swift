//
//  MainViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Alamofire
import AlamofireImage
import Foundation
import UIKit

class MainViewController: UITabBarController, UITabBarControllerDelegate {
    var username: String?
    var loginName: String!

    lazy var imageView: UIImageView = {
        let profileImage = UIImage(named: "default_profile")?.renderOriginal
        let image = renderer.image { _ in
            profileImage!.draw(in: CGRect(origin: CGPoint.zero, size: CGSize(width: 40, height: 40)))
        }
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.image = image
        imageView.round(borderColor: .white)
        return imageView
    }()

    lazy var renderer: UIGraphicsImageRenderer = {
        UIGraphicsImageRenderer(size: CGSize(width: 40, height: 40))
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.setFirstTab()
        self.setTabs()
        self.setProfile()
        self.fetchFinancesToken()
        setLoginName(self.loginName)
        if self.username != nil {
            let biometricVC = BiometricViewController(nibName: "TableViewController", bundle: nil)
            biometricVC.loginName = self.loginName
            navigationController?.pushViewController(biometricVC, animated: true)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleRefreshProfile(notification:)), name: Notification.Name(NotificationConstants.REFRESH_PROFILE), object: nil)
    }

    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let selectedIndex = tabBarController.viewControllers?.firstIndex(of: viewController)
        switch selectedIndex {
        case 0: self.setFirstTab()
        case 1: self.title = "second_tab_title".localized()
        case 2: self.title = "third_tab_title".localized()
        case 3: self.title = "fourth_tab_title".localized()
        case 4: self.title = "fifth_tab_title".localized()
        default: return
        }
    }

    @objc private func handleRefreshProfile(notification: Notification) {
        self.fetchImage()
    }

    private func setProfile() {
        let horizontalStackView = UIStackView()
        horizontalStackView.axis = .horizontal
        horizontalStackView.alignment = .fill
        horizontalStackView.distribution = .fillProportionally
        horizontalStackView.spacing = 10

        let label = UILabel()
        label.text = profileName
        label.textColor = .white
        label.useMediumFont()
        horizontalStackView.addArrangedSubview(self.imageView)
        horizontalStackView.addArrangedSubview(label)
        let profileButton = UIBarButtonItem(customView: horizontalStackView)
        horizontalStackView.addTapGesture(tapNumber: 1, target: self, action: #selector(self.handleProfileClick))
        self.navigationItem.leftBarButtonItem = profileButton

        ImageConnectionFactory.fetchToken(
            success: { response in
                imageKey = decryptImageString(request: response.body?.key ?? "") ?? ""
                imageBearerToken = response.body?.tokenCode ?? ""
                if getProfileImage() == nil {
                    self.fetchImage()
                } else {
                    let image = UIImage(data: getProfileImage()!)
                    let circleImage = self.renderer.image { _ in
                        image?.draw(in: CGRect(origin: CGPoint.zero, size: CGSize(width: 40, height: 40)))
                    }
                    self.imageView.image = circleImage
                }
            },
            failed: { _ in
                if getProfileImage() != nil {
                    let image = UIImage(data: getProfileImage()!)
                    let circleImage = self.renderer.image { _ in
                        image?.draw(in: CGRect(origin: CGPoint.zero, size: CGSize(width: 40, height: 40)))
                    }
                    self.imageView.image = circleImage
                }
            }
        )
    }

    private func fetchFinancesToken() {
        FinancesConnectionFactory.fetchToken(
            sessionId: sessionId,
            success: { response in
                if response.header?.status == "Success" {
                    financesBearerToken = response.body?.token ?? ""
                }
            },
            failed: { _ in
            }
        )
    }

    private func fetchImage() {
        ImageConnectionFactory.fetchList(
            success: { response in
                response.body?.forEach { image in
                    if image.imageCategory == "PRO" {
                        ImageResponseSerializer.addAcceptableImageContentTypes([])
                        var request = URLRequest(url: URL(string: getImageRequestUrl() + "download?imageName=" + image.imageName)!)
                        request.httpMethod = HTTPMethod.get.rawValue
                        request.setValue("Bearer " + imageBearerToken, forHTTPHeaderField: "Authorization")
                        let downloader = ImageDownloader()
                        downloader.download(request) { response in
                            if case .success(let image) = response.result {
                                setProfileImage(image.jpegData(compressionQuality: 1.0)!)
                                let circleImage = self.renderer.image { _ in
                                    image.draw(in: CGRect(origin: CGPoint.zero, size: CGSize(width: 40, height: 40)))
                                }
                                self.imageView.image = circleImage
                            }
                        }
                    }
                }
            },
            failed: { _ in }
        )
    }

    private func setFirstTab() {
        self.title = "first_tab_title".localized()
    }

    private func setTabs() {
        let firstTab = self.tabBar.items?[0]
        firstTab?.image = UIImage(named: "main")?.renderOriginal
        firstTab?.selectedImage = UIImage(named: "main_selected")?.renderOriginal
        firstTab?.title = "first_tab_title".localized()
        let secondTab = self.tabBar.items?[1]
        secondTab?.image = UIImage(named: "finances")?.renderOriginal
        secondTab?.selectedImage = UIImage(named: "finances_selected")?.renderOriginal
        secondTab?.title = "second_tab_title".localized()
        let thirdTab = self.tabBar.items?[2]
        thirdTab?.image = UIImage(named: "transaction")?.renderOriginal
        thirdTab?.selectedImage = UIImage(named: "transaction_selected")?.renderOriginal
        thirdTab?.title = "third_tab_title".localized()
        let fourthTab = self.tabBar.items?[3]
        fourthTab?.image = UIImage(named: "payment")?.renderOriginal
        fourthTab?.selectedImage = UIImage(named: "payment_selected")?.renderOriginal
        fourthTab?.title = "fourth_tab_title".localized()
        let fifthTab = self.tabBar.items?[4]
        fifthTab?.image = UIImage(named: "services")?.renderOriginal
        fifthTab?.selectedImage = UIImage(named: "services_selected")?.renderOriginal
        fifthTab?.title = "fifth_tab_title".localized()
    }

    @objc private func handleProfileClick() {
        let profileActionDialog = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        profileActionDialog.popoverPresentationController?.barButtonItem = navigationItem.leftBarButtonItem
        let profileSettingsAction = UIAlertAction(title: "profile_settings_action".localized(), style: .default, handler: { _ in
            let storyboard = UIStoryboard(name: "Profile", bundle: nil)
            let viewController = storyboard.instantiateInitialViewController()
            guard let VC = viewController else {
                return
            }
            self.navigationController?.pushViewController(VC, animated: true)
        })
        let logoutAction = UIAlertAction(title: "logout_action".localized(), style: .destructive, handler: { _ in
            let loginStoryBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
            guard let loginVC = loginStoryBoard.instantiateInitialViewController() else {
                return
            }
            guard let window = UIApplication.shared.windows.first else {
                return
            }
            window.rootViewController = loginVC
            window.makeKeyAndVisible()
            UIView.transition(with: window, duration: 0.5, options: .transitionCrossDissolve, animations: nil, completion: { _ in
                NotificationCenter.default.post(name: Notification.Name(NotificationConstants.LOGOUT), object: nil)
            })
        })
        let cancelAction = UIAlertAction(title: "cancel_action".localized(), style: .cancel, handler: nil)
        profileActionDialog.addAction(profileSettingsAction)
        profileActionDialog.addAction(logoutAction)
        profileActionDialog.addAction(cancelAction)
        present(profileActionDialog, animated: true)
    }
}
