//
//  PaymentViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class PaymentViewController: BaseUIViewController {
    @IBOutlet var firstMenuBox: DefaultMenuBox!
    @IBOutlet var secondMenuBox: DefaultMenuBox!
    @IBOutlet var fourthMenuBox: DefaultMenuBox!
    @IBOutlet var collectionView: UICollectionView!
    
    let shortcuts = shortcutList.filter { $0.TRANTYPE == "CCP" || $0.TRANTYPE == "IBP" || $0.TRANTYPE == "LAP" }
    
    lazy var helpButton: UIBarButtonItem? = {
        UIBarButtonItem(image: UIImage(named: "help"), style: .plain, target: self, action: #selector(helpClicked))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.registerCell(nibName: "DefaultQuickAccessCell")
        firstMenuBox.onMenuTap = {
            let storyboard = UIStoryboard(name: "BillPayment", bundle: nil)
            let viewController = storyboard.instantiateInitialViewController()
            guard let VC = viewController else {
                return
            }
            self.navigationController?.pushViewController(VC, animated: true)
        }
        
        secondMenuBox.onMenuTap = {
            let unitVC = PaymentNumberViewController(nibName: "TableViewController", bundle: nil)
            unitVC.type = .Unit
            self.navigationController?.pushViewController(unitVC, animated: true)
        }
        
        fourthMenuBox.onMenuTap = {
            let customsVC = CustomsMenuViewController(nibName: "MenuTableViewController", bundle: nil)
            self.navigationController?.pushViewController(customsVC, animated: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarController?.navigationItem.rightBarButtonItems = [helpButton!]
    }
    
    @objc private func helpClicked() {
        let helpVC = HelpViewController(nibName: "TableViewController", bundle: nil)
        navigationController?.pushViewController(helpVC, animated: true)
    }
}

extension PaymentViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        shortcuts.count > 2 ? 2 : shortcuts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DefaultQuickAccessCell", for: indexPath) as! DefaultQuickAccessCell
        var shortcut: LoginResponse.Shortcut?
        switch indexPath.row {
        case 0:
            shortcut = shortcuts[0]
            cell.titleText = shortcut!.TRANDESC.orEmpty
            cell.descriptionText = "amount_with_currency_symbol".localized(with: shortcut!.TRANAMOUNT.orEmpty, shortcut!.TRANCRN.orEmpty.toCurrencySymbol)
        case 1:
            shortcut = shortcuts[1]
            cell.titleText = shortcut!.TRANDESC.orEmpty
            cell.descriptionText = "amount_with_currency_symbol".localized(with: shortcut!.TRANAMOUNT.orEmpty, shortcut!.TRANCRN.orEmpty.toCurrencySymbol)
        default: break
        }
        switch shortcut?.TRANTYPE.orEmpty {
        case "CCP":
            cell.accountText = shortcut!.ENTITYID.orEmpty
        case "LAP":
            cell.accountText = String(format: "%@ \u{2794} %@", shortcut!.ACCT.orEmpty, shortcut!.ENTITYID.orEmpty)
        case "IBP":
            let selectedPayment = billPaymentList.filter { $0.SUBSCRIPTION_IDS == shortcut!.ENTITYID }.first
            guard let payment = selectedPayment else {
                break
            }
            cell.titleText = String(payment.CONSUMER_CODE_DET.orEmpty.replacingOccurrences(of: "|", with: ",").dropLast())
            cell.accountText = payment.BILLERS_NAME.orEmpty
        default:
            break
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let shortcut = shortcuts[indexPath.row]
        switch shortcut.TRANTYPE.orEmpty {
        case "CCP":
            let cardVC = CreditCardTransactionViewController(nibName: "TableViewController", bundle: nil)
            cardVC.shortcut = shortcut
            navigationController?.pushViewController(cardVC, animated: true)
        case "LAP":
            let loanVC = LoanTransactionViewController(nibName: "TableViewController", bundle: nil)
            loanVC.shortcut = shortcut
            navigationController?.pushViewController(loanVC, animated: true)
        case "IBP":
            let paymentStoryboard: UIStoryboard = UIStoryboard(name: "BillPayment", bundle: nil)
            guard let billVC = paymentStoryboard.instantiateInitialViewController() as? BillPaymentViewController else {
                return
            }
            billVC.shortcut = shortcut
            navigationController?.pushViewController(billVC, animated: true)
        default:
            break
        }
    }
}

extension PaymentViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.bounds.size.height
        let width = height * 3
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
