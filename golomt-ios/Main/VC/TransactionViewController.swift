//
//  TransactionViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/29/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class TransactionViewController: BaseUIViewController {
    @IBOutlet var firstMenuBox: DefaultMenuBox!
    @IBOutlet var secondMenuBox: DefaultMenuBox!
    @IBOutlet var thirdMenuBox: DefaultMenuBox!
    @IBOutlet var fourthMenuBox: DefaultMenuBox!
    @IBOutlet var fifthMenuBox: DefaultMenuBox!
    @IBOutlet var sixthMenuBox: DefaultMenuBox!
    @IBOutlet var collectionView: UICollectionView!
    
    let shortcuts = shortcutList.filter { $0.TRANTYPE == "INB" || $0.TRANTYPE == "XFR" || $0.TRANTYPE == "OSB" }
    
    lazy var helpButton: UIBarButtonItem? = {
        UIBarButtonItem(image: UIImage(named: "help"), style: .plain, target: self, action: #selector(helpClicked))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.registerCell(nibName: "DefaultQuickAccessCell")
        
        firstMenuBox.onMenuTap = {
            let ownVC = OwnTransactionViewController(nibName: "TableViewController", bundle: nil)
            self.navigationController?.pushViewController(ownVC, animated: true)
        }
        
        secondMenuBox.onMenuTap = {
            let golomtVC = GolomtTransactionViewController(nibName: "TableViewController", bundle: nil)
            self.navigationController?.pushViewController(golomtVC, animated: true)
        }
        
        thirdMenuBox.onMenuTap = {
            let bankVC = BankTransactionViewController(nibName: "TableViewController", bundle: nil)
            self.navigationController?.pushViewController(bankVC, animated: true)
        }
        
        fourthMenuBox.onMenuTap = {
            let foreignVC = TransactionForeignViewController(nibName: "TableViewController", bundle: nil)
            self.navigationController?.pushViewController(foreignVC, animated: true)
        }
        
        fifthMenuBox.onMenuTap = {
            let creditVC = CreditCardTransactionViewController(nibName: "TableViewController", bundle: nil)
            self.navigationController?.pushViewController(creditVC, animated: true)
        }
        
        sixthMenuBox.onMenuTap = {
            let loanVC = LoanTransactionViewController(nibName: "TableViewController", bundle: nil)
            self.navigationController?.pushViewController(loanVC, animated: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarController?.navigationItem.rightBarButtonItems = [helpButton!]
    }
    
    @objc private func helpClicked() {
        let helpVC = HelpViewController(nibName: "TableViewController", bundle: nil)
        navigationController?.pushViewController(helpVC, animated: true)
    }
}

extension TransactionViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        shortcuts.count > 2 ? 2 : shortcuts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DefaultQuickAccessCell", for: indexPath) as! DefaultQuickAccessCell
        switch indexPath.row {
        case 0:
            let shortcut = shortcuts[0]
            cell.titleText = shortcut.TRANDESC.orEmpty
            cell.descriptionText = "amount_with_currency_symbol".localized(with: shortcut.TRANAMOUNT.orEmpty, shortcut.TRANCRN.orEmpty.toCurrencySymbol)
            cell.accountText = String.init(format: "%@ \u{2794} %@", shortcut.ACCT.orEmpty, shortcut.ENTITYID.orEmpty)
        case 1:
            let shortcut = shortcuts[1]
            cell.titleText = shortcut.TRANDESC.orEmpty
            cell.descriptionText = "amount_with_currency_symbol".localized(with: shortcut.TRANAMOUNT.orEmpty, shortcut.TRANCRN.orEmpty.toCurrencySymbol)
            cell.accountText = String.init(format: "%@ \u{2794} %@", shortcut.ACCT.orEmpty, shortcut.ENTITYID.orEmpty)
        default: break
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let shortcut = shortcuts[indexPath.row]
        switch shortcut.TRANTYPE.orEmpty {
        case "XFR":
            let ownVC = OwnTransactionViewController(nibName: "TableViewController", bundle: nil)
            ownVC.shortcut = shortcut
            self.navigationController?.pushViewController(ownVC, animated: true)
        case "INB":
            let golomtVC = GolomtTransactionViewController(nibName: "TableViewController", bundle: nil)
            golomtVC.shortcut = shortcut
            self.navigationController?.pushViewController(golomtVC, animated: true)
        case "OSB":
            let bankVC = BankTransactionViewController(nibName: "TableViewController", bundle: nil)
            bankVC.shortcut = shortcut
            self.navigationController?.pushViewController(bankVC, animated: true)
        default:
            break
        }
    }
}

extension TransactionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.bounds.size.height
        let width = height * 3
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
