//
//  DefaultAccountBookCheckCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/21/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class DefaultAccountBookCheckCell: UITableViewCell {
    @IBOutlet var saveAccountBookCheckBox: DefaultCheckBox!
    @IBOutlet var warningContainerView: UIView!
    @IBOutlet var warningLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }
    
    var usedAccountBook: Bool = false {
        didSet {
            saveAccountBookCheckBox.isHidden = usedAccountBook
        }
    }
    
    func shouldSaveToAccountBook() -> Bool {
        if usedAccountBook == true {
            return false
        }
        switch saveAccountBookCheckBox.checkBox.checkState {
        case .checked:
            return true
        default:
            return false
        }
    }
    
    private func initComponent() {
        selectionStyle = .none
        saveAccountBookCheckBox.isPrimary = true
        saveAccountBookCheckBox.labelText = "transaction_confirm_save_to_account_book_label".localized()
        warningContainerView.corner(cornerRadius: 5)
        warningLabel.textColor = .defaultPrimaryText
        warningLabel.font = .italicSystemFont(ofSize: 13)
        warningLabel.adjustsFontSizeToFitWidth = true
        warningLabel.text = "transaction_confirm_warning_label".localized()
    }
}
