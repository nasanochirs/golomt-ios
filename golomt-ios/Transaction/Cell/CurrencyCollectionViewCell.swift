//
//  CurrencyCollectionViewCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/25/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class CurrencyCollectionViewCell: UICollectionViewCell {
    @IBOutlet var flagLabel: UILabel!
    @IBOutlet var currencyLabel: UILabel!
    @IBOutlet var separatorView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        flagLabel.font = .systemFont(ofSize: 30)
        separatorView.backgroundColor = .defaultSeparator
    }

    override var isSelected: Bool {
        didSet {
            if isSelected {
                contentView.backgroundColor = .defaultTertiaryBackground
                contentView.corner(cornerRadius: 10)
            } else {
                contentView.backgroundColor = .defaultSecondaryBackground
                contentView.corner(cornerRadius: 10)
            }
        }
    }

    var currency: CurrencyConstants? {
        get {
            CurrencyConstants(rawValue: currencyLabel.text ?? "")
        }
        set {
            if newValue != nil {
                flagLabel.text = newValue?.rawValue.toFlag
                currencyLabel.text = newValue!.rawValue
            }
        }
    }
}
