//
//  TransactionDetailResultCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/26/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Lottie
import UIKit

class TransactionDetailResultCell: UITableViewCell {
    @IBOutlet var animationView: AnimationView!
    @IBOutlet var transactionStatusLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        transactionStatusLabel.textColor = .defaultPrimaryText
        transactionStatusLabel.useLargeFont()
    }

    func setResult(message: String, isSuccessful: Bool) {
        transactionStatusLabel.text = message
        switch isSuccessful {
        case true:
            animationView.animation = Animation.named("lottie-transaction-success")
            animationView.currentProgress = 1
        case false:
            animationView.animation = Animation.named("lottie-transaction-failed")
            animationView.currentProgress = 1
        }
    }
}
