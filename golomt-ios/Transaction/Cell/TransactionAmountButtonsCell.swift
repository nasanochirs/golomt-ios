//
//  TransactionAmountButtonsCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/18/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class TransactionAmountButtonsCell: UITableViewCell {
    @IBOutlet var firstButtonView: UIView!
    @IBOutlet var secondButtonView: UIView!
    @IBOutlet var firstButtonTitleLabel: UILabel!
    @IBOutlet var firstButtonTextLabel: UILabel!
    @IBOutlet var secondButtonTitleLabel: UILabel!
    @IBOutlet var secondButtonTextLabel: UILabel!
    @IBOutlet var separatorView: UIView!
    
    var onButtonClick: ((Int) -> Void)?
    var onStateChange: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        firstButtonView.corner(cornerRadius: 10)
        secondButtonView.corner(cornerRadius: 10)
        separatorView.backgroundColor = .defaultSeparator
        firstButtonView.backgroundColor = .defaultTertiaryBackground
        secondButtonView.backgroundColor = .defaultTertiaryBackground
        firstButtonTitleLabel?.useXSmallFont()
        firstButtonTextLabel?.useSmallFont()
        secondButtonTitleLabel?.useXSmallFont()
        secondButtonTextLabel?.useSmallFont()
        firstButtonTitleLabel.textColor = .defaultSecondaryText
        secondButtonTitleLabel.textColor = .defaultSecondaryText
        firstButtonTextLabel.textColor = .defaultPrimaryText
        secondButtonTextLabel.textColor = .defaultPrimaryText
        firstButtonView.tag = 0
        firstButtonView.addTapGesture(tapNumber: 1, target: self, action: #selector(buttonSelect(_:)))
        secondButtonView.tag = 1
        secondButtonView.addTapGesture(tapNumber: 1, target: self, action: #selector(buttonSelect(_:)))
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setButtonTitles(_ firstTitle: String, _ secondTitle: String) {
        firstButtonTitleLabel.text = firstTitle.localized()
        secondButtonTitleLabel.text = secondTitle.localized()
    }
    
    func setButtonLabels(_ firstLabel: String, _ secondLabel: String) {
        firstButtonTextLabel.text = firstLabel.localized()
        secondButtonTextLabel.text = secondLabel.localized()
        onStateChange?()
    }
    
    func hideLine() {
        separatorView.isHidden = true
    }
    
    @objc private func buttonSelect(_ target: UITapGestureRecognizer) {
        guard let tag = target.view?.tag else {
            return
        }
        onButtonClick?(tag)
    }
}
