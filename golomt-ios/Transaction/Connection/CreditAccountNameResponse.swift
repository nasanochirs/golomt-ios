//
//  CreditAccountNameResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/18/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class CreditAccountNameResponse: BaseResponse {
    let detail: Detail?
    let currencyList: [Currency]?
    
    private enum CodingKeys: String, CodingKey {
        case basic
        case currencyList_REC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(detail, forKey: .basic)
        try container.encode(currencyList, forKey: .currencyList_REC)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.detail = try container.decodeIfPresent(Detail.self, forKey: .basic)
        self.currencyList = try container.decodeIfPresent([Currency].self, forKey: .currencyList_REC)
        try super.init(from: decoder)
    }
    
    struct Currency: Codable {
        var CURRENCY: String?
    }
    
    struct Detail: Codable {
        var MINIMUM_AMOUNT_DUE: String?
        var CC_ACCOUNT_NAME: String?
        var CC_STATUS: String?
        
        func getMaskedName() -> String {
            let fullName = CC_ACCOUNT_NAME.orEmpty
            var maskedName = ""
            switch fullName.count {
            case 1...2:
                maskedName = String(fullName.enumerated().map { index, char in
                    [fullName.count - 1].contains(index) ? char : "*"
                })
            case 3...5:
                maskedName = String(fullName.enumerated().map { index, char in
                    [0, fullName.count - 1].contains(index) ? char : "*"
                })
            default:
                maskedName = String(fullName.enumerated().map { index, char in
                    [0, 1, fullName.count - 1, fullName.count - 2].contains(index) ? char : "*"
                })
            }
            return maskedName
        }
    }
}
