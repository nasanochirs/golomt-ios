//
//  AccountBookSaveRequest.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/28/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class AccountBookSaveRequest: BaseRequest {
    var body = Body()

    private enum CodingKeys: String, CodingKey {
        case body
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(body, forKey: .body)
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    override init() {
        super.init()
        header.__SRVCID__ = "ACPDTL"
    }

    struct Body: Codable {
        var BNF_NAME = ""
        var BNF_NICK_NAME = ""
        var BNF_ACCT_NUMBER = ""
        let BNF_ENTITY_TYPE = "C"
        var NETWORK_ID = ""
        var BANK_IDENTIFIER = ""
        var BNF_ACCT_CRN = ""
    }
}
