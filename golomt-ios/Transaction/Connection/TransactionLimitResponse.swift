//
//  TransactionLimitResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class TransactionLimitResponse: BaseResponse {
    let detail: Detail?
    
    private enum CodingKeys: String, CodingKey {
        case TransactionLimitDtls
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(detail, forKey: .TransactionLimitDtls)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.detail = try container.decodeIfPresent(Detail.self, forKey: .TransactionLimitDtls)
        try super.init(from: decoder)
    }
    
    struct Detail: Codable {
        var DAILY_LIMIT_AVAILABLE: String?
    }
}
