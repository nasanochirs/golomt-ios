//
//  TransactionLimitRequest.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/13/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class TransactionLimitRequest: BaseRequest {
    var body = Body()

    private enum CodingKeys: String, CodingKey {
        case body
    }

    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(body, forKey: .body)
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    override init() {
        super.init()
        header.__SRVCID__ = "TLMEND"
    }

    struct Body: Codable {
        var SCHEME_CODE = "T06"
        var CHANNEL_TYPE = "T"
        var EFFEVTIVE_TRANSACTION_TYPE = "TTL"
        var AS_ON_DATE = ""
        var TWO_FACTOR_SECURITY = "N"
    }
}
