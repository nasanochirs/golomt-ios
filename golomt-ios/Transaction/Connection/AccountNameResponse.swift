//
//  AccountNameResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/11/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class AccountNameResponse: BaseResponse {
    let detail: Detail?
    
    private enum CodingKeys: String, CodingKey {
        case GeneralDetails
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(detail, forKey: .GeneralDetails)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.detail = try container.decodeIfPresent(Detail.self, forKey: .GeneralDetails)
        try super.init(from: decoder)
    }
    
    struct Detail: Codable {
        var ACCOUNT_ID: String?
        var ACCOUNT_NAME: String?
        var ACCOUNT_BRANCH: String?
        var ACCOUNT_CURRENCY: String?
        var CUST_TYPE: String?
        var COMPONENT_FIELD1: String?
        var COMPONENT_FIELD2: String?
        var COMPONENT_FIELD3: String?
        var COMPONENT_FIELD4: String?
        var COMPONENT_FIELD5: String?
        
        func getExtraComponents() -> [Component] {
            var tmpList = [String?]()
            tmpList.append(COMPONENT_FIELD1.orEmpty)
            tmpList.append(COMPONENT_FIELD2.orEmpty)
            tmpList.append(COMPONENT_FIELD3.orEmpty)
            tmpList.append(COMPONENT_FIELD4.orEmpty)
            tmpList.append(COMPONENT_FIELD5.orEmpty)
            var componentList = [Component]()
            tmpList.forEach { component in
                let splitComponent = component.orEmpty.components(separatedBy: "|")
                let title = splitComponent[0]
                let keyboardType = splitComponent[2]
                let length = Int(splitComponent[1]) ?? 255
                let validationType = splitComponent[3]
                if !title.isEmpty {
                    componentList.append(
                        Component(title: title, length: length, keyboardType: keyboardType, validationType: validationType)
                    )
                }
            }
            return componentList
        }
        
        struct Component {
            var title: String?
            var length: Int
            var keyboardType: String?
            var validationType: String?
        }
        
        func getMaskedName() -> String {
            if CUST_TYPE == "C" {
                return ACCOUNT_NAME.orEmpty
            }
            let fullName = ACCOUNT_NAME?.components(separatedBy: " ")
            let firstName = fullName?[0] ?? ""
            let lastName = fullName?[1] ?? ""
            var maskedFirstName = ""
            var maskedLastName = ""
            switch firstName.count {
            case 1...2:
                maskedFirstName = String(firstName.enumerated().map { index, char in
                    [firstName.count - 1].contains(index) ? char : "*"
                })
            case 3...5:
                maskedFirstName = String(firstName.enumerated().map { index, char in
                    [0, firstName.count - 1].contains(index) ? char : "*"
                })
            default:
                maskedFirstName = String(firstName.enumerated().map { index, char in
                    [0, 1, firstName.count - 1, firstName.count - 2].contains(index) ? char : "*"
                })
            }
            switch lastName.count {
            case 1...2:
                maskedLastName = String(lastName.enumerated().map { index, char in
                    [lastName.count - 1].contains(index) ? char : "*"
                         })
            case 3...5:
                maskedLastName = String(lastName.enumerated().map { index, char in
                    [0, lastName.count - 1].contains(index) ? char : "*"
                })
            default:
                maskedLastName = String(lastName.enumerated().map { index, char in
                    [0, 1, lastName.count - 1, lastName.count - 2].contains(index) ? char : "*"
                })
            }
            return "transaction_between_golomt_masked_full_name".localized(with: maskedFirstName, maskedLastName)
        }
    }
}
