//
//  BankListResponse.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class BankListResponse: BaseResponse {
    let bankList: [Bank]?
    
    private enum CodingKeys: String, CodingKey {
        case BankDetails_REC
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(bankList, forKey: .BankDetails_REC)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.bankList = try container.decodeIfPresent([Bank].self, forKey: .BankDetails_REC)
        try super.init(from: decoder)
    }
    
    struct Bank: Codable {
        var BANK_REF_NO: String?
        var INSTITUTION_NAMES: String?
        var ROUTING_NOS: String?
    }
}
