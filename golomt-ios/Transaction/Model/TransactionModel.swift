//
//  TransactionModel.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/18/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class TransactionModel: NSObject {
    var senderAccountInfo = AccountResponse.Account()
    var beneficiaryAccountInfo = BeneficiaryAccountInfo()
    var extraInfo = ExtraInfo()
    var transactionInfo = TransactionInfo()

    struct BeneficiaryAccountInfo {
        var number: String?
        var displayNumber: String?
        var name: String?
        var displayName: String?
        var currency: String?
        var bank: String?
        var displayBank: String?
        var usedAccountBook: Bool = false
    }
    
    struct ExtraInfo {
        var overdueAmount: Double = 0.0
    }

    struct TransactionInfo {
        var amount: Double?
        var displayAmount: String?
        var remark: String?
        var currency: String?
        var fee: String?
        var type: TransactionTypeConstants?
        var networkType: TransactionNetworkConstants?
    }
}
