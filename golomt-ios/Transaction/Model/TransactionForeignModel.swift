//
//  TransactionForeignModel.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/6/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation

class TransactionForeignModel: NSObject {
    var senderAccountInfo = AccountResponse.Account()
    var beneficiaryAccountInfo = BeneficiaryAccountInfo()
    var beneficiaryBankInfo = BeneficiaryBankInfo()
    var transactionInfo = TransactionInfo()
    var isRuble = false

    struct BeneficiaryAccountInfo {
        var name: String?
        var address: String?
        var number: String?
        var inn: String?
        var kpp: String?
        var voCode: String?
    }

    struct BeneficiaryBankInfo {
        var swift: SwiftListResponse.Bank?
        var bankCode: String?
        var bankNumber: String?
        var bankName: String?
        var bankAddress: String?
        var otherField: String?
    }

    struct TransactionInfo {
        var amount: Double?
        var displayAmount: String?
        var remark: String?
        var currency: String?
        var feeType: String?
        var displayFeeType: String?
        var purpose: String?
        var displayPurpose: String?
    }
}
