//
//  TransactionDetailViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/23/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Lottie
import UIKit

class TransactionDetailViewController: TableViewController {
    var isSuccessful: Bool = false
    var onFinish: (() -> Void)?
    var transactionMessage: String = ""
    var controllerTitle: String = ""

    lazy var resultField: TransactionDetailResultCell = {
        let nib = Bundle.main.loadNibNamed("TransactionDetailResultCell", owner: self, options: nil)
        let cell = nib?.first as? TransactionDetailResultCell
        guard let unwrappedCell = cell else {
            return TransactionDetailResultCell()
        }
        unwrappedCell.setResult(message: self.transactionMessage, isSuccessful: self.isSuccessful)
        return unwrappedCell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Finish)

        title = controllerTitle
        navigationItem.hidesBackButton = true
        navigationItem.rightBarButtonItem = .init(barButtonSystemItem: .action, target: self, action: #selector(handleShare))

        onContinue = {
            self.handleFinish()
        }

        model.sections.insert(
            TableViewModel.Section(
                rows: [
                    TableViewModel.Row(
                        cell: resultField
                    ),
                ]
            ),
            at: 0
        )
    }

    private func handleFinish() {
        onFinish?()
    }

    @objc private func handleShare() {
        let deletedSection = model.sections[1]
        var rect = tableView.rect(forSection: 0)
        rect = rect.union(tableView.rect(forSection: 2))
        rect = rect.union(tableView.rect(forSection: 3))
        UIGraphicsBeginImageContextWithOptions(rect.size, true, 0.0)
        model.sections.remove(at: 1)
        tableView.backgroundColor = .defaultPrimaryBackground
        tableView.reloadData()
        let context = UIGraphicsGetCurrentContext()!
        tableView.layer.render(in: context)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        model.sections.insert(deletedSection, at: 1)
        tableView.backgroundColor = .clear
        tableView.reloadData()
        let ac = UIActivityViewController(activityItems: [img!], applicationActivities: nil)
        present(ac, animated: true)
    }
}
