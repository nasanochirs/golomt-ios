//
//  TransactionConfirmation.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/18/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class TransactionConfirmationViewController: TableViewController {
    var transactionModel = TransactionModel()

    lazy var saveAccountBookField: DefaultAccountBookCheckCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAccountBookCheckCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAccountBookCheckCell
        guard let unwrappedCell = cell else {
            return DefaultAccountBookCheckCell()
        }
        unwrappedCell.usedAccountBook = self.transactionModel.beneficiaryAccountInfo.usedAccountBook
        if self.transactionModel.transactionInfo.type == TransactionTypeConstants.creditCard {
            unwrappedCell.usedAccountBook = true
        }
        return unwrappedCell
    }()

    lazy var senderSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "transaction_confirm_sender_info_label".localized(),
            rows: [
                TableViewModel.Row(
                    title: "transaction_confirm_sender_account_number_title".localized(), info: "transaction_confirm_sender_account_number_label".localized(with: transactionModel.senderAccountInfo.ACCT_NUMBER ?? "", transactionModel.senderAccountInfo.ACCT_CURRENCY ?? "")
                ),
                TableViewModel.Row(
                    title: "transaction_confirm_sender_account_nickname_title".localized(), info: transactionModel.senderAccountInfo.ACCT_NICKNAME
                )
            ]
        )
    }()

    lazy var beneficiarySection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "transaction_confirm_beneficiary_info_label".localized(),
            rows: [
                TableViewModel.Row(
                    title: "transaction_confirm_beneficiary_account_number_title".localized(), info: "transaction_confirm_beneficiary_account_number_label".localized(with: transactionModel.beneficiaryAccountInfo.displayNumber ?? "", transactionModel.beneficiaryAccountInfo.currency ?? "")
                ),
                TableViewModel.Row(
                    title: "transaction_confirm_beneficiary_account_name_title".localized(), info: transactionModel.beneficiaryAccountInfo.displayName ?? ""
                ),
                TableViewModel.Row(
                    title: "transaction_confirm_beneficiary_bank_title".localized(), info: transactionModel.beneficiaryAccountInfo.displayBank ?? ""
                )
            ]
        )
    }()

    lazy var transactionSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "transaction_confirm_transaction_info_label".localized(),
            rows: [
                TableViewModel.Row(
                    title: "transaction_confirm_transaction_amount_title".localized(),
                    info: "transaction_confirm_transaction_amount_label".localized(
                        with: transactionModel.transactionInfo.displayAmount ?? "", transactionModel.transactionInfo.currency ?? ""
                    ),
                    infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "transaction_confirm_transaction_remark_title".localized(), info: transactionModel.transactionInfo.remark ?? ""
                ),
                TableViewModel.Row(
                    title: "transaction_confirm_transaction_fee_title".localized(), info: transactionModel.transactionInfo.fee ?? ""
                )
            ]
        )
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Confirm)
        model.sections = [
            senderSection,
            beneficiarySection,
            transactionSection,
            TableViewModel.Section(
                rows: [
                    TableViewModel.Row(
                        cell: saveAccountBookField
                    )
                ]
            )
        ]
        onContinue = {
            self.handleContinue()
        }
        if transactionModel.senderAccountInfo.ACCT_CURRENCY != transactionModel.transactionInfo.currency {
            showLoader()
            ConnectionFactory.fetchExchangeRate(
                model: transactionModel,
                success: { response in
                    self.transactionSection.rows.insert(
                        TableViewModel.Row(
                            title: "transaction_confirm_transaction_currency_rate_title".localized(),
                            info: response.rateDetail?.RESULTANT_COUNTER_RATE ?? ""
                        ),
                        at: 1
                    )
                    self.model.sections[2] = self.transactionSection
                    self.tableView.reloadData()
                    self.hideLoader()
                },
                failed: { _ in
                    self.hideLoader()
                }
            )
        }
    }

    private func handleContinue() {
        let loaderViewController = TransactionLoadingController(nibName: "TransactionLoadingController", bundle: nil)
        navigationController?.pushViewController(loaderViewController, animated: true)
        let detailViewController = TransactionDetailViewController(nibName: "TableViewController", bundle: nil)
        detailViewController.model.sections = [senderSection, beneficiarySection, transactionSection]
        switch transactionModel.transactionInfo.type {
        case .own:
            detailViewController.controllerTitle = "transaction_between_own_title".localized()
        case .golomt:
            detailViewController.controllerTitle = "transaction_between_golomt_title".localized()
        case .bank:
            detailViewController.controllerTitle = "transaction_between_bank_title".localized()
        case .creditCard:
            detailViewController.controllerTitle = "transaction_between_ccd_title".localized()
        case .loan:
            detailViewController.controllerTitle = "transaction_loan_payment_title".localized()
        default:
            break
        }
        detailViewController.onFinish = {
            self.navigationController?.popToRootViewController(animated: true)
            if detailViewController.isSuccessful {
                NotificationCenter.default.post(name: Notification.Name(NotificationConstants.REFRESH_ACCOUNTS), object: nil)
            }
        }
        ConnectionFactory.makeTransaction(
            model: transactionModel,
            success: { response in
                let resultMessage = response.getMessage()
                detailViewController.transactionMessage = resultMessage
                func setSuccessful() {
                    loaderViewController.setResult(isSuccessful: true, message: resultMessage, completion: {
                        detailViewController.isSuccessful = true
                        self.navigationController?.pushViewController(detailViewController, animated: false)
                    })
                }
                if self.saveAccountBookField.shouldSaveToAccountBook() {
                    ConnectionFactory.saveAccountBook(
                        model: self.transactionModel,
                        success: { _ in
                            setSuccessful()
                        },
                        failed: { _ in
                            setSuccessful()
                        }
                    )
                } else {
                    setSuccessful()
                }
            },
            failed: { reason in
                let resultMessage = reason?.MESSAGE_DESC ?? ""
                detailViewController.transactionMessage = resultMessage
                loaderViewController.setResult(isSuccessful: false, message: resultMessage, completion: {
                    detailViewController.isSuccessful = false
                    self.navigationController?.pushViewController(detailViewController, animated: false)
                })
            }
        )
    }
}
