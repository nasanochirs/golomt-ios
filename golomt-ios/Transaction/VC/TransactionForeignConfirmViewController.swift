//
//  TransactionForeignConfirmViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/6/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class TransactionForeignConfirmViewController: TableViewController {
    var transactionModel = TransactionForeignModel()

    lazy var senderSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "transaction_confirm_sender_info_label".localized(),
            rows: [
                TableViewModel.Row(
                    title: "transaction_confirm_sender_account_number_title".localized(), info: "transaction_confirm_sender_account_number_label".localized(with: transactionModel.senderAccountInfo.ACCT_NUMBER ?? "", transactionModel.senderAccountInfo.ACCT_CURRENCY ?? "")
                ),
                TableViewModel.Row(
                    title: "transaction_confirm_sender_account_nickname_title".localized(), info: transactionModel.senderAccountInfo.ACCT_NICKNAME
                ),
            ]
        )
    }()

    lazy var beneficiarySection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "transaction_confirm_beneficiary_info_label".localized(),
            rows: [
                TableViewModel.Row(
                    title: "transaction_foreign_confirm_beneficiary_name_title".localized(),
                    info: self.transactionModel.beneficiaryAccountInfo.name
                ),
                TableViewModel.Row(
                    title: "transaction_foreign_iban_title".localized(),
                    info: self.transactionModel.beneficiaryAccountInfo.number
                ),
                TableViewModel.Row(
                    title: "transaction_foreign_confirm_beneficiary_address_title".localized(),
                    info: self.transactionModel.beneficiaryAccountInfo.address
                ),
            ]
        )
    }()

    lazy var beneficiaryRubleSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "transaction_confirm_beneficiary_info_label".localized(),
            rows: [
                TableViewModel.Row(
                    title: "transaction_foreign_beneficiary_name_title".localized(),
                    info: self.transactionModel.beneficiaryAccountInfo.name
                ),
                TableViewModel.Row(
                    title: "transaction_foreign_beneficiary_number_title".localized(),
                    info: self.transactionModel.beneficiaryAccountInfo.number
                ),
                TableViewModel.Row(
                    title: "transaction_foreign_beneficiary_address_title".localized(),
                    info: self.transactionModel.beneficiaryAccountInfo.address
                ),
                TableViewModel.Row(
                    title: "transaction_foreign_inn_title".localized(),
                    info: self.transactionModel.beneficiaryAccountInfo.inn
                ),
                TableViewModel.Row(
                    title: "transaction_foreign_kpp_title".localized(),
                    info: self.transactionModel.beneficiaryAccountInfo.kpp
                ),
                TableViewModel.Row(
                    title: "transaction_foreign_vo_code_title".localized(),
                    info: self.transactionModel.beneficiaryAccountInfo.voCode
                ),
            ]
        )
    }()

    lazy var bankSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "transaction_foreign_bank_info_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "transaction_foreign_swift_title".localized(), info: transactionModel.beneficiaryBankInfo.swift?.INSTITUTION_NAMES ?? ""
                ),
                TableViewModel.Row(
                    title: "transaction_foreign_confirm_fee_type_title".localized(), info: transactionModel.transactionInfo.displayFeeType ?? ""
                ),
                TableViewModel.Row(
                    title: "transaction_foreign_confirm_other_title".localized(), info: transactionModel.beneficiaryBankInfo.otherField ?? ""
                ),
            ]
        )
    }()

    lazy var bankRubleSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "transaction_foreign_bank_info_details_title".localized(),
            rows: [
                TableViewModel.Row(
                    title: "transaction_foreign_confirm_bank_code_title".localized(), info: transactionModel.beneficiaryBankInfo.bankCode ?? ""
                ),
                TableViewModel.Row(
                    title: "transaction_foreign_confirm_bank_number_title".localized(), info: transactionModel.beneficiaryBankInfo.bankNumber ?? ""
                ),
                TableViewModel.Row(
                    title: "transaction_foreign_confirm_bank_name_title".localized(), info: transactionModel.beneficiaryBankInfo.bankName ?? ""
                ),
                TableViewModel.Row(
                    title: "transaction_foreign_confirm_bank_address_title".localized(), info: transactionModel.beneficiaryBankInfo.bankAddress ?? ""
                ),
                TableViewModel.Row(
                    title: "transaction_foreign_confirm_fee_type_title".localized(), info: transactionModel.transactionInfo.displayFeeType ?? ""
                ),
                TableViewModel.Row(
                    title: "transaction_foreign_confirm_other_title".localized(), info: transactionModel.beneficiaryBankInfo.otherField ?? ""
                ),
            ]
        )
    }()

    lazy var transactionSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "transaction_confirm_transaction_info_label".localized(),
            rows: [
                TableViewModel.Row(
                    title: "transaction_foreign_transfer_amount_title".localized(),
                    info: "transaction_confirm_transaction_amount_label".localized(
                        with: transactionModel.transactionInfo.displayAmount ?? "", transactionModel.transactionInfo.currency ?? ""
                    ),
                    infoProperty: .DefaultAmount
                ),
                TableViewModel.Row(
                    title: "transcation_foreign_purpose_title".localized(), info: transactionModel.transactionInfo.displayPurpose ?? ""
                ),
                TableViewModel.Row(
                    title: "transaction_foreign_remark_title".localized(), info: transactionModel.transactionInfo.remark ?? ""
                ),
            ]
        )
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Continue)
        model.sections = [
            senderSection,
            transactionSection,
        ]
        switch transactionModel.isRuble {
        case true:
            model.sections.insert(beneficiaryRubleSection, at: 1)
            model.sections.insert(bankRubleSection, at: 2)
        case false:
            model.sections.insert(beneficiarySection, at: 1)
            model.sections.insert(bankSection, at: 2)
        }
        tableView.reloadData()
        onContinue = {
            self.handleContinue()
        }
    }

    private func handleContinue() {
        let loaderViewController = TransactionLoadingController(nibName: "TransactionLoadingController", bundle: nil)
        navigationController?.pushViewController(loaderViewController, animated: true)
        let detailViewController = TransactionDetailViewController(nibName: "TableViewController", bundle: nil)
        detailViewController.model.sections = [senderSection, beneficiarySection, transactionSection]
        detailViewController.onFinish = {
            self.navigationController?.popToRootViewController(animated: true)
            if detailViewController.isSuccessful {
                NotificationCenter.default.post(name: Notification.Name(NotificationConstants.REFRESH_ACCOUNTS), object: nil)
            }
        }
        ConnectionFactory.makeForeignTransaction(
            model: transactionModel,
            success: { response in
                let resultMessage = response.getMessage()
                detailViewController.transactionMessage = resultMessage
                loaderViewController.setResult(isSuccessful: true, message: resultMessage, completion: {
                    detailViewController.isSuccessful = true
                    self.navigationController?.pushViewController(detailViewController, animated: false)
                    }
                )
            },
            failed: { reason in
                let resultMessage = reason?.MESSAGE_DESC ?? ""
                detailViewController.transactionMessage = resultMessage
                loaderViewController.setResult(isSuccessful: false, message: resultMessage, completion: {
                    detailViewController.isSuccessful = false
                    self.navigationController?.pushViewController(detailViewController, animated: false)
                })
            }
        )
    }
}
