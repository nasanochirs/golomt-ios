//
//  GolomtTransactionViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/2/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import IQKeyboardManagerSwift
import STPopup
import UIKit

class GolomtTransactionViewController: TableViewController {
//    @IBOutlet var tableView: UITableView!
    var transactionModel = TransactionModel()
    var selectedAccountBook: AccountBookResponse.AccountBook?
    var shortcut: LoginResponse.Shortcut?

    lazy var senderAccountField: DefaultAccountPickerCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAccountPickerCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAccountPickerCell
        guard let unwrappedCell = cell else {
            return DefaultAccountPickerCell()
        }
        self.onAccountSelect = { account in
            self.senderAccountField.account = account
            self.selectedAccount = account
        }
        unwrappedCell.onPickerClick = {
            self.showSenderAccountPicker()
        }
        return unwrappedCell
    }()

    lazy var beneficiaryAccountField: DefaultTextFieldInfoCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldTitleLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultTextFieldInfoCell
        guard let unwrappedCell = cell else {
            return DefaultTextFieldInfoCell()
        }
        unwrappedCell.title = "transaction_between_golomt_beneficiary_title".localized()
        unwrappedCell.subTitle = "transaction_between_golomt_beneficiary_name_label".localized()
        unwrappedCell.onImageClick = {
            unwrappedCell.focusTextField()
            let controller = AccountBookPickerController()
            controller.accountBooks = golomtAccountBookList
            controller.onAccountBookSelect = { accountBook in
                unwrappedCell.setData(accountBook)
                self.selectedAccountBook = accountBook
                controller.popupController?.dismiss(completion: {
                    self.amountField.focusTextField()
                })
            }
            controller.selectedAccountBook = self.selectedAccountBook
            controller.type = TransactionNetworkConstants.golomt.rawValue
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.setTag(ComponentConstants.INFO_TAG, delegate: self)
        unwrappedCell.keyboardType = .numberPad
        unwrappedCell.onTextChanged = {
            self.selectedAccountBook = nil
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        unwrappedCell.customErrorText = "transaction_between_golomt_error_account_number_wrong".localized()
        return unwrappedCell
    }()

    lazy var amountField: DefaultAmountFieldCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAmountFieldInfoCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAmountFieldCell
        guard let unwrappedCell = cell else {
            return DefaultAmountFieldCell()
        }
        unwrappedCell.title = "transaction_between_golomt_amount_title".localized()
        unwrappedCell.subTitle = "transaction_between_golomt_amount_limit_label".localized()
        unwrappedCell.onCurrencyClick = {
            let controller = PickerController()
            controller.list = self.amountField.currencyList
            controller.onItemSelect = { currency in
                controller.popupController?.dismiss()
                self.amountField.selectedCurrency = currency as? CurrencyConstants
            }
            controller.selectedItem = self.amountField.selectedCurrency
            controller.title = "currency_title".localized()
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var remarkField: DefaultTextFieldInfoCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultTextFieldInfoCell
        guard let unwrappedCell = cell else {
            return DefaultTextFieldInfoCell()
        }
        unwrappedCell.title = "transaction_between_golomt_remark_title".localized()
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        unwrappedCell.hideLine()
        self.returnKeyHandler?.addResponderFromView(unwrappedCell.contentView)
        return unwrappedCell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "transaction_between_golomt_title".localized()
        navigationItem.largeTitleDisplayMode = .never

        setInitialRows()
        hasFooterButton = true

        onContinue = {
            self.handleContinue()
        }

        if selectedAccount == nil, shortcut == nil {
            showSenderAccountPicker()
        } else {
            senderAccountField.account = selectedAccount
        }
        if shortcut != nil {
            senderAccountField.account = operativeAccountList.filter { $0.ACCT_NUMBER == shortcut!.ACCT }.first
            switch shortcut!.ENTITYTYPE {
            case "H":
                beneficiaryAccountField.inputText = shortcut!.ENTITYID.orEmpty
            case "P":
                guard let accountBook = golomtAccountBookList.filter({ $0.PAYEE_LIST_ID == shortcut!.ENTITYID }).first else {
                    return
                }
                self.selectedAccountBook = accountBook
            default:
                break
            }
            amountField.inputText = shortcut!.TRANAMOUNT.orEmpty
            amountField.selectedCurrency = CurrencyConstants(rawValue: shortcut!.TRANCRN.orEmpty)
            remarkField.inputText = shortcut!.TRANDESC.orEmpty
        }
        fetchTransactionLimit()
    }

    private func setInitialRows() {
        model.sections = [
            TableViewModel.Section(
                rows: [
                    TableViewModel.Row(cell: senderAccountField),
                    TableViewModel.Row(cell: beneficiaryAccountField),
                    TableViewModel.Row(cell: amountField),
                    TableViewModel.Row(cell: remarkField),
                ]
            ),
        ]
        tableView.reloadData()
    }

    private func fetchTransactionLimit() {
        showLoader()
        ConnectionFactory.fetchTransactionLimit(
            transactionType: "TTL",
            success: { response in
                self.hideLoader()
                self.amountField.subInfo = response.detail?.DAILY_LIMIT_AVAILABLE
                self.setupAccountBook()
            },
            failed: { _ in
                self.hideLoader()
                self.setupAccountBook()
            }
        )
    }

    private func setupAccountBook() {
        if selectedAccountBook != nil {
            beneficiaryAccountField.setData(selectedAccountBook!)
            fetchAccountName()
        }
    }

    private func fetchAccountName() {
        showLoader()
        ConnectionFactory.fetchAccountName(
            accountNumber: beneficiaryAccountField.inputText,
            success: { response in
                self.hideLoader()
                let currency = CurrencyConstants(rawValue: response.detail?.ACCOUNT_CURRENCY ?? "")
                var currencyList = [CurrencyConstants]()
                if currency != nil {
                    currencyList.append(currency!)
                }
                self.amountField.currencyList = currencyList
                self.beneficiaryAccountField.subInfo = response.detail?.getMaskedName()
                self.transactionModel.beneficiaryAccountInfo = TransactionModel.BeneficiaryAccountInfo(
                    number: self.selectedAccountBook?.PAYEE_LIST_ID ?? response.detail?.ACCOUNT_ID,
                    displayNumber: response.detail?.ACCOUNT_ID,
                    name: response.detail?.ACCOUNT_NAME,
                    displayName: response.detail?.getMaskedName(),
                    currency: response.detail?.ACCOUNT_CURRENCY,
                    bank: response.detail?.ACCOUNT_BRANCH,
                    displayBank: "transaction_confirm_beneficiary_golomt_label".localized()
                )
                self.amountField.focusTextField()
                self.setupExtraComponents(response.detail?.getExtraComponents())
            },
            failed: { _ in
                self.beneficiaryAccountField.subInfo = nil
                self.hideLoader()
            }
        )
    }

    private func setupExtraComponents(_ components: [AccountNameResponse.Detail.Component]?) {
        guard let componentList = components else {
            return
        }
        if componentList.count == 0 {
            return
        }
        setInitialRows()
        let section = model.sections[0]
        section.rows.remove(at: 3)
        for (index, component) in componentList.enumerated() {
            let nib = Bundle.main.loadNibNamed("DefaultTextFieldCell", owner: self, options: nil)
            let cell = nib?.first as? DefaultTextFieldInfoCell
            cell?.title = component.title.orEmpty
            cell?.onStateChange = {
                UIView.performWithoutAnimation {
                    self.tableView.beginUpdates()
                    cell?.layoutSubviews()
                    self.tableView.endUpdates()
                }
            }
            switch component.keyboardType {
            case "n", "N":
                cell?.keyboardType = .numberPad
            default:
                break
            }
            cell?.textLength = component.length
            switch component.validationType {
            case "L":
                cell?.lengthValidation = .Less
            case "E":
                cell?.lengthValidation = .Equal
            default:
                break
            }
            if index == componentList.count - 1 {
                cell?.hideLine()
            }
            guard let unwrappedCell = cell else {
                return
            }
            section.rows.append(TableViewModel.Row(cell: unwrappedCell))
        }
        tableView.reloadData()
    }

    private func handleContinue() {
        if senderAccountField.hasError() {
            showSenderAccountPicker()
            return
        }
        let beneficiaryAccountFieldError = beneficiaryAccountField.hasError()
        let amountFieldError = amountField.hasError()
        var hasError = false
        var corporateRemark = [String]()
        let section = model.sections[0]
        if section.rows.count > 4 {
            for index in 3..<section.rows.count {
                let row = section.rows[index]
                let textField = row.cell as! DefaultTextFieldInfoCell
                if textField.hasError() {
                    hasError = true
                }
                corporateRemark.append(textField.inputText)
            }
        }
        if beneficiaryAccountFieldError || amountFieldError || hasError {
            return
        }
        let confirmationVC = TransactionConfirmationViewController(nibName: "TableViewController", bundle: nil)
        let account = senderAccountField.account!
        confirmationVC.transactionModel.senderAccountInfo = account
        transactionModel.beneficiaryAccountInfo.usedAccountBook = selectedAccountBook != nil
        confirmationVC.transactionModel.beneficiaryAccountInfo = transactionModel.beneficiaryAccountInfo
        var remark = remarkField.inputText
        if remark.isEmpty {
            remark = "transaction_confirm_default_remark".localized(with: profileName)
        }
        if !corporateRemark.isEmpty {
            remark = corporateRemark.joined(separator: ", ")
        }
        confirmationVC.transactionModel.transactionInfo = TransactionModel.TransactionInfo(
            amount: amountField.inputText.toDouble,
            displayAmount: amountField.inputText,
            remark: remark,
            currency: amountField.selectedCurrency?.rawValue,
            fee: "100.00 MNT",
            type: .golomt,
            networkType: .golomt
        )
        navigationController?.pushViewController(confirmationVC, animated: true)
    }
}

extension GolomtTransactionViewController: UITextFieldDelegate, UITextViewDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case ComponentConstants.INFO_TAG:
            if beneficiaryAccountField.checkError() == true, !beneficiaryAccountField.inputText.isEmpty {
                fetchAccountName()
            }
        default:
            break
        }
    }
}
