//
//  BankTransactionViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class BankTransactionViewController: TableViewController {
//    @IBOutlet var tableView: UITableView!
    var selectedAccountBook: AccountBookResponse.AccountBook?
    var shortcut: LoginResponse.Shortcut?

    lazy var senderAccountField: DefaultAccountPickerCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAccountPickerCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAccountPickerCell
        guard let unwrappedCell = cell else {
            return DefaultAccountPickerCell()
        }
        self.onAccountSelect = { account in
            self.senderAccountField.account = account
            self.selectedAccount = account
        }
        unwrappedCell.onPickerClick = {
            self.showSenderAccountPicker()
        }
        return unwrappedCell
    }()

    lazy var beneficiaryBankField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerButtonCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultPickerButtonCell
        guard let unwrappedCell = cell else {
            return DefaultPickerButtonCell()
        }
        unwrappedCell.title = "transaction_between_bank_bank_title".localized()
        unwrappedCell.onImageClick = {
            let controller = AccountBookPickerController()
            controller.accountBooks = bankAccountBookList
            controller.onAccountBookSelect = { accountBook in
                controller.popupController?.dismiss()
                unwrappedCell.pickerList.forEach { bank in
                    if let bank = bank as? BankListResponse.Bank, bank.ROUTING_NOS == accountBook.BNK_IDENTIFIER {
                        unwrappedCell.selectedItem = bank
                        self.beneficiaryAccountNumberField.inputText = accountBook.BNIF_ACCNT_NUMBER.orEmpty
                        self.beneficiaryAccountNameField.inputText = accountBook.BNIF_NAME.orEmpty
                    }
                }
                self.selectedAccountBook = accountBook
            }
            controller.selectedAccountBook = self.selectedAccountBook
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onPickerClick = {
            let controller = PickerController()
            controller.title = "transaction_between_bank_bank_title".localized()
            controller.list = self.beneficiaryBankField.pickerList as! [BankListResponse.Bank]
            controller.onItemSelect = { bank in
                controller.popupController?.dismiss()
                self.beneficiaryAccountNumberField.focusTextField()
                unwrappedCell.selectedItem = bank
            }
            controller.selectedItem = self.beneficiaryBankField.selectedItem
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onItemSelect = { bank in
            self.selectedAccountBook = nil
            self.beneficiaryAccountNumberField.inputText = ""
            self.beneficiaryAccountNameField.inputText = ""
            if let bank = bank as? BankListResponse.Bank {
                let bankCode = bank.ROUTING_NOS?.components(separatedBy: "|").last
                ConnectionFactory.fetchBankCurrencyList(
                    bankCode: bankCode.orEmpty,
                    success: { response in
                        var currencyList = [CurrencyConstants]()
                        response.list?.forEach { item in
                            let currency = CurrencyConstants(rawValue: item.CM_CODE.orEmpty)
                            if currency != nil {
                                currencyList.append(currency!)
                            }
                            self.amountField.currencyList = currencyList
                            self.amountField.selectedCurrency = CurrencyConstants(rawValue: self.selectedAccountBook?.BNF_ACCT_CRN ?? "")
                            if self.selectedAccountBook != nil {
                                self.amountField.focusTextField()
                            }
                        }
                        if self.shortcut != nil {
                            self.amountField.selectedCurrency = CurrencyConstants(rawValue: self.shortcut!.TRANCRN.orEmpty)
                        }
                    }, failed: { _ in
                    }
                )
            }
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var beneficiaryAccountNumberField: DefaultTextFieldInfoCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultTextFieldInfoCell
        guard let unwrappedCell = cell else {
            return DefaultTextFieldInfoCell()
        }
        unwrappedCell.keyboardType = .numberPad
        unwrappedCell.title = "transaction_between_bank_beneficiary_account_number_title".localized()
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var beneficiaryAccountNameField: DefaultTextFieldInfoCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultTextFieldInfoCell
        guard let unwrappedCell = cell else {
            return DefaultTextFieldInfoCell()
        }
        unwrappedCell.title = "transaction_between_bank_beneficiary_account_name_title".localized()
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
//        self.returnKeyHandler?.addResponderFromView(unwrappedCell.contentView)
        return unwrappedCell
    }()

    lazy var amountField: DefaultAmountFieldCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAmountFieldInfoCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAmountFieldCell
        guard let unwrappedCell = cell else {
            return DefaultAmountFieldCell()
        }
        unwrappedCell.title = "transaction_between_bank_amount_title".localized()
        unwrappedCell.subTitle = "transaction_between_bank_amount_limit_label".localized()
        unwrappedCell.onCurrencyClick = {
            let controller = PickerController()
            controller.list = self.amountField.currencyList
            controller.onItemSelect = { currency in
                controller.popupController?.dismiss()
                self.amountField.selectedCurrency = currency as? CurrencyConstants
            }
            controller.selectedItem = self.amountField.selectedCurrency
            controller.title = "currency_title".localized()
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var remarkField: DefaultTextFieldInfoCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultTextFieldInfoCell
        guard let unwrappedCell = cell else {
            return DefaultTextFieldInfoCell()
        }
        unwrappedCell.title = "transaction_between_bank_remark_title".localized()
        unwrappedCell.hideLine()
        return unwrappedCell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "transaction_between_bank_title".localized()
        navigationItem.largeTitleDisplayMode = .never
        hasFooterButton = true
        setInitialRows()

        onContinue = {
            self.handleContinue()
        }

        if selectedAccount == nil, shortcut == nil {
            showSenderAccountPicker()
        } else {
            senderAccountField.account = selectedAccount
        }
        if shortcut != nil {
            senderAccountField.account = operativeAccountList.filter { $0.ACCT_NUMBER == shortcut!.ACCT }.first
            if shortcut!.ENTITYTYPE == "P" {
                guard let accountBook = golomtAccountBookList.filter({ $0.PAYEE_LIST_ID == shortcut!.ENTITYID }).first else {
                    return
                }
                selectedAccountBook = accountBook
            }
            amountField.inputText = shortcut!.TRANAMOUNT.orEmpty
            remarkField.inputText = shortcut!.TRANDESC.orEmpty
        }
        fetchTransactionLimit()
    }

    private func fetchTransactionLimit() {
        showLoader()
        ConnectionFactory.fetchTransactionLimit(
            transactionType: "TTL",
            success: { response in
                self.hideLoader()
                self.amountField.subInfo = response.detail?.DAILY_LIMIT_AVAILABLE
                self.fetchBankList()
            },
            failed: { _ in
                self.hideLoader()
                self.fetchBankList()
            }
        )
    }

    private func fetchBankList() {
        showLoader()
        ConnectionFactory.fetchBankList(
            success: { response in
                self.hideLoader()
                self.beneficiaryBankField.pickerList = response.bankList ?? []

                if self.selectedAccountBook != nil {
                    let accountBook = self.selectedAccountBook!
                    response.bankList?.forEach { bank in
                        if bank.ROUTING_NOS == accountBook.BNK_IDENTIFIER {
                            self.beneficiaryBankField.selectedItem = bank
                            self.beneficiaryAccountNumberField.inputText = accountBook.BNIF_ACCNT_NUMBER.orEmpty
                            self.beneficiaryAccountNameField.inputText = accountBook.BNIF_NAME.orEmpty
                        }
                    }
                    self.selectedAccountBook = accountBook
                }

                if self.shortcut != nil, self.shortcut?.ENTITYTYPE == "H" {
                    response.bankList?.forEach { bank in
                        if bank.ROUTING_NOS == self.shortcut!.ENTITYROUTE {
                            self.beneficiaryBankField.selectedItem = bank
                            self.beneficiaryAccountNumberField.inputText = self.shortcut!.ENTITYID.orEmpty
                            self.beneficiaryAccountNameField.inputText = self.shortcut!.ENTITYNICKNAME.orEmpty
                        }
                    }
                }
            },
            failed: { _ in
                self.hideLoader()
            }
        )
    }

    private func setInitialRows() {
        model.sections = [
            TableViewModel.Section(
                rows: [
                    TableViewModel.Row(cell: senderAccountField),
                    TableViewModel.Row(cell: beneficiaryBankField),
                    TableViewModel.Row(cell: beneficiaryAccountNumberField),
                    TableViewModel.Row(cell: beneficiaryAccountNameField),
                    TableViewModel.Row(cell: amountField),
                    TableViewModel.Row(cell: remarkField),
                ]
            ),
        ]
        tableView.reloadData()
    }

    private func handleContinue() {
        if senderAccountField.hasError() {
            showSenderAccountPicker()
            return
        }
        let beneficiaryBankFieldError = beneficiaryBankField.hasError()
        let beneficiaryAccountNumberFieldError = beneficiaryAccountNumberField.hasError()
        let beneficiaryAccountNameFieldError = beneficiaryAccountNameField.hasError()
        let amountFieldError = amountField.hasError()
        if beneficiaryBankFieldError || beneficiaryAccountNumberFieldError || beneficiaryAccountNameFieldError || amountFieldError {
            return
        }
        let confirmationVC = TransactionConfirmationViewController(nibName: "TableViewController", bundle: nil)
        let account = senderAccountField.account!
        confirmationVC.transactionModel.senderAccountInfo = account
        var accountNumber = beneficiaryAccountNumberField.inputText
        let accountName = beneficiaryAccountNameField.inputText
        if selectedAccountBook != nil {
            accountNumber = selectedAccountBook!.PAYEE_LIST_ID.orEmpty
        }
        let bank = beneficiaryBankField.selectedItem as? BankListResponse.Bank
        confirmationVC.transactionModel.beneficiaryAccountInfo = TransactionModel.BeneficiaryAccountInfo(
            number: accountNumber,
            displayNumber: beneficiaryAccountNumberField.inputText,
            name: accountName,
            displayName: accountName,
            currency: amountField.selectedCurrency?.rawValue,
            bank: bank?.ROUTING_NOS,
            displayBank: bank?.INSTITUTION_NAMES,
            usedAccountBook: selectedAccountBook != nil
        )
        var fee = "300.00 MNT"
        if amountField.selectedCurrency != CurrencyConstants.MNT {
            fee = "400.00 MNT"
        }
        let amount = amountField.inputText.toDouble
        if amount > 3000000 {
            fee = "400.00 MNT"
        }
        var remark = remarkField.inputText
        if remark.isEmpty {
            remark = "transaction_confirm_default_remark_v2".localized(with: profileName, accountName)
        }
        confirmationVC.transactionModel.transactionInfo = TransactionModel.TransactionInfo(
            amount: amount,
            displayAmount: amountField.inputText,
            remark: remark,
            currency: amountField.selectedCurrency?.rawValue,
            fee: fee,
            type: .bank,
            networkType: .bank
        )
        navigationController?.pushViewController(confirmationVC, animated: true)
    }
}
