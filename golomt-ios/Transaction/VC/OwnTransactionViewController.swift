//
//  OwnTransactionViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/28/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class OwnTransactionViewController: TableViewController {
    var transactionModel = TransactionModel()
    var selectedBeneficiaryAccount: AccountResponse.Account?
    var shortcut: LoginResponse.Shortcut?

    lazy var senderAccountField: DefaultAccountPickerCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAccountPickerCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultAccountPickerCell else {
            return DefaultAccountPickerCell()
        }
        self.onAccountSelect = { account in
            self.senderAccountField.account = account
            self.selectedAccount = account
            self.beneficiaryAccountField.selectedItem = nil
            self.selectedBeneficiaryAccount = nil
            self.setCurrency()
        }
        cell.onPickerClick = {
            self.showSenderAccountPicker()
        }
        return cell
    }()

    lazy var beneficiaryAccountField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultPickerButtonCell
        guard let unwrappedCell = cell else {
            return DefaultPickerButtonCell()
        }
        unwrappedCell.title = "transaction_between_own_beneficiary_title".localized()
        unwrappedCell.onPickerClick = {
            let controller = AccountPickerController()
            controller.selectedAccount = self.selectedBeneficiaryAccount
            controller.onAccountSelect = { account in
                self.beneficiaryAccountField.selectedItem = account
                self.selectedBeneficiaryAccount = account
                self.setCurrency()
            }
            controller.title = "transaction_between_own_beneficiary_title".localized()
            var filterList = [AccountResponse.Account]()
            if self.selectedAccount != nil {
                filterList.append(self.selectedAccount!)
            }
            controller.accounts = Array(Set(operativeAccountList).subtracting(filterList))
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var amountField: DefaultAmountFieldCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAmountFieldInfoCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAmountFieldCell
        guard let unwrappedCell = cell else {
            return DefaultAmountFieldCell()
        }
        unwrappedCell.title = "transaction_between_golomt_amount_title".localized()
        unwrappedCell.subTitle = "transaction_between_golomt_amount_limit_label".localized()
        unwrappedCell.onCurrencyClick = {
            let controller = PickerController()
            controller.list = self.amountField.currencyList
            controller.onItemSelect = { currency in
                controller.popupController?.dismiss()
                self.amountField.selectedCurrency = currency as? CurrencyConstants
            }
            controller.selectedItem = self.amountField.selectedCurrency
            controller.title = "currency_title".localized()
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var remarkField: DefaultTextFieldInfoCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultTextFieldInfoCell
        guard let unwrappedCell = cell else {
            return DefaultTextFieldInfoCell()
        }
        unwrappedCell.title = "transaction_between_golomt_remark_title".localized()
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        unwrappedCell.hideLine()
        self.returnKeyHandler?.addResponderFromView(unwrappedCell.contentView)
        return unwrappedCell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "transaction_between_own_title".localized()
        navigationItem.largeTitleDisplayMode = .never

        setInitialRows()
        hasFooterButton = true

        if selectedAccount != nil {
            setSenderAccount(selectedAccount!)
        }
        if selectedBeneficiaryAccount != nil {
            setBeneficiaryAccount(selectedBeneficiaryAccount!)
        }
        if shortcut != nil {
            guard let senderAccount = operativeAccountList.filter({ $0.ACCT_NUMBER == shortcut!.ACCT }).first else {
                return
            }
            setSenderAccount(senderAccount)
            guard let beneficiaryAccount = operativeAccountList.filter({ $0.ACCT_NUMBER == shortcut!.ENTITYID }).first else {
                return
            }
            setBeneficiaryAccount(beneficiaryAccount)
            amountField.inputText = shortcut!.TRANAMOUNT.orEmpty
            amountField.selectedCurrency = CurrencyConstants(rawValue: shortcut!.TRANCRN.orEmpty)
            remarkField.inputText = shortcut!.TRANDESC.orEmpty
        }
        if selectedAccount == nil, shortcut == nil {
            showSenderAccountPicker()
        }
        
        onContinue = {
            self.handleContinue()
        }
        
        fetchTransactionLimit()
    }

    override func viewDidAppear(_ animated: Bool) {
        if !didAppear, selectedBeneficiaryAccount != nil, shortcut == nil {
            amountField.focusTextField()
        }
        super.viewDidAppear(animated)
    }
    
    private func setSenderAccount(_ account: AccountResponse.Account) {
        senderAccountField.account = account
        setCurrency()
    }
    
    private func setBeneficiaryAccount(_ account: AccountResponse.Account) {
        beneficiaryAccountField.selectedItem = account
        selectedBeneficiaryAccount = account
        setCurrency()
    }

    private func setCurrency() {
        let currency = CurrencyConstants(rawValue: selectedAccount?.ACCT_CURRENCY.orEmpty ?? "")
        var currencyList = [CurrencyConstants]()
        if currency != nil {
            currencyList.append(currency!)
        }
        let beneficiaryCurrency = CurrencyConstants(rawValue: selectedBeneficiaryAccount?.ACCT_CURRENCY.orEmpty ?? "")
        if beneficiaryCurrency != nil {
            currencyList.append(beneficiaryCurrency!)
            currencyList = currencyList.uniqueValues()
        }
        amountField.currencyList = currencyList
    }

    private func setInitialRows() {
        model.sections = [
            TableViewModel.Section(
                rows: [
                    TableViewModel.Row(cell: senderAccountField),
                    TableViewModel.Row(cell: beneficiaryAccountField),
                    TableViewModel.Row(cell: amountField),
                    TableViewModel.Row(cell: remarkField),
                ]
            ),
        ]
        tableView.reloadData()
    }

    private func fetchTransactionLimit() {
        showLoader()
        ConnectionFactory.fetchTransactionLimit(
            transactionType: "TTL",
            success: { response in
                self.hideLoader()
                self.amountField.subInfo = response.detail?.DAILY_LIMIT_AVAILABLE
            },
            failed: { _ in
                self.hideLoader()
            }
        )
    }

    private func handleContinue() {
        if senderAccountField.hasError() {
            showSenderAccountPicker()
            return
        }
        let beneficiaryAccountFieldError = beneficiaryAccountField.hasError()
        let amountFieldError = amountField.hasError()
        if beneficiaryAccountFieldError || amountFieldError {
            return
        }
        let confirmationVC = TransactionConfirmationViewController(nibName: "TableViewController", bundle: nil)
        let account = senderAccountField.account!
        let beneficiaryAccount = selectedBeneficiaryAccount!
        confirmationVC.transactionModel.senderAccountInfo = account
        confirmationVC.transactionModel.beneficiaryAccountInfo = TransactionModel.BeneficiaryAccountInfo(
            number: beneficiaryAccount.ACCT_NUMBER.orEmpty,
            displayNumber: beneficiaryAccount.ACCT_NUMBER.orEmpty,
            name: beneficiaryAccount.ACCT_NICKNAME.orEmpty,
            displayName: beneficiaryAccount.ACCT_NICKNAME.orEmpty,
            currency: beneficiaryAccount.ACCT_CURRENCY.orEmpty,
            bank: beneficiaryAccount.BRANCH_ID.orEmpty,
            displayBank: "transaction_confirm_beneficiary_golomt_label".localized(),
            usedAccountBook: true
        )
        var remark = remarkField.inputText
        if remark.isEmpty {
            remark = "transaction_confirm_own_remark".localized()
        }
        confirmationVC.transactionModel.transactionInfo = TransactionModel.TransactionInfo(
            amount: amountField.inputText.toDouble,
            displayAmount: amountField.inputText,
            remark: remark,
            currency: amountField.selectedCurrency?.rawValue,
            fee: "transaction_own_fee".localized(),
            type: .own,
            networkType: .golomt
        )
        navigationController?.pushViewController(confirmationVC, animated: true)
    }
}
