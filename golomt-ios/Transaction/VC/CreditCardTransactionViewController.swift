//
//  CreditCardTransactionViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/18/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class CreditCardTransactionViewController: TableViewController {
    var transactionModel = TransactionModel()
    var selectedBeneficiaryAccount: AccountResponse.Account?
    var minimumAmountDue = ""
    var shortcut: LoginResponse.Shortcut?

    lazy var senderAccountField: DefaultAccountPickerCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAccountPickerCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAccountPickerCell
        guard let unwrappedCell = cell else {
            return DefaultAccountPickerCell()
        }
        self.onAccountSelect = { account in
            self.senderAccountField.account = account
            self.selectedAccount = account
        }
        unwrappedCell.onPickerClick = {
            self.showSenderAccountPicker()
        }
        return unwrappedCell
    }()

    lazy var accountTypeField: DefaultSegmentedGroupCell = {
        let nib = Bundle.main.loadNibNamed("DefaultSegmentedGroupCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultSegmentedGroupCell
        guard let unwrappedCell = cell else {
            return DefaultSegmentedGroupCell()
        }
        unwrappedCell.title = "transaction_ccd_payment_type_title".localized()
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        unwrappedCell.segments = [DefaultSegmentedGroupCell.SegmentModel(code: "", value: "transaction_ccd_payment_type_own_title".localized()), DefaultSegmentedGroupCell.SegmentModel(code: "", value: "transaction_ccd_payment_type_others_title".localized())]
        unwrappedCell.selectedSegmentIndex = 0
        unwrappedCell.onSegmentChange = { index in
            var section = self.model.sections[0]
            switch index {
            case 0:
                section.rows[2].cell = self.beneficiaryAccountField
                section.rows.insert(TableViewModel.Row(cell: self.amountButtonsField), at: 3)
                self.tableView.insertRows(at: [IndexPath(row: 3, section: 0)], with: .fade)
                self.setCurrency()
                self.tableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .automatic)
            case 1:
                section.rows[2].cell = self.beneficiaryOtherAccountField
                section.rows.remove(at: 3)
                self.tableView.deleteRows(at: [IndexPath(row: 3, section: 0)], with: .fade)
                self.amountField.currencyList = []
                self.tableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .automatic)
            default:
                break
            }
        }
        return unwrappedCell
    }()

    lazy var beneficiaryAccountField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultPickerButtonCell
        guard let unwrappedCell = cell else {
            return DefaultPickerButtonCell()
        }
        unwrappedCell.title = "transaction_ccd_payment_beneficiary_title".localized()
        unwrappedCell.onPickerClick = {
            let controller = AccountPickerController()
            controller.selectedAccount = self.selectedBeneficiaryAccount
            controller.onAccountSelect = { account in
                self.beneficiaryAccountField.selectedItem = account
            }
            controller.title = "transaction_ccd_payment_beneficiary_title".localized()
            controller.accounts = creditAccountList
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onItemSelect = { account in
            self.handleBeneficiarySelect(account as? AccountResponse.Account)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var beneficiaryOtherAccountField: DefaultTextFieldInfoCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldTitleLabelCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultTextFieldInfoCell
        guard let unwrappedCell = cell else {
            return DefaultTextFieldInfoCell()
        }
        unwrappedCell.title = "transaction_ccd_payment_beneficiary_title".localized()
        unwrappedCell.subTitle = "transaction_between_golomt_beneficiary_name_label".localized()
        unwrappedCell.setTag(ComponentConstants.INFO_TAG, delegate: self)
        unwrappedCell.keyboardType = .numberPad
        unwrappedCell.hasImage = false
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        unwrappedCell.customErrorText = "transaction_ccd_payment_error_card_number_wrong".localized()
        return unwrappedCell
    }()

    lazy var amountButtonsField: TransactionAmountButtonsCell = {
        let nib = Bundle.main.loadNibNamed("TransactionAmountButtonsCell", owner: self, options: nil)
        let cell = nib?.first as? TransactionAmountButtonsCell
        guard let unwrappedCell = cell else {
            return TransactionAmountButtonsCell()
        }
        unwrappedCell.setButtonTitles("transaction_ccd_payment_minimum_amount_title", "transaction_ccd_payment_total_amount_title")
        unwrappedCell.onButtonClick = { tag in
            switch tag {
            case 0:
                switch self.accountTypeField.selectedSegmentIndex {
                case 0:
                    self.amountField.inputText = self.selectedBeneficiaryAccount?.MINIMUM_AMOUNT_DUE_ARRAY?.toAmount.formattedWithComma ?? ""
                    self.amountField.selectedCurrency = CurrencyConstants(rawValue: self.selectedBeneficiaryAccount?.ACCT_CURRENCY ?? "")
                case 1:
                    self.amountField.inputText = self.minimumAmountDue
                default:
                    break
                }
            case 1:
                switch self.accountTypeField.selectedSegmentIndex {
                case 0:
                    self.amountField.inputText = self.selectedBeneficiaryAccount?.ACCOUNT_CURRENT_BALANCE_ARRAY?.toAmount.formattedWithComma ?? ""
                    self.amountField.selectedCurrency = CurrencyConstants(rawValue: self.selectedBeneficiaryAccount?.ACCT_CURRENCY ?? "")
                case 1:
                    break
                default:
                    break
                }
            default:
                break
            }
        }
        return unwrappedCell
    }()

    lazy var amountField: DefaultAmountFieldCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAmountFieldInfoCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAmountFieldCell
        guard let unwrappedCell = cell else {
            return DefaultAmountFieldCell()
        }
        unwrappedCell.title = "transaction_between_golomt_amount_title".localized()
        unwrappedCell.subTitle = "transaction_between_golomt_amount_limit_label".localized()
        unwrappedCell.onCurrencyClick = {
            let controller = PickerController()
            controller.list = self.amountField.currencyList
            controller.onItemSelect = { currency in
                controller.popupController?.dismiss()
                self.amountField.selectedCurrency = currency as? CurrencyConstants
            }
            controller.selectedItem = self.amountField.selectedCurrency
            controller.title = "currency_title".localized()
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var remarkField: DefaultTextFieldInfoCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultTextFieldInfoCell
        guard let unwrappedCell = cell else {
            return DefaultTextFieldInfoCell()
        }
        unwrappedCell.title = "transaction_between_golomt_remark_title".localized()
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        unwrappedCell.hideLine()
        self.returnKeyHandler?.addResponderFromView(unwrappedCell.contentView)
        return unwrappedCell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Continue)
        title = "transaction_between_ccd_title".localized()
        navigationItem.largeTitleDisplayMode = .never
        setInitialRows()
        hasFooterButton = true

        onContinue = {
            self.handleContinue()
        }

        senderAccountField.account = selectedAccount

        if selectedBeneficiaryAccount != nil {
            beneficiaryAccountField.selectedItem = selectedBeneficiaryAccount!
        }
        
        if shortcut != nil {
            senderAccountField.account = operativeAccountList.filter { $0.ACCT_NUMBER == shortcut!.ACCT }.first
            switch shortcut!.ENTITYTYPE {
            case "H":
                beneficiaryOtherAccountField.inputText = shortcut!.ENTITYID.orEmpty
                accountTypeField.selectedSegmentIndex = 1
            case "O":
                guard let creditCard = creditAccountList.filter({ $0.ACCT_NUMBER == shortcut!.ENTITYID }).first else {
                    return
                }
                self.beneficiaryAccountField.selectedItem = creditCard
            default:
                break
            }
            amountField.inputText = shortcut!.TRANAMOUNT.orEmpty
            amountField.selectedCurrency = CurrencyConstants(rawValue: shortcut!.TRANCRN.orEmpty)
            remarkField.inputText = shortcut!.TRANDESC.orEmpty
        }
        
        fetchTransactionLimit()
    }

    private func setCurrency() {
        var currencyList = [CurrencyConstants]()
        let beneficiaryCurrency = CurrencyConstants(rawValue: selectedBeneficiaryAccount?.ACCT_CURRENCY.orEmpty ?? "")
        if beneficiaryCurrency != nil {
            currencyList.append(beneficiaryCurrency!)
            currencyList = currencyList.uniqueValues()
        }
        amountField.currencyList = currencyList
    }

    private func setInitialRows() {
        model.sections = [
            TableViewModel.Section(
                rows: [
                    TableViewModel.Row(cell: senderAccountField),
                    TableViewModel.Row(cell: accountTypeField),
                    TableViewModel.Row(cell: beneficiaryAccountField),
                    TableViewModel.Row(cell: amountButtonsField),
                    TableViewModel.Row(cell: amountField),
                    TableViewModel.Row(cell: remarkField),
                ]
            ),
        ]
        tableView.reloadData()
    }

    private func fetchTransactionLimit() {
        showLoader()
        ConnectionFactory.fetchTransactionLimit(
            transactionType: "TTL",
            success: { response in
                self.hideLoader()
                self.amountField.subInfo = response.detail?.DAILY_LIMIT_AVAILABLE
                if self.shortcut != nil, self.shortcut!.ENTITYTYPE == "H" {
                    self.fetchAccountName()
                }
            },
            failed: { _ in
                self.hideLoader()
            }
        )
    }

    private func fetchAccountName() {
        showLoader()
        ConnectionFactory.fetchCreditAccountName(
            cardNumber: beneficiaryOtherAccountField.inputText,
            success: { response in
                self.hideLoader()
                var currencyList = [CurrencyConstants]()
                response.currencyList?.forEach { currency in
                    let rawCurrency = CurrencyConstants(rawValue: currency.CURRENCY.orEmpty)
                    if rawCurrency != nil {
                        currencyList.append(rawCurrency!)
                    }
                }
                self.amountField.currencyList = currencyList
                self.beneficiaryOtherAccountField.subInfo = response.detail?.getMaskedName()
                let amount = response.detail?.MINIMUM_AMOUNT_DUE ?? ""
                self.amountButtonsField.setButtonLabels(amount, "")
                self.minimumAmountDue = amount
                self.transactionModel.beneficiaryAccountInfo = TransactionModel.BeneficiaryAccountInfo(
                    number: self.beneficiaryOtherAccountField.inputText,
                    displayNumber: self.beneficiaryOtherAccountField.inputText,
                    name: response.detail?.CC_ACCOUNT_NAME,
                    displayName: response.detail?.getMaskedName(),
                    currency: "MNT",
                    bank: "110",
                    displayBank: "transaction_confirm_beneficiary_golomt_label".localized(),
                    usedAccountBook: true
                )
                self.amountField.focusTextField()
            },
            failed: { _ in
                self.beneficiaryOtherAccountField.subInfo = nil
                self.hideLoader()
            }
        )
    }

    private func handleBeneficiarySelect(_ account: AccountResponse.Account?) {
        selectedBeneficiaryAccount = account
        setCurrency()
        if selectedBeneficiaryAccount != nil {
            amountButtonsField.setButtonLabels(selectedBeneficiaryAccount!.MINIMUM_AMOUNT_DUE_ARRAY.toAmountWithCurrencySymbol, selectedBeneficiaryAccount!.ACCOUNT_CURRENT_BALANCE_ARRAY.toAmountWithCurrencySymbol)
        }
    }

    private func handleContinue() {
        if senderAccountField.hasError() {
            showSenderAccountPicker()
            return
        }
        let amountFieldError = amountField.hasError()
        if amountFieldError {
            return
        }
        let confirmationVC = TransactionConfirmationViewController(nibName: "TableViewController", bundle: nil)
        let account = senderAccountField.account!
        confirmationVC.transactionModel.senderAccountInfo = account
        switch accountTypeField.selectedSegmentIndex {
        case 0:
            let beneficiaryAccountFieldError = beneficiaryAccountField.hasError()
            if beneficiaryAccountFieldError {
                return
            }
            let beneficiaryAccount = selectedBeneficiaryAccount!
            confirmationVC.transactionModel.beneficiaryAccountInfo = TransactionModel.BeneficiaryAccountInfo(
                number: beneficiaryAccount.ACCT_NUMBER.orEmpty,
                displayNumber: beneficiaryAccount.ACCT_NUMBER.orEmpty,
                name: beneficiaryAccount.ACCT_NICKNAME.orEmpty,
                displayName: beneficiaryAccount.ACCT_NICKNAME.orEmpty,
                currency: beneficiaryAccount.ACCT_CURRENCY.orEmpty,
                bank: beneficiaryAccount.BRANCH_ID.orEmpty,
                displayBank: "transaction_confirm_beneficiary_golomt_label".localized(),
                usedAccountBook: false
            )
        case 1:
            let beneficiaryAccountFieldError = beneficiaryOtherAccountField.hasError()
            if beneficiaryAccountFieldError {
                return
            }
            confirmationVC.transactionModel.beneficiaryAccountInfo = transactionModel.beneficiaryAccountInfo
        default:
            break
        }

        var remark = remarkField.inputText
        if remark.isEmpty {
            remark = "transaction_confirm_ccd_remark".localized()
        }
        confirmationVC.transactionModel.transactionInfo = TransactionModel.TransactionInfo(
            amount: amountField.inputText.toDouble,
            displayAmount: amountField.inputText,
            remark: remark,
            currency: amountField.selectedCurrency?.rawValue,
            fee: "transaction_own_fee".localized(),
            type: .creditCard,
            networkType: .golomt
        )
        navigationController?.pushViewController(confirmationVC, animated: true)
    }
}

extension CreditCardTransactionViewController: UITextFieldDelegate, UITextViewDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case ComponentConstants.INFO_TAG:
            if beneficiaryOtherAccountField.checkError() == true, !beneficiaryOtherAccountField.inputText.isEmpty {
                fetchAccountName()
            }
        default:
            break
        }
    }
}
