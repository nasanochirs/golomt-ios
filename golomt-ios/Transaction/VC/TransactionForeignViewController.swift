//
//  TransactionForeignViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 7/5/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class TransactionForeignViewController: TableViewController {
    var transactionModel = TransactionForeignModel()
    
    var purposeList = [DataListResponse.Item]()
    var swiftSearchValue = ""
    var swiftBankList = [SwiftListResponse.Bank]()
    var selectedSwift: SwiftListResponse.Bank?
    var otherCurrencyList: [CurrencyConstants] = [.USD, .CNY, .JPY, .EUR, .GBP, .CHF, .KRW, .AUD, .HKD, .SEK, .SGD, .CAD]
    
    lazy var otherCurrencySection = [
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(cell: senderAccountField),
                TableViewModel.Row(cell: currencyTypeField),
                TableViewModel.Row(cell: beneficiaryNameField),
                TableViewModel.Row(cell: beneficiaryAddressField),
                TableViewModel.Row(cell: beneficiaryNumberField),
                TableViewModel.Row(cell: swiftField),
                TableViewModel.Row(cell: amountField),
                TableViewModel.Row(cell: purposeField),
                TableViewModel.Row(cell: remarkField),
                TableViewModel.Row(cell: otherField),
                TableViewModel.Row(cell: feeTypeField),
            ]
        ),
    ]
    
    lazy var rubleCurrencySection = [
        TableViewModel.Section(
            rows: [
                TableViewModel.Row(cell: senderAccountField),
                TableViewModel.Row(cell: currencyTypeField),
                TableViewModel.Row(cell: beneficiaryRubleNameField),
                TableViewModel.Row(cell: beneficiaryAddressField),
                TableViewModel.Row(cell: beneficiaryRubleNumberField),
                TableViewModel.Row(cell: bankField),
                TableViewModel.Row(cell: bankNumberField),
                TableViewModel.Row(cell: bankNameField),
                TableViewModel.Row(cell: bankAddressField),
                TableViewModel.Row(cell: INNField),
                TableViewModel.Row(cell: KPPField),
                TableViewModel.Row(cell: VOCodeField),
                TableViewModel.Row(cell: amountField),
                TableViewModel.Row(cell: purposeField),
                TableViewModel.Row(cell: remarkRubleField),
                TableViewModel.Row(cell: otherRubleField),
                TableViewModel.Row(cell: feeTypeField),
            ]
        ),
    ]
    
    lazy var senderAccountField: DefaultAccountPickerCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAccountPickerCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAccountPickerCell
        guard let unwrappedCell = cell else {
            return DefaultAccountPickerCell()
        }
        self.onAccountSelect = { account in
            self.senderAccountField.account = account
            self.selectedAccount = account
        }
        unwrappedCell.onPickerClick = {
            self.showSenderAccountPicker()
        }
        return unwrappedCell
    }()
    
    lazy var currencyTypeField: DefaultSegmentedGroupCell = {
        let nib = Bundle.main.loadNibNamed("DefaultSegmentedGroupCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultSegmentedGroupCell
        guard let unwrappedCell = cell else {
            return DefaultSegmentedGroupCell()
        }
        unwrappedCell.title = "transaction_foreign_currency_type_title".localized()
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        unwrappedCell.segments = [DefaultSegmentedGroupCell.SegmentModel(code: "", value: "transaction_foreign_currency_others_label".localized()), DefaultSegmentedGroupCell.SegmentModel(code: "", value: "transaction_foreign_currency_ruble_label".localized())]
        unwrappedCell.selectedSegmentIndex = 0
        unwrappedCell.onSegmentChange = { segment in
            switch segment {
            case 0:
                self.amountField.currencyList = self.otherCurrencyList
                self.model.sections = self.otherCurrencySection
                self.tableView.reloadData()
            case 1:
                self.amountField.currencyList = [.RUB]
                self.model.sections = self.rubleCurrencySection
                self.tableView.reloadData()
            default:
                break
            }
        }
        return unwrappedCell
    }()
    
    lazy var beneficiaryNameField: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        cell.title = "transaction_foreign_beneficiary_name_title".localized()
        cell.subInfo = "transaction_foreign_only_english_label".localized()
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var beneficiaryRubleNameField: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        cell.title = "transaction_foreign_beneficiary_name_title".localized()
        cell.subInfo = "transaction_foreign_only_cyrillic_label".localized()
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var beneficiaryAddressField: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        cell.title = "transaction_foreign_beneficiary_address_title".localized()
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var beneficiaryNumberField: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        cell.title = "transaction_foreign_iban_title".localized()
        cell.subInfo = "transaction_foreign_iban_warning_label".localized()
        cell.keyboardType = .numberPad
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var beneficiaryRubleNumberField: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        cell.title = "transaction_foreign_beneficiary_number_title".localized()
        cell.subInfo = "transaction_foreign_beneficiary_number_warning_label".localized()
        cell.keyboardType = .numberPad
        cell.textLength = 20
        cell.lengthValidation = .Equal
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var swiftField: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldButtonCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        cell.title = "transaction_foreign_swift_title".localized()
        cell.onImageClick = {
            self.fetchSwiftBankList()
        }
        cell.buttonImage = "swift"
        cell.buttonColor = .defaultPrimaryText
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var bankField: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        cell.title = "transaction_foreign_bank_code_title".localized()
        cell.subInfo = "transaction_foreign_bank_code_warning_label".localized()
        cell.keyboardType = .numberPad
        cell.textLength = 9
        cell.lengthValidation = .Equal
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var bankNumberField: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        cell.title = "transaction_foreign_bank_number_title".localized()
        cell.subInfo = "transaction_foreign_bank_number_warning_label".localized()
        cell.keyboardType = .numberPad
        cell.textLength = 20
        cell.lengthValidation = .Equal
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var bankNameField: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        cell.title = "transaction_foreign_bank_name_title".localized()
        cell.subInfo = "transaction_foreign_only_cyrillic_label".localized()
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var bankAddressField: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        cell.title = "transaction_foreign_bank_address_title".localized()
        cell.subInfo = "transaction_foreign_only_cyrillic_label".localized()
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var INNField: DefaultTextFieldInfoCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldInfoCell else {
            return DefaultTextFieldInfoCell()
        }
        cell.title = "transaction_foreign_inn_title".localized()
        cell.keyboardType = .numberPad
        cell.textLength = 12
        cell.lengthValidation = .Equal
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var KPPField: DefaultTextFieldInfoCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldInfoCell else {
            return DefaultTextFieldInfoCell()
        }
        cell.title = "transaction_foreign_kpp_title".localized()
        cell.keyboardType = .numberPad
        cell.textLength = 12
        cell.lengthValidation = .Equal
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var VOCodeField: DefaultTextFieldInfoCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldInfoCell else {
            return DefaultTextFieldInfoCell()
        }
        cell.title = "transaction_foreign_vo_code_title".localized()
        cell.keyboardType = .numberPad
        cell.textLength = 5
        cell.lengthValidation = .Equal
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var amountField: DefaultAmountFieldCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAmountFieldCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultAmountFieldCell else {
            return DefaultAmountFieldCell()
        }
        cell.title = "transaction_foreign_transfer_amount_title".localized()
        cell.currencyList = self.otherCurrencyList
        cell.onCurrencyClick = {
            let controller = PickerController()
            controller.list = self.amountField.currencyList
            controller.onItemSelect = { currency in
                controller.popupController?.dismiss()
                self.amountField.selectedCurrency = currency as? CurrencyConstants
            }
            controller.selectedItem = self.amountField.selectedCurrency
            controller.title = "currency_title".localized()
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var purposeField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultPickerButtonCell
        guard let unwrappedCell = cell else {
            return DefaultPickerButtonCell()
        }
        unwrappedCell.title = "transcation_foreign_purpose_title".localized()
        unwrappedCell.onPickerClick = {
            let controller = PickerController()
            controller.title = "transcation_foreign_purpose_title".localized()
            controller.list = self.purposeList
            controller.onItemSelect = { purpose in
                controller.popupController?.dismiss()
                unwrappedCell.selectedItem = purpose
            }
            controller.selectedItem = self.purposeField.selectedItem
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()
    
    lazy var remarkField: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        cell.title = "transaction_foreign_remark_title".localized()
        cell.subInfo = "transaction_foreign_remark_english_warning_label".localized()
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var remarkRubleField: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        cell.title = "transaction_foreign_remark_title".localized()
        cell.subInfo = "transaction_foreign_remark_russian_warning_label".localized()
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var otherField: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        cell.title = "transaction_foreign_other_title".localized()
        cell.subInfo = "transaction_foreign_other_english_warning_label".localized()
        cell.hideLine()
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var otherRubleField: DefaultTextFieldLabelCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldLabelCell", owner: self, options: nil)
        guard let cell = nib?.first as? DefaultTextFieldLabelCell else {
            return DefaultTextFieldLabelCell()
        }
        cell.title = "transaction_foreign_other_title".localized()
        cell.subInfo = "transaction_foreign_other_russian_warning_label".localized()
        cell.hideLine()
        cell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return cell
    }()
    
    lazy var feeTypeField: DefaultSegmentedGroupCell = {
        let nib = Bundle.main.loadNibNamed("DefaultSegmentedGroupCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultSegmentedGroupCell
        guard let unwrappedCell = cell else {
            return DefaultSegmentedGroupCell()
        }
        unwrappedCell.title = "transaction_foreign_fee_type_title".localized()
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        unwrappedCell.segments = [DefaultSegmentedGroupCell.SegmentModel(code: "SHA", value: "transaction_foreign_fee_type_beneficiary_label".localized()), DefaultSegmentedGroupCell.SegmentModel(code: "OUR", value: "transaction_foreign_fee_type_remitter_label".localized()), DefaultSegmentedGroupCell.SegmentModel(code: "GOUR", value: "transaction_foreign_fee_type_transmitter_label".localized())]
        unwrappedCell.selectedSegmentIndex = 0
        unwrappedCell.onSegmentChange = { _ in
        }
        return unwrappedCell
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "transaction_foreign_title".localized()
        navigationItem.largeTitleDisplayMode = .never
        
        setInitialRows()
        hasFooterButton = true
        
        onContinue = {
            self.handleContinue()
        }
        if selectedAccount == nil {
            showSenderAccountPicker()
        }
        fetchPurposeList()
    }
    
    private func setInitialRows() {
        model.sections = otherCurrencySection
        tableView.reloadData()
    }
    
    private func handleContinue() {
        if senderAccountField.hasError() {
            showSenderAccountPicker()
            return
        }
        
        var isRuble = false
        switch currencyTypeField.selectedSegmentIndex {
        case 1:
            isRuble = true
            let nameError = beneficiaryRubleNameField.hasError()
            let addressError = beneficiaryAddressField.hasError()
            let numberError = beneficiaryRubleNumberField.hasError()
            let bankError = bankField.hasError()
            let bankNumberError = bankNumberField.hasError()
            let bankNameError = bankNameField.hasError()
            let bankAddressError = bankAddressField.hasError()
            let innError = INNField.hasError()
            let kppError = KPPField.hasError()
            let voError = VOCodeField.hasError()
            let amountError = amountField.hasError()
            let purposeError = purposeField.hasError()
            let remarkError = remarkRubleField.hasError()
            if nameError || addressError || numberError || bankError || bankNumberError || bankNameError || bankAddressError || innError || kppError || voError || amountError || purposeError || remarkError {
                return
            }
        default:
            let nameError = beneficiaryNameField.hasError()
            let addressError = beneficiaryAddressField.hasError()
            let numberError = beneficiaryNumberField.hasError()
            let swiftError = swiftField.hasError()
            let amountError = amountField.hasError()
            let purposeError = purposeField.hasError()
            let remarkError = remarkField.hasError()
            if nameError || addressError || numberError || swiftError || amountError || purposeError || remarkError {
                return
            }
            if selectedSwift == nil {
                return
            }
        }
        let purpose = purposeField.selectedItem as? DataListResponse.Item
        transactionModel.senderAccountInfo = selectedAccount!
        transactionModel.beneficiaryAccountInfo = TransactionForeignModel.BeneficiaryAccountInfo(
            name: isRuble ? beneficiaryRubleNameField.inputText : beneficiaryNameField.inputText,
            address: beneficiaryAddressField.inputText,
            number: isRuble ? beneficiaryRubleNumberField.inputText : beneficiaryNumberField.inputText,
            inn: INNField.inputText,
            kpp: KPPField.inputText,
            voCode: VOCodeField.inputText
        )
        transactionModel.beneficiaryBankInfo = TransactionForeignModel.BeneficiaryBankInfo(
            swift: selectedSwift,
            bankCode: bankField.inputText,
            bankNumber: bankNumberField.inputText,
            bankName: bankNameField.inputText,
            bankAddress: bankAddressField.inputText,
            otherField: isRuble ? otherRubleField.inputText : otherField.inputText
        )
        transactionModel.transactionInfo = TransactionForeignModel.TransactionInfo(
            amount: amountField.inputText.toDouble,
            displayAmount: amountField.inputText,
            remark: isRuble ? remarkRubleField.inputText : remarkField.inputText,
            currency: amountField.selectedCurrency?.rawValue,
            purpose: purpose?.CM_CODE,
            displayPurpose: purpose?.CD_DESC
        )
        let selectedFeeType = feeTypeField.selectedSegment
        transactionModel.isRuble = isRuble
        transactionModel.transactionInfo.feeType = selectedFeeType.code
        transactionModel.transactionInfo.displayFeeType = selectedFeeType.value
        let confirmVC = TransactionForeignConfirmViewController(nibName: "TableViewController", bundle: nil)
        confirmVC.transactionModel = transactionModel
        navigationController?.pushViewController(confirmVC, animated: true)
    }
    
    private func fetchPurposeList() {
        showLoader()
        ConnectionFactory.fetchList(
            code: "POR",
            success: { response in
                self.hideLoader()
                self.purposeList = response.list ?? []
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
    
    private func fetchSwiftBankList() {
        if swiftSearchValue == swiftField.inputText {
            showSwiftList()
            return
        }
        swiftSearchValue = swiftField.inputText
        showLoader()
        ConnectionFactory.fetchSwiftBankList(
            searchValue: swiftSearchValue,
            success: { response in
                self.hideLoader()
                self.swiftBankList = response.bankList ?? []
                self.showSwiftList()
            },
            failed: { reason in
                self.hideLoader()
                self.handleRequestFailure(reason)
            }
        )
    }
    
    private func showSwiftList() {
        let controller = PickerController()
        controller.list = swiftBankList
        controller.onItemSelect = { swift in
            controller.popupController?.dismiss()
            let swiftBank = swift as? SwiftListResponse.Bank
            self.swiftField.inputText = swiftBank?.ROUTING_NOS ?? self.swiftField.inputText
            self.selectedSwift = swiftBank
        }
        controller.selectedItem = selectedSwift
        controller.title = "transaction_foreign_swift_title".localized()
        resignFirstResponder()
        let popupController = STPopupController(rootViewController: controller)
        popupController.style = .bottomSheet
        popupController.present(in: self)
    }
}
