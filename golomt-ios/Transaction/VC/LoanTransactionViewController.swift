//
//  LoanTransactionViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/18/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class LoanTransactionViewController: TableViewController {
    var transactionModel = TransactionModel()
    var selectedBeneficiaryAccount: AccountResponse.Account?
    var overdueAmount = 0.0
    var liabilityAmount = 0.0
    var shortcut: LoginResponse.Shortcut?

    lazy var senderAccountField: DefaultAccountPickerCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAccountPickerCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAccountPickerCell
        guard let unwrappedCell = cell else {
            return DefaultAccountPickerCell()
        }
        self.onAccountSelect = { account in
            self.senderAccountField.account = account
            self.selectedAccount = account
        }
        unwrappedCell.onPickerClick = {
            self.showSenderAccountPicker()
        }
        return unwrappedCell
    }()

    lazy var beneficiaryAccountField: DefaultPickerButtonCell = {
        let nib = Bundle.main.loadNibNamed("DefaultPickerCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultPickerButtonCell
        guard let unwrappedCell = cell else {
            return DefaultPickerButtonCell()
        }
        unwrappedCell.title = "transaction_loan_payment_beneficiary_title".localized()
        unwrappedCell.onPickerClick = {
            let controller = AccountPickerController()
            controller.selectedAccount = self.selectedBeneficiaryAccount
            controller.onAccountSelect = { account in
                self.beneficiaryAccountField.selectedItem = account
                self.selectedBeneficiaryAccount = account
                self.setCurrency()
                self.fetchAccountDetails()
            }
            controller.title = "transaction_loan_payment_beneficiary_title".localized()
            controller.accounts = loanAccountList
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var amountButtonsField: TransactionAmountButtonsCell = {
        let nib = Bundle.main.loadNibNamed("TransactionAmountButtonsCell", owner: self, options: nil)
        let cell = nib?.first as? TransactionAmountButtonsCell
        guard let unwrappedCell = cell else {
            return TransactionAmountButtonsCell()
        }
        unwrappedCell.setButtonTitles("transaction_loan_payment_overdue_amount_title", "transaction_loan_payment_liability_amount_title")
        unwrappedCell.onButtonClick = { tag in
            switch tag {
            case 0:
                self.amountField.inputText = self.overdueAmount.formattedWithComma
                self.amountField.selectedCurrency = CurrencyConstants(rawValue: self.selectedBeneficiaryAccount?.ACCT_CURRENCY ?? "")
            case 1:
                self.amountField.inputText = self.liabilityAmount.formattedWithComma
                self.amountField.selectedCurrency = CurrencyConstants(rawValue: self.selectedBeneficiaryAccount?.ACCT_CURRENCY ?? "")
            default:
                break
            }
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var amountField: DefaultAmountFieldCell = {
        let nib = Bundle.main.loadNibNamed("DefaultAmountFieldInfoCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultAmountFieldCell
        guard let unwrappedCell = cell else {
            return DefaultAmountFieldCell()
        }
        unwrappedCell.title = "transaction_between_golomt_amount_title".localized()
        unwrappedCell.subTitle = "transaction_between_golomt_amount_limit_label".localized()
        unwrappedCell.onCurrencyClick = {
            let controller = PickerController()
            controller.list = self.amountField.currencyList
            controller.onItemSelect = { currency in
                controller.popupController?.dismiss()
                self.amountField.selectedCurrency = currency as? CurrencyConstants
            }
            controller.selectedItem = self.amountField.selectedCurrency
            controller.title = "currency_title".localized()
            let popupController = STPopupController(rootViewController: controller)
            popupController.style = .bottomSheet
            popupController.present(in: self)
        }
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        return unwrappedCell
    }()

    lazy var remarkField: DefaultTextFieldInfoCell = {
        let nib = Bundle.main.loadNibNamed("DefaultTextFieldCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultTextFieldInfoCell
        guard let unwrappedCell = cell else {
            return DefaultTextFieldInfoCell()
        }
        unwrappedCell.title = "transaction_between_golomt_remark_title".localized()
        unwrappedCell.onStateChange = {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                unwrappedCell.layoutSubviews()
                self.tableView.endUpdates()
            }
        }
        unwrappedCell.hideLine()
        self.returnKeyHandler?.addResponderFromView(unwrappedCell.contentView)
        return unwrappedCell
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle(.Continue)
        title = "transaction_loan_payment_title".localized()
        navigationItem.largeTitleDisplayMode = .never
        setInitialRows()
        hasFooterButton = true

        onContinue = {
            self.handleContinue()
        }
        
        senderAccountField.account = selectedAccount

        if selectedBeneficiaryAccount != nil {
            beneficiaryAccountField.selectedItem = selectedBeneficiaryAccount!
            setCurrency()
        }
        
        if shortcut != nil {
            senderAccountField.account = operativeAccountList.filter { $0.ACCT_NUMBER == shortcut!.ACCT }.first
            switch shortcut!.ENTITYTYPE {
            case "R":
                guard let loan = loanAccountList.filter({ $0.ACCT_NUMBER == shortcut!.ENTITYID }).first else {
                    return
                }
                self.beneficiaryAccountField.selectedItem = loan
            default:
                break
            }
            amountField.inputText = shortcut!.TRANAMOUNT.orEmpty
            amountField.selectedCurrency = CurrencyConstants(rawValue: shortcut!.TRANCRN.orEmpty)
            remarkField.inputText = shortcut!.TRANDESC.orEmpty
        }
        
        fetchTransactionLimit()
    }

    private func setCurrency() {
        var currencyList = [CurrencyConstants]()
        let beneficiaryCurrency = CurrencyConstants(rawValue: selectedBeneficiaryAccount?.ACCT_CURRENCY.orEmpty ?? "")
        if beneficiaryCurrency != nil {
            currencyList.append(beneficiaryCurrency!)
            currencyList = currencyList.uniqueValues()
        }
        amountField.currencyList = currencyList
    }

    private func setInitialRows() {
        model.sections = [
            TableViewModel.Section(
                rows: [
                    TableViewModel.Row(cell: senderAccountField),
                    TableViewModel.Row(cell: beneficiaryAccountField),
                    TableViewModel.Row(cell: amountButtonsField),
                    TableViewModel.Row(cell: amountField),
                    TableViewModel.Row(cell: remarkField),
                ]
            )
        ]
        tableView.reloadData()
    }

    private func fetchTransactionLimit() {
        showLoader()
        ConnectionFactory.fetchTransactionLimit(
            transactionType: "TTL",
            success: { response in
                self.hideLoader()
                self.amountField.subInfo = response.detail?.DAILY_LIMIT_AVAILABLE
                self.fetchAccountDetails()
            },
            failed: { _ in
                self.hideLoader()
                self.fetchAccountDetails()
            }
        )
    }

    private func fetchAccountDetails() {
        if selectedBeneficiaryAccount != nil {
            showLoader()
            ConnectionFactory.fetchLONDetail(
                account: selectedBeneficiaryAccount,
                success: { response in
                    self.hideLoader()
                    self.overdueAmount = abs(response.detail?.OVERDUE_AMT?.toAmount ?? 0.0)
                    self.liabilityAmount = abs(response.detail?.LIABILITY_AMT?.toAmount ?? 0.0)
                    self.amountButtonsField.setButtonLabels(response.detail?.OVERDUE_AMT.toAmountWithCurrencySymbol ?? "", response.detail?.LIABILITY_AMT.toAmountWithCurrencySymbol ?? "")

                }, failed: { reason in
                    self.hideLoader()
                    self.handleRequestFailure(reason)
                }
            )
        }
    }

    private func handleContinue() {
        if senderAccountField.hasError() {
            showSenderAccountPicker()
            return
        }
        let amountFieldError = amountField.hasError()
        let beneficiaryAccountFieldError = beneficiaryAccountField.hasError()
        if beneficiaryAccountFieldError || amountFieldError {
            return
        }
        if amountField.inputText.toDouble >= liabilityAmount {
            let confirmationDialog = UIAlertController(title: "transaction_loan_payment_over_pay_error_title".localized(), message: "transaction_loan_payment_over_pay_error_label".localized(), preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "transaction_loan_payment_over_pay_negative_label".localized(), style: .cancel, handler: nil)
            let confirmAction = UIAlertAction(
                title: "transaction_loan_payment_over_pay_positive_label".localized(),
                style: .default,
                handler: { _ in
                    let closeVC = LoanCloseViewController(nibName: "TableViewController", bundle: nil)
                    self.navigationController?.pushViewController(closeVC, animated: true)
                }
            )
            confirmationDialog.addAction(cancelAction)
            confirmationDialog.addAction(confirmAction)
            present(confirmationDialog, animated: true)
            return
        }
        let confirmationVC = TransactionConfirmationViewController(nibName: "TableViewController", bundle: nil)
        let account = senderAccountField.account!
        confirmationVC.transactionModel.senderAccountInfo = account

        var remark = remarkField.inputText
        if remark.isEmpty {
            remark = "transaction_confirm_loan_payment_remark".localized()
        }
        let beneficiaryAccount = selectedBeneficiaryAccount!
        confirmationVC.transactionModel.beneficiaryAccountInfo = TransactionModel.BeneficiaryAccountInfo(
            number: beneficiaryAccount.ACCT_NUMBER.orEmpty,
            displayNumber: beneficiaryAccount.ACCT_NUMBER.orEmpty,
            name: beneficiaryAccount.ACCT_NICKNAME.orEmpty,
            displayName: beneficiaryAccount.ACCT_NICKNAME.orEmpty,
            currency: beneficiaryAccount.ACCT_CURRENCY.orEmpty,
            bank: beneficiaryAccount.BRANCH_ID.orEmpty,
            displayBank: "transaction_confirm_beneficiary_golomt_label".localized(),
            usedAccountBook: true
        )
        confirmationVC.transactionModel.extraInfo = TransactionModel.ExtraInfo(
            overdueAmount: overdueAmount
        )
        confirmationVC.transactionModel.transactionInfo = TransactionModel.TransactionInfo(
            amount: amountField.inputText.toDouble,
            displayAmount: amountField.inputText,
            remark: remark,
            currency: amountField.selectedCurrency?.rawValue,
            fee: "transaction_own_fee".localized(),
            type: .loan,
            networkType: .golomt
        )
        navigationController?.pushViewController(confirmationVC, animated: true)
    }
}
