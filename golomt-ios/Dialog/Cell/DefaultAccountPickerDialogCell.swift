//
//  DefaultAccountPickerDialogCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/5/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class DefaultAccountPickerDialogCell: UITableViewCell {
    @IBOutlet var nicknameLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var balanceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }

    func setAccount(_ account: AccountResponse.Account) {
        nicknameLabel.text = account.ACCT_NICKNAME
        numberLabel.text = account.ACCT_NUMBER
        balanceLabel.text = account.ACCT_BALANCE?.toAmountWithCurrency
    }

    private func initComponent() {
        nicknameLabel.useMediumFont()
        numberLabel.useMediumFont()
        balanceLabel.useMediumFont()
        balanceLabel.makeBold()
        nicknameLabel.textColor = .defaultPrimaryText
        numberLabel.textColor = .defaultPrimaryText
        balanceLabel.textColor = .defaultSecondaryText
    }
}
