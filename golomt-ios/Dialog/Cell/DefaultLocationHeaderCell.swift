//
//  DefaultLocationHeaderCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class DefaultLocationHeaderCell: UITableViewCell {
    @IBOutlet var locationImageView: UIImageView!
    @IBOutlet var locationNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        locationNameLabel.useMediumFont()
        locationNameLabel.textColor = .defaultPrimaryText
    }
    
    func setLocation(_ data: LocationResponse.LocationData.Location) {
        let base64 = data.imageBase64.orEmpty.components(separatedBy: ",")
        let decodedData = Data(base64Encoded: base64.last.orEmpty)
        if let imageData = decodedData {
            locationImageView.image = UIImage(data: imageData)
        }
        locationNameLabel.text = data.getName()
    }
}
