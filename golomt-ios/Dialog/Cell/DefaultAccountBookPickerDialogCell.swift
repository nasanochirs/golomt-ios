//
//  DefaultAccountBookPickerDialogCellTableViewCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/7/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class DefaultAccountBookPickerDialogCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var nicknameLabel: UILabel!
    @IBOutlet var bankImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }

    func setAccountBook(type: String?, _ accountBook: AccountBookResponse.AccountBook) {
        nicknameLabel.text = accountBook.BNIF_NICK_NAME
        numberLabel.text = accountBook.BNIF_ACCNT_NUMBER
        nameLabel.text = accountBook.BNIF_NAME
        if type == TransactionNetworkConstants.golomt.rawValue {
            bankImage.image = UIImage(named: "golomt_logo")
            return
        }
        if let identifier = accountBook.BNK_IDENTIFIER {
            bankImage.image = UIImage(named: identifier)
        }
    }

    private func initComponent() {
        nicknameLabel.textColor = .defaultPrimaryText
        numberLabel.textColor = .defaultSecondaryText
        nameLabel.textColor = .defaultPrimaryText
        nicknameLabel.useMediumFont()
        numberLabel.useMediumFont()
        nameLabel.useMediumFont()
        nicknameLabel.makeBold()
    }
}
