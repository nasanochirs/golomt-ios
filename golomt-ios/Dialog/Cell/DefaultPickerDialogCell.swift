//
//  DefaultPickerDialogCell.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/11/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import UIKit

class DefaultPickerDialogCell: UITableViewCell {
    @IBOutlet var itemLabel: UILabel!
    @IBOutlet var itemImage: UIImageView?
    @IBOutlet var itemImageLabel: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        initComponent()
    }

    func setItem(_ item: Any) {
        switch item {
        case let currency as CurrencyConstants:
            itemLabel.text = currency.rawValue
            itemImageLabel?.text = currency.rawValue.toFlag
        case let bank as BankListResponse.Bank:
            itemLabel.text = bank.INSTITUTION_NAMES
            itemImage?.image = UIImage(named: bank.ROUTING_NOS.orEmpty)
        case let product as ExtendDepositProductResponse.Product:
            itemLabel.text = product.RENEW_INSTRUCTION_NAME_ARRAY
        case let month as ExtendDepositMonthResponse.Month:
            itemLabel.text = month.FD_RENEW_MONTHS_ARRAY
        case let data as DataListResponse.Item:
            itemLabel.text = data.CD_DESC.orEmpty
        case let swift as SwiftListResponse.Bank:
            itemLabel.text = swift.INSTITUTION_NAMES.orEmpty + ", " + swift.ADDRESS.orEmpty
        case let count as ChallengeGiftResponse.Count:
            itemLabel.text = count.GIFT_ORG.orEmpty
        case let organization as BillPaymentOrganizationResponse.Organization:
            itemLabel.text = organization.BILLER_NAME_INFO.orEmpty
        case let number as Int:
            itemLabel.text = number.toString
        case let type as (index: Int, text: String):
            itemLabel.text = type.text
        default:
            break
        }
    }

    private func initComponent() {
        itemLabel.textColor = .defaultPrimaryText
        itemLabel.useMediumFont()
        itemLabel.wordWrap()
        itemImageLabel?.font = .systemFont(ofSize: 30)
    }
}
