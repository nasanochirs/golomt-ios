//
//  Dialog.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/12/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

func errorDialog(message: String) -> UIAlertController {
    let errorDialog = UIAlertController(title: "dialog_error_title".localized(), message: message.localized(), preferredStyle: .alert)
    let cancelAction = UIAlertAction(title: "dialog_error_cancel_button_label".localized(), style: .cancel, handler: nil)
    errorDialog.addAction(cancelAction)
    return errorDialog
}

func warningDialog(message: String) -> UIAlertController {
    let warningDialog = UIAlertController(title: "dialog_warning_title".localized(), message: message, preferredStyle: .alert)
    let okAction = UIAlertAction(title: "Ok".localized(), style: .cancel, handler: nil)
    warningDialog.addAction(okAction)
    return warningDialog
}

func infoDialog(message: String) -> UIAlertController {
    let infoDialog = UIAlertController(title: nil, message: message, preferredStyle: .alert)
    let okAction = UIAlertAction(title: "Ok".localized(), style: .cancel, handler: nil)
    infoDialog.addAction(okAction)
    return infoDialog
}


func successDialog(message: String, dismiss: @escaping () -> Void) -> UIAlertController {
    let successDialog = UIAlertController(title: "Амжилттай".localized(), message: message, preferredStyle: .alert)
    let okAction = UIAlertAction(title: "Ok".localized(), style: .cancel, handler: { (action) -> Void in
        dismiss()
    })

    successDialog.addAction(okAction)
    return successDialog
}
