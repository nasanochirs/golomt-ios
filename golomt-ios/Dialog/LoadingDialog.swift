//
//  LoadingDialog.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 3/12/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import Lottie
import UIKit

class LoadingDialog: UIView {
    class var shared: LoadingDialog {
        struct Static {
            static let instance: LoadingDialog = LoadingDialog()
        }
        return Static.instance
    }
    
    lazy var transparentBackground: UIView = {
        let transparentBackground = UIView(frame: UIScreen.main.bounds)
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = UIApplication.shared.windows.first!.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        transparentBackground.addSubview(blurEffectView)
        return transparentBackground
    }()
    
    lazy var loadingAnimation: AnimationView = {
        let loaderAnimationView = AnimationView(name: "lottie-loader")
        loaderAnimationView.loopMode = LottieLoopMode.loop
        return loaderAnimationView
    }()
    
    func isLoading() -> Bool {
        return loadingAnimation.isAnimationPlaying
    }
    
    func showLoader() {
        DispatchQueue.main.async {
            self.addSubview(self.transparentBackground)
            self.loadingAnimation.play()
            self.transparentBackground.addSubview(self.loadingAnimation)
            self.transparentBackground.bringSubviewToFront(self.loadingAnimation)
            self.loadingAnimation.center = self.transparentBackground.center
        }
        DispatchQueue.main.async {
            UIApplication.shared.windows.first?.addSubview(self.transparentBackground)
            UIApplication.shared.windows.first?.isUserInteractionEnabled = false
        }
    }
    
    func hideLoader() {
        DispatchQueue.main.async {
            self.transparentBackground.removeFromSuperview()
            UIApplication.shared.windows.first?.isUserInteractionEnabled = true
        }
    }
}
