//
//  AccountPickerController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/5/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class AccountPickerController: DialogTableViewController {
    var accounts = [AccountResponse.Account]()
    var onAccountSelect: ((_ account: AccountResponse.Account) -> Void)?
    var selectedAccount: AccountResponse.Account?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.registerCell(nibName: "DefaultAccountPickerDialogCell")
        tableView.rowHeight = 70
        
        configContentSize()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        accounts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultAccountPickerDialogCell") as? DefaultAccountPickerDialogCell
        guard let unwrappedCell = cell else {
            return UITableViewCell()
        }
        let account = accounts[indexPath.row]
        unwrappedCell.setAccount(account)
        if selectedAccount != nil, selectedAccount!.ACCT_NUMBER == account.ACCT_NUMBER {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
        return unwrappedCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let account = accounts[indexPath.row]
        onAccountSelect?(account)
        popupController?.dismiss()
    }
}
