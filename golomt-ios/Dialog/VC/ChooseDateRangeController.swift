//
//  ChooseDateRangeController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 4/23/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import FSCalendar
import STPopup
import UIKit

class ChooseDateRangeController: PopupController {
    var startDate: Date?
    var endDate: Date?
    
    var onDateRangeSelect: ((_ startDate: String, _ endDate: String) -> Void)?
    
    lazy var calendar: FSCalendar = {
        let calendar = FSCalendar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 300))
        calendar.dataSource = self
        calendar.delegate = self
        calendar.backgroundColor = .defaultPrimaryBackground
        calendar.allowsMultipleSelection = true
        calendar.appearance.headerTitleColor = .defaultBlueGradientEnd
        calendar.appearance.weekdayTextColor = .defaultBlueGradientEnd
        calendar.appearance.headerDateFormat = "yyyy, MMM"
        calendar.appearance.titlePlaceholderColor = .gray
        calendar.appearance.titleDefaultColor = .defaultPrimaryText
        calendar.today = nil
        calendar.locale = Locale(identifier: getLanguage())
        calendar.register(DateRangePickerCell.self, forCellReuseIdentifier: "DateRangePickerCell")
        calendar.firstWeekday = 2
        calendar.placeholderType = .fillHeadTail
        return calendar
    }()
    
    lazy var gregorian: Calendar = {
        Calendar(identifier: .gregorian)
    }()
    
    lazy var chooseButton: UIButton = {
//        let button = DefaultGradientButton(frame: CGRect(x: 20, y: self.calendar.frame.maxY + 20, width: self.view.bounds.size.width - 40, height: 60))
//        let button = UIButton(frame: CGRect(x: 20, y: self.calendar.frame.maxY + 20, width: self.view.bounds.size.width - 40, height: 60))
        let button = DefaultGradientButton(frame: CGRect(x: 20, y: self.calendar.frame.maxY + 20, width: self.view.bounds.size.width - 40, height: 60))
        button.layoutSubviews()
        button.setTitle("statement_month_picker_choose".localized(), for: .normal)
        button.addTarget(self, action: #selector(handleDateSelection), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initComponent()
    }
    
    @objc private func handleDateSelection() {
        guard let unwrappedStartDate = startDate else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        guard let unwrappedEndDate = endDate else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        self.onDateRangeSelect?(unwrappedStartDate.toFormatted, unwrappedEndDate.toFormatted)
    }
    
    private func initComponent() {
        view.addSubview(self.calendar)
        view.addSubview(self.chooseButton)
        
        title = "statement_month_picker_title".localized()
        
        var height = CGFloat(chooseButton.frame.maxY)
        height += view.safeAreaInsets.bottom
        height += 20
        
        contentSizeInPopup = CGSize(width: view.bounds.size.width, height: height)
    }
    
    private func configureCell(_ cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        let rangePickerCell = cell as! DateRangePickerCell
        
        var selectionType = SelectionType.none
        
        if self.startDate != nil, self.endDate != nil {
            let isMiddle = date.compare(self.startDate!) != date.compare(self.endDate!)
            if isMiddle {
                selectionType = .middle
            } else {
                selectionType = .none
            }
            if self.gregorian.isDate(date, inSameDayAs: self.startDate!) {
                selectionType = .leftBorder
            }
            if self.gregorian.isDate(date, inSameDayAs: self.endDate!) {
                selectionType = .rightBorder
            }
        } else {
            selectionType = .none
            if let startDate = startDate {
                if self.gregorian.isDate(date, inSameDayAs: startDate) {
                    selectionType = .single
                }
            }
            if let endDate = endDate {
                if self.gregorian.isDate(date, inSameDayAs: endDate) {
                    selectionType = .single
                }
            }
        }
        
        if selectionType == .none {
            rangePickerCell.selectionLayer.isHidden = true
            rangePickerCell.middleLayer.isHidden = true
        }
        if selectionType == .middle {
            rangePickerCell.selectionLayer.isHidden = true
            rangePickerCell.middleLayer.isHidden = false
        }
        if selectionType == .leftBorder || selectionType == .rightBorder || selectionType == .single {
            rangePickerCell.selectionLayer.isHidden = false
            rangePickerCell.middleLayer.isHidden = true
        }
        rangePickerCell.selectionType = selectionType
    }
    
    private func configureVisibleCells() {
        self.calendar.visibleCells().forEach { cell in
            let date = calendar.date(for: cell)
            let position = calendar.monthPosition(for: cell)
            self.configureCell(cell, for: date!, at: position)
        }
    }
}

extension ChooseDateRangeController: FSCalendarDelegate, FSCalendarDataSource {
    func minimumDate(for calendar: FSCalendar) -> Date {
        return "1999-09-05".toDate!
    }
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    
    func calendar(_ calendar: FSCalendar, titleFor date: Date) -> String? {
        return nil
    }
    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: "DateRangePickerCell", for: date, at: position)
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.configureCell(cell, for: date, at: monthPosition)
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return true
    }
    
    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return false
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
        
        if calendar.swipeToChooseGesture.state == UIGestureRecognizer.State.changed {
            if self.startDate == nil {
                self.startDate = date
            } else {
                if self.endDate != nil {
                    calendar.deselect(self.endDate!)
                }
                self.endDate = date
            }
        } else {
            if self.endDate != nil {
                calendar.deselect(self.startDate!)
                calendar.deselect(self.endDate!)
                self.startDate = date
                self.endDate = nil
            } else if self.startDate == nil {
                self.startDate = date
            } else {
                if self.startDate! > date {
                    self.endDate = self.startDate
                    self.startDate = date
                } else {
                    self.endDate = date
                }
            }
        }
        self.configureVisibleCells()
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.configureVisibleCells()
    }
}
