//
//  CurrencyPickerController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/11/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class PickerController: DialogTableViewController {
    var list = [Any]()
    var onItemSelect: ((_ selected: Any) -> Void)?
    var selectedItem: Any?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.registerCell(nibName: "DefaultPickerDialogCell")
        tableView.registerCell(nibName: "DefaultPickerLabelDialogCell")
        tableView.registerCell(nibName: "DefaultPickerImageDialogCell")
        tableView.rowHeight = 70
        configContentSize()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        list.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = list[indexPath.row]
        switch item {
        case let currency as CurrencyConstants:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultPickerLabelDialogCell") as! DefaultPickerDialogCell
            cell.setItem(item)
            if let selectedCurrency = selectedItem as? CurrencyConstants {
                if currency == selectedCurrency {
                    tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                }
            }
            return cell
        case let bank as BankListResponse.Bank:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultPickerImageDialogCell") as! DefaultPickerDialogCell
            cell.setItem(item)
            if let selectedBank = selectedItem as? BankListResponse.Bank {
                if bank.ROUTING_NOS == selectedBank.ROUTING_NOS {
                    tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                }
            }
            return cell
        case let product as ExtendDepositProductResponse.Product:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultPickerDialogCell") as! DefaultPickerDialogCell
            cell.setItem(item)
            if let selectedProduct = selectedItem as? ExtendDepositProductResponse.Product {
                if product.RENEW_INSTRUCTION_CODE_ARRAY == selectedProduct.RENEW_INSTRUCTION_CODE_ARRAY {
                    tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                }
            }
            return cell
        case let month as ExtendDepositMonthResponse.Month:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultPickerDialogCell") as! DefaultPickerDialogCell
            cell.setItem(item)
            if let selectedMonth = selectedItem as? ExtendDepositMonthResponse.Month {
                if month.FD_RENEW_MONTHS_ARRAY == selectedMonth.FD_RENEW_MONTHS_ARRAY {
                    tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                }
            }
            return cell
        case let data as DataListResponse.Item:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultPickerDialogCell") as! DefaultPickerDialogCell
            cell.setItem(item)
            if let selectedData = selectedItem as? DataListResponse.Item {
                if data.CM_CODE == selectedData.CM_CODE {
                    tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                }
            }
            return cell
        case let swift as SwiftListResponse.Bank:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultPickerDialogCell") as! DefaultPickerDialogCell
            cell.setItem(item)
            if let selectedSwift = selectedItem as? SwiftListResponse.Bank {
                if swift.ROUTING_NOS == selectedSwift.ROUTING_NOS {
                    tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                }
            }
            return cell
        case _ as ChallengeGiftResponse.Count:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultPickerDialogCell") as! DefaultPickerDialogCell
            cell.setItem(item)
            return cell
        case _ as BillPaymentOrganizationResponse.Organization:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultPickerDialogCell") as! DefaultPickerDialogCell
            cell.setItem(item)
            return cell
        case let number as Int:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultPickerDialogCell") as! DefaultPickerDialogCell
            cell.setItem(item)
            if let selectedNumber = selectedItem as? Int {
                if selectedNumber == number {
                    tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                }
            }
            return cell
        case let type as (index: Int, text: String):
            let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultPickerDialogCell") as! DefaultPickerDialogCell
            cell.setItem(item)
            if let selectedString = selectedItem as? (index: Int, text: String) {
                if selectedString == type {
                    tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                }
            }
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = list[indexPath.row]
        onItemSelect?(item)
    }
}
