//
//  LocationController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 6/15/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class LocationController: UITableViewController {
    var location = LocationResponse.LocationData.Location()
    var model = TableViewModel()
    var onBranchSelect: (() -> Void)?

    lazy var locationField: DefaultLocationHeaderCell = {
        let nib = Bundle.main.loadNibNamed("DefaultLocationHeaderCell", owner: self, options: nil)
        let cell = nib?.first as? DefaultLocationHeaderCell
        guard let unwrappedCell = cell else {
            return DefaultLocationHeaderCell()
        }
        unwrappedCell.setLocation(self.location)
        return unwrappedCell
    }()

    lazy var locationSection: TableViewModel.Section = {
        TableViewModel.Section(
            title: "Салбарын мэдээлэл",
            rows: [
                TableViewModel.Row(
                    cell: self.locationField
                ),
                TableViewModel.Row(
                    title: "location_address_title".localized(), info: self.location.getAddress()
                ),
                TableViewModel.Row(
                    title: "location_timetable_title".localized(), info: self.location.timetable?.getTimeLine()
                )
            ]
        )
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "card_order_map_dialog_title".localized()
        tableView.registerCell(nibName: "DefaultVerticalTitleLabelCell")
        tableView.registerCell(nibName: "DefaultTitleLabelCell")
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "BaseHeaderCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "BaseHeaderCell")
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .defaultPrimaryBackground
        model.sections = [locationSection]

        tableView.reloadData()
    }

    var hasButton: Bool = true {
        didSet {
            switch hasButton {
            case true:
                let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 70))
                let chooseButton = DefaultGradientButton(frame: CGRect(x: 20, y: 10, width: footerView.frame.width - 40, height: 50))
                chooseButton.titleText = "location_choose_branch_title"
                chooseButton.addTarget(self, action: #selector(handleChoose), for: .touchUpInside)
                footerView.addSubview(chooseButton)
                tableView.tableFooterView = footerView
            case false:
                tableView.tableFooterView = UIView()
                ConnectionFactory.getBranchQueue(
                    solId: self.location.solId.orEmpty,
                    success: { response in
                        guard let unwrappedQueue = response.queues?.first else {
                            return
                        }
                        let section = TableViewModel.Section(title: "Салбарын ачаалал", rows: [])
                        unwrappedQueue.types?.forEach(
                            { type in
                                section.rows.append(
                                    TableViewModel.Row(
                                        title: type.typeName.orEmpty,
                                        info: type.count?.toString
                                    )
                                )
                            }
                        )
                        self.model.sections.append(section)
                        self.tableView.reloadData()
                    }, failed: { _ in
                    }
                )
            }
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        model.sections.count
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let foundSection = model.sections[section]
        if foundSection.title != nil {
            return UITableView.automaticDimension
        } else {
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let foundSection = model.sections[section]
        if foundSection.title != nil {
            let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "BaseHeaderCell") as? BaseHeaderCell
            cell?.setupLabel(foundSection.title.orEmpty)
            return cell
        } else {
            return nil
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = model.sections[section]
        return section.rows.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = model.sections[indexPath.section]
        let row = section.rows[indexPath.row]
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                return row.cell!
            default:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultVerticalTitleLabelCell") as? DefaultVerticalTitleLabelCell else {
                    return UITableViewCell()
                }
                cell.setData(row)
                return cell
            }
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultTitleLabelCell") as? DefaultTitleLabelCell else {
                return UITableViewCell()
            }
            cell.setData(row)
            cell.setConstraint(5)
            return cell
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {}

    @objc private func handleChoose() {
        onBranchSelect?()
    }
}
