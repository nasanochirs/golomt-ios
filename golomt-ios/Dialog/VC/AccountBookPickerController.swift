//
//  AccountBookPickerController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 5/7/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import IQKeyboardManagerSwift
import UIKit

class AccountBookPickerController: DialogTableViewController {
    var accountBooks = [AccountBookResponse.AccountBook]()
    var filtered = [AccountBookResponse.AccountBook]()
    var filterText = ""
    var onAccountBookSelect: ((_ accountBook: AccountBookResponse.AccountBook) -> Void)?
    var selectedAccountBook: AccountBookResponse.AccountBook?
    var type: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "account_book_title".localized()
        tableView.registerCell(nibName: "DefaultAccountBookPickerDialogCell")
        tableView.rowHeight = 70
        tableView.estimatedRowHeight = 70
        
        let searchBar = UISearchBar()
        searchBar.delegate = self
        searchBar.showsCancelButton = false
        searchBar.searchBarStyle = .default
        searchBar.placeholder = "transaction_account_book_search_title".localized()
        searchBar.sizeToFit()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    
        tableView.tableHeaderView = searchBar
        tableView.reloadData()
        
        configContentSize()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !filterText.isEmpty {
            return filtered.count
        }
        return accountBooks.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultAccountBookPickerDialogCell") as? DefaultAccountBookPickerDialogCell
        guard let unwrappedCell = cell else {
            return UITableViewCell()
        }
        var accountBook = accountBooks[indexPath.row]
        if !filterText.isEmpty {
            accountBook = filtered[indexPath.row]
        }
        unwrappedCell.setAccountBook(type: type, accountBook)
        if selectedAccountBook != nil, selectedAccountBook!.BNIF_ACCNT_NUMBER == accountBook.BNIF_ACCNT_NUMBER {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
        return unwrappedCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var accountBook = accountBooks[indexPath.row]
        if !filterText.isEmpty {
            accountBook = filtered[indexPath.row]
        }
        onAccountBookSelect?(accountBook)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            configContentSize(with: keyboardHeight)
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        configContentSize()
    }
}

extension AccountBookPickerController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let text = searchText.lowercased()
        filtered = accountBooks.filter {
            ($0.BNIF_ACCNT_NUMBER?.contains(text) ?? false) ||
                ($0.BNIF_NICK_NAME?.contains(text) ?? false) ||
                ($0.BNIF_NAME?.contains(text) ?? false)
        }
        filterText = text
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
