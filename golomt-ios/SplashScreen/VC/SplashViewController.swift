//
//  SplashViewController.swift
//  golomt-ios
//
//  Created by Nasan-Ochir Sukhbaatar on 2/21/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import UIKit

class SplashViewController: BaseUIViewController {
    @IBOutlet var splashImage: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setDefaultBackgroundImage()
        switch getLanguage() {
        case "mn":
            splashImage.image = UIImage(named: "image_splash_mongolian")?.withRenderingMode(.alwaysOriginal)
        default:
            splashImage.image = UIImage(named: "image_splash_english")?.withRenderingMode(.alwaysOriginal)
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 1.25, animations: {
            self.animateSplashImage()
        }, completion: { _ in
            self.navigateToLogin()
        })
    }

    func animateSplashImage() {
        let safeAreaInsetTop = view.safeAreaInsets.top
        splashImage.frame.origin.y = safeAreaInsetTop + 50
    }

    func navigateToLogin() {
        let loginStoryBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
        UIApplication.shared.windows.first?.rootViewController = loginStoryBoard.instantiateInitialViewController()
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}
