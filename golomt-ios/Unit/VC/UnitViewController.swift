//
//  UnitViewController.swift
//  golomt-ios
//
//  Created by Khulan on 8/11/20.
//  Copyright © 2020 Nasan-Ochir Sukhbaatar. All rights reserved.
//

import Foundation
import STPopup
import UIKit

class UnitViewController: BaseUIViewController {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var numberTextField: UITextField!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var separator: UIView!
    @IBOutlet var contactLabel: UILabel!
    @IBOutlet var arrowDownImage: UIImageView!
    @IBOutlet var continueButton: DefaultGradientButton!

    var unitModel = UnitModel()
    var phoneNumber = ""
    var onTextChanged: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "unit_payment_title".localized()
        initComponent()
    }

    private func initComponent() {
        numberTextField.keyboardType = .numberPad

        titleLabel.textColor = .defaultSeparator
        titleLabel.useSmallFont()
        titleLabel.makeBold()
        titleLabel.text = "unit_mobile_number_label".localized()

        continueButton?.titleText = "continue_button_title"
        continueButton.addTarget(self, action: #selector(handleContinue), for: .touchUpInside)

        errorLabel.textColor = .red
        errorLabel.useSmallFont()

        numberTextField.borderStyle = .none
        numberTextField.textColor = .defaultPrimaryText
        numberTextField.useXLargeFont()
        numberTextField.delegate = self
        numberTextField.makeBold()
        numberTextField.becomeFirstResponder()
        numberTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)

        separator.backgroundColor = .defaultSeparator
    }

    func checkError() {
        errorLabel.text = nil
        separator.backgroundColor = .defaultSeparator
        if numberTextField.text!.isEmpty {
            separator.backgroundColor = .defaultError
            errorLabel.textColor = .defaultError
            errorLabel.text = "error_not_empty".localized(with: titleLabel.text.orEmpty)
        }
    }

    private func chooseOperator() {
        let controller = PickerController()
        controller.title = "unit_choose_operator_label".localized()
        controller.list = [(0, "unit_gmobile_label".localized()), (1, "unit_mobicom_label".localized()), (2, "unit_skytel_label".localized()), (3, "unit_unitel_label".localized())]
        controller.onItemSelect = { type in
            controller.popupController?.dismiss()
            let type = type as? (index: Int, text: String)
            switch type?.index {
            case 0:
                self.unitModel.type = .GMobile
            case 1:
                self.unitModel.type = .Mobicom
            case 2:
                self.unitModel.type = .Skytel
            case 3:
                self.unitModel.type = .Unitel
            default:
                break
            }
            self.handleContinue()
        }
        controller.selectedItem = unitModel.type
        let popupController = STPopupController(rootViewController: controller)
        popupController.style = .bottomSheet
        popupController.present(in: self)
    }

    @objc private func handleContinue() {
        errorLabel.text = nil
        separator.backgroundColor = .defaultSeparator
        if numberTextField.text!.isEmpty {
            separator.backgroundColor = .defaultError
            errorLabel.textColor = .defaultError
            errorLabel.text = "error_not_empty".localized(with: titleLabel.text.orEmpty)
        }
        else {
            let depositStoryBoard: UIStoryboard = UIStoryboard(name: "Unit", bundle: nil)

            let viewController = depositStoryBoard.instantiateViewController(withIdentifier: "UnitID")
            let typeVC = viewController as? UnitChooseViewController
            guard let unwrappedVC = typeVC else {
                return
            }
            unwrappedVC.phoneNumber = numberTextField.text.orEmpty
            unwrappedVC.unitModel = unitModel
            navigationController?.pushViewController(unwrappedVC, animated: true)
        }
    }

    @objc private func textFieldDidChange() {
        onTextChanged?()
        checkError()
    }
}

extension UnitViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let stringRange = Range(range, in: textField.text.orEmpty) else {
            return false
        }
        let newString = textField.text.orEmpty.replacingCharacters(in: stringRange, with: string)
        if newString.count == 8 {
            let index = textField.text.orEmpty.index(textField.text.orEmpty.startIndex, offsetBy: 2)
            let substring = textField.text.orEmpty[..<index]
            if Int(substring) == 99 || Int(substring) == 95 || Int(substring) == 94 || Int(substring) == 85 {
                unitModel.type = .Mobicom
            }
            else if Int(substring) == 96 || Int(substring) == 91 || Int(substring) == 90 {
                unitModel.type = .Skytel
            }
            else if Int(substring) == 98 || Int(substring) == 97 || Int(substring) == 93 {
                unitModel.type = .GMobile
            }
            else if Int(substring) == 88 || Int(substring) == 86 || Int(substring) == 89 || Int(substring) == 80 {
                unitModel.type = .Unitel
            }
            else {
                chooseOperator()
            }
            handleContinue()
        }
        return newString.count <= 8
    }
}
